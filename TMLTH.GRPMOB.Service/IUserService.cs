﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TMLTH.GRPMOB.DTO.Request;
using TMLTH.GRPMOB.DTO.Response;


namespace TMLTH.GRPMOB.Service
{
    // 1. สร้าง Interface สำหรับเป็น Contract ของระบบ
    [ServiceContract]
    public interface IUserService
    {
        // 2. กำหนด operation สำหรับ contract ที่จะใช้เป็น web method ให้ user ใช้งาน 
        //    2.1 สร้าง interface method ชื่อ GetCurrentVatRate มีการรีเทิร์นค่ากลับเป็น float type
        //[OperationContract]
        //float GetCurrentVatRate();

        //    2.2 สร้าง interface method ชื่อ GetAllProduct มีการรีเทิร์นค่ากลับเป็น Product Class 
        //    แต่เนื่องจาก Product Class นั้นยังไม่ได้สร้าง จึงต้องไปสร้าง class ที่ข้อ 2.3 
        //[OperationContract]
        //List<User> GetAllUser();

        [OperationContract]
        LoginResponse Login(LoginRequest request);


        [OperationContract]
        LogoutResponse Logout(LogoutRequest request);

        [OperationContract]
        ChkMemberResponse ChkMember(ChkMemberRequest request);

        [OperationContract]
        GetOTPResponse GetOTP(GetOTPRequest request);

        [OperationContract]
        ChkOTPResponse ChkOTP(ChkOTPRequest request);

        [OperationContract]
        RegisterResponse Register(RegisterRequest request);

        [OperationContract]
        UpdPwdResponse UpdPwd(UpdPwdRequest request);

        [OperationContract]
        ChkMemberForgetPINResponse ChkMemberForgetPIN(ChkMemberForgetPINRequest request);
               
        [OperationContract]
        TestGetOTPResponse TestGetOTP(TestGetOTPRequest request);

        [OperationContract]
        ChkForgetPassResponse ChkForgetPass(ChkForgetPassRequest request);
        
    }

    //// 2.3  สร้าง Product Class เพื่อใช้เป็น DataContract สำหรับ IProductService.GetAllProduct() method ในข้อ 2.2
    //[DataContract] // DataContract เป็นตัวระบุว่าให้เปิดเผย Type นี้ผ่านทาง web service
    //public class User
    //{
    //    [DataMember] // DataMember เป็นตัวระบุว่าให้เปิดเผย member ตัวนี้ผ่านทาง web service
    //    public string ProductID { get; set; }
    //    [DataMember]
    //    public string ProductName { get; set; }
    //    [DataMember]
    //    public decimal Price { get; set; }
    //    [DataMember]
    //    public float Weight { get; set; }
    //    [DataMember]
    //    public string Model { get; set; }
    //}



}
