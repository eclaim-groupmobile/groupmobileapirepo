﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.Service.Data
{
    [DataContract] // DataContract เป็นตัวระบุว่าให้เปิดเผย Type นี้ผ่านทาง web service
    public class RegisterRequest:BaseRequest
    {
        [DataMember]
        public String policyNo { get; set; }
        [DataMember]
        public String memberNo { get; set; }
        [DataMember]
        public String citizenID { get; set; }
        [DataMember]
        public String DOB { get; set; }
        [DataMember]
        public String mobileNo { get; set; }
        [DataMember]
        public String email { get; set; }
        [DataMember]
        public String lineID { get; set; }
        [DataMember]
        public String OTP { get; set; }

        
    }
}