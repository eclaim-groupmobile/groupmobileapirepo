﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.Service.Data
{
    [DataContract] // DataContract เป็นตัวระบุว่าให้เปิดเผย Type นี้ผ่านทาง web service
    public class BaseResponse
    {
        [DataMember]
        public String errorCode { get; set; }  // 0 = success, x = Error with description.
        [DataMember]
        public String errorDesc { get; set; }  // 
        
    }
}