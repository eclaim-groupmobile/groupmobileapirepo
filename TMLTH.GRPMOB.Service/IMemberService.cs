﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TMLTH.GRPMOB.DTO.Request;
using TMLTH.GRPMOB.DTO.Response;

namespace TMLTHApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMemberService" in both code and config file together.
    [ServiceContract]
    public interface IMemberService
    {
        [OperationContract]
        GetMembersBenefitResponse GetMemberBenefit(GetMembersBenefitRequest request);
        [OperationContract]
        GetMasterDataResponse GetMasterData(GetMasterDataRequest request);

        [OperationContract]
        UpdateMemberInfoResponse UpdateMemberInfo(UpdateMemberInfoRequest request);

        [OperationContract]
        GetECardResponse GetECard(GetECardRequest request);

        [OperationContract]
        GetHealthTipDetailResponse GetHealthTipDetail(GetHealthTipDetailRequest request);

        [OperationContract]
        UpdateClaimNotifyKeyResponse UpdateClaimNotifyKey(UpdateClaimNotifyKeyRequest request);

        [OperationContract]
        GetIPDClaimDetailResponse GetIPDClaimDetail(GetIPDClaimDetailRequest request);
        [OperationContract]
        GetOPDClaimDetailResponse GetOPDClaimDetail(GetOPDClaimDetailRequest request);
        [OperationContract]
        GetAMEClaimDetailResponse GetAMEClaimDetail(GetAMEClaimDetailRequest request);
        [OperationContract]
        GetDENClaimDetailResponse GetDENClaimDetail(GetDENClaimDetailRequest request);
        [OperationContract]
        GetLABClaimDetailResponse GetLABClaimDetail(GetLABClaimDetailRequest request);


        [OperationContract]
        GetMasterAgreementResponse GetMasterAgreement(GetMasterAgreementRequest request);
        [OperationContract]
        UpdateTransAgreementResponse UpdateTransAgreement(UpdateTransAgreementRequest request);


        [OperationContract]
        TestGetResponse TestGet(TestGetRequest request);
    }
}
