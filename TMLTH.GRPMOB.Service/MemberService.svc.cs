﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TMLTH.GRPMOB.DTO.Request;
using TMLTH.GRPMOB.DTO.Response;
using TMLTH.GRPMOB.BusinessController;
using System.Web.Script.Serialization;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using TMLTH.GRPMOB.UTIL;

namespace TMLTHApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MemberService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MemberService.svc or MemberService.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(
RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MemberService : IMemberService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public GetMembersBenefitResponse GetMemberBenefit(GetMembersBenefitRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));

            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetMembersBenefitResponse response = new GetMembersBenefitResponse();
            try
            {
                request.serviceName = "GetMemberBenefit";

                //UserController controller = new UserController();
                //controller.Execute(request, response);
                MemberController controller = new MemberController();
                controller.Execute(request, response);

                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }

        public GetMasterDataResponse GetMasterData(GetMasterDataRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetMasterDataResponse response = new GetMasterDataResponse();
            try
            {
                request.serviceName = "GetMasterData";

                //UserController controller = new UserController();
                //controller.Execute(request, response);
                MemberController controller = new MemberController();
                controller.Execute(request, response);

                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }


        }


        public UpdateMemberInfoResponse UpdateMemberInfo(UpdateMemberInfoRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            UpdateMemberInfoResponse response = new UpdateMemberInfoResponse();
            try
            {
                request.serviceName = "UpdateMemberInfo";

                //UserController controller = new UserController();
                //controller.Execute(request, response);
                MemberController controller = new MemberController();
                controller.Execute(request, response);

                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }

        public GetECardResponse GetECard(GetECardRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetECardResponse response = new GetECardResponse();
            try
            {
                request.serviceName = "GetECard";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
        }

        public GetHealthTipDetailResponse GetHealthTipDetail(GetHealthTipDetailRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetHealthTipDetailResponse response = new GetHealthTipDetailResponse();
            try
            {
                request.serviceName = "GetHealthTipDetail";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
        }

        public UpdateClaimNotifyKeyResponse UpdateClaimNotifyKey(UpdateClaimNotifyKeyRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            UpdateClaimNotifyKeyResponse response = new UpdateClaimNotifyKeyResponse();
            try
            {
                request.serviceName = "UpdateClaimNotifyKey";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
        }
        public GetIPDClaimDetailResponse GetIPDClaimDetail(GetIPDClaimDetailRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetIPDClaimDetailResponse response = new GetIPDClaimDetailResponse();
            try
            {
                request.serviceName = "GetIPDClaimDetail";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
        }
        public GetOPDClaimDetailResponse GetOPDClaimDetail(GetOPDClaimDetailRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetOPDClaimDetailResponse response = new GetOPDClaimDetailResponse();
            try
            {
                request.serviceName = "GetOPDClaimDetail";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
        }
        public GetAMEClaimDetailResponse GetAMEClaimDetail(GetAMEClaimDetailRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetAMEClaimDetailResponse response = new GetAMEClaimDetailResponse();
            try
            {
                request.serviceName = "GetAMEClaimDetail";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }
        public GetDENClaimDetailResponse GetDENClaimDetail(GetDENClaimDetailRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetDENClaimDetailResponse response = new GetDENClaimDetailResponse();
            try
            {
                request.serviceName = "GetDENClaimDetail";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }
        public GetLABClaimDetailResponse GetLABClaimDetail(GetLABClaimDetailRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetLABClaimDetailResponse response = new GetLABClaimDetailResponse();
            try
            {
                request.serviceName = "GetLABClaimDetail";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
        }
        public GetMasterAgreementResponse GetMasterAgreement(GetMasterAgreementRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetMasterAgreementResponse response = new GetMasterAgreementResponse();
            try
            {
                request.serviceName = "GetMasterAgreement";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
        }
        public UpdateTransAgreementResponse UpdateTransAgreement(UpdateTransAgreementRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            UpdateTransAgreementResponse response = new UpdateTransAgreementResponse();
            try
            {
                request.serviceName = "UpdateTransAgreement";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
        }

        public TestGetResponse TestGet(TestGetRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            TestGetResponse response = new TestGetResponse();
            try
            {
                request.serviceName = "TestGet";
                MemberController controller = new MemberController();
                controller.Execute(request, response);
                return response;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                //log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
        }

        /*
                 [OperationContract]
        GetIPDClaimDetailResponse GetIPDClaimDetail(GetIPDClaimDetailRequest request);
        [OperationContract]
        GetOPDClaimDetailResponse GetOPDClaimDetail(GetOPDClaimDetailRequest request);
        [OperationContract]
        GetAMEClaimDetailResponse GetAMEClaimDetail(GetAMEClaimDetailRequest request);
        [OperationContract]
        GetDENClaimDetailResponse GetDENClaimDetail(GetDENClaimDetailRequest request);
        [OperationContract]
        GetLABClaimDetailResponse GetLABClaimDetail(GetLABClaimDetailRequest request);
         */
    }
}
