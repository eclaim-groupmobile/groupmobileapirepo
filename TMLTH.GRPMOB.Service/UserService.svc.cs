﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TMLTH.GRPMOB.DTO.Request;
using TMLTH.GRPMOB.DTO.Response;
using TMLTH.GRPMOB.BusinessController;
using TMLTH.GRPMOB.UTIL;
using System.Web.Script.Serialization;
using System.ServiceModel.Activation;

namespace TMLTH.GRPMOB.Service
{
    // ProductService คือ Service Class ที่เราจะเขียนโค้ดควบคุมการทำงานของเซอร์วิส
    [AspNetCompatibilityRequirements(
RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class UserService : IUserService // นำ contract ที่ประกาศไว้มาใช้งานกับ service class
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // operation จาก contract ถูกบังคับให้ประกาศใช้งานใน Service Class
        public float GetCurrentVatRate()
        {
            return 0.7f;
        }
        // operation จาก contract ถูกบังคับให้ประกาศใช้งานใน Service Class
        //public List<User> GetAllUser()
        //{
        //    return new List<User>
        //    {
        //        new User{ProductID = "01", ProductName = "VIAO", Model = "VGN-FE44S/W", Price = 70000, Weight = 2.8f},
        //        new User{ProductID = "02", ProductName = "VIAO", Model = "VGN-FE45S/W", Price = 80000, Weight = 2.5f},
        //        new User{ProductID = "03", ProductName = "VIAO", Model = "VGN-FE46S/W", Price = 90000, Weight = 2.5f},
        //        new User{ProductID = "04", ProductName = "VIAO", Model = "VGN-FE47S/W", Price = 100000, Weight = 2.2f},
        //        new User{ProductID = "05", ProductName = "VIAO", Model = "VGN-FE48S/W", Price = 120000, Weight = 1.9f}
        //    };
        //}

        public LoginResponse Login(LoginRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            LoginResponse response = new LoginResponse();
            try
            {
                request.serviceName = "Login";
                UserController controller = new UserController();
                controller.Execute(request, response);

                return response;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }
        public LogoutResponse Logout(LogoutRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            LogoutResponse response = new LogoutResponse();
            try
            {
                request.serviceName = "Logout";
                UserController controller = new UserController();
                controller.Execute(request, response);

                return response;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }

        // operation จาก contract ถูกบังคับให้ประกาศใช้งานใน Service Class
        public ChkMemberResponse ChkMember(ChkMemberRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            ChkMemberResponse response = new ChkMemberResponse();
            try
            {
                request.serviceName = "ChkMember";
                UserController controller = new UserController();
                controller.Execute(request, response);

                return response;
                
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
            //return new ChkMemberResponse { errorCode = "0", errorDesc = "Success"};


        }

        public GetOTPResponse GetOTP(GetOTPRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            GetOTPResponse response = new GetOTPResponse();
            try
            {
                request.serviceName = "GetOTP";
                UserController controller = new UserController();
                controller.Execute(request, response);

                return response;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }


        }

        public ChkOTPResponse ChkOTP(ChkOTPRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            ChkOTPResponse response = new ChkOTPResponse();
            try
            {
                request.serviceName = "ChkOTP";
                UserController controller = new UserController();
                controller.Execute(request, response);

                return response;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }
        public RegisterResponse Register(RegisterRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            RegisterResponse response = new RegisterResponse();
            try
            {
                request.serviceName = "Register";
                UserController controller = new UserController();
                controller.Execute(request, response);

                return response;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }

        public UpdPwdResponse UpdPwd(UpdPwdRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            UpdPwdResponse response = new UpdPwdResponse();
            try
            {
                request.serviceName = "UpdPwd";
                UserController controller = new UserController();
                controller.Execute(request, response);

                return response;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }

        public ChkMemberForgetPINResponse ChkMemberForgetPIN(ChkMemberForgetPINRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            ChkMemberForgetPINResponse response = new ChkMemberForgetPINResponse();
            try
            {
                request.serviceName = "ChkMemberForgetPIN";
                UserController controller = new UserController();
                controller.Execute(request, response);

                return response;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }
        public TestGetOTPResponse TestGetOTP(TestGetOTPRequest request)
        {
            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            TestGetOTPResponse response = new TestGetOTPResponse();
            try
            {
                request.serviceName = "TestGetOTP";
                UserController controller = new UserController();
                controller.Execute(request, response);

                return response;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }

        }

        public ChkForgetPassResponse ChkForgetPass(ChkForgetPassRequest request)
        {

            log.Info("in");
            log.Debug(new JavaScriptSerializer().Serialize(request));
            request.clientIPAddress = ChannelHelper.GetClientIP();
            ChkForgetPassResponse response = new ChkForgetPassResponse();
            try
            {
                request.serviceName = "ChkForgetPass";
                UserController controller = new UserController();
                controller.Execute(request, response);

                return response;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return response;
            }
            finally
            {
                log.Debug(new JavaScriptSerializer().Serialize(response));
                log.Info("out");
            }
        }

    }
}
