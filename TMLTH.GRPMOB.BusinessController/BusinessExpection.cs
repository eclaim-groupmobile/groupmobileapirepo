﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using TMLTH.GRPMOB.DTO.Response;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;
using System.Configuration;

namespace TMLTH.GRPMOB.BusinessController
{
    public class BusinessExpection:Exception
    {
        static Hashtable _ErrorTable;

        static ErrorList _ErrorList;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public BusinessExpection(Int32 errorCode, BaseResponse res)
        {
            try
            {
                if (_ErrorList == null)
                {
                    CreateErrorTable();
                }

                if (res == null)
                    res = new BaseResponse();

                res.errorCode = errorCode.ToString();
                //res.errorDescEN = _ErrorList.Errors[errorCode].ErrorDescEN;
                Error error = _ErrorList.Errors.Find(a => a.ErrorCode.Equals(errorCode.ToString()));
                res.errorDescEN = error.ErrorDescEN;
                res.errorDescTH = error.ErrorDescTH;
            }
            catch (Exception ex)
            {
                res.log = ex.ToString();
                log.Error(ex.ToString());
                throw;
            }

        }
        public String GetErrorDesc(EnumException error)
        {
            if (_ErrorTable.ContainsKey(error))
            {
                string temp = (String)_ErrorTable[error];
                return temp;
            }
            return "";
        }
        public void CreateErrorTable()
        {
            _ErrorTable = new Hashtable();

            //_ErrorTable.Add(EnumException.ServiceNotSupport, "Service is not support.");
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ErrorList));
                var configPath = ConfigurationManager.AppSettings.Get("ErrorPath");
                using (FileStream fileStream = new FileStream(configPath, FileMode.Open))
                {
                    ErrorList result = (ErrorList)serializer.Deserialize(fileStream);

                    _ErrorList = result;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
        }
    }

    public enum EnumException
    {

        ServiceNotSupport = 999
    }
}
