﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMLTH.GRPMOB.DTO.Request;
using TMLTH.GRPMOB.DTO.Response;

namespace TMLTH.GRPMOB.BusinessController
{
    public class MemberController : BaseController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        GetMembersBenefitRequest _GetMembersBenefitRequest;
        GetMembersBenefitResponse _GetMembersBenefitResponse;
        GetMasterDataRequest _GetMasterDataRequest;
        GetMasterDataResponse _GetMasterDataResponse;
        UpdateMemberInfoRequest _UpdateMemberInfoRequest;
        UpdateMemberInfoResponse _UpdateMemberInfoResponse;
        GetECardRequest _GetECardRequest;
        GetECardResponse _GetECardResponse;
        GetHealthTipDetailRequest _GetHealthTipDetailRequest;
        GetHealthTipDetailResponse _GetHealthTipDetailResponse;
        UpdateClaimNotifyKeyRequest _UpdateClaimNotifyKeyRequest;
        UpdateClaimNotifyKeyResponse _UpdateClaimNotifyKeyResponse;
        GetIPDClaimDetailRequest _GetIPDClaimDetailRequest;
        GetIPDClaimDetailResponse _GetIPDClaimDetailResponse;
        GetOPDClaimDetailRequest _GetOPDClaimDetailRequest;
        GetOPDClaimDetailResponse _GetOPDClaimDetailResponse;
        GetAMEClaimDetailRequest _GetAMEClaimDetailRequest;
        GetAMEClaimDetailResponse _GetAMEClaimDetailResponse;
        GetDENClaimDetailRequest _GetDENClaimDetailRequest;
        GetDENClaimDetailResponse _GetDENClaimDetailResponse;
        GetLABClaimDetailRequest _GetLABClaimDetailRequest;
        GetLABClaimDetailResponse _GetLABClaimDetailResponse;
        GetMasterAgreementRequest _GetMasterAgreementRequest;
        GetMasterAgreementResponse _GetMasterAgreementResponse;

        UpdateTransAgreementRequest _UpdateTransAgreementRequest;
        UpdateTransAgreementResponse _UpdateTransAgreementResponse;

        TestGetRequest _TestGetRequest;
        TestGetResponse _TestGetResponse;

        public override void DoTransformObject()
        {
            try
            {
                if (_Request.serviceName.Equals("GetMemberBenefit"))
                {
                    _GetMembersBenefitRequest = (GetMembersBenefitRequest)_Request;
                    _GetMembersBenefitResponse = (GetMembersBenefitResponse)_Response;


                }
                else if (_Request.serviceName.Equals("GetMasterData"))
                {
                    _GetMasterDataRequest = (GetMasterDataRequest)_Request;
                    _GetMasterDataResponse = (GetMasterDataResponse)_Response;
                }
                else if (_Request.serviceName.Equals("UpdateMemberInfo"))
                {
                    _UpdateMemberInfoRequest = (UpdateMemberInfoRequest)_Request;
                    _UpdateMemberInfoResponse = (UpdateMemberInfoResponse)_Response;
                }
                else if (_Request.serviceName.Equals("GetECard"))
                {
                    _GetECardRequest = (GetECardRequest)_Request;
                    _GetECardResponse = (GetECardResponse)_Response;
                }
                else if (_Request.serviceName.Equals("GetHealthTipDetail"))
                {
                    _GetHealthTipDetailRequest = (GetHealthTipDetailRequest)_Request;
                    _GetHealthTipDetailResponse = (GetHealthTipDetailResponse)_Response;
                }
                else if (_Request.serviceName.Equals("UpdateClaimNotifyKey"))
                {
                    _UpdateClaimNotifyKeyRequest = (UpdateClaimNotifyKeyRequest)_Request;
                    _UpdateClaimNotifyKeyResponse = (UpdateClaimNotifyKeyResponse)_Response;
                }
                else if (_Request.serviceName.Equals("GetIPDClaimDetail"))
                {
                    _GetIPDClaimDetailRequest = (GetIPDClaimDetailRequest)_Request;
                    _GetIPDClaimDetailResponse = (GetIPDClaimDetailResponse)_Response;
                }
                else if (_Request.serviceName.Equals("GetOPDClaimDetail"))
                {
                    _GetOPDClaimDetailRequest = (GetOPDClaimDetailRequest)_Request;
                    _GetOPDClaimDetailResponse = (GetOPDClaimDetailResponse)_Response;
                }
                else if (_Request.serviceName.Equals("GetAMEClaimDetail"))
                {
                    _GetAMEClaimDetailRequest = (GetAMEClaimDetailRequest)_Request;
                    _GetAMEClaimDetailResponse = (GetAMEClaimDetailResponse)_Response;
                }
                else if (_Request.serviceName.Equals("GetDENClaimDetail"))
                {
                    _GetDENClaimDetailRequest = (GetDENClaimDetailRequest)_Request;
                    _GetDENClaimDetailResponse = (GetDENClaimDetailResponse)_Response;
                }
                else if (_Request.serviceName.Equals("GetLABClaimDetail"))
                {
                    _GetLABClaimDetailRequest = (GetLABClaimDetailRequest)_Request;
                    _GetLABClaimDetailResponse = (GetLABClaimDetailResponse)_Response;
                }
                else if (_Request.serviceName.Equals("GetMasterAgreement"))
                {
                    _GetMasterAgreementRequest = (GetMasterAgreementRequest)_Request;
                    _GetMasterAgreementResponse = (GetMasterAgreementResponse)_Response;
                }
                else if (_Request.serviceName.Equals("UpdateTransAgreement"))
                {
                    _UpdateTransAgreementRequest = (UpdateTransAgreementRequest)_Request;
                    _UpdateTransAgreementResponse = (UpdateTransAgreementResponse)_Response;
                }

                else if (_Request.serviceName.Equals("TestGet"))
                {
                    _TestGetRequest = (TestGetRequest)_Request;
                    _TestGetResponse = (TestGetResponse)_Response;
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
        }
        public override void DoValidate()
        {
            log.Info("in");
            try
            {
                if (_Request.serviceName.Equals("GetMemberBenefit"))
                {
                    //if (_ChkMemberRequest.memberNo.Equals("") || _ChkMemberRequest.policyNo.Equals(""))
                    //{
                    //    throw new BusinessExpection(2, _ChkMemberResponse);
                    //}
                    if (!ValidateToken(_GetMembersBenefitRequest.memberNo, _GetMembersBenefitRequest.policyNo, _GetMembersBenefitRequest.token))
                    {
                        throw new BusinessExpection(9, _GetMembersBenefitResponse);
                    }
                }

                else if (_Request.serviceName.Equals("GetMasterData"))
                {

                }
                else if (_Request.serviceName.Equals("UpdateMemberInfo"))
                {
                    if (_UpdateMemberInfoRequest.mobileNo.Equals(""))
                    {
                        throw new BusinessExpection(2, _UpdateMemberInfoResponse);
                    }

                    if (!ValidateToken(_UpdateMemberInfoRequest.memberNo, _UpdateMemberInfoRequest.policyNo, _UpdateMemberInfoRequest.token))
                    {
                        throw new BusinessExpection(9, _UpdateMemberInfoResponse);
                    }

                }
                else if (_Request.serviceName.Equals("GetECard"))
                {

                    if (!ValidateToken(_GetECardRequest.memberNo, _GetECardRequest.policyNo, _GetECardRequest.token))
                    {
                        throw new BusinessExpection(9, _GetECardResponse);
                    }

                }
                else if (_Request.serviceName.Equals("UpdateClaimNotifyKey"))
                {

                    if (!ValidateToken(_UpdateClaimNotifyKeyRequest.loginedMemberNo, _UpdateClaimNotifyKeyRequest.policyNo, _UpdateClaimNotifyKeyRequest.token))
                    {
                        throw new BusinessExpection(9, _UpdateClaimNotifyKeyResponse);
                    }
                }
                else if (_Request.serviceName.Equals("GetIPDClaimDetail"))
                {
                    if (!ValidateToken(_GetIPDClaimDetailRequest.loginedMemberNo, _GetIPDClaimDetailRequest.policy, _GetIPDClaimDetailRequest.token))
                    {
                        throw new BusinessExpection(9, _GetIPDClaimDetailResponse);
                    }
                }
                else if (_Request.serviceName.Equals("GetOPDClaimDetail"))
                {
                    if (!ValidateToken(_GetOPDClaimDetailRequest.loginedMemberNo, _GetOPDClaimDetailRequest.policy, _GetOPDClaimDetailRequest.token))
                    {
                        throw new BusinessExpection(9, _GetOPDClaimDetailResponse);
                    }
                }
                else if (_Request.serviceName.Equals("GetAMEClaimDetail"))
                {
                    if (!ValidateToken(_GetAMEClaimDetailRequest.loginedMemberNo, _GetAMEClaimDetailRequest.policy, _GetAMEClaimDetailRequest.token))
                    {
                        throw new BusinessExpection(9, _GetAMEClaimDetailResponse);
                    }
                }
                else if (_Request.serviceName.Equals("GetDENClaimDetail"))
                {
                    if (!ValidateToken(_GetDENClaimDetailRequest.loginedMemberNo, _GetDENClaimDetailRequest.policy, _GetDENClaimDetailRequest.token))
                    {
                        throw new BusinessExpection(9, _GetDENClaimDetailResponse);
                    }
                }
                else if (_Request.serviceName.Equals("GetLABClaimDetail"))
                {
                    if (!ValidateToken(_GetLABClaimDetailRequest.loginedMemberNo, _GetLABClaimDetailRequest.policy, _GetLABClaimDetailRequest.token))
                    {
                        throw new BusinessExpection(9, _GetLABClaimDetailResponse);
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }
        private bool ValidateToken(string memberNo, string policyNo, string token)
        {
            if (token.Equals("-"))
            {
                return true;
            }

            UserLogic user = new UserLogic();
            if (user.ValidateToken(memberNo, policyNo, token))
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        public override void DoProcess()
        {
            log.Info("in");
            try
            {
                if (_Request.serviceName.Equals("GetMemberBenefit"))
                {
                    MemberLogic logic = new MemberLogic();
                    _GetMembersBenefitResponse.token = _GetMembersBenefitRequest.token;
                    logic.GetMemberBenefit_New(_GetMembersBenefitRequest, _GetMembersBenefitResponse);


                }

                else if (_Request.serviceName.Equals("GetMasterData"))
                {
                    log.Info("GetMasterData");
                    MemberLogic logic = new MemberLogic();
                    _GetMasterDataResponse.token = _GetMasterDataRequest.token;
                    logic.GetMasterData(_GetMasterDataRequest, _GetMasterDataResponse);


                }
                else if (_Request.serviceName.Equals("UpdateMemberInfo"))
                {
                    MemberLogic logic = new MemberLogic();
                    _UpdateMemberInfoResponse.token = _UpdateMemberInfoRequest.token;
                    logic.UpdateMemberInfo(_UpdateMemberInfoRequest, _UpdateMemberInfoResponse);


                }
                else if (_Request.serviceName.Equals("GetECard"))
                {
                    MemberLogic logic = new MemberLogic();
                    _GetECardResponse.token = _GetECardRequest.token;
                    logic.GetECard(_GetECardRequest, _GetECardResponse);


                }
                else if (_Request.serviceName.Equals("GetHealthTipDetail"))
                {
                    MemberLogic logic = new MemberLogic();

                    logic.GetHealthTipDetail(_GetHealthTipDetailRequest, _GetHealthTipDetailResponse);


                }
                else if (_Request.serviceName.Equals("UpdateClaimNotifyKey"))
                {
                    MemberLogic logic = new MemberLogic();
                    logic.UpdateClaimNotifyKey(_UpdateClaimNotifyKeyRequest, _UpdateClaimNotifyKeyResponse);
                }
                else if (_Request.serviceName.Equals("GetIPDClaimDetail"))
                {
                    MemberLogic logic = new MemberLogic();
                    logic.GetIPDClaimDetail(_GetIPDClaimDetailRequest, _GetIPDClaimDetailResponse);
                }
                else if (_Request.serviceName.Equals("GetOPDClaimDetail"))
                {
                    MemberLogic logic = new MemberLogic();
                    logic.GetOPDClaimDetail(_GetOPDClaimDetailRequest, _GetOPDClaimDetailResponse);
                }
                else if (_Request.serviceName.Equals("GetAMEClaimDetail"))
                {
                    MemberLogic logic = new MemberLogic();
                    logic.GetAMEClaimDetail(_GetAMEClaimDetailRequest, _GetAMEClaimDetailResponse);
                }
                else if (_Request.serviceName.Equals("GetDENClaimDetail"))
                {
                    MemberLogic logic = new MemberLogic();
                    logic.GetDENClaimDetail(_GetDENClaimDetailRequest, _GetDENClaimDetailResponse);
                }
                else if (_Request.serviceName.Equals("GetLABClaimDetail"))
                {
                    MemberLogic logic = new MemberLogic();
                    logic.GetLABClaimDetail(_GetLABClaimDetailRequest, _GetLABClaimDetailResponse);
                }
                else if (_Request.serviceName.Equals("GetMasterAgreement"))
                {
                    MemberLogic logic = new MemberLogic();
                    logic.GetMasterAgreement(_GetMasterAgreementRequest, _GetMasterAgreementResponse);
                }

                else if (_Request.serviceName.Equals("UpdateTransAgreement"))
                {
                    MemberLogic logic = new MemberLogic();
                    logic.UpdateTransAgreement(_UpdateTransAgreementRequest, _UpdateTransAgreementResponse);
                }

                else if (_Request.serviceName.Equals("TestGet"))
                {
                    MemberLogic logic = new MemberLogic();
                    logic.TestGetQuery(_TestGetRequest, _TestGetResponse);
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally { log.Info("out"); }
        }
        public override void DoPostProcess()
        {
            try
            {
                if (_Request.serviceName.Equals("ChkMember"))
                {

                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
        }

        public void GetMediaNews(LoadMediaNewsRequest req, LoadMediaNewsResponse res)
        {


            MemberLogic logic = new MemberLogic();
            logic.GetMediaNews(req, res);
            res.test = "aaa";

            
        }
    }
}
