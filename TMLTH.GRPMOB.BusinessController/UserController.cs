﻿using System;
using System.Configuration;
using TMLTH.GRPMOB.DTO.Request;
using TMLTH.GRPMOB.DTO.Response;
using TMLTH.GRPMOB.Model;

namespace TMLTH.GRPMOB.BusinessController
{
    public class UserController:BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        ChkMemberRequest _ChkMemberRequest;
        ChkMemberResponse _ChkMemberResponse;
        GetOTPRequest _GetOTPRequest;
        GetOTPResponse _GetOTPResponse;
        ChkOTPRequest _ChkOTPRequest;
        ChkOTPResponse _ChkOTPResponse;
        RegisterRequest _RegisterRequest;
        RegisterResponse _RegisterResponse;
        UpdPwdRequest _UpdPwdRequest;
        UpdPwdResponse _UpdPwdResponse;
        LoginRequest _LoginRequest;
        LoginResponse _LoginResponse;
        LogoutRequest _LogoutRequest;
        LogoutResponse _LogoutResponse;
        ChkMemberForgetPINRequest _ChkMemberForgetPINRequest;
        ChkMemberForgetPINResponse _ChkMemberForgetPINResponse;
        ChkForgetPassRequest _ChkForgetPassRequest;
        ChkForgetPassResponse _ChkForgetPassResponse;


        TestGetOTPRequest _TestGetOTPRequest;
        TestGetOTPResponse _TestGetOTPResponse;

        public override void DoTransformObject()
        {
            try
            {
                if (_Request.serviceName.Equals("ChkMember"))
                {
                    _ChkMemberRequest = (ChkMemberRequest)_Request;
                    _ChkMemberResponse = (ChkMemberResponse)_Response;
                }
                else if (_Request.serviceName.Equals("GetOTP"))
                {
                    _GetOTPRequest = (GetOTPRequest)_Request;
                    _GetOTPResponse = (GetOTPResponse)_Response;
                }
                else if (_Request.serviceName.Equals("ChkOTP"))
                {
                    _ChkOTPRequest = (ChkOTPRequest)_Request;
                    _ChkOTPResponse = (ChkOTPResponse)_Response;
                }
                else if (_Request.serviceName.Equals("Register"))
                {
                    _RegisterRequest = (RegisterRequest)_Request;
                    _RegisterResponse = (RegisterResponse)_Response;
                }
                else if (_Request.serviceName.Equals("UpdPwd"))
                {
                    _UpdPwdRequest = (UpdPwdRequest)_Request;
                    _UpdPwdResponse = (UpdPwdResponse)_Response;
                }
                else if (_Request.serviceName.Equals("Login"))
                {
                    _LoginRequest = (LoginRequest)_Request;
                    _LoginResponse = (LoginResponse)_Response;
                }
                else if (_Request.serviceName.Equals("ChkMemberForgetPIN"))
                {
                    _ChkMemberForgetPINRequest = (ChkMemberForgetPINRequest)_Request;
                    _ChkMemberForgetPINResponse = (ChkMemberForgetPINResponse)_Response;
                }
                else if (_Request.serviceName.Equals("TestGetOTP"))
                {
                    _TestGetOTPRequest = (TestGetOTPRequest)_Request;
                    _TestGetOTPResponse = (TestGetOTPResponse)_Response;
                }
                else if (_Request.serviceName.Equals("ChkForgetPass"))
                {
                    _ChkForgetPassRequest = (ChkForgetPassRequest)_Request;
                    _ChkForgetPassResponse = (ChkForgetPassResponse)_Response;
                }
                else if (_Request.serviceName.Equals("Logout"))
                {
                    _LogoutRequest = (LogoutRequest)_Request;
                    _LogoutResponse = (LogoutResponse)_Response;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
        }
        public override void DoValidate()
        {
            try
            {
                if (_Request.serviceName.Equals("ChkMember"))
                {
                    if (_ChkMemberRequest.memberNo.Equals("") || _ChkMemberRequest.policyNo.Equals(""))
                    {
                        throw new BusinessExpection(2, _ChkMemberResponse);
                    }

                }
                else if (_Request.serviceName.Equals("GetOTP"))
                {
                    if (_GetOTPRequest.action.Equals("REGISTER"))
                    {
                    }
                    else if (_GetOTPRequest.action.Equals("REREGISTER"))
                    {
                    }
                    else if (_GetOTPRequest.action.Equals("RESETPWD"))
                    {
                        if (!ValidateToken(_GetOTPRequest.memberNo, _GetOTPRequest.policyNo, _GetOTPRequest.token))
                        {
                            throw new BusinessExpection(9, _GetOTPResponse);
                        }
                    }
                    else
                    {
                        throw new BusinessExpection(11, _GetOTPResponse);
                    }
                }
                else if (_Request.serviceName.Equals("ChkOTP"))
                {
                    if (_ChkOTPRequest.OTP.Equals(""))
                    {
                        throw new BusinessExpection(2, _ChkOTPResponse);
                    }

                    if (_ChkOTPRequest.action.Equals("REGISTER"))
                    {
                    }
                    else if (_ChkOTPRequest.action.Equals("RESETPWD"))
                    {
                        if (!ValidateToken(_ChkOTPRequest.memberNo, _ChkOTPRequest.policyNo, _ChkOTPRequest.token))
                        {
                            throw new BusinessExpection(9, _ChkOTPResponse);
                        }
                    }
                    else if (_ChkOTPRequest.action.Equals("REREGISTER"))
                    {
                    }
                    else
                    {
                        throw new BusinessExpection(11, _GetOTPResponse);
                    }

                }

                else if (_Request.serviceName.Equals("Register"))
                {
                    if (_RegisterRequest.citizenID.Equals(""))
                    {
                        throw new BusinessExpection(2, _RegisterResponse);
                    }
                    if (_RegisterRequest.DOB.Equals(""))
                    {
                        throw new BusinessExpection(2, _RegisterResponse);
                    }
                    //if (_RegisterRequest.email.Equals(""))
                    //{
                    //    throw new BusinessExpection(2, _RegisterResponse);
                    //}
                    //if (_RegisterRequest.lineID.Equals(""))
                    //{
                    //    throw new BusinessExpection(2, _RegisterResponse);
                    //}
                    if (_RegisterRequest.memberNo.Equals(""))
                    {
                        throw new BusinessExpection(2, _RegisterResponse);
                    }
                    if (_RegisterRequest.mobileNo.Equals(""))
                    {
                        throw new BusinessExpection(2, _RegisterResponse);
                    }
                    if (_RegisterRequest.OTP.Equals(""))
                    {
                        throw new BusinessExpection(2, _RegisterResponse);
                    }
                    if (_RegisterRequest.policyNo.Equals(""))
                    {
                        throw new BusinessExpection(2, _RegisterResponse);
                    }
                    if (_RegisterRequest.token.Equals(""))
                    {
                        throw new BusinessExpection(3, _RegisterResponse);
                    }

                }
                else if (_Request.serviceName.Equals("UpdPwd"))
                {


                    if (_UpdPwdRequest.memberNo.Equals("") || _UpdPwdRequest.policyNo.Equals(""))
                    {
                        throw new BusinessExpection(2, _UpdPwdResponse);
                    }
                    if (_UpdPwdRequest.action.Equals("REGISTER"))
                    {

                    }
                    else if (_UpdPwdRequest.action.Equals("REREGISTER"))
                    {

                    }
                    else if (_UpdPwdRequest.action.Equals("FORGETPWD"))
                    {

                    }
                    else if (_UpdPwdRequest.action.Equals("RESETPWD"))
                    {
                        if (_UpdPwdRequest.oldPassWord.Equals(""))
                        {
                            throw new BusinessExpection(2, _UpdPwdResponse);
                        }
                        if (_UpdPwdRequest.passWord.Equals(""))
                        {
                            throw new BusinessExpection(2, _UpdPwdResponse);
                        }
                        if (_UpdPwdRequest.passWord.Equals(_UpdPwdRequest.oldPassWord))
                        {
                            throw new BusinessExpection(10, _UpdPwdResponse);
                        }
                        if (!ValidateToken(_UpdPwdRequest.memberNo, _UpdPwdRequest.policyNo, _UpdPwdRequest.token))
                        {
                            throw new BusinessExpection(9, _UpdPwdResponse);
                        }
                    }
                    else
                    {
                        throw new BusinessExpection(11, _UpdPwdResponse);
                    }




                }
                else if (_Request.serviceName.Equals("Login"))
                {


                    //if (_UpdPwdRequest.memberNo.Equals("") || _UpdPwdRequest.policyNo.Equals(""))
                    //{
                    //    throw new BusinessExpection(2, _UpdPwdResponse);
                    //}


                }

                else if (_Request.serviceName.Equals("ChkMemberForgetPIN"))
                {




                }


            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            
        }
        private bool ValidateToken(string memberNo, string policyNo, string token)
        {


                UserLogic user = new UserLogic();
            if (user.ValidateToken(memberNo, policyNo, token))
            {
                return true;
            }
            else
            {
                return false;
            }



        }
        public override void DoProcess()
        {
            try
            {
                if (_Request.serviceName.Equals("ChkMember"))
                {
                    UserLogic logic = new UserLogic();
                    _ChkMemberResponse.token = _ChkMemberRequest.token;
                    logic.chkGUserPass(_ChkMemberRequest, _ChkMemberResponse);


                }
                else if (_Request.serviceName.Equals("GetOTP"))
                {

                    if (_GetOTPRequest.action.Equals("REGISTER"))
                    {
                        try
                        {
                            _GetOTPResponse.token = _GetOTPRequest.token;

                            OTPLogic logic = new OTPLogic();
                            _GetOTPResponse.refID = logic.GetRefCode();
                            string strOTP = logic.GetOTPForRegister(_GetOTPRequest.mobileNo, _GetOTPRequest.citizenID, _GetOTPRequest.policyNo, _GetOTPRequest.memberNo, _GetOTPResponse.refID);
                            
                            
                            
                            //****** add data to table 

                            var otpDebug = ConfigurationManager.AppSettings.Get("OTPDebug");

                            if (otpDebug.ToString().Equals("Y"))
                            {
                                // create user logic to save otp
                                UserLogic userLogic = new UserLogic();
                                userLogic.UpdateTempOTP(_GetOTPRequest.action, _GetOTPRequest.citizenID, _GetOTPRequest.memberNo, _GetOTPRequest.policyNo, _GetOTPRequest.mobileNo, strOTP, _GetOTPResponse.refID);
                            }
                            _GetOTPResponse.errorCode = "0";


                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.ToString());
                            throw new BusinessExpection(13, _GetOTPResponse);
                        }
         
                    }

                    else if (_GetOTPRequest.action.Equals("REREGISTER"))
                    {
                        try
                        {
                            _GetOTPResponse.token = _GetOTPRequest.token;

                            OTPLogic logic = new OTPLogic();
                            _GetOTPResponse.refID = logic.GetRefCode();
                            string strOTP = logic.GetOTPForRegister(_GetOTPRequest.mobileNo, _GetOTPRequest.citizenID, _GetOTPRequest.policyNo, _GetOTPRequest.memberNo, _GetOTPResponse.refID);



                            //****** add data to table 

                            var otpDebug = ConfigurationManager.AppSettings.Get("OTPDebug");

                            if (otpDebug.ToString().Equals("Y"))
                            {
                                // create user logic to save otp
                                UserLogic userLogic = new UserLogic();
                                userLogic.UpdateTempOTP(_GetOTPRequest.action, _GetOTPRequest.citizenID, _GetOTPRequest.memberNo, _GetOTPRequest.policyNo, _GetOTPRequest.mobileNo, strOTP, _GetOTPResponse.refID);
                            }
                            _GetOTPResponse.errorCode = "0";


                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.ToString());
                            throw new BusinessExpection(13, _GetOTPResponse);
                        }

                    }
                    else if (_GetOTPRequest.action.Equals("RESETPWD"))
                    {
                        try
                        {
                            _GetOTPResponse.token = _GetOTPRequest.token;

                            OTPLogic logic = new OTPLogic();
                            _GetOTPResponse.refID = logic.GetRefCode();
                            string strOTP = logic.GetOTPForForgetPass(_GetOTPRequest.mobileNo, _GetOTPRequest.citizenID, _GetOTPRequest.policyNo, _GetOTPRequest.memberNo, _GetOTPResponse.refID);
                                                        
                            //****** add data to table 

                            var otpDebug = ConfigurationManager.AppSettings.Get("OTPDebug");

                            if (otpDebug.ToString().Equals("Y"))
                            {
                                // create user logic to save otp
                                UserLogic userLogic = new UserLogic();
                                userLogic.UpdateTempOTP(_GetOTPRequest.action, _GetOTPRequest.citizenID, _GetOTPRequest.memberNo, _GetOTPRequest.policyNo, _GetOTPRequest.mobileNo, strOTP, _GetOTPResponse.refID);
                            }
                            _GetOTPResponse.errorCode = "0";


                        }
                        catch (Exception)
                        {
                            throw new BusinessExpection(13, _GetOTPResponse);
                        }

                    }
                }
                else if (_Request.serviceName.Equals("ChkOTP"))
                {

                    if (_ChkOTPRequest.action.Equals("REGISTER"))
                    {
                        _ChkOTPResponse.errorCode = "0";
                        _ChkOTPResponse.token = _ChkOTPRequest.token;

                        var otpDebug = ConfigurationManager.AppSettings.Get("OTPDebug");

                        if (otpDebug.ToString().Equals("Y"))
                        {
                            UserLogic logic = new UserLogic();
                            string strStatus = logic.GetTempOTPStatus(_ChkOTPRequest.action, _ChkOTPRequest.citizenID, _ChkOTPRequest.memberNo, _ChkOTPRequest.policyNo, _ChkOTPRequest.mobileNo, _ChkOTPRequest.OTP, _ChkOTPRequest.refID);
                            _ChkOTPResponse.status = strStatus;
                        }
                        else
                        {
                            OTPLogic logic = new OTPLogic();
                            string strStatus = logic.ChkOTPForRegister(_ChkOTPRequest.mobileNo, _ChkOTPRequest.citizenID, _ChkOTPRequest.policyNo, _ChkOTPRequest.memberNo, _ChkOTPRequest.OTP, _ChkOTPRequest.refID);
                            _ChkOTPResponse.status = strStatus;
                        }
                        _ChkOTPResponse.refID = _ChkOTPRequest.refID;
                    }
                    else if (_ChkOTPRequest.action.Equals("REREGISTER"))
                    {
                        _ChkOTPResponse.errorCode = "0";
                        _ChkOTPResponse.token = _ChkOTPRequest.token;

                        var otpDebug = ConfigurationManager.AppSettings.Get("OTPDebug");

                        if (otpDebug.ToString().Equals("Y"))
                        {
                            UserLogic logic = new UserLogic();
                            string strStatus = logic.GetTempOTPStatus(_ChkOTPRequest.action, _ChkOTPRequest.citizenID, _ChkOTPRequest.memberNo, _ChkOTPRequest.policyNo, _ChkOTPRequest.mobileNo, _ChkOTPRequest.OTP, _ChkOTPRequest.refID);
                            _ChkOTPResponse.status = strStatus;
                        }
                        else
                        {
                            OTPLogic logic = new OTPLogic();
                            string strStatus = logic.ChkOTPForRegister(_ChkOTPRequest.mobileNo, _ChkOTPRequest.citizenID, _ChkOTPRequest.policyNo, _ChkOTPRequest.memberNo, _ChkOTPRequest.OTP, _ChkOTPRequest.refID);
                            _ChkOTPResponse.status = strStatus;
                        }
                        _ChkOTPResponse.refID = _ChkOTPRequest.refID;
                    }
                    else if (_ChkOTPRequest.action.Equals("RESETPWD"))
                    {
                        _ChkOTPResponse.errorCode = "0";
                        _ChkOTPResponse.token = _ChkOTPRequest.token;

                        var otpDebug = ConfigurationManager.AppSettings.Get("OTPDebug");

                        if (otpDebug.ToString().Equals("Y"))
                        {
                            UserLogic logic = new UserLogic();
                            string strStatus = logic.GetTempOTPStatus(_ChkOTPRequest.action, _ChkOTPRequest.citizenID, _ChkOTPRequest.memberNo, _ChkOTPRequest.policyNo, _ChkOTPRequest.mobileNo, _ChkOTPRequest.OTP, _ChkOTPRequest.refID);
                            _ChkOTPResponse.status = strStatus;
                        }
                        else
                        {
                            OTPLogic logic = new OTPLogic();
                            string strStatus = logic.ChkOTPForForgetPass(_ChkOTPRequest.mobileNo, _ChkOTPRequest.citizenID, _ChkOTPRequest.policyNo, _ChkOTPRequest.memberNo, _ChkOTPRequest.OTP, _ChkOTPRequest.refID);
                            _ChkOTPResponse.status = strStatus;
                        }
                        _ChkOTPResponse.refID = _ChkOTPRequest.refID;
                    }

                    return;
                }
                else if (_Request.serviceName.Equals("Register"))
                {
                    UserLogic logic = new UserLogic();
                    _RegisterResponse.token = _RegisterResponse.token;
                    logic.Register(_RegisterRequest, _RegisterResponse);


                }
                else if (_Request.serviceName.Equals("UpdPwd"))
                {
                    UserLogic logic = new UserLogic();
                    _UpdPwdResponse.token = _UpdPwdRequest.token;

                    if (_UpdPwdRequest.action.Equals("REGISTER"))
                    {
                        logic.UpdPwdForRegister(_UpdPwdRequest, _UpdPwdResponse);
                    }
                    else if (_UpdPwdRequest.action.Equals("REREGISTER"))
                    {
                        logic.UpdPwdForRegister(_UpdPwdRequest, _UpdPwdResponse);
                    }
                    else if (_UpdPwdRequest.action.Equals("RESETPWD"))
                    {
                        logic.UpdPwdForReset(_UpdPwdRequest, _UpdPwdResponse);
                    }
                    else if (_UpdPwdRequest.action.Equals("FORGETPWD"))
                    {
                        logic.UpdPwdForForgetPass(_UpdPwdRequest, _UpdPwdResponse);
                    }


                }
                else if (_Request.serviceName.Equals("Login"))
                {
                    UserLogic logic = new UserLogic();
                    //_LoginResponse.token = _LoginRequest.token;
                    if (_LoginRequest.passWord.Equals("-"))
                    {
                        logic.GenerateTokenForLogin(_LoginRequest, _LoginResponse);
                    }
                    else
                    {

                        logic.Login(_LoginRequest, _LoginResponse);
                    }


                }

                else if (_Request.serviceName.Equals("ChkMemberForgetPIN"))
                {
                    UserLogic logic = new UserLogic();
                    //_LoginResponse.token = _LoginRequest.token;
                    logic.ChkMemberForgetPIN(_ChkMemberForgetPINRequest, _ChkMemberForgetPINResponse);


                }
                else if (_Request.serviceName.Equals("ChkForgetPass"))
                {
                    UserLogic logic = new UserLogic();
                    //_LoginResponse.token = _LoginRequest.token;
                    logic.ChkForgetPass(_ChkForgetPassRequest, _ChkForgetPassResponse);


                }
                else if (_Request.serviceName.Equals("TestGetOTP"))
                {
                    _TestGetOTPResponse.errorCode = "0";

                     UserLogic logic = new UserLogic();

                    logic.TestGetTempOTP(_TestGetOTPRequest, _TestGetOTPResponse);



                    return;
                }
                else if (_Request.serviceName.Equals("Logout"))
                {
                    _LogoutResponse.errorCode = "0";

                    // Clear token
                    UserLogic logic = new UserLogic();
                    logic.Logout(_LogoutRequest, _LogoutResponse);

                    AuditHelper.UpdateTransEventLog("Logout", _LogoutRequest.memberNo, _LogoutRequest.policyNo, EnumMasEventLogId.Logout, _LogoutRequest.osPlatform, _LogoutRequest.clientIPAddress);



                    return;
                }


            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
        }
        public override void DoPostProcess()
        {
            try
            {
                if (_Request.serviceName.Equals("ChkMember"))
                {

                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
        }
    }
}
