﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TMLTH.GRPMOB.BusinessController
{
    [XmlRoot("ErrorList")]
    public class ErrorList
    {
        [XmlElement("Error")]
        public List<Error> Errors { get; set; }
    }



    public class Error
    {
        [XmlElement("ErrorCode")]
        public string ErrorCode { get; set; }
        [XmlElement("ErrorDescEN")]
        public string ErrorDescEN { get; set; }
        [XmlElement("ErrorDescTH")]
        public string ErrorDescTH { get; set; }
    }
}
