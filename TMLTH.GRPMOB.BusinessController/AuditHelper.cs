﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMLTH.GRPMOB.Model;

namespace TMLTH.GRPMOB.BusinessController
{
    public static class AuditHelper
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void UpdateTransEventLog(string serviceName, string memberNo, string policyNo, EnumMasEventLogId eventId, string osPlatform, string clientIP)
        {
            try
            {
                MemberModel member = new MemberModel();
                member.UpdateAuditLog(serviceName, memberNo, policyNo, eventId, osPlatform, clientIP);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
            finally
            { }
        }
    }
}
