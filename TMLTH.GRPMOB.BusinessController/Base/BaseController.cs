﻿using System;
using System.Collections.Generic;
using System.Text;
using TMLTH.GRPMOB.DTO.Request;
using TMLTH.GRPMOB.DTO.Response;

namespace TMLTH.GRPMOB.BusinessController
{
    public abstract class BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected BaseRequest _Request;
        protected BaseResponse _Response;
        public virtual void Execute(BaseRequest req, BaseResponse res)
        {
            if (null == req.serviceName)
            {
                
                throw new BusinessExpection(999, res);
            }
            _Request = req;
            _Response = res;
            try
            {
                DoTransformObject();
                DoValidate();                
                DoProcess();
                DoPostProcess();
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
            }
        }

        public abstract void DoTransformObject();

        public abstract void DoValidate();
               
        public abstract void DoProcess();

        public abstract void DoPostProcess();
       
    }
}
