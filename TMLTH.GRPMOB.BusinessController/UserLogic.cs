﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using TMLTH.GRPMOB.DTO.Request;
using TMLTH.GRPMOB.DTO.Response;
using TMLTH.GRPMOB.Model;
using TMLTH.GRPMOB.UTIL;

namespace TMLTH.GRPMOB.BusinessController
{
    public class UserLogic
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void chkGUserPass(ChkMemberRequest req, ChkMemberResponse res)
        {
            try
            {
                UserModel user = new UserModel();

                if (!user.IsExistOnSG_MEMBER_DETAIL(req.memberNo, req.policyNo))
                {
                    AuditHelper.UpdateTransEventLog("Register", req.memberNo, req.policyNo, EnumMasEventLogId.RegisterMissingUser, req.osPlatform, req.clientIPAddress);
                    throw new BusinessExpection(4, res);
                }
                else if (!user.IsUserRegisCompleted(req.memberNo, req.policyNo))
                {
                    res.errorCode = "0";
                }
                else
                {
                    AuditHelper.UpdateTransEventLog("Register", req.memberNo, req.policyNo, EnumMasEventLogId.RegisterExistUser, req.osPlatform, req.clientIPAddress);

                    throw new BusinessExpection(1, res);
                    
                }
            }
            catch (Exception ex)
            {
                // res.errorCode = "998"; // unknow error
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
        }

        public void Register(RegisterRequest req, RegisterResponse res)
        {
            try
            {
                UserModel user = new UserModel();
                if (!user.IsExistOnSG_MEMBER_DETAIL(req.memberNo, req.policyNo))
                {
                    AuditHelper.UpdateTransEventLog("Register", req.memberNo, req.policyNo, EnumMasEventLogId.RegisterMissingUser, req.osPlatform, req.clientIPAddress);
                    throw new BusinessExpection(4, res);
                }
                else if (!user.IsDOBValid(req.DOB,req.memberNo, req.policyNo))
                {
                    throw new BusinessExpection(5, res);
                }
                else if (!user.IsCitizenValid(req.citizenID, req.memberNo, req.policyNo))
                {
                    throw new BusinessExpection(6, res);
                }

                else if (user.DoRegister(req.citizenID, req.DOB, req.email, req.lineID, req.memberNo, req.OTP, req.policyNo, req.mobileNo))
                {
                    res.errorCode = "0";
                    res.policyNo = req.policyNo;
                    res.DOB = req.DOB;
                    res.citizenID = req.citizenID;
                    res.email = req.email;
                    res.lineID = req.lineID;
                    res.memberNo = req.memberNo;
                    res.mobileNo = req.mobileNo;

                    AuditHelper.UpdateTransEventLog("Register", req.memberNo, req.policyNo, EnumMasEventLogId.RegisterPass, req.osPlatform, req.clientIPAddress);
                }
                else
                {
                    AuditHelper.UpdateTransEventLog("Register", req.memberNo, req.policyNo, EnumMasEventLogId.RegisterExistUser, req.osPlatform, req.clientIPAddress);
                    throw new BusinessExpection(1, res);

                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                //res.errorCode = "998"; // unknow error
                res.log = ex.ToString();
                throw;
            }
        }

        public void UpdPwdForReset(UpdPwdRequest req, UpdPwdResponse res)
        {
            try
            {
                UserModel user = new UserModel();

                string strOldPassword = "";
                if (!user.IsUserRegis(req.memberNo, req.policyNo))
                {
                    throw new BusinessExpection(7, res);
                }
                byte[] encrypted;
                try
                {
                    // decrypt password
                    encrypted = Convert.FromBase64String(req.oldPassWord);
                }
                catch (Exception)
                {
                    throw new BusinessExpection(8, res);
                }
                // check old password
                string strKey = req.installationID.Replace("-", "");
                using (RijndaelManaged myRijndael = new RijndaelManaged())
                {

                    //myRijndael.GenerateKey();
                    //myRijndael.GenerateIV();
                    myRijndael.Key = Encoding.ASCII.GetBytes(strKey.Substring(0, 16));
                    myRijndael.IV = Encoding.ASCII.GetBytes(strKey.Substring(0, 16));
                    // Encrypt the string to an array of bytes.
                    //byte[] encrypted = EncryptionHelper.EncryptStringToBytes(req.passWord, myRijndael.Key, myRijndael.IV);
                    //byte[] encrypted = Encoding.ASCII.GetBytes(req.passWord);
                    string strEncrypt = Convert.ToBase64String(encrypted);
                    //string strEncrypt = ;
                    // Decrypt the bytes to a string.
                    string roundtrip = EncryptionHelper.DecryptStringFromBytes(encrypted, myRijndael.Key, myRijndael.IV);

                    log.Debug("pass: " + roundtrip);
                    string strEncryptPass =  AesEncyptsDecrypts.aes128Encrypt(roundtrip);

                    log.Debug("pass encrypt: " + strEncryptPass);
                    // get the password from DB
                    if (!user.IsPasswordCorrect(req.memberNo, req.policyNo, strEncryptPass))
                    {
                        throw new BusinessExpection(8, res);
                    }

                    strOldPassword = roundtrip;
                }


                // decrypt new password                
                try
                {
                    // decrypt password
                    encrypted = Convert.FromBase64String(req.passWord);
                }
                catch (Exception)
                {
                    throw new BusinessExpection(8, res);
                }
                strKey = req.installationID.Replace("-", "");
                using (RijndaelManaged myRijndael = new RijndaelManaged())
                {

                    //myRijndael.GenerateKey();
                    //myRijndael.GenerateIV();
                    myRijndael.Key = Encoding.ASCII.GetBytes(strKey.Substring(0, 16));
                    myRijndael.IV = Encoding.ASCII.GetBytes(strKey.Substring(0, 16));
                    // Encrypt the string to an array of bytes.
                    //byte[] encrypted = EncryptionHelper.EncryptStringToBytes(req.passWord, myRijndael.Key, myRijndael.IV);
                    //byte[] encrypted = Encoding.ASCII.GetBytes(req.passWord);
                    string strEncrypt = Convert.ToBase64String(encrypted);
                    //string strEncrypt = ;
                    // Decrypt the bytes to a string.
                    string roundtrip = EncryptionHelper.DecryptStringFromBytes(encrypted, myRijndael.Key, myRijndael.IV);

                    if (roundtrip.Equals(strOldPassword))
                    {
                        throw new BusinessExpection(10, res);
                    }

                    // encrypt by tokio module
                    string strEncryptPass = AesEncyptsDecrypts.aes128Encrypt(roundtrip);

                    if (user.DoUpdatePassword(req.memberNo, req.policyNo, strEncryptPass))
                    {
                        res.errorCode = "0";
                        res.memberNo = req.memberNo;
                        res.policyNo = res.policyNo;
                    }
                    else
                    {
                        throw new BusinessExpection(1, res);

                    }
                }


            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                //res.errorCode = "998"; // unknow error
                res.log = ex.ToString();
                throw;
            }
        }

        public void UpdPwdForRegister(UpdPwdRequest req, UpdPwdResponse res)
        {
            try
            {
                UserModel user = new UserModel();
                                
                if (!user.IsUserRegis(req.memberNo, req.policyNo))
                {
                    throw new BusinessExpection(7, res);
                }
                byte[] encrypted;
                try
                {
                    // decrypt password
                    encrypted = Convert.FromBase64String(req.passWord);
                }
                catch (Exception)
                {
                    throw new BusinessExpection(8, res);
                }
                // check old password
                string strKey = req.installationID.Replace("-", "");
                using (RijndaelManaged myRijndael = new RijndaelManaged())
                {

                    //myRijndael.GenerateKey();
                    //myRijndael.GenerateIV();
                    myRijndael.Key = Encoding.ASCII.GetBytes(strKey.Substring(0, 16));
                    myRijndael.IV = Encoding.ASCII.GetBytes(strKey.Substring(0, 16));
                    // Encrypt the string to an array of bytes.
                    //byte[] encrypted = EncryptionHelper.EncryptStringToBytes(req.passWord, myRijndael.Key, myRijndael.IV);
                    //byte[] encrypted = Encoding.ASCII.GetBytes(req.passWord);
                    string strEncrypt = Convert.ToBase64String(encrypted);
                    //string strEncrypt = ;
                    // Decrypt the bytes to a string.
                    string roundtrip = EncryptionHelper.DecryptStringFromBytes(encrypted, myRijndael.Key, myRijndael.IV);
                    log.Debug("pass:" + roundtrip);
                    // encrypt by tokio module
                    string strEncryptPass = AesEncyptsDecrypts.aes128Encrypt(roundtrip);

                    log.Debug("encrypt pass:" + strEncryptPass);
                    if (user.DoUpdatePassword(req.memberNo, req.policyNo, strEncryptPass))
                    {
                        res.errorCode = "0";
                        res.memberNo = req.memberNo;
                        res.policyNo = res.policyNo;
                    }
                    else
                    {
                        throw new BusinessExpection(1, res);

                    }
                }
               

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                //res.errorCode = "998"; // unknow error
                res.log = ex.ToString();
                throw;
            }
        }
        public void UpdPwdForForgetPass(UpdPwdRequest req, UpdPwdResponse res)
        {
            try
            {
                UserModel user = new UserModel();

                if (!user.IsUserRegis(req.memberNo, req.policyNo))
                {
                    throw new BusinessExpection(7, res);
                }
                byte[] encrypted;
                try
                {
                    // decrypt password
                    encrypted = Convert.FromBase64String(req.passWord);
                }
                catch (Exception)
                {
                    throw new BusinessExpection(8, res);
                }
                // check old password
                string strKey = req.installationID.Replace("-", "");
                using (RijndaelManaged myRijndael = new RijndaelManaged())
                {

                    //myRijndael.GenerateKey();
                    //myRijndael.GenerateIV();
                    myRijndael.Key = Encoding.ASCII.GetBytes(strKey.Substring(0, 16));
                    myRijndael.IV = Encoding.ASCII.GetBytes(strKey.Substring(0, 16));
                    // Encrypt the string to an array of bytes.
                    //byte[] encrypted = EncryptionHelper.EncryptStringToBytes(req.passWord, myRijndael.Key, myRijndael.IV);
                    //byte[] encrypted = Encoding.ASCII.GetBytes(req.passWord);
                    string strEncrypt = Convert.ToBase64String(encrypted);
                    //string strEncrypt = ;
                    // Decrypt the bytes to a string.
                    string roundtrip = EncryptionHelper.DecryptStringFromBytes(encrypted, myRijndael.Key, myRijndael.IV);
                    log.Debug("pass:" + roundtrip);
                    // encrypt by tokio module
                    string strEncryptPass = AesEncyptsDecrypts.aes128Encrypt(roundtrip);

                    log.Debug("encrypt pass:" + strEncryptPass);
                    if (user.DoUpdatePassword(req.memberNo, req.policyNo, strEncryptPass))
                    {
                        res.errorCode = "0";
                        res.memberNo = req.memberNo;
                        res.policyNo = req.policyNo;
                        res.action = req.action;
                    }
                    else
                    {
                        throw new BusinessExpection(1, res);

                    }
                }


            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                //res.errorCode = "998"; // unknow error
                res.log = ex.ToString();
                throw;
            }
        }
        public void Login(LoginRequest req, LoginResponse res)
        {
            try
            {
                UserModel user = new UserModel();
                // check if any user

                //if (!user.IsUserRegisCompleted(req.memberNo, req.policyNo))
                //{
                //    throw new BusinessExpection(4, res);

                //}
                bool bSkip = false;
                byte[] encrypted = null ;
                try
                {
                    // decrypt password
                    encrypted = Convert.FromBase64String(req.passWord);
                }
                catch (Exception)
                {

                    if (req.passWord.Equals("?"))
                    {
                        bSkip = true;
                    }
                    if (!bSkip)
                    {
                        AuditHelper.UpdateTransEventLog("Login", req.memberNo, req.policyNo, EnumMasEventLogId.LoginWrongPass, req.osPlatform, req.clientIPAddress);
                        throw new BusinessExpection(8, res);
                    }
                }
                //EncryptionHelper.SetKey(req.installationID);
                //string original = "hok12345";
                //string original = req.passWord;
                // Create a new instance of the RijndaelManaged
                // class.  This generates a new key and initialization 
                // vector (IV).
                string strKey = req.installationID.Replace("-", "");

                string strEncrypt = "";
                string roundtrip = "";
                using (RijndaelManaged myRijndael = new RijndaelManaged())
                {
                    if (!bSkip)
                    {
                        //myRijndael.GenerateKey();
                        //myRijndael.GenerateIV();
                        myRijndael.Key = Encoding.ASCII.GetBytes(strKey.Substring(0, 16));
                        myRijndael.IV = Encoding.ASCII.GetBytes(strKey.Substring(0, 16));
                        // Encrypt the string to an array of bytes.
                        //byte[] encrypted = EncryptionHelper.EncryptStringToBytes(req.passWord, myRijndael.Key, myRijndael.IV);
                        //byte[] encrypted = Encoding.ASCII.GetBytes(req.passWord);
                        strEncrypt = Convert.ToBase64String(encrypted);
                        //string strEncrypt = ;
                        // Decrypt the bytes to a string.
                        roundtrip = EncryptionHelper.DecryptStringFromBytes(encrypted, myRijndael.Key, myRijndael.IV);
                    }
                    log.Debug("pass: " + roundtrip);
                    string strEncryptPass = AesEncyptsDecrypts.aes128Encrypt(roundtrip);

                    log.Debug("pass encrypt: " + strEncryptPass);
                    // get the password from DB
                    if (!bSkip)
                    {


                        if (!user.IsPasswordCorrect(req.memberNo, req.policyNo, strEncryptPass))
                        {
                            AuditHelper.UpdateTransEventLog("Login", req.memberNo, req.policyNo, EnumMasEventLogId.LoginWrongPass, req.osPlatform, req.clientIPAddress);
                            throw new BusinessExpection(8, res);
                        }
                        else
                        {
                            // generate token
                            string token = EncryptionHelper.GetToken();
                            string strLastLogin = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                            user.DoUpdateLogin(token, req.installationID, strLastLogin, req.memberNo, req.policyNo);

                            try
                            {
                                // get information
                                DataSet ds = user.GetUserInfo(req.memberNo, req.policyNo);

                                if (null != ds.Tables[0].Rows[0])
                                {
                                    res.lineID = ds.Tables[0].Rows[0]["line_id"].ToString();
                                    res.mobileNo = ds.Tables[0].Rows[0]["mobile_no"].ToString();
                                    res.email = ds.Tables[0].Rows[0]["email"].ToString();
                                    res.firstName = ds.Tables[0].Rows[0]["LGIVNAME"].ToString().TrimEnd();
                                    res.lastName = ds.Tables[0].Rows[0]["LSURNAME"].ToString().TrimEnd();
                                    res.dob = ds.Tables[0].Rows[0]["CLTDOB"].ToString().TrimEnd();
                                    res.citizenID = ds.Tables[0].Rows[0]["SECUITYNO"].ToString().TrimEnd();

                                    res.forceAcceptAgreement = "TRUE";
                                    res.needReRegister = "Y";

                                    if (ds.Tables[0].Rows[0]["completed"].ToString().Equals("N"))
                                    {
                                        res.needReRegister = "Y";
                                    }
                                    else
                                    {
                                        res.needReRegister = "N";
                                    }


                                    MemberModel member = new MemberModel();

                                    DataSet dsUser = user.GetAcceptAgreement(req.policyNo, req.memberNo);
                                    DataSet dsMember = member.GetMasterAgreement();


                                    if (null != dsUser && null != dsMember)
                                    {
                                        long latestAgreement = Convert.ToInt64(dsMember.Tables[0].Rows[0]["VERSION"].ToString());

                                        long latestSubmitAgreement = Convert.ToInt64(dsUser.Tables[0].Rows[0]["MAS_AGREEMENT_VERSION"].ToString());



                                        if (latestAgreement != latestSubmitAgreement)
                                            res.forceAcceptAgreement = "TRUE";
                                        else
                                            res.forceAcceptAgreement = "FALSE";
                                    }


                                    res.errorCode = "0";
                                    res.token = token;


                                    AuditHelper.UpdateTransEventLog("Login", req.memberNo, req.policyNo, EnumMasEventLogId.LoginPass, req.osPlatform, req.clientIPAddress);
                                }
                                else
                                {
                                    throw new BusinessExpection(4, res);
                                }
                            }
                            catch (Exception)
                            {

                                throw new BusinessExpection(4, res);

                            }
                        }
                    }

                    else
                    {

                        // generate token
                        string token = EncryptionHelper.GetToken();
                        string strLastLogin = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

                        try
                        {
                            // get information
                            DataSet ds = user.GetUserInfo(req.memberNo, req.policyNo);

                            if (null != ds.Tables[0].Rows[0])
                            {
                                res.lineID = ds.Tables[0].Rows[0]["line_id"].ToString();
                                res.mobileNo = ds.Tables[0].Rows[0]["mobile_no"].ToString();
                                res.email = ds.Tables[0].Rows[0]["email"].ToString();
                                res.firstName = ds.Tables[0].Rows[0]["LGIVNAME"].ToString().TrimEnd();
                                res.lastName = ds.Tables[0].Rows[0]["LSURNAME"].ToString().TrimEnd();
                                res.dob = ds.Tables[0].Rows[0]["CLTDOB"].ToString().TrimEnd();
                                res.citizenID = ds.Tables[0].Rows[0]["SECUITYNO"].ToString().TrimEnd();

                                res.forceAcceptAgreement = "TRUE";
                                res.needReRegister = "Y";

                                if (ds.Tables[0].Rows[0]["completed"].ToString().Equals("N"))
                                {
                                    res.needReRegister = "Y";
                                }
                                else
                                {
                                    res.needReRegister = "N";
                                }


                                MemberModel member = new MemberModel();

                                DataSet dsUser = user.GetAcceptAgreement(req.policyNo, req.memberNo);
                                DataSet dsMember = member.GetMasterAgreement();


                                if (null != dsUser && null != dsMember)
                                {
                                    long latestAgreement = Convert.ToInt64(dsMember.Tables[0].Rows[0]["VERSION"].ToString());

                                    long latestSubmitAgreement = Convert.ToInt64(dsUser.Tables[0].Rows[0]["MAS_AGREEMENT_VERSION"].ToString());



                                    if (latestAgreement != latestSubmitAgreement)
                                        res.forceAcceptAgreement = "TRUE";
                                    else
                                        res.forceAcceptAgreement = "FALSE";
                                }


                                res.errorCode = "0";
                                res.token = token;


                                AuditHelper.UpdateTransEventLog("Login", req.memberNo, req.policyNo, EnumMasEventLogId.LoginPass, req.osPlatform, req.clientIPAddress);
                            }
                            else
                            {
                                throw new BusinessExpection(4, res);
                            }

                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.ToString());
                        }
                    }

                }



            }
            catch (BusinessExpection bex)
            {
                log.Error(bex.ToString());
                return;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                //res.errorCode = "998"; // unknow error
                res.log = ex.ToString();
                throw;
            }
        }
        public void Logout(LogoutRequest req, LogoutResponse res)
        {
            log.Info("in");
            try

            {
                UserModel model = new UserModel();
                model.DoClearToken("", req.installationID, req.memberNo, req.policyNo);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());

            }

            finally
            {
                log.Info("out");
            }
        }
            public void GenerateTokenForLogin(LoginRequest req, LoginResponse res)
        {
            try
            {
                UserModel user = new UserModel();
                // check if any user

                if (!user.IsUserRegis(req.memberNo, req.policyNo))
                {
                    throw new BusinessExpection(4, res);

                }

                DataSet ds = user.GetUserInfo(req.memberNo, req.policyNo);

                if (ds.Tables[0].Rows[0]["token"].ToString().Trim().Equals(req.token) && ds.Tables[0].Rows[0]["installation_id"].ToString().Trim().Equals(req.installationID))
                {

                    // generate token
                    string token = EncryptionHelper.GetToken();
                    string strLastLogin = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                    user.DoUpdateLogin(token, req.installationID, strLastLogin, req.memberNo, req.policyNo);


                    res.errorCode = "0";
                    res.token = token;
                    res.lineID = ds.Tables[0].Rows[0]["line_id"].ToString();
                    res.mobileNo = ds.Tables[0].Rows[0]["mobile_no"].ToString();
                    res.email = ds.Tables[0].Rows[0]["email"].ToString();
                    res.firstName = ds.Tables[0].Rows[0]["LGIVNAME"].ToString().TrimEnd();
                    res.lastName = ds.Tables[0].Rows[0]["LSURNAME"].ToString().TrimEnd();
                }
                else 
                {
                    // generate token
                    string token = EncryptionHelper.GetToken();
                    string strLastLogin = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                    user.DoUpdateLogin(token, req.installationID, strLastLogin, req.memberNo, req.policyNo);


                    res.errorCode = "0";
                    res.token = token;
                    res.lineID = ds.Tables[0].Rows[0]["line_id"].ToString();
                    res.mobileNo = ds.Tables[0].Rows[0]["mobile_no"].ToString();
                    res.email = ds.Tables[0].Rows[0]["email"].ToString();
                    res.firstName = ds.Tables[0].Rows[0]["LGIVNAME"].ToString().TrimEnd();
                    res.lastName = ds.Tables[0].Rows[0]["LSURNAME"].ToString().TrimEnd();
                }
                //else
                //{
                //    throw new BusinessExpection(9, res);
                //}
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                //res.errorCode = "998"; // unknow error
                res.log = ex.ToString();
                throw;
            }
        }
        public void ChkMemberForgetPIN(ChkMemberForgetPINRequest req, ChkMemberForgetPINResponse res)
        {
            try
            {
                UserModel user = new UserModel();
                // check if any user

                if (!user.IsMemberInfoValid(req.DOB, req.citizenID, req.memberNo, req.policyNo))
                {
                    throw new BusinessExpection(12, res);

                }                       

                else
                {
                    // all correct.
                    res.errorCode = "0";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                //res.errorCode = "998"; // unknow error
                res.log = ex.ToString();
                throw;
            }
        }

        public bool ValidateToken(string memberNo, string policyNo, string token)
        {
            try
            {
                UserModel user = new UserModel();
                // check if any user                
                if (!user.IsUserRegis(memberNo, policyNo))
                {
                    return false;
                }
                DataSet ds = user.GetUserInfo(memberNo, policyNo);

                if (ds.Tables[0].Rows[0]["token"].ToString().Trim().Equals(token))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                //res.errorCode = "998"; // unknow error
                return false;
                throw;
            }
        }

        public bool UpdateTempOTP(string action, string citizenId, string memberNo, string policyNo, string mobileNo, string OTP, string refId)
        {
            try
            {
                UserModel model = new UserModel();
                if (model.UpdateTempOTP(action, citizenId, memberNo, policyNo, mobileNo, OTP, refId))
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());

                throw;
            }
        }

        public string GetTempOTPStatus(string action, string citizenId, string memberNo, string policyNo, string mobileNo, string OTP, string refId)
        {
            try
            {
                UserModel model = new UserModel();
                return model.GetTempOTPStatus(action, citizenId, memberNo, policyNo, mobileNo, OTP, refId);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());

                throw;
            }
        }
        public void ChkForgetPass(ChkForgetPassRequest req, ChkForgetPassResponse res )
        {
            try
            {
                UserModel model = new UserModel();
                DataSet ds =  model.GetUserInfo(req.memberNo, req.policyNo);

                if (null != ds)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0]["SECUITYNO"].ToString().Trim().Equals(req.citizenID))
                        {
                            string strDateWhFormat = req.dob.Substring(6, 2) + "/" + req.dob.Substring(4, 2) + "/" + req.dob.Substring(0, 4);
                            if (ds.Tables[0].Rows[0]["CLTDOB"].ToString().Trim().Equals(strDateWhFormat))
                            {
                                res.errorCode = "0";

                                AuditHelper.UpdateTransEventLog("ForgetPass", req.memberNo, req.policyNo, EnumMasEventLogId.ForgetPasswordChanged, req.osPlatform, req.clientIPAddress);
                                return;
                            }
                        }
                    }
                    else

                    {
                        throw new BusinessExpection(7, res);
                    }

                }
                AuditHelper.UpdateTransEventLog("ForgetPass", req.memberNo, req.policyNo, EnumMasEventLogId.ForgetPasswordError, req.osPlatform, req.clientIPAddress);
                throw new BusinessExpection(16, res);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());

                throw;
            }
        }
        public void TestGetTempOTP(TestGetOTPRequest req, TestGetOTPResponse res)
        {
            try
            {
                UserModel model = new UserModel();
                DataSet ds = model.TestGetTempOTP(req.action, req.memberNo);

                res.OTP = ds.Tables[0].Rows[0]["otp_value"].ToString();
                res.status = ds.Tables[0].Rows[0]["status"].ToString();
                res.refID = ds.Tables[0].Rows[0]["ref_id"].ToString();
                return;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());

                throw;
            }
        }
    }
}
