﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMLTH.GRPMOB.DTO.Request;
using TMLTH.GRPMOB.DTO.Response;
using TMLTH.GRPMOB.Model;
using TMLTH.GRPMOB.Model.EDM;
using System.Data;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Web.Script.Serialization;
using THMLTH.GRPMOB.DTO.Common;
using System.Text.RegularExpressions;

namespace TMLTH.GRPMOB.BusinessController
{
    public class MemberLogic
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void GetMemberBenefit_New(GetMembersBenefitRequest req, GetMembersBenefitResponse res)
        {
            log.Info("in");

            try
            {
                MemberModel member = new MemberModel();
                DataSet dsMemberDetail = member.GetMemberDetailAllFamily(req.policyNo, req.memberNo);

                DataSet dsGUserInfo = member.GetGUserInfoInFamily(req.policyNo, req.memberNo);

                DataSet dsClaim = member.GetMemberClaim(req.policyNo, req.memberNo);

                DataSet dsBen = member.GetMemberBen(req.policyNo, req.memberNo);

                string strDOB;
                int iYear, iMonth;
                DateTime dtDOB;

                DataRow tmpRow = null;
                List<DataRow> tmpRowList = null;


                res.errorCode = "0";
                res.policyNo = req.policyNo;
                res.startDate = "";
                res.endDate = "";
                res.companyNM = "";
                res.language = "TH";
                res.claimTotNotify = "";



                EnumerableRowCollection<DataRow> EmpRow = null;
                EnumerableRowCollection<DataRow> EmpGUserInfoRow = null;
                EnumerableRowCollection<DataRow> EmpClaimRow = null;
                EnumerableRowCollection<DataRow> EmpBenefitRow = null;


                if (null != dsMemberDetail)
                {
                    EmpRow = from MemberRows in dsMemberDetail.Tables[0].AsEnumerable()
                             where MemberRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-00"
                             select MemberRows;
                }
                if (null != dsGUserInfo)
                {
                    EmpGUserInfoRow = from EmpGUserInfoRows in dsGUserInfo.Tables[0].AsEnumerable()
                                      where EmpGUserInfoRows.Field<string>("usernum").Trim() == (req.memberNo.Substring(0, 5) + "-00").Trim()
                                      select EmpGUserInfoRows;
                }
                if (null != dsClaim)
                {
                    EmpClaimRow = from ClaimRows in dsClaim.Tables[0].AsEnumerable()
                                  where ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-00" &&
                                         ClaimRows.Field<string>("CHDRNUM") == req.policyNo
                                  select ClaimRows;
                }
                if (null != dsBen)
                {
                    EmpBenefitRow = from BenefitRows in dsBen.Tables[0].AsEnumerable()
                                    where BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-00"
                                    select BenefitRows;
                }
                bool bEmpHasAME = false, bEmpHasDEN = false, bEmpHasIPD = false, bEmpHasLAB = false, bEmpHasOPD = false;
                if (null != EmpRow)
                {


                    var FirstEmpRow = EmpRow.First();

                    bool bHasECard = member.IsExistECard(req.policyNo, FirstEmpRow["MEMBERNO"].ToString());

                    res.hasECard = bHasECard.ToString().ToUpper();
                    // res.employeeInfo = new EmployeeInfo();
                    res.employeeInfo.memberTypeTh = "พนักงาน";
                    res.employeeInfo.memberTypeEn = "Employee";
                    res.employeeInfo.empMemberNo = FirstEmpRow["MEMBERNO"].ToString();
                    res.employeeInfo.empFName = FirstEmpRow["LGIVNAME"].ToString();
                    res.employeeInfo.empLName = FirstEmpRow["LSURNAME"].ToString();
                    res.employeeInfo.empDOB = FirstEmpRow["CLTDOB"].ToString();
                    res.employeeInfo.empSex = FirstEmpRow["CLTSEX"].ToString();
                    res.employeeInfo.empCompanyName = FirstEmpRow["POLLSURNAME"].ToString();
                    res.employeeInfo.empCompanyNameEN = member.GetCompanyNameEN(req.policyNo);//FirstEmpRow["POLLSURNAME"].ToString();
                    string strStartDate = FirstEmpRow["CCDATE04"].ToString();
                    string strEndDate = FirstEmpRow["CRDATE04"].ToString();
                    res.employeeInfo.empNationality = member.GetMemberNationality(req.policyNo, res.employeeInfo.empMemberNo);

                    res.employeeInfo.empStartDate = strStartDate.Substring(6, 2) + "/" + strStartDate.Substring(4, 2) + "/" + strStartDate.Substring(0, 4);
                    res.employeeInfo.empEndDate = strEndDate.Substring(6, 2) + "/" + strEndDate.Substring(4, 2) + "/" + strEndDate.Substring(0, 4);

                    strDOB = res.employeeInfo.empDOB;
                    dtDOB = new DateTime(Convert.ToInt32(strDOB.Substring(6, 4)), Convert.ToInt32(strDOB.Substring(3, 2)), Convert.ToInt32(strDOB.Substring(0, 2)));


                    iYear = DateTime.Today.Year - dtDOB.Year;

                    iMonth = 0;

                    if (DateTime.Now.DayOfYear < dtDOB.DayOfYear)
                    {
                        iYear = iYear - 1;

                        // today still not yet birth day
                        iMonth = (12 - dtDOB.Month) + (DateTime.Now.Month - 1);
                    }
                    else
                    {
                        // today pass the birth day
                        iMonth = DateTime.Now.Month - dtDOB.Month;
                    }

                    res.employeeInfo.empAgeTH = iYear.ToString() + " ปี " + iMonth.ToString() + " เดือน";
                    res.employeeInfo.empAgeEN = iYear.ToString() + " Year " + iMonth.ToString() + " Month";
                    if (null != EmpGUserInfoRow && EmpGUserInfoRow.Count() > 0)
                    {
                        res.employeeInfo.empMobile = EmpGUserInfoRow.First()["mobile_no"].ToString();
                        res.employeeInfo.empEmail = EmpGUserInfoRow.First()["email"].ToString();
                        res.employeeInfo.empLineID = EmpGUserInfoRow.First()["line_id"].ToString();
                    }
                    res.employeeInfo.empCitizenID = FirstEmpRow["SECUITYNO"].ToString();


                    try
                    {
                        tmpRowList = EmpClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.employeeInfo.empMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GTLB") || a["PRODTYP"].ToString().Equals("GTLX")).First();
                        if (tmpRow != null)
                        {
                            decimal dAmount = Convert.ToDecimal(tmpRow["SUMINSU"].ToString());

                            if (dAmount > 1000)
                            {
                                res.employeeInfo.empBenefitInfo.empGTLDetlTH = tmpRow["prd_th_name"].ToString();
                                res.employeeInfo.empBenefitInfo.empGTLDetlEN = tmpRow["prd_en_name"].ToString();
                                res.employeeInfo.empBenefitInfo.empGTL = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                            }
                        }
                    }
                    catch (Exception) { }

                    try
                    {
                        tmpRowList = EmpClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.employeeInfo.empMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GAD1") || a["PRODTYP"].ToString().Equals("GAD2") || a["PRODTYP"].ToString().Equals("GAD3") || a["PRODTYP"].ToString().Equals("GAD4")).First();

                        res.employeeInfo.empBenefitInfo.empGADDetlTH = tmpRow["prd_th_name"].ToString();
                        res.employeeInfo.empBenefitInfo.empGADDetlEN = tmpRow["prd_en_name"].ToString();
                        res.employeeInfo.empBenefitInfo.empGAD = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                    }
                    catch (Exception)
                    { }


                    try
                    {
                        tmpRowList = EmpClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.employeeInfo.empMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GDA1") || a["PRODTYP"].ToString().Equals("GDA2") || a["PRODTYP"].ToString().Equals("GDA3") || a["PRODTYP"].ToString().Equals("GDA4")).First();

                        res.employeeInfo.empBenefitInfo.empGDADetlTH = tmpRow["prd_th_name"].ToString();
                        res.employeeInfo.empBenefitInfo.empGDADetlEN = tmpRow["prd_en_name"].ToString();
                        res.employeeInfo.empBenefitInfo.empGDA = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        tmpRowList = EmpClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.employeeInfo.empMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GRC1") || a["PRODTYP"].ToString().Equals("GRC2") || a["PRODTYP"].ToString().Equals("GRC3") || a["PRODTYP"].ToString().Equals("GRC4")).First();

                        res.employeeInfo.empBenefitInfo.empGRCDetlTH = tmpRow["prd_th_name"].ToString();
                        res.employeeInfo.empBenefitInfo.empGRCDetlEN = tmpRow["prd_en_name"].ToString();
                        res.employeeInfo.empBenefitInfo.empGRC = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        tmpRowList = EmpClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.employeeInfo.empMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GPTD")).First();

                        res.employeeInfo.empBenefitInfo.empGPTDDetlTH = tmpRow["prd_th_name"].ToString();
                        res.employeeInfo.empBenefitInfo.empGPTDDetlEN = tmpRow["prd_en_name"].ToString();
                        res.employeeInfo.empBenefitInfo.empGPTD = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        tmpRowList = EmpClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.employeeInfo.empMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GCI1")).First();

                        res.employeeInfo.empBenefitInfo.empGCIDetlTH = tmpRow["prd_th_name"].ToString();
                        res.employeeInfo.empBenefitInfo.empGCIDetlEN = tmpRow["prd_en_name"].ToString();
                        res.employeeInfo.empBenefitInfo.empGCI = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                    }
                    catch (Exception)
                    { }


                    /************************ IPD ***************************/
                    try
                    {
                        // IT_REQUEST_1A20181130_1A20200087 :: Edit by Tirachit B. @31/01/2020
                        //var allowedBen = new[] { "BH11", "BH12", "BH13", "BH14", "BH15", "BH16", "BH17", "BH18", "BH19", "BH20", "BH21", "BH22", "BH23", "BH24" };
                        var allowedBen = new[] { "BH11", "BH12", "BH13", "BH14", "BH15", "BH16", "BH17", "BH18", "BH19", "BH20", "BH21", "BH22", "BH23", "BH24", "BH26", "BH28", "BH25", "BH29" };
                        tmpRowList = EmpBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                        if (tmpRowList != null)
                        {

                            foreach (DataRow dr in tmpRowList)
                            {

                                SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bEmpHasAME, ref bEmpHasDEN, ref bEmpHasIPD, ref bEmpHasLAB, ref bEmpHasOPD);

                                EmpIPDInfo ipd = new EmpIPDInfo();
                                if (dr["BENCDE"].ToString().Equals("BH11") || dr["BENCDE"].ToString().Equals("BH12"))
                                {
                                    int iMaxDay = Convert.ToInt32(dr["MAX_DAY"].ToString());
                                    decimal dPervis = Convert.ToDecimal(dr["PERVIS"].ToString().Replace(",", ""));

                                    ipd.empBHCode = dr["BENCDE"].ToString();
                                    if (iMaxDay == 0)
                                    {

                                        ipd.empIPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                        ipd.empIPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";
                                    }
                                    else if (dPervis > 0 && iMaxDay > 0)
                                    {
                                        ipd.empIPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท สูงสุด " + dr["MAX_DAY"].ToString() + " วันต่อครั้ง";
                                        ipd.empIPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht, Max. " + dr["MAX_DAY"].ToString() + " Days/visit";
                                    }
                                }
                                else if (dr["BENCDE"].ToString().Equals("BH29"))
                                {
                                    ipd.empBHCode = dr["BENCDE"].ToString();
                                    ipd.empIPDDetlTH = "ค่ารักษาพยาบาลผู้ป่วยนอกต่อเนื่องจากผู้ป่วยใน หลังออกจากโรงพยาบาล " + SetNumberNoDecimalPoint(dr["MAX_DAY"].ToString()) + " วัน";
                                    ipd.empIPDDetlEN = "Including OPD follow up within " + SetNumberNoDecimalPoint(dr["MAX_DAY"].ToString()) + " days after discharging from hospital";
                                }
                                else
                                {
                                    ipd.empBHCode = dr["BENCDE"].ToString();
                                    ipd.empIPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                    ipd.empIPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";
                                }

                                //sunisa add on 04/02/2020
                                //ipd.empIPDCoPay = dr["COPAY"].ToString();
                                //ipd.empIPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                {
                                    ipd.empIPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    ipd.empIPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                }
                                else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                {
                                    ipd.empIPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    ipd.empIPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }
                                else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                {
                                    ipd.empIPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    ipd.empIPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }

                                res.employeeInfo.empBenefitInfo.empIPDList.Add(ipd);
                            }
                        }
                    }
                    catch (Exception) { }

                    res.employeeInfo.empBenefitInfo.empIPDList = res.employeeInfo.empBenefitInfo.empIPDList.OrderBy(a => a.empBHCode).ToList();
                    /************************ OPD ***************************/
                    try
                    {
                        var allowedBen = new[] { "BH01", "BH04", "BOL1", "BOL2", "BOL3" };
                        tmpRowList = EmpBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                        if (tmpRowList != null)
                        {

                            foreach (DataRow dr in tmpRowList)
                            {
                                SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bEmpHasAME, ref bEmpHasDEN, ref bEmpHasIPD, ref bEmpHasLAB, ref bEmpHasOPD);
                                if (dr["BENCDE"].ToString().Equals("BH01"))
                                {
                                    EmpOPDInfo opd = new EmpOPDInfo();
                                    int iMaxDay = Convert.ToInt32(dr["MAX_DAY"].ToString());
                                    decimal dPervis = Convert.ToDecimal(dr["PERVIS"].ToString().Replace(",", ""));
                                    decimal dPerYear = Convert.ToInt32(dr["PERYEAR"].ToString().Replace(",", ""));

                                    opd.empBHCode = dr["BENCDE"].ToString();
                                    if (dPerYear != 0 && iMaxDay > 0)
                                    {

                                        opd.empOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาท สูงสุด " + dr["MAX_DAY"].ToString() + " ครั้งต่อปี";
                                        opd.empOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht, Max. " + dr["MAX_DAY"].ToString() + " visits/year";

                                    }
                                    else if (iMaxDay == 0 && (dPervis != 0 && dPerYear != 0))//sunisa add on 11/03/2020
                                    {
                                        opd.empOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี" + " ไม่เกิน " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง";
                                        opd.empOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year" + " Max " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/visit";
                                    }
                                    else if (dPerYear != 0 && iMaxDay == 0)
                                    {
                                        opd.empOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี";
                                        opd.empOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year";
                                    }
                                    else if (dPervis != 0 && iMaxDay > 0)
                                    {

                                        opd.empOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง สูงสุด " + dr["MAX_DAY"].ToString() + " ครั้งต่อปี";
                                        opd.empOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/visit, max.  " + dr["MAX_DAY"].ToString() + " visits/year";
                                    }
                                    else if (dPervis != 0 && iMaxDay == 0)
                                    {
                                        opd.empOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง";
                                        opd.empOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/visit";
                                    }


                                    //sunisa add on 04/02/2020
                                    //opd.empOPDCoPay = dr["COPAY"].ToString();
                                    //opd.empOPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        opd.empOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        opd.empOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        opd.empOPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        opd.empOPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        opd.empOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        opd.empOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }


                                    res.employeeInfo.empBenefitInfo.empOPDList.Add(opd);
                                }
                                else if (dr["BENCDE"].ToString().Equals("BH04"))
                                {
                                    EmpOPDInfo opd = new EmpOPDInfo();
                                    int iMaxDay = Convert.ToInt32(dr["MAX_DAY"].ToString());
                                    decimal dPerYear = Convert.ToInt32(dr["PERYEAR"].ToString().Replace(",", ""));
                                    opd.empBHCode = dr["BENCDE"].ToString();

                                    if (dPerYear != 0 && iMaxDay == 0)
                                    {

                                        opd.empOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาท/ปี";
                                        opd.empOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year";

                                        //sunisa add on 04/02/2020
                                        //opd.empOPDCoPay = dr["COPAY"].ToString();
                                        //opd.empOPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                        if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                        {
                                            opd.empOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            opd.empOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        }
                                        else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                        {
                                            opd.empOPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            opd.empOPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        }
                                        else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                        {
                                            opd.empOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            opd.empOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        }

                                        res.employeeInfo.empBenefitInfo.empOPDList.Add(opd);
                                    }

                                }

                                else
                                {
                                    // "BOL1", "BOL2", "BOL3"
                                    EmpOPDInfo opd = new EmpOPDInfo();
                                    opd.empBHCode = dr["BENCDE"].ToString();
                                    opd.empOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาท";
                                    opd.empOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht";

                                    //sunisa add on 04/02/2020
                                    //opd.empOPDCoPay = dr["COPAY"].ToString();
                                    //opd.empOPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        opd.empOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        opd.empOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        opd.empOPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        opd.empOPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        opd.empOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        opd.empOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.employeeInfo.empBenefitInfo.empOPDList.Add(opd);
                                }
                            }
                        }
                    }
                    catch (Exception) { }

                    res.employeeInfo.empBenefitInfo.empOPDList = res.employeeInfo.empBenefitInfo.empOPDList.OrderBy(a => a.empBHCode).ToList();
                    /************************ Dental ***************************/
                    try
                    {
                        var allowedBen = new[] { "BDT2", "BDT3" };
                        tmpRowList = EmpBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                        if (tmpRowList != null)
                        {

                            foreach (DataRow dr in tmpRowList)
                            {
                                SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bEmpHasAME, ref bEmpHasDEN, ref bEmpHasIPD, ref bEmpHasLAB, ref bEmpHasOPD);


                                if (dr["BENCDE"].ToString().Equals("BDT2"))
                                {
                                    EmpDentalInfo dental = new EmpDentalInfo();
                                    dental.empBHCode = dr["BENCDE"].ToString();
                                    dental.empDentalDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  สูงสุด " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี " + dr["PERVIS"].ToString() + " บาทต่อครั้ง";
                                    dental.empDentalDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  Max. " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year " + dr["PERVIS"].ToString() + " Baht/visit";

                                    //sunisa add on 04/02/2020
                                    //dental.empDentalCoPay = dr["COPAY"].ToString();
                                    //dental.empDentalDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        dental.empDentalCoPay += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        dental.empDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        dental.empDentalCoPay += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        dental.empDentalDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        dental.empDentalCoPay += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        dental.empDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.employeeInfo.empBenefitInfo.empDentalList.Add(dental);
                                }
                                else if (dr["BENCDE"].ToString().Equals("BDT3"))
                                {
                                    EmpDentalInfo dental = new EmpDentalInfo();
                                    dental.empBHCode = dr["BENCDE"].ToString();
                                    dental.empDentalDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี ";
                                    dental.empDentalDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year ";

                                    //sunisa add on 04/02/2020
                                    //dental.empDentalCoPay = dr["COPAY"].ToString();
                                    //dental.empDentalDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        dental.empDentalCoPay += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        dental.empDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        dental.empDentalCoPay += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        dental.empDentalDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        dental.empDentalCoPay += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        dental.empDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.employeeInfo.empBenefitInfo.empDentalList.Add(dental);

                                }

                            }
                        }
                    }
                    catch (Exception) { }
                }
                res.employeeInfo.empBenefitInfo.empDentalList = res.employeeInfo.empBenefitInfo.empDentalList.OrderBy(a => a.empBHCode).ToList();
                /************************ Accident ***************************/
                try
                {
                    var allowedBen = new[] { "BA05" };
                    tmpRowList = EmpBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                    if (tmpRowList != null)
                    {

                        foreach (DataRow dr in tmpRowList)
                        {
                            SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bEmpHasAME, ref bEmpHasDEN, ref bEmpHasIPD, ref bEmpHasLAB, ref bEmpHasOPD);
                            if (dr["BENCDE"].ToString().Equals("BA05"))
                            {
                                EmpAccidentInfo accident = new EmpAccidentInfo();
                                accident.empBHCode = dr["BENCDE"].ToString();
                                accident.empAccidentDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง";
                                accident.empAccidentDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/accident";

                                //sunisa add on 04/02/2020
                                //accident.empAccidentCoPay = dr["COPAY"].ToString();
                                //accident.empAccidentDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                {
                                    accident.empAccidentCoPay += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    accident.empAccidentDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                }
                                else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                {
                                    accident.empAccidentCoPay += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    accident.empAccidentDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }
                                else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                {
                                    accident.empAccidentCoPay += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    accident.empAccidentDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }

                                res.employeeInfo.empBenefitInfo.empAccidentList.Add(accident);
                            }


                        }
                    }
                }
                catch (Exception) { }
                res.employeeInfo.empBenefitInfo.empAccidentList = res.employeeInfo.empBenefitInfo.empAccidentList.OrderBy(a => a.empBHCode).ToList();

                /************************ Critical ***************************/
                try
                {
                    var allowedBen = new[] { "BCI1" };
                    tmpRowList = EmpBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                    if (tmpRowList != null)
                    {

                        foreach (DataRow dr in tmpRowList)
                        {

                            SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bEmpHasAME, ref bEmpHasDEN, ref bEmpHasIPD, ref bEmpHasLAB, ref bEmpHasOPD);
                            if (dr["BENCDE"].ToString().Equals("BCI1"))
                            {
                                EmpCriticalInfo critical = new EmpCriticalInfo();
                                critical.empBHCode = dr["BENCDE"].ToString();
                                critical.empCriticalDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                critical.empCriticalDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                //sunisa add on 04/02/2020
                                //critical.empCriticalCoPay = dr["COPAY"].ToString();
                                //critical.empCriticalDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                {
                                    critical.empCriticalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    critical.empCriticalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                }
                                else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                {
                                    critical.empCriticalDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    critical.empCriticalDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }
                                else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                {
                                    critical.empCriticalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    critical.empCriticalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }

                                res.employeeInfo.empBenefitInfo.empCriticalList.Add(critical);
                            }


                        }
                    }
                }
                catch (Exception) { }
                res.employeeInfo.empBenefitInfo.empCriticalList = res.employeeInfo.empBenefitInfo.empCriticalList.OrderBy(a => a.empBHCode).ToList();
                /************************ Maternity ***************************/
                try
                {
                    //var allowedBen = new[] { "BM01", "BM02", "BM03" };
                    var allowedBen = new[] { "BM01", "BM02", "BM03", "BM04", "BM05", "BM06" };
                    tmpRowList = EmpBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                    if (tmpRowList != null)
                    {
                        foreach (DataRow dr in tmpRowList)
                        {
                            SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bEmpHasAME, ref bEmpHasDEN, ref bEmpHasIPD, ref bEmpHasLAB, ref bEmpHasOPD);
                            if (dr["BENCDE"].ToString().Equals("BM01"))
                            {
                                EmpMaternityInfo maternity = new EmpMaternityInfo();
                                maternity.empBHCode = dr["BENCDE"].ToString();
                                maternity.empMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                maternity.empMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                //sunisa add on 04/02/2020
                                //maternity.empMaternityCoPay = dr["COPAY"].ToString();
                                //maternity.empMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                }
                                else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    maternity.empMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }
                                else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }

                                res.employeeInfo.empBenefitInfo.empMaternityList.Add(maternity);
                            }
                            else if (dr["BENCDE"].ToString().Equals("BM02"))
                            {
                                EmpMaternityInfo maternity = new EmpMaternityInfo();
                                maternity.empBHCode = dr["BENCDE"].ToString();
                                maternity.empMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                maternity.empMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                //sunisa add on 04/02/2020
                                //maternity.empMaternityCoPay = dr["COPAY"].ToString();
                                //maternity.empMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                }
                                else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    maternity.empMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }
                                else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }

                                res.employeeInfo.empBenefitInfo.empMaternityList.Add(maternity);
                            }
                            else if (dr["BENCDE"].ToString().Equals("BM03"))
                            {
                                EmpMaternityInfo maternity = new EmpMaternityInfo();
                                maternity.empBHCode = dr["BENCDE"].ToString();
                                maternity.empMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                maternity.empMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                //sunisa add on 04/02/2020
                                //maternity.empMaternityCoPay = dr["COPAY"].ToString();
                                //maternity.empMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                }
                                else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    maternity.empMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }
                                else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }

                                res.employeeInfo.empBenefitInfo.empMaternityList.Add(maternity);
                            }
                            else if (dr["BENCDE"].ToString().Equals("BM04") || dr["BENCDE"].ToString().Equals("BM05") || dr["BENCDE"].ToString().Equals("BM06"))
                            {
                                EmpMaternityInfo maternity = new EmpMaternityInfo();
                                maternity.empBHCode = dr["BENCDE"].ToString();
                                maternity.empMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                maternity.empMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                //sunisa add on 04/02/2020
                                //maternity.empMaternityCoPay = dr["COPAY"].ToString();
                                //maternity.empMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                }
                                else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    maternity.empMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }
                                else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                {
                                    maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }

                                res.employeeInfo.empBenefitInfo.empMaternityList.Add(maternity);
                            }

                        }
                    }
                }
                catch (Exception) { }
                res.employeeInfo.empBenefitInfo.empMaternityList = res.employeeInfo.empBenefitInfo.empMaternityList.OrderBy(a => a.empBHCode).ToList();

                // get claim for employee
                int iAllNotify = 0, iAMENotify = 0, iDentalNotify = 0, iIPDNotify = 0, iLABNotify = 0, iOPDNotify = 0;

                GetClaimInformation(res.employeeInfo.empMemberNo, req.policyNo, res.employeeInfo.empStartDate, res.employeeInfo.empEndDate, ref iAMENotify, ref iDentalNotify, ref iIPDNotify, ref iLABNotify, ref iOPDNotify);
                res.employeeInfo.empClaimAMENotify = iAMENotify.ToString();
                res.employeeInfo.empClaimDentalNotify = iDentalNotify.ToString();
                res.employeeInfo.empClaimIPDNotify = iIPDNotify.ToString();
                res.employeeInfo.empClaimLabsNotify = iLABNotify.ToString();
                res.employeeInfo.empClaimOPDNotify = iOPDNotify.ToString();
                res.employeeInfo.empHasAME = bEmpHasAME.ToString().ToUpper();
                res.employeeInfo.empHasDEN = bEmpHasDEN.ToString().ToUpper();
                res.employeeInfo.empHasIPD = bEmpHasIPD.ToString().ToUpper();
                res.employeeInfo.empHasLAB = bEmpHasLAB.ToString().ToUpper();
                res.employeeInfo.empHasOPD = bEmpHasOPD.ToString().ToUpper();

                iAllNotify += iAMENotify + iDentalNotify + iIPDNotify + iLABNotify + iOPDNotify;

                EnumerableRowCollection<DataRow> SPRow = null;
                EnumerableRowCollection<DataRow> SPGuserInfoRow = null;
                EnumerableRowCollection<DataRow> SPClaimRow = null;
                EnumerableRowCollection<DataRow> SPBenefitRow = null;

                bool bSpuHasAME = false, bSpuHasDEN = false, bSpuHasIPD = false, bSpuHasLAB = false, bSpuHasOPD = false;
                if (null != dsMemberDetail)
                {
                    SPRow = from SPRows in dsMemberDetail.Tables[0].AsEnumerable()
                            where SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-01" ||
                            SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-30" ||
                            SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-31" ||
                            SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-32" ||
                            SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-33" ||
                            SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-34" ||
                            SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-35" ||
                            SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-36" ||
                            SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-37" ||
                            SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-38" ||
                            SPRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-39"
                            select SPRows;
                }
                if (null != dsGUserInfo)
                {
                    SPGuserInfoRow = from SPGuserInfoRows in dsGUserInfo.Tables[0].AsEnumerable()
                                     where SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-01" ||
                                     SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-30" ||
                                     SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-31" ||
                                     SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-32" ||
                                     SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-33" ||
                                     SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-34" ||
                                     SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-35" ||
                                     SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-36" ||
                                     SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-37" ||
                                     SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-38" ||
                                     SPGuserInfoRows.Field<string>("usernum").Trim() == req.memberNo.Substring(0, 5) + "-39"
                                     select SPGuserInfoRows;
                }
                if (null != dsClaim)
                {

                    SPClaimRow = from ClaimRows in dsClaim.Tables[0].AsEnumerable()
                                 where
                                 (
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-01" ||
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-30" ||
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-31" ||
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-32" ||
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-33" ||
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-34" ||
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-35" ||
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-36" ||
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-37" ||
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-38" ||
                                   ClaimRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-39"

                                 )
                                 &&
                                        ClaimRows.Field<string>("CHDRNUM") == req.policyNo
                                 select ClaimRows;
                }
                if (null != dsBen)
                {
                    SPBenefitRow = from BenefitRows in dsBen.Tables[0].AsEnumerable()
                                   where BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-01" ||
                                         BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-30" ||
                                         BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-31" ||
                                         BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-32" ||
                                         BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-33" ||
                                         BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-34" ||
                                         BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-35" ||
                                         BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-36" ||
                                         BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-37" ||
                                         BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-38" ||
                                         BenefitRows.Field<string>("MEMBERNO") == req.memberNo.Substring(0, 5) + "-39"
                                   select BenefitRows;
                }
                if (null != SPRow && SPRow.Count() != 0)
                {
                    var FirstSpRow = SPRow.First();
                    res.spouseInfo.memberTypeTh = "คู่สมรส";
                    res.spouseInfo.memberTypeEn = "Spouse";
                    res.spouseInfo.spuMemberNo = FirstSpRow["MEMBERNO"].ToString();
                    res.spouseInfo.spuFName = FirstSpRow["LGIVNAME"].ToString();
                    res.spouseInfo.spuLName = FirstSpRow["LSURNAME"].ToString();
                    res.spouseInfo.spuDOB = FirstSpRow["CLTDOB"].ToString();
                    res.spouseInfo.spuSex = FirstSpRow["CLTSEX"].ToString();
                    res.spouseInfo.spuNationality = member.GetMemberNationality(req.policyNo, res.spouseInfo.spuMemberNo);

                    res.spouseInfo.spuCompanyName = FirstSpRow["POLLSURNAME"].ToString();
                    res.spouseInfo.spuCompanyNameEN = member.GetCompanyNameEN(req.policyNo);
                    string strStartDate = FirstSpRow["CCDATE04"].ToString();
                    string strEndDate = FirstSpRow["CRDATE04"].ToString();


                    res.spouseInfo.spuStartDate = strStartDate.Substring(6, 2) + "/" + strStartDate.Substring(4, 2) + "/" + strStartDate.Substring(0, 4);
                    res.spouseInfo.spuEndDate = strEndDate.Substring(6, 2) + "/" + strEndDate.Substring(4, 2) + "/" + strEndDate.Substring(0, 4);

                    strDOB = res.spouseInfo.spuDOB;
                    dtDOB = new DateTime(Convert.ToInt32(strDOB.Substring(6, 4)), Convert.ToInt32(strDOB.Substring(3, 2)), Convert.ToInt32(strDOB.Substring(0, 2)));


                    iYear = DateTime.Today.Year - dtDOB.Year;

                    iMonth = 0;

                    if (DateTime.Now.DayOfYear < dtDOB.DayOfYear)
                    {
                        iYear = iYear - 1;

                        // today still not yet birth day
                        iMonth = (12 - dtDOB.Month) + (DateTime.Now.Month - 1);
                    }
                    else
                    {
                        // today pass the birth day
                        iMonth = DateTime.Now.Month - dtDOB.Month;
                    }

                    res.spouseInfo.spuAgeTH = iYear.ToString() + " ปี " + iMonth.ToString() + " เดือน";
                    res.spouseInfo.spuAgeEN = iYear.ToString() + " Year " + iMonth.ToString() + " Month";

                    if (null != SPGuserInfoRow && SPGuserInfoRow.Count() > 0)
                    {
                        res.spouseInfo.spuMobile = SPGuserInfoRow.First()["mobile_no"].ToString();
                        res.spouseInfo.spuEmail = SPGuserInfoRow.First()["email"].ToString();
                        res.spouseInfo.spuLineID = SPGuserInfoRow.First()["line_id"].ToString();
                    }
                    res.spouseInfo.spuCitizenID = FirstSpRow["SECUITYNO"].ToString();



                    try
                    {
                        tmpRowList = SPClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.spouseInfo.spuMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GTLB") || a["PRODTYP"].ToString().Equals("GTLX")).First();

                        if (tmpRow != null)
                        {
                            decimal dAmount = Convert.ToDecimal(tmpRow["SUMINSU"].ToString());

                            if (dAmount > 1000)
                            {
                                res.spouseInfo.spuBenefitInfo.spuGTLDetlTH = tmpRow["prd_th_name"].ToString();
                                res.spouseInfo.spuBenefitInfo.spuGTLDetlEN = tmpRow["prd_en_name"].ToString();
                                res.spouseInfo.spuBenefitInfo.spuGTL = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                            }
                        }
                    }
                    catch (Exception) { }
                    try
                    {
                        tmpRowList = SPClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.spouseInfo.spuMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GAD1") || a["PRODTYP"].ToString().Equals("GAD2") || a["PRODTYP"].ToString().Equals("GAD3") || a["PRODTYP"].ToString().Equals("GAD4")).First();
                        res.spouseInfo.spuBenefitInfo.spuGADDetlTH = tmpRow["prd_th_name"].ToString();
                        res.spouseInfo.spuBenefitInfo.spuGADDetlEN = tmpRow["prd_en_name"].ToString();
                        res.spouseInfo.spuBenefitInfo.spuGAD = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                    }
                    catch (Exception) { }
                    try
                    {
                        tmpRowList = SPClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.spouseInfo.spuMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GDA1") || a["PRODTYP"].ToString().Equals("GDA2") || a["PRODTYP"].ToString().Equals("GDA3") || a["PRODTYP"].ToString().Equals("GDA4")).First();
                        res.spouseInfo.spuBenefitInfo.spuGDADetlTH = tmpRow["prd_th_name"].ToString();
                        res.spouseInfo.spuBenefitInfo.spuGDADetlEN = tmpRow["prd_en_name"].ToString();
                        res.spouseInfo.spuBenefitInfo.spuGDA = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                    }
                    catch (Exception) { }

                    try
                    {
                        tmpRowList = SPClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.spouseInfo.spuMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GRC1") || a["PRODTYP"].ToString().Equals("GRC2") || a["PRODTYP"].ToString().Equals("GRC3") || a["PRODTYP"].ToString().Equals("GRC4")).First();
                        res.spouseInfo.spuBenefitInfo.spuGRCDetlTH = tmpRow["prd_th_name"].ToString();
                        res.spouseInfo.spuBenefitInfo.spuGRCDetlEN = tmpRow["prd_en_name"].ToString();
                        res.spouseInfo.spuBenefitInfo.spuGRC = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                    }
                    catch (Exception) { }
                    try
                    {
                        tmpRowList = SPClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.spouseInfo.spuMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GPTD")).First();
                        res.spouseInfo.spuBenefitInfo.spuGPTDDetlTH = tmpRow["prd_th_name"].ToString();
                        res.spouseInfo.spuBenefitInfo.spuGPTDDetlEN = tmpRow["prd_en_name"].ToString();
                        res.spouseInfo.spuBenefitInfo.spuGPTD = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                    }
                    catch (Exception) { }

                    try
                    {
                        tmpRowList = SPClaimRow.Where(a => a["MEMBERNO"].ToString().Equals(res.spouseInfo.spuMemberNo)).ToList();
                        tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GCI1")).First();
                        res.spouseInfo.spuBenefitInfo.spuGCIDetlTH = tmpRow["prd_th_name"].ToString();
                        res.spouseInfo.spuBenefitInfo.spuGCIDetlEN = tmpRow["prd_en_name"].ToString();
                        res.spouseInfo.spuBenefitInfo.spuGCI = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                    }
                    catch (Exception) { }

                    /************************ IPD ***************************/
                    try
                    {
                        // IT_REQUEST_1A20181130_1A20200087 :: Edit by Tirachit B. @31/01/2020
                        //var allowedBen = new[] { "BH11", "BH12", "BH13", "BH14", "BH15", "BH16", "BH17", "BH18", "BH19", "BH20", "BH21", "BH22", "BH23", "BH24" };
                        var allowedBen = new[] { "BH11", "BH12", "BH13", "BH14", "BH15", "BH16", "BH17", "BH18", "BH19", "BH20", "BH21", "BH22", "BH23", "BH24", "BH26", "BH28", "BH25", "BH29" };
                        tmpRowList = SPBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                        if (tmpRowList != null)
                        {
                            foreach (DataRow dr in tmpRowList)
                            {
                                SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bSpuHasAME, ref bSpuHasDEN, ref bSpuHasIPD, ref bSpuHasLAB, ref bSpuHasOPD);

                                SpuIPDInfo ipd = new SpuIPDInfo();
                                if (dr["BENCDE"].ToString().Equals("BH11") || dr["BENCDE"].ToString().Equals("BH12"))
                                {
                                    int iMaxDay = Convert.ToInt32(dr["MAX_DAY"].ToString());
                                    decimal dPervis = Convert.ToDecimal(dr["PERVIS"].ToString().Replace(",", ""));
                                    if (iMaxDay == 0)
                                    {
                                        ipd.spuBHCode = dr["BENCDE"].ToString();
                                        ipd.spuIPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                        ipd.spuIPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";
                                    }
                                    else if (dPervis > 0 && iMaxDay > 0)
                                    {
                                        ipd.spuBHCode = dr["BENCDE"].ToString();
                                        ipd.spuIPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท สูงสุด " + dr["MAX_DAY"].ToString() + " วันต่อครั้ง";
                                        ipd.spuIPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht, Max. " + dr["MAX_DAY"].ToString() + " Days/visit";
                                    }
                                }
                                else if (dr["BENCDE"].ToString().Equals("BH29"))
                                {
                                    ipd.spuBHCode = dr["BENCDE"].ToString();
                                    ipd.spuIPDDetlTH = "ค่ารักษาพยาบาลผู้ป่วยนอกต่อเนื่องจากผู้ป่วยใน หลังออกจากโรงพยาบาล " + SetNumberNoDecimalPoint(dr["MAX_DAY"].ToString()) + " วัน";
                                    ipd.spuIPDDetlEN = "Including OPD follow up within " + SetNumberNoDecimalPoint(dr["MAX_DAY"].ToString()) + " days after discharging from hospital";
                                }
                                else
                                {
                                    ipd.spuBHCode = dr["BENCDE"].ToString();
                                    ipd.spuIPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                    ipd.spuIPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";
                                }

                                //sunisa add on 04/02/2020
                                //ipd.spuIPDCoPay = dr["COPAY"].ToString();
                                //ipd.spuIPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                {
                                    ipd.spuIPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    ipd.spuIPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                }
                                else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                {
                                    ipd.spuIPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    ipd.spuIPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }
                                else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                {
                                    ipd.spuIPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    ipd.spuIPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                }

                                res.spouseInfo.spuBenefitInfo.spuIPDList.Add(ipd);
                            }
                        }
                    }
                    catch (Exception) { }

                    res.spouseInfo.spuBenefitInfo.spuIPDList = res.spouseInfo.spuBenefitInfo.spuIPDList.OrderBy(a => a.spuBHCode).ToList();

                    /************************ OPD ***************************/
                    try
                    {
                        var allowedBen = new[] { "BH01", "BH04", "BOL1", "BOL2", "BOL3" };
                        tmpRowList = SPBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                        if (tmpRowList != null)
                        {
                            foreach (DataRow dr in tmpRowList)
                            {
                                SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bSpuHasAME, ref bSpuHasDEN, ref bSpuHasIPD, ref bSpuHasLAB, ref bSpuHasOPD);
                                if (dr["BENCDE"].ToString().Equals("BH01"))
                                {
                                    SpuOPDInfo opd = new SpuOPDInfo();
                                    int iMaxDay = Convert.ToInt32(dr["MAX_DAY"].ToString());
                                    decimal dPervis = Convert.ToDecimal(dr["PERVIS"].ToString().Replace(",", ""));
                                    decimal dPerYear = Convert.ToInt32(dr["PERYEAR"].ToString().Replace(",", ""));
                                    if (dPerYear != 0 && iMaxDay > 0)
                                    {
                                        opd.spuBHCode = dr["BENCDE"].ToString();
                                        opd.spuOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาท สูงสุด " + dr["MAX_DAY"].ToString() + " ครั้งต่อปี";
                                        opd.spuOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht, Max. " + dr["MAX_DAY"].ToString() + " visits/year";

                                    }
                                    else if (iMaxDay == 0 && (dPervis != 0 && dPerYear != 0))//sunisa add on 24/04/2020
                                    {
                                        opd.spuOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี" + " ไม่เกิน " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง";
                                        opd.spuOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year" + " Max " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/visit";
                                    }
                                    else if (dPerYear != 0 && iMaxDay == 0)
                                    {
                                        opd.spuBHCode = dr["BENCDE"].ToString();
                                        opd.spuOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี";
                                        opd.spuOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year";
                                    }
                                    else if (dPervis != 0 && iMaxDay > 0)
                                    {
                                        opd.spuBHCode = dr["BENCDE"].ToString();
                                        opd.spuOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง สูงสุด " + dr["MAX_DAY"].ToString() + " ครั้งต่อปี";
                                        opd.spuOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/visit, max.  " + dr["MAX_DAY"].ToString() + " visits/year";
                                    }
                                    else if (dPervis != 0 && iMaxDay == 0)
                                    {
                                        opd.spuBHCode = dr["BENCDE"].ToString();
                                        opd.spuOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง";
                                        opd.spuOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/visit";
                                    }

                                    //sunisa add on 04/02/2020
                                    //opd.spuOPDCoPay = dr["COPAY"].ToString();
                                    //opd.spuOPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        opd.spuOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        opd.spuOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        opd.spuOPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        opd.spuOPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        opd.spuOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        opd.spuOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.spouseInfo.spuBenefitInfo.spuOPDList.Add(opd);
                                }
                                else if (dr["BENCDE"].ToString().Equals("BH04"))
                                {
                                    SpuOPDInfo opd = new SpuOPDInfo();
                                    int iMaxDay = Convert.ToInt32(dr["MAX_DAY"].ToString());
                                    decimal dPerYear = Convert.ToInt32(dr["PERYEAR"].ToString().Replace(",", ""));
                                    opd.spuBHCode = dr["BENCDE"].ToString();

                                    if (dPerYear != 0 && iMaxDay == 0)
                                    {
                                        opd.spuOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาท/ปี";
                                        opd.spuOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year";

                                        //sunisa add on 04/02/2020
                                        //opd.spuOPDCoPay = dr["COPAY"].ToString();
                                        //opd.spuOPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                        if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                        {
                                            opd.spuOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            opd.spuOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        }
                                        else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                        {
                                            opd.spuOPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            opd.spuOPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        }
                                        else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                        {
                                            opd.spuOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            opd.spuOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        }

                                        res.spouseInfo.spuBenefitInfo.spuOPDList.Add(opd);
                                    }

                                }

                                else
                                {
                                    // "BOL1", "BOL2", "BOL3"
                                    SpuOPDInfo opd = new SpuOPDInfo();
                                    opd.spuBHCode = dr["BENCDE"].ToString();
                                    opd.spuOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":   " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาท";
                                    opd.spuOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":   " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht";

                                    //sunisa add on 04/02/2020
                                    //opd.spuOPDCoPay = dr["COPAY"].ToString();
                                    //opd.spuOPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        opd.spuOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        opd.spuOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        opd.spuOPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        opd.spuOPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        opd.spuOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        opd.spuOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.spouseInfo.spuBenefitInfo.spuOPDList.Add(opd);
                                }
                            }
                        }
                    }
                    catch (Exception) { }
                    res.spouseInfo.spuBenefitInfo.spuOPDList = res.spouseInfo.spuBenefitInfo.spuOPDList.OrderBy(a => a.spuBHCode).ToList();
                    /************************ Dental ***************************/
                    try
                    {
                        var allowedBen = new[] { "BDT2", "BDT3" };
                        tmpRowList = SPBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                        if (tmpRowList != null)
                        {
                            foreach (DataRow dr in tmpRowList)
                            {
                                SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bSpuHasAME, ref bSpuHasDEN, ref bSpuHasIPD, ref bSpuHasLAB, ref bSpuHasOPD);
                                if (dr["BENCDE"].ToString().Equals("BDT2"))
                                {
                                    SpuDentalInfo dental = new SpuDentalInfo();
                                    dental.spuBHCode = dr["BENCDE"].ToString();
                                    dental.spuDentalDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  สูงสุด " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี " + dr["PERVIS"].ToString() + " บาทต่อครั้ง";
                                    dental.spuDentalDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  Max. " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year " + dr["PERVIS"].ToString() + " Baht/visit";

                                    //sunisa add on 04/02/2020
                                    //dental.spuDentalCoPay = dr["COPAY"].ToString();
                                    //dental.spuDentalDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        dental.spuDentalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        dental.spuDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        dental.spuDentalDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        dental.spuDentalDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        dental.spuDentalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        dental.spuDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.spouseInfo.spuBenefitInfo.spuDentalList.Add(dental);
                                }
                                else if (dr["BENCDE"].ToString().Equals("BDT3"))
                                {
                                    SpuDentalInfo dental = new SpuDentalInfo();
                                    dental.spuBHCode = dr["BENCDE"].ToString();
                                    dental.spuDentalDetlTH = dr["BENCTHNAME"].ToString().Trim() + ": " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี ";
                                    dental.spuDentalDetlEN = dr["BENCENNAME"].ToString().Trim() + ": " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year ";

                                    //sunisa add on 04/02/2020
                                    //dental.spuDentalCoPay = dr["COPAY"].ToString();
                                    //dental.spuDentalDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        dental.spuDentalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        dental.spuDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        dental.spuDentalDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        dental.spuDentalDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        dental.spuDentalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        dental.spuDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.spouseInfo.spuBenefitInfo.spuDentalList.Add(dental);

                                }

                            }
                        }
                    }
                    catch (Exception) { }
                    res.spouseInfo.spuBenefitInfo.spuDentalList = res.spouseInfo.spuBenefitInfo.spuDentalList.OrderBy(a => a.spuBHCode).ToList();

                    /************************ Accident ***************************/
                    try
                    {
                        var allowedBen = new[] { "BA05" };
                        tmpRowList = SPBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                        if (tmpRowList != null)
                        {
                            foreach (DataRow dr in tmpRowList)
                            {
                                SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bSpuHasAME, ref bSpuHasDEN, ref bSpuHasIPD, ref bSpuHasLAB, ref bSpuHasOPD);
                                if (dr["BENCDE"].ToString().Equals("BA05"))
                                {
                                    SpuAccidentInfo accident = new SpuAccidentInfo();
                                    accident.spuBHCode = dr["BENCDE"].ToString();
                                    accident.spuAccidentDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง";
                                    accident.spuAccidentDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/accident";

                                    //sunisa add on 04/02/2020
                                    //accident.spuAccidentCoPay = dr["COPAY"].ToString();
                                    //accident.spuAccidentDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        accident.spuAccidentDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        accident.spuAccidentDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        accident.spuAccidentDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        accident.spuAccidentDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        accident.spuAccidentDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        accident.spuAccidentDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.spouseInfo.spuBenefitInfo.spuAccidentList.Add(accident);
                                }


                            }
                        }
                    }
                    catch (Exception) { }
                    res.spouseInfo.spuBenefitInfo.spuDentalList = res.spouseInfo.spuBenefitInfo.spuDentalList.OrderBy(a => a.spuBHCode).ToList();
                    /************************ Critical ***************************/
                    try
                    {
                        var allowedBen = new[] { "BCI1" };
                        tmpRowList = SPBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                        if (tmpRowList != null)
                        {
                            foreach (DataRow dr in tmpRowList)
                            {
                                SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bSpuHasAME, ref bSpuHasDEN, ref bSpuHasIPD, ref bSpuHasLAB, ref bSpuHasOPD);
                                if (dr["BENCDE"].ToString().Equals("BCI1"))
                                {
                                    SpuCriticalInfo critical = new SpuCriticalInfo();
                                    critical.spuBHCode = dr["BENCDE"].ToString();
                                    critical.spuCriticalDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                    critical.spuCriticalDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                    //sunisa add on 04/02/2020
                                    //critical.spuCriticalCoPay = dr["COPAY"].ToString();
                                    //critical.spuCriticalDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        critical.spuCriticalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        critical.spuCriticalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        critical.spuCriticalDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        critical.spuCriticalDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        critical.spuCriticalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        critical.spuCriticalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.spouseInfo.spuBenefitInfo.spuCriticalList.Add(critical);
                                }


                            }
                        }
                    }
                    catch (Exception) { }
                    res.spouseInfo.spuBenefitInfo.spuCriticalList = res.spouseInfo.spuBenefitInfo.spuCriticalList.OrderBy(a => a.spuBHCode).ToList();
                    /************************ Maternity ***************************/
                    try
                    {
                        //var allowedBen = new[] { "BM01", "BM02", "BM03" };
                        var allowedBen = new[] { "BM01", "BM02", "BM03", "BM04", "BM05", "BM06" };
                        tmpRowList = SPBenefitRow.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                        if (tmpRowList != null)
                        {
                            foreach (DataRow dr in tmpRowList)
                            {
                                SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bSpuHasAME, ref bSpuHasDEN, ref bSpuHasIPD, ref bSpuHasLAB, ref bSpuHasOPD);
                                if (dr["BENCDE"].ToString().Equals("BM01"))
                                {
                                    SpuMaternityInfo maternity = new SpuMaternityInfo();
                                    maternity.spuBHCode = dr["BENCDE"].ToString();
                                    maternity.spuMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":   " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                    maternity.spuMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":   " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                    //sunisa add on 04/02/2020
                                    //maternity.spuMaternityCoPay = dr["COPAY"].ToString();
                                    //maternity.spuMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        maternity.spuMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        maternity.spuMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        maternity.spuMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        maternity.spuMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        maternity.spuMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        maternity.spuMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.spouseInfo.spuBenefitInfo.spuMaternityList.Add(maternity);
                                }
                                else if (dr["BENCDE"].ToString().Equals("BM02"))
                                {
                                    SpuMaternityInfo maternity = new SpuMaternityInfo();
                                    maternity.spuBHCode = dr["BENCDE"].ToString();
                                    maternity.spuMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                    maternity.spuMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                    //sunisa add on 04/02/2020
                                    //maternity.spuMaternityCoPay = dr["COPAY"].ToString();
                                    //maternity.spuMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        maternity.spuMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        maternity.spuMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        maternity.spuMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        maternity.spuMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        maternity.spuMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        maternity.spuMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.spouseInfo.spuBenefitInfo.spuMaternityList.Add(maternity);
                                }
                                else if (dr["BENCDE"].ToString().Equals("BM03"))
                                {
                                    SpuMaternityInfo maternity = new SpuMaternityInfo();
                                    maternity.spuBHCode = dr["BENCDE"].ToString();
                                    maternity.spuMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                    maternity.spuMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + "   " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                    //sunisa add on 04/02/2020
                                    //maternity.spuMaternityCoPay = dr["COPAY"].ToString();
                                    //maternity.spuMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        maternity.spuMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        maternity.spuMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        maternity.spuMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        maternity.spuMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        maternity.spuMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        maternity.spuMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.spouseInfo.spuBenefitInfo.spuMaternityList.Add(maternity);
                                }
                                else if (dr["BENCDE"].ToString().Equals("BM04") || dr["BENCDE"].ToString().Equals("BM05") || dr["BENCDE"].ToString().Equals("BM06"))
                                {
                                    EmpMaternityInfo maternity = new EmpMaternityInfo();
                                    maternity.empBHCode = dr["BENCDE"].ToString();
                                    maternity.empMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                    maternity.empMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                    //sunisa add on 04/02/2020
                                    //maternity.spuMaternityCoPay = dr["COPAY"].ToString();
                                    //maternity.spuMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                    if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                    {
                                        maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                    }
                                    else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                    {
                                        maternity.empMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        maternity.empMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }
                                    else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                    {
                                        maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                    }

                                    res.employeeInfo.empBenefitInfo.empMaternityList.Add(maternity);
                                }

                            }
                        }
                    }
                    catch (Exception) { }

                    res.spouseInfo.spuBenefitInfo.spuMaternityList = res.spouseInfo.spuBenefitInfo.spuMaternityList.OrderBy(a => a.spuBHCode).ToList();



                    // Claim information

                    // get claim for employee
                    iAMENotify = 0;
                    iDentalNotify = 0;
                    iIPDNotify = 0;
                    iLABNotify = 0;
                    iOPDNotify = 0;


                    GetClaimInformation(res.spouseInfo.spuMemberNo, req.policyNo, res.spouseInfo.spuStartDate, res.spouseInfo.spuEndDate, ref iAMENotify, ref iDentalNotify, ref iIPDNotify, ref iLABNotify, ref iOPDNotify);
                    res.spouseInfo.spuClaimAMENotify = iAMENotify.ToString();
                    res.spouseInfo.spuClaimDentalNotify = iDentalNotify.ToString();
                    res.spouseInfo.spuClaimIPDNotify = iIPDNotify.ToString();
                    res.spouseInfo.spuClaimLabsNotify = iLABNotify.ToString();
                    res.spouseInfo.spuClaimOPDNotify = iOPDNotify.ToString();
                    res.spouseInfo.spuHasAME = bSpuHasAME.ToString().ToUpper();
                    res.spouseInfo.spuHasDEN = bSpuHasDEN.ToString().ToUpper();
                    res.spouseInfo.spuHasIPD = bSpuHasIPD.ToString().ToUpper();
                    res.spouseInfo.spuHasLAB = bSpuHasLAB.ToString().ToUpper();
                    res.spouseInfo.spuHasOPD = bSpuHasOPD.ToString().ToUpper();

                    iAllNotify += iAMENotify + iDentalNotify + iIPDNotify + iLABNotify + iOPDNotify;
                }


                else
                {
                    res.spouseInfo = null;
                }

                List<DataRow> ChRows = null;
                List<DataRow> ChGuserInfoRows = null;
                List<DataRow> ChClaimRows = null;
                List<DataRow> ChBenefitRows = null;
                bool bChildHasAME = false, bChildHasDEN = false, bChildHasIPD = false, bChildHasLAB = false, bChildHasOPD = false;
                if (null != dsMemberDetail)
                {
                    ChRows = (from ChildRows in dsMemberDetail.Tables[0].AsEnumerable()
                              where ChildRows.Field<string>("MEMBERNO").StartsWith(req.memberNo.Substring(0, 5) + "-0")
                              select ChildRows).ToList();

                    // get only child
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-00"));
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-01"));
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-31"));
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-32"));
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-33"));
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-34"));
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-35"));
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-36"));
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-37"));
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-38"));
                    ChRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-39"));

                }
                if (null != dsGUserInfo)
                {
                    ChGuserInfoRows = (from ChildGuserInfoRows in dsGUserInfo.Tables[0].AsEnumerable()
                                       where ChildGuserInfoRows.Field<string>("usernum").StartsWith(req.memberNo.Substring(0, 5) + "-0")
                                       select ChildGuserInfoRows).ToList();

                    // get only child
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-00"));
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-01"));
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-31"));
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-32"));
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-33"));
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-34"));
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-35"));
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-36"));
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-37"));
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-38"));
                    ChGuserInfoRows.RemoveAll(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-39"));
                }

                if (null != dsClaim)
                {
                    ChClaimRows = (from ChildClaimRows in dsClaim.Tables[0].AsEnumerable()
                                   where ChildClaimRows.Field<string>("MEMBERNO").StartsWith(req.memberNo.Substring(0, 5) + "-0")
                                   select ChildClaimRows).ToList();

                    // get only child
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-00"));
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-01"));
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-31"));
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-32"));
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-33"));
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-34"));
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-35"));
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-36"));
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-37"));
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-38"));
                    ChClaimRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-39"));
                }

                if (null != dsBen)
                {
                    ChBenefitRows = (from ChildBenefitRows in dsBen.Tables[0].AsEnumerable()
                                     where ChildBenefitRows.Field<string>("MEMBERNO").StartsWith(req.memberNo.Substring(0, 5) + "-0")
                                     select ChildBenefitRows).ToList();

                    // get only child
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-00"));
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-01"));
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-31"));
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-32"));
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-33"));
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-34"));
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-35"));
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-36"));
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-37"));
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-38"));
                    ChBenefitRows.RemoveAll(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-39"));
                }

                for (int i = 1; i < 20; i++)
                {


                    DataRow TmpChRow = null;
                    DataRow TmpChGuserInfoRow = null;
                    List<DataRow> TmpChClaimRows = null;
                    List<DataRow> TmpChBenefitRows = null;

                    if (null != ChRows)
                        TmpChRow = ChRows.Where(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-0" + i.ToString())).FirstOrDefault();
                    if (null != ChGuserInfoRows && ChGuserInfoRows.Count() > 0)
                        TmpChGuserInfoRow = ChGuserInfoRows.Where(a => a.Field<string>("usernum").Trim().Equals(req.memberNo.Substring(0, 5) + "-0" + i.ToString())).FirstOrDefault();
                    if (null != ChClaimRows)
                        TmpChClaimRows = ChClaimRows.Where(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-0" + i.ToString())).ToList();
                    if (null != ChBenefitRows)
                        TmpChBenefitRows = ChBenefitRows.Where(a => a.Field<string>("MEMBERNO").Equals(req.memberNo.Substring(0, 5) + "-0" + i.ToString())).ToList();

                    if (null != TmpChRow)
                    {
                        ChildInfo child = new ChildInfo();
                        child.memberTypeTh = "บุตร";
                        child.memberTypeEn = "Child";
                        child.childMemberNo = TmpChRow["MEMBERNO"].ToString();
                        child.childFName = TmpChRow["LGIVNAME"].ToString();
                        child.childLName = TmpChRow["LSURNAME"].ToString();
                        child.childDOB = TmpChRow["CLTDOB"].ToString();
                        child.childSex = TmpChRow["CLTSEX"].ToString();
                        child.childNationality = member.GetMemberNationality(req.policyNo, child.childMemberNo);

                        child.childCompanyName = TmpChRow["POLLSURNAME"].ToString();
                        child.childCompanyNameEN = member.GetCompanyNameEN(req.policyNo);
                        string strStartDate = TmpChRow["CCDATE04"].ToString();
                        string strEndDate = TmpChRow["CRDATE04"].ToString();

                        child.childStartDate = strStartDate.Substring(6, 2) + "/" + strStartDate.Substring(4, 2) + "/" + strStartDate.Substring(0, 4);
                        child.childEndDate = strEndDate.Substring(6, 2) + "/" + strEndDate.Substring(4, 2) + "/" + strEndDate.Substring(0, 4);
                        strDOB = child.childDOB;
                        dtDOB = new DateTime(Convert.ToInt32(strDOB.Substring(6, 4)), Convert.ToInt32(strDOB.Substring(3, 2)), Convert.ToInt32(strDOB.Substring(0, 2)));
                        iYear = DateTime.Today.Year - dtDOB.Year;

                        iMonth = 0;

                        if (DateTime.Now.DayOfYear < dtDOB.DayOfYear)
                        {
                            iYear = iYear - 1;

                            // today still not yet birth day
                            iMonth = (12 - dtDOB.Month) + (DateTime.Now.Month - 1);
                        }
                        else
                        {
                            // today pass the birth day
                            iMonth = DateTime.Now.Month - dtDOB.Month;
                        }

                        child.childAgeTH = iYear.ToString() + " ปี " + iMonth.ToString() + " เดือน";
                        child.childAgeEN = iYear.ToString() + " Year " + iMonth.ToString() + " Month";
                        if (null != TmpChGuserInfoRow)
                        {
                            child.childMobile = TmpChGuserInfoRow["mobile_no"].ToString();
                            child.childEmail = TmpChGuserInfoRow["email"].ToString();
                            child.childLineID = TmpChGuserInfoRow["line_id"].ToString();
                        }
                        child.childCitizenID = TmpChRow["SECUITYNO"].ToString();


                        child.childBenefitInfo = new ChildBenefitInfo();
                        tmpRow = null;
                        if (null != TmpChClaimRows)
                        {
                            try
                            {
                                tmpRowList = TmpChClaimRows.Where(a => a["MEMBERNO"].ToString().Equals(child.childMemberNo)).ToList();
                                tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GTLB") || a["PRODTYP"].ToString().Equals("GTLX")).First();

                                if (null != tmpRow)
                                {
                                    decimal dAmount = Convert.ToDecimal(tmpRow["SUMINSU"].ToString());

                                    if (dAmount > 1000)
                                    {
                                        child.childBenefitInfo.childGTLDetlTH = tmpRow["prd_th_name"].ToString();
                                        child.childBenefitInfo.childGTLDetlEN = tmpRow["prd_en_name"].ToString();
                                        child.childBenefitInfo.childGTL = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                                    }

                                }
                            }
                            catch (Exception) { }

                            try
                            {
                                tmpRowList = TmpChClaimRows.Where(a => a["MEMBERNO"].ToString().Equals(child.childMemberNo)).ToList();
                                tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GAD1") || a["PRODTYP"].ToString().Equals("GAD2") || a["PRODTYP"].ToString().Equals("GAD3") || a["PRODTYP"].ToString().Equals("GAD4")).First();
                                child.childBenefitInfo.childGADDetlTH = tmpRow["prd_th_name"].ToString();
                                child.childBenefitInfo.childGADDetlEN = tmpRow["prd_en_name"].ToString();
                                child.childBenefitInfo.childGAD = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                            }

                            catch (Exception) { }

                            try
                            {
                                tmpRowList = TmpChClaimRows.Where(a => a["MEMBERNO"].ToString().Equals(child.childMemberNo)).ToList();
                                tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GDA1") || a["PRODTYP"].ToString().Equals("GDA2") || a["PRODTYP"].ToString().Equals("GDA3") || a["PRODTYP"].ToString().Equals("GDA4")).First();
                                child.childBenefitInfo.childGDADetlTH = tmpRow["prd_th_name"].ToString();
                                child.childBenefitInfo.childGDADetlEN = tmpRow["prd_en_name"].ToString();
                                child.childBenefitInfo.childGDA = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                            }

                            catch (Exception) { }


                            try
                            {
                                tmpRowList = TmpChClaimRows.Where(a => a["MEMBERNO"].ToString().Equals(child.childMemberNo)).ToList();
                                tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GRC1") || a["PRODTYP"].ToString().Equals("GRC2") || a["PRODTYP"].ToString().Equals("GRC3") || a["PRODTYP"].ToString().Equals("GRC4")).First();
                                child.childBenefitInfo.childGRCDetlTH = tmpRow["prd_th_name"].ToString();
                                child.childBenefitInfo.childGRCDetlEN = tmpRow["prd_en_name"].ToString();
                                child.childBenefitInfo.childGRC = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                            }

                            catch (Exception) { }


                            try
                            {
                                tmpRowList = TmpChClaimRows.Where(a => a["MEMBERNO"].ToString().Equals(child.childMemberNo)).ToList();
                                tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GPTD")).First();
                                child.childBenefitInfo.childGPTDDetlTH = tmpRow["prd_th_name"].ToString();
                                child.childBenefitInfo.childGPTDDetlEN = tmpRow["prd_en_name"].ToString();
                                child.childBenefitInfo.childGPTD = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                            }

                            catch (Exception) { }


                            try
                            {
                                tmpRowList = TmpChClaimRows.Where(a => a["MEMBERNO"].ToString().Equals(child.childMemberNo)).ToList();
                                tmpRow = tmpRowList.Where(a => a["PRODTYP"].ToString().Equals("GCI1")).First();
                                child.childBenefitInfo.childGCIDetlTH = tmpRow["prd_th_name"].ToString();
                                child.childBenefitInfo.childGCIDetlEN = tmpRow["prd_en_name"].ToString();
                                child.childBenefitInfo.childGCI = SetNumberNoDecimalPoint(tmpRow["SUMINSU"].ToString());
                            }

                            catch (Exception) { }
                        }
                        if (null != TmpChBenefitRows)
                        {
                            /************************ IPD ***************************/
                            try
                            {
                                // IT_REQUEST_1A20181130_1A20200087 :: Edit by Tirachit B. @31/01/2020
                                //var allowedBen = new[] { "BH11", "BH12", "BH13", "BH14", "BH15", "BH16", "BH17", "BH18", "BH19", "BH20", "BH21", "BH22", "BH23", "BH24" };
                                var allowedBen = new[] { "BH11", "BH12", "BH13", "BH14", "BH15", "BH16", "BH17", "BH18", "BH19", "BH20", "BH21", "BH22", "BH23", "BH24", "BH26", "BH28", "BH25", "BH29" };
                                tmpRowList = TmpChBenefitRows.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                                if (tmpRowList != null)
                                {
                                    foreach (DataRow dr in tmpRowList)
                                    {
                                        SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bChildHasAME, ref bChildHasDEN, ref bChildHasIPD, ref bChildHasLAB, ref bChildHasOPD);

                                        ChildIPDInfo ipd = new ChildIPDInfo();
                                        if (dr["BENCDE"].ToString().Equals("BH11") || dr["BENCDE"].ToString().Equals("BH12"))
                                        {

                                            int iMaxDay = Convert.ToInt32(dr["MAX_DAY"].ToString());
                                            decimal dPervis = Convert.ToDecimal(dr["PERVIS"].ToString().Replace(",", ""));
                                            ipd.childBHCode = dr["BENCDE"].ToString();
                                            if (iMaxDay == 0)
                                            {
                                                ipd.childIPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                                ipd.childIPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";
                                            }
                                            else if (dPervis > 0 && iMaxDay > 0)
                                            {
                                                ipd.childIPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท สูงสุด " + dr["MAX_DAY"].ToString() + " วันต่อครั้ง";
                                                ipd.childIPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht, Max. " + dr["MAX_DAY"].ToString() + " Days/visit";
                                            }
                                        }
                                        else if (dr["BENCDE"].ToString().Equals("BH29"))
                                        {
                                            ipd.childBHCode = dr["BENCDE"].ToString();
                                            ipd.childIPDDetlTH = "ค่ารักษาพยาบาลผู้ป่วยนอกต่อเนื่องจากผู้ป่วยใน หลังออกจากโรงพยาบาล " + SetNumberNoDecimalPoint(dr["MAX_DAY"].ToString()) + " วัน";
                                            ipd.childIPDDetlEN = "Including OPD follow up within " + SetNumberNoDecimalPoint(dr["MAX_DAY"].ToString()) + " days after discharging from hospital";
                                        }
                                        else
                                        {
                                            ipd.childBHCode = dr["BENCDE"].ToString();
                                            ipd.childIPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                            ipd.childIPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";
                                        }

                                        //sunisa add on 04/02/2020
                                        //ipd.childIPDCoPay = dr["COPAY"].ToString();
                                        //ipd.childIPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                        if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                        {
                                            ipd.childIPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            ipd.childIPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                        }
                                        else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                        {
                                            ipd.childIPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            ipd.childIPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        }
                                        else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                        {
                                            ipd.childIPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            ipd.childIPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                        }

                                        child.childBenefitInfo.childIPDList.Add(ipd);
                                    }
                                }
                            }
                            catch (Exception) { }
                            child.childBenefitInfo.childIPDList = child.childBenefitInfo.childIPDList.OrderBy(a => a.childBHCode).ToList();
                            /************************ OPD ***************************/
                            try
                            {
                                var allowedBen = new[] { "BH01", "BH04", "BOL1", "BOL2", "BOL3" };
                                tmpRowList = TmpChBenefitRows.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                                if (tmpRowList != null)
                                {
                                    foreach (DataRow dr in tmpRowList)
                                    {
                                        SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bChildHasAME, ref bChildHasDEN, ref bChildHasIPD, ref bChildHasLAB, ref bChildHasOPD);

                                        if (dr["BENCDE"].ToString().Equals("BH01"))
                                        {
                                            ChildOPDInfo opd = new ChildOPDInfo();
                                            int iMaxDay = Convert.ToInt32(dr["MAX_DAY"].ToString());
                                            decimal dPervis = Convert.ToDecimal(dr["PERVIS"].ToString().Replace(",", ""));
                                            decimal dPerYear = Convert.ToInt32(dr["PERYEAR"].ToString().Replace(",", ""));
                                            opd.childBHCode = dr["BENCDE"].ToString();
                                            if (dPerYear != 0 && iMaxDay > 0)
                                            {

                                                opd.childOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาท สูงสุด " + dr["MAX_DAY"].ToString() + " ครั้งต่อปี";
                                                opd.childOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht, Max. " + dr["MAX_DAY"].ToString() + " visits/year";

                                            }
                                            else if (iMaxDay == 0 && (dPervis != 0 && dPerYear != 0))//sunisa add on 24/04/2020
                                            {
                                                opd.childOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี" + " ไม่เกิน " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง";
                                                opd.childOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year" + " Max " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/visit";
                                            }
                                            else if (dPerYear != 0 && iMaxDay == 0)
                                            {
                                                opd.childOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี";
                                                opd.childOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year";
                                            }
                                            else if (dPervis != 0 && iMaxDay > 0)
                                            {
                                                opd.childOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง สูงสุด " + dr["MAX_DAY"].ToString() + " ครั้งต่อปี";
                                                opd.childOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/visit, max.  " + dr["MAX_DAY"].ToString() + " visits/year";
                                            }
                                            else if (dPervis != 0 && iMaxDay == 0)
                                            {
                                                opd.childOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง";
                                                opd.childOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/visit";
                                            }

                                            //sunisa add on 04/02/2020
                                            //opd.childOPDCoPay = dr["COPAY"].ToString();
                                            //opd.childOPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                            if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                            {
                                                opd.childOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                opd.childOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            }
                                            else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                            {
                                                opd.childOPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                opd.childOPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }
                                            else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                            {
                                                opd.childOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                opd.childOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }

                                            child.childBenefitInfo.childOPDList.Add(opd);
                                        }
                                        else if (dr["BENCDE"].ToString().Equals("BH04"))
                                        {
                                            ChildOPDInfo opd = new ChildOPDInfo();
                                            int iMaxDay = Convert.ToInt32(dr["MAX_DAY"].ToString());
                                            decimal dPerYear = Convert.ToInt32(dr["PERYEAR"].ToString().Replace(",", ""));
                                            opd.childBHCode = dr["BENCDE"].ToString();

                                            if (dPerYear != 0 && iMaxDay == 0)
                                            {
                                                opd.childOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาท/ปี";
                                                opd.childOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year";

                                                //sunisa add on 04/02/2020
                                                //opd.childOPDCoPay = dr["COPAY"].ToString();
                                                //opd.childOPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                                if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                                {
                                                    opd.childOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                    opd.childOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                }
                                                else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                                {
                                                    opd.childOPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                    opd.childOPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                }
                                                else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                                {
                                                    opd.childOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                    opd.childOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                }

                                                child.childBenefitInfo.childOPDList.Add(opd);
                                            }

                                        }

                                        else
                                        {
                                            // "BOL1", "BOL2", "BOL3"
                                            ChildOPDInfo opd = new ChildOPDInfo();
                                            opd.childBHCode = dr["BENCDE"].ToString();
                                            opd.childOPDDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาท";
                                            opd.childOPDDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht";

                                            //sunisa add on 04/02/2020
                                            //opd.childOPDCoPay = dr["COPAY"].ToString();
                                            //opd.childOPDDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                            if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                            {
                                                opd.childOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                opd.childOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            }
                                            else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                            {
                                                opd.childOPDDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                opd.childOPDDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }
                                            else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                            {
                                                opd.childOPDDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                opd.childOPDDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }

                                            child.childBenefitInfo.childOPDList.Add(opd);
                                        }
                                    }
                                }
                            }
                            catch (Exception) { }
                            child.childBenefitInfo.childOPDList = child.childBenefitInfo.childOPDList.OrderBy(a => a.childBHCode).ToList();

                            /************************ Dental ***************************/
                            try
                            {
                                var allowedBen = new[] { "BDT2", "BDT3" };
                                tmpRowList = TmpChBenefitRows.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                                if (tmpRowList != null)
                                {
                                    foreach (DataRow dr in tmpRowList)
                                    {
                                        SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bChildHasAME, ref bChildHasDEN, ref bChildHasIPD, ref bChildHasLAB, ref bChildHasOPD);
                                        if (dr["BENCDE"].ToString().Equals("BDT2"))
                                        {
                                            ChildDentalInfo dental = new ChildDentalInfo();
                                            dental.childBHCode = dr["BENCDE"].ToString();
                                            dental.childDentalDetlTH = dr["BENCTHNAME"].ToString().Trim() + ": สูงสุด " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี " + dr["PERVIS"].ToString() + " บาทต่อครั้ง";
                                            dental.childDentalDetlEN = dr["BENCENNAME"].ToString().Trim() + ": Max. " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year " + dr["PERVIS"].ToString() + " Baht/visit";

                                            //sunisa add on 04/02/2020
                                            //dental.childDentalCoPay = dr["COPAY"].ToString();
                                            //dental.childDentalDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                            if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                            {
                                                dental.childDentalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                dental.childDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            }
                                            else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                            {
                                                dental.childDentalDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                dental.childDentalDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }
                                            else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                            {
                                                dental.childDentalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                dental.childDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }

                                            child.childBenefitInfo.childDentalList.Add(dental);

                                        }
                                        else if (dr["BENCDE"].ToString().Equals("BDT3"))
                                        {
                                            ChildDentalInfo dental = new ChildDentalInfo();
                                            dental.childBHCode = dr["BENCDE"].ToString();
                                            dental.childDentalDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " บาทต่อปี ";
                                            dental.childDentalDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERYEAR"].ToString()) + " Baht/year ";

                                            //sunisa add on 04/02/2020
                                            //dental.childDentalCoPay = dr["COPAY"].ToString();
                                            //dental.childDentalDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                            if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                            {
                                                dental.childDentalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                dental.childDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            }
                                            else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                            {
                                                dental.childDentalDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                dental.childDentalDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }
                                            else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                            {
                                                dental.childDentalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                dental.childDentalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }

                                            child.childBenefitInfo.childDentalList.Add(dental);

                                        }

                                    }
                                }
                            }
                            catch (Exception) { }
                            child.childBenefitInfo.childDentalList = child.childBenefitInfo.childDentalList.OrderBy(a => a.childBHCode).ToList();

                            /************************ Accident ***************************/
                            try
                            {
                                var allowedBen = new[] { "BA05" };
                                tmpRowList = TmpChBenefitRows.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                                if (tmpRowList != null)
                                {
                                    foreach (DataRow dr in tmpRowList)
                                    {
                                        SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bChildHasAME, ref bChildHasDEN, ref bChildHasIPD, ref bChildHasLAB, ref bChildHasOPD);
                                        if (dr["BENCDE"].ToString().Equals("BA05"))
                                        {
                                            ChildAccidentInfo accident = new ChildAccidentInfo();
                                            accident.childBHCode = dr["BENCDE"].ToString();
                                            accident.childAccidentDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาทต่อครั้ง";
                                            accident.childAccidentDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht/accident";

                                            //sunisa add on 04/02/2020
                                            //accident.childAccidentCoPay = dr["COPAY"].ToString();
                                            //accident.childAccidentDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                            if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                            {
                                                accident.childAccidentDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                accident.childAccidentDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            }
                                            else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                            {
                                                accident.childAccidentDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                accident.childAccidentDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }
                                            else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                            {
                                                accident.childAccidentDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                accident.childAccidentDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }

                                            child.childBenefitInfo.childAccidentList.Add(accident);
                                        }


                                    }
                                }
                            }
                            catch (Exception) { }
                            child.childBenefitInfo.childAccidentList = child.childBenefitInfo.childAccidentList.OrderBy(a => a.childBHCode).ToList();
                            /************************ Critical ***************************/
                            try
                            {
                                var allowedBen = new[] { "BCI1" };
                                tmpRowList = TmpChBenefitRows.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                                if (tmpRowList != null)
                                {
                                    foreach (DataRow dr in tmpRowList)
                                    {

                                        if (dr["BENCDE"].ToString().Equals("BCI1"))
                                        {
                                            ChildCriticalInfo critical = new ChildCriticalInfo();
                                            critical.childBHCode = dr["BENCDE"].ToString();
                                            critical.childCriticalDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                            critical.childCriticalDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                            //sunisa add on 04/02/2020
                                            //critical.childCriticalCoPay = dr["COPAY"].ToString();
                                            //critical.childCriticalDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                            if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                            {
                                                critical.childCriticalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                critical.childCriticalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            }
                                            else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                            {
                                                critical.childCriticalDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                critical.childCriticalDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }
                                            else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                            {
                                                critical.childCriticalDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                critical.childCriticalDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }

                                            child.childBenefitInfo.childCriticalList.Add(critical);
                                        }


                                    }
                                }
                            }
                            catch (Exception) { }
                            child.childBenefitInfo.childCriticalList = child.childBenefitInfo.childCriticalList.OrderBy(a => a.childBHCode).ToList();
                            /************************ Maternity ***************************/
                            try
                            {
                                //var allowedBen = new[] { "BM01", "BM02", "BM03" };
                                var allowedBen = new[] { "BM01", "BM02", "BM03", "BM04", "BM05", "BM06" };
                                tmpRowList = TmpChBenefitRows.Where(a => allowedBen.Contains(a["BENCDE"].ToString())).ToList();
                                if (tmpRowList != null)
                                {
                                    foreach (DataRow dr in tmpRowList)
                                    {
                                        SetBenefit(dr["PRODUCT_TYPE"].ToString(), ref bChildHasAME, ref bChildHasDEN, ref bChildHasIPD, ref bChildHasLAB, ref bChildHasOPD);
                                        if (dr["BENCDE"].ToString().Equals("BM01"))
                                        {
                                            ChildMaternityInfo maternity = new ChildMaternityInfo();
                                            maternity.childBHCode = dr["BENCDE"].ToString();
                                            maternity.childMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                            maternity.childMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                            //sunisa add on 04/02/2020
                                            //maternity.childMaternityCoPay = dr["COPAY"].ToString();
                                            //maternity.childMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                            if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                            {
                                                maternity.childMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                maternity.childMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            }
                                            else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                            {
                                                maternity.childMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                maternity.childMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }
                                            else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                            {
                                                maternity.childMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                maternity.childMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }

                                            child.childBenefitInfo.childMaternityList.Add(maternity);
                                        }
                                        else if (dr["BENCDE"].ToString().Equals("BM02"))
                                        {
                                            ChildMaternityInfo maternity = new ChildMaternityInfo();
                                            maternity.childBHCode = dr["BENCDE"].ToString();
                                            maternity.childMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                            maternity.childMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                            //sunisa add on 04/02/2020
                                            //maternity.childMaternityCoPay = dr["COPAY"].ToString();
                                            //maternity.childMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                            if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                            {
                                                maternity.childMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                maternity.childMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            }
                                            else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                            {
                                                maternity.childMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                maternity.childMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }
                                            else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                            {
                                                maternity.childMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                maternity.childMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }

                                            child.childBenefitInfo.childMaternityList.Add(maternity);
                                        }
                                        else if (dr["BENCDE"].ToString().Equals("BM03"))
                                        {
                                            ChildMaternityInfo maternity = new ChildMaternityInfo();
                                            maternity.childBHCode = dr["BENCDE"].ToString();
                                            maternity.childMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                            maternity.childMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                            //sunisa add on 04/02/2020
                                            //maternity.childMaternityCoPay = dr["COPAY"].ToString();
                                            //maternity.childMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                            if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                            {
                                                maternity.childMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                maternity.childMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            }
                                            else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                            {
                                                maternity.childMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                maternity.childMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }
                                            else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                            {
                                                maternity.childMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                maternity.childMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }

                                            child.childBenefitInfo.childMaternityList.Add(maternity);
                                        }
                                        else if (dr["BENCDE"].ToString().Equals("BM04") || dr["BENCDE"].ToString().Equals("BM05") || dr["BENCDE"].ToString().Equals("BM06"))
                                        {
                                            EmpMaternityInfo maternity = new EmpMaternityInfo();
                                            maternity.empBHCode = dr["BENCDE"].ToString();
                                            maternity.empMaternityDetlTH = dr["BENCTHNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " บาท";
                                            maternity.empMaternityDetlEN = dr["BENCENNAME"].ToString().Trim() + ":  " + SetNumberNoDecimalPoint(dr["PERVIS"].ToString()) + " Baht";

                                            //sunisa add on 04/02/2020
                                            //maternity.empMaternityCoPay = dr["COPAY"].ToString();
                                            //maternity.empMaternityDeduc = SetNumberNoDecimalPoint(dr["DEDUC"].ToString());
                                            if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() == "")
                                            {
                                                maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                                maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ")";
                                            }
                                            else if (dr["COPAY"].ToString() == "" && dr["DEDUC"].ToString() != "")
                                            {
                                                maternity.empMaternityDetlTH += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                maternity.empMaternityDetlEN += " \n (Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }
                                            else if (dr["COPAY"].ToString() != "" && dr["DEDUC"].ToString() != "")
                                            {
                                                maternity.empMaternityDetlTH += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                                maternity.empMaternityDetlEN += " \n (Co-pay : " + dr["COPAY"].ToString() + ", Deductible : " + SetNumberNoDecimalPoint(dr["DEDUC"].ToString()) + ")";
                                            }

                                            res.employeeInfo.empBenefitInfo.empMaternityList.Add(maternity);
                                        }

                                    }
                                }
                            }
                            catch (Exception) { }

                            child.childBenefitInfo.childMaternityList = child.childBenefitInfo.childMaternityList.OrderBy(a => a.childBHCode).ToList();
                        }


                        // get claim information
                        // get claim for employee
                        iAMENotify = 0;
                        iDentalNotify = 0;
                        iIPDNotify = 0;
                        iLABNotify = 0;
                        iOPDNotify = 0;

                        GetClaimInformation(child.childMemberNo, req.policyNo, child.childStartDate, child.childEndDate, ref iAMENotify, ref iDentalNotify, ref iIPDNotify, ref iLABNotify, ref iOPDNotify);
                        child.childClaimAMENotify = iAMENotify.ToString();
                        child.childClaimDentalNotify = iDentalNotify.ToString();
                        child.childClaimIPDNotify = iIPDNotify.ToString();
                        child.childClaimLabsNotify = iLABNotify.ToString();
                        child.childClaimOPDNotify = iOPDNotify.ToString();

                        child.childHasAME = bChildHasAME.ToString().ToUpper();
                        child.childHasDEN = bChildHasDEN.ToString().ToUpper();
                        child.childHasIPD = bChildHasIPD.ToString().ToUpper();
                        child.childHasLAB = bChildHasLAB.ToString().ToUpper();
                        child.childHasOPD = bChildHasOPD.ToString().ToUpper();

                        iAllNotify += iAMENotify + iDentalNotify + iIPDNotify + iLABNotify + iOPDNotify;

                        res.childrensInfo.Add(child);
                    }   // end if (null != TmpChRow)
                }
                res.claimTotNotify = iAllNotify.ToString();

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }

            finally
            {
                log.Info("out");
            }


        }
        public string SetNumberNoDecimalPoint(string strDecimal)
        {
            log.Info("in");

            try
            {
                if (string.IsNullOrEmpty(strDecimal)) { return string.Empty; }

                Decimal dDecimal = Convert.ToDecimal(strDecimal);

                dDecimal = Decimal.Round(dDecimal, MidpointRounding.AwayFromZero);
                return String.Format("{0:n0}", dDecimal);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw;
            }
            finally
            {

                log.Info("out");
            }




        }
        public void SetBenefit(string productType, ref bool bHasAME, ref bool bHasDEN, ref bool bHasIPD, ref bool bHasLAB, ref bool bHasOPD)
        {
            try
            {
                if (productType.Contains("GOL"))
                {
                    bHasLAB = true;
                }
                else if (productType.Contains("GHS") || productType.Contains("GMJ") || productType.Contains("GMT"))
                {
                    bHasIPD = true;
                }
                else if (productType.Contains("GME"))
                {
                    bHasAME = true;
                }
                else if (productType.Contains("GHO"))
                {
                    bHasOPD = true;
                }
                else if (productType.Contains("GDT"))
                {
                    bHasDEN = true;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw;

            }
            finally
            {
                log.Info("out");
            }
        }
        public void GetClaimInformation(string memberNo, string policyNo, string strStartDate, string strEndDate, ref int iAMENotify, ref int iDentalNotify, ref int iIPDNotify, ref int iLabsNotify, ref int iOPDNotify)
        {
            try
            {
                MemberModel model = new MemberModel();

                // delete claim
                model.DeleteClaimByMemberNo(memberNo, policyNo);

                // get from claim mn2, AS400
                DataSet dsAS400 = model.GetClaimAS400(memberNo, policyNo);
                log.Debug("Get Claim AS400 done");
                // get from hospital, still not settle with TOKIO yet.
                DataSet dsHospitalCheck = model.GetClaimHospitalCheck(memberNo, policyNo);
                log.Debug("Get Claim Hospital done");


                List<AllClaimsDetailsEDM> listClaim = new List<AllClaimsDetailsEDM>();

                // check start and end date
                DateTime dtStart, dtEnd, dtStartTemp;
                int iAdditionalNotifyAME = 0, iAdditionalNotifyDEN = 0, iAdditionalNotifyLAB = 0, iAdditionalNotifyIPD = 0, iAdditionalNotifyOPD = 0;

                dtStart = new DateTime(Convert.ToInt32(strStartDate.Substring(6, 4)), Convert.ToInt32(strStartDate.Substring(3, 2)), Convert.ToInt32(strStartDate.Substring(0, 2)));
                dtEnd = new DateTime(Convert.ToInt32(strEndDate.Substring(6, 4)), Convert.ToInt32(strEndDate.Substring(3, 2)), Convert.ToInt32(strEndDate.Substring(0, 2)));

                //***** fill in benefit.



                if (null != dsAS400)
                {
                    log.Debug("Do all calim list for AS400");
                    // transform all claim data to list
                    foreach (DataRow row in dsAS400.Tables[0].Rows)
                    {

                        dtStartTemp = new DateTime(Convert.ToInt32(row["START_DATE_HOSPITAL"].ToString().Substring(6, 4)), Convert.ToInt32(row["START_DATE_HOSPITAL"].ToString().Substring(3, 2)), Convert.ToInt32(row["START_DATE_HOSPITAL"].ToString().Substring(0, 2)));
                        log.Debug("start date hospital:" + row["START_DATE_HOSPITAL"].ToString());
                        if (dtStartTemp >= dtStart && dtStartTemp <= dtEnd)
                        {
                            AllClaimsDetailsEDM claim = new AllClaimsDetailsEDM();

                            claim.CONTRACT_NUMBER = row["CONTRACT_NUMBER"].ToString();
                            claim.MEMBER_NO = row["MEMBER_NO"].ToString();
                            claim.DEPENDENCEP_NO = row["DEPENDENCEP_NO"].ToString();
                            claim.CLAMNUM = row["CLAMNUM"].ToString();
                            claim.VISIT_TYPE = row["VISIT_TYPE"].ToString();
                            claim.PRODUCT_TYPE_CODE = row["PRODUCT_TYPE_CODE"].ToString();
                            claim.START_DATE_HOSPITAL = row["START_DATE_HOSPITAL"].ToString();
                            claim.END_DATE_HOSPITAL = row["END_DATE_HOSPITAL"].ToString();
                            claim.DIAGNOSE_TH = row["DIAGNOSE_TH"].ToString();
                            claim.DIAGNOSE_EN = row["DIAGNOSE_EN"].ToString();
                            claim.FORFEIT = row["FORFEIT"].ToString();
                            claim.INCURRED = Convert.ToDecimal(row["INCURRED"].ToString());
                            claim.HMOSHARE = Convert.ToDecimal(row["HMOSHARE"].ToString());
                            claim.MBRSHARE = Convert.ToDecimal(row["MBRSHARE"].ToString());
                            claim.HOSPITAL = row["HOSPITAL"].ToString();
                            claim.GCSTS = row["GCSTS"].ToString();
                            claim.GCSTSENNAME = row["GCSTSENNAME"].ToString();
                            claim.GCSTSTHNAME = row["GCSTSTHNAME"].ToString();
                            claim.PRODUCT_TYPE = row["PRODUCT_TYPE"].ToString();
                            claim.CLAIM_TYPE = row["CLATYPE"].ToString();
                            if (claim.CLAIM_TYPE.Equals("REG"))
                            {
                                claim.CLAIM_TYPE_DESC = "Cash Claim";
                            }
                            else if (claim.CLAIM_TYPE.Equals("CRD"))
                            {
                                claim.CLAIM_TYPE_DESC = "Credit Claim";
                            }
                            else if (claim.CLAIM_TYPE.Equals("FAX"))
                            {
                                claim.CLAIM_TYPE_DESC = "FAX Claim";
                            }

                            if (claim.VISIT_TYPE.Equals("IPD"))
                            {
                                if (claim.CLAIM_TYPE.Equals("CRD"))
                                {
                                    claim.CLAIM_TYPE_DESC = "FAX Claim";
                                }
                            }

                            claim.DATE_AUTH = row["DATEAUTH"].ToString();
                            claim.DDTEVISIT = row["DDTEVISIT"].ToString();
                            claim.INCURRED_DATE = row["INCURRED_DATE"].ToString();
                            if (null != row["HOSPITAL_NAME_ENG"])
                            {
                                log.Debug("HOSPITAL_NAME_ENG not null");
                                claim.HOSPITAL_NAME_ENG = row["HOSPITAL_NAME_ENG"].ToString();
                            }
                            else
                            {
                                log.Debug("HOSPITAL_NAME_ENG null");
                                claim.HOSPITAL_NAME_ENG = "";
                            }

                            claim.DATA_FROM = "A";

                            listClaim.Add(claim);


                        }
                    }
                }
                if (null != dsHospitalCheck)
                {
                    log.Debug("Do all calim list for hospital check");
                    foreach (DataRow row in dsHospitalCheck.Tables[0].Rows)
                    {
                        dtStartTemp = new DateTime(Convert.ToInt32(row["START_DATE_HOSPITAL"].ToString().Substring(6, 4)), Convert.ToInt32(row["START_DATE_HOSPITAL"].ToString().Substring(3, 2)), Convert.ToInt32(row["START_DATE_HOSPITAL"].ToString().Substring(0, 2)));

                        log.Debug("start date hospital:" + row["START_DATE_HOSPITAL"].ToString());
                        if (dtStartTemp >= dtStart && dtStartTemp <= dtEnd)
                        {
                            AllClaimsDetailsEDM claim = new AllClaimsDetailsEDM();

                            claim.CONTRACT_NUMBER = row["CONTRACT_NUMBER"].ToString();
                            claim.MEMBER_NO = row["MEMBER_NO"].ToString() + "-" + row["DEPENDENCEP_NO"].ToString();
                            claim.DEPENDENCEP_NO = row["DEPENDENCEP_NO"].ToString();
                            claim.CLAMNUM = row["CLAMNUM"].ToString();
                            claim.VISIT_TYPE = row["VISIT_TYPE"].ToString();
                            claim.PRODUCT_TYPE_CODE = row["PRODUCT_TYPE_CODE"].ToString();
                            claim.START_DATE_HOSPITAL = row["START_DATE_HOSPITAL"].ToString();
                            claim.END_DATE_HOSPITAL = row["END_DATE_HOSPITAL"].ToString();
                            claim.DIAGNOSE_TH = row["Diagnose"].ToString();
                            claim.DIAGNOSE_EN = row["Diagnose_EN"].ToString();
                            claim.FORFEIT = row["FORFEIT"].ToString();
                            claim.INCURRED = Convert.ToDecimal(row["INCURRED"].ToString());
                            claim.HMOSHARE = Convert.ToDecimal(row["HMOSHARE"].ToString());
                            claim.MBRSHARE = Convert.ToDecimal(row["MBRSHARE"].ToString());
                            claim.HOSPITAL = row["HOSPITAL"].ToString();
                            claim.GCSTS = row["GCSTS"].ToString();
                            claim.GCSTSENNAME = row["GCSTSENNAME"].ToString();
                            claim.GCSTSTHNAME = row["GCSTSTHNAME"].ToString();
                            claim.PRODUCT_TYPE = row["PRODUCT_TYPE"].ToString();
                            claim.CLAIM_TYPE = row["CLATYPE"].ToString();
                            if (claim.CLAIM_TYPE.Equals("REG"))
                            {
                                claim.CLAIM_TYPE_DESC = "Cash Claim";
                            }
                            else if (claim.CLAIM_TYPE.Equals("CRD"))
                            {
                                claim.CLAIM_TYPE_DESC = "Credit Claim";
                            }
                            else if (claim.CLAIM_TYPE.Equals("FAX"))
                            {
                                claim.CLAIM_TYPE_DESC = "FAX Claim";
                            }

                            claim.DATE_AUTH = row["DDTEVISIT"].ToString();  /**************************/
                            claim.DDTEVISIT = row["DDTEVISIT"].ToString();
                            claim.INCURRED_DATE = row["INCURRED_DATE"].ToString();
                            try

                            {

                                claim.HOSPITAL_NAME_ENG = row["HOSPITAL_NAME_EN"].ToString();

                            }
                            catch (Exception ex)
                            {
                                log.Error(ex.ToString());

                                log.Debug("try another field");
                                claim.HOSPITAL_NAME_ENG = row["HOSPITAL_NAME_ENG"].ToString();
                            }

                            claim.DATA_FROM = "H";

                            listClaim.Add(claim);

                        }
                    }
                }
                listClaim = listClaim.Where(a => a.MEMBER_NO.Equals(memberNo)).ToList();


                // insert all claim
                bool bInsert = model.InsertAllClaims(listClaim);
                if (bInsert)
                {

                    DataSet dsClaimNotify = model.GetCalimNotify(policyNo, memberNo);

                    if (null != dsClaimNotify)
                    {
                        // in case , no one ever read.
                        if (dsClaimNotify.Tables[0].Rows.Count == 0)
                        {
                            iAMENotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("AME")).Count();
                            iDentalNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("DEN")).Count();
                            iIPDNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("IPD")).Count();
                            iLabsNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("LAB")).Count();
                            iOPDNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("OPD")).Count();
                        }
                        else
                        {
                            // has data.
                            List<DataRow> notifyclaimRow = (from NewClaimRows in dsClaimNotify.Tables[0].AsEnumerable()
                                                            select NewClaimRows).ToList();
                            DataRow AMENotifyclaimRow = null;
                            DataRow DentalNotifyclaimRow = null;
                            DataRow IPDNotifyclaimRow = null;
                            DataRow LABNotifyclaimRow = null;
                            DataRow OPDNotifyclaimRow = null;

                            try
                            {
                                AMENotifyclaimRow = (from NewClaimRows in dsClaimNotify.Tables[0].AsEnumerable()
                                                     where NewClaimRows.Field<string>("VISIT_TYPE") == "AME"
                                                     select NewClaimRows).First();
                            }
                            catch (Exception)
                            {
                                log.Info("No AME");
                            }
                            try
                            {
                                DentalNotifyclaimRow = (from NewClaimRows in dsClaimNotify.Tables[0].AsEnumerable()
                                                        where NewClaimRows.Field<string>("VISIT_TYPE") == "DEN"
                                                        select NewClaimRows).First();
                            }
                            catch (Exception)
                            {
                                log.Info("No Dental");
                            }
                            try
                            {
                                IPDNotifyclaimRow = (from NewClaimRows in dsClaimNotify.Tables[0].AsEnumerable()
                                                     where NewClaimRows.Field<string>("VISIT_TYPE") == "IPD"
                                                     select NewClaimRows).First();
                            }
                            catch (Exception)
                            {
                                log.Info("No IPD");
                            }
                            try
                            {
                                LABNotifyclaimRow = (from NewClaimRows in dsClaimNotify.Tables[0].AsEnumerable()
                                                     where NewClaimRows.Field<string>("VISIT_TYPE") == "LAB"
                                                     select NewClaimRows).First();
                            }
                            catch (Exception)
                            {
                                log.Info("No LAB");
                            }
                            try
                            {
                                OPDNotifyclaimRow = (from NewClaimRows in dsClaimNotify.Tables[0].AsEnumerable()
                                                     where NewClaimRows.Field<string>("VISIT_TYPE") == "OPD"
                                                     select NewClaimRows).First();
                            }
                            catch (Exception)
                            {
                                log.Info("No OPD");
                            }

                            List<AllClaimsDetailsEDM> listClaimTmp = new List<AllClaimsDetailsEDM>();

                            foreach (AllClaimsDetailsEDM claimDetail in listClaim)
                            {

                                DateTime dtClaimDetail = new DateTime(Convert.ToInt32(claimDetail.START_DATE_HOSPITAL.Substring(6, 4)), Convert.ToInt32(claimDetail.START_DATE_HOSPITAL.Substring(3, 2)), Convert.ToInt32(claimDetail.START_DATE_HOSPITAL.Substring(0, 2)));
                                try
                                {
                                    if (claimDetail.VISIT_TYPE.Equals("AME"))
                                    {
                                        if (null != AMENotifyclaimRow)
                                        {
                                            DateTime dtAMENotifyClaim = new DateTime(Convert.ToInt32(AMENotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(6, 4)), Convert.ToInt32(AMENotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(3, 2)), Convert.ToInt32(AMENotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(0, 2)));
                                            if (dtClaimDetail >= dtAMENotifyClaim)
                                            {
                                                if (!AMENotifyclaimRow["CLAIM_ID"].ToString().Contains(claimDetail.CLAMNUM))
                                                    listClaimTmp.Add(claimDetail);

                                                else if (AMENotifyclaimRow["CLAIM_ID"].ToString().Contains(claimDetail.CLAMNUM))
                                                {
                                                    string claimDup = claimDetail.CLAMNUM;
                                                    string strDateCompare = dtClaimDetail.Year.ToString() + dtClaimDetail.Month.ToString().PadLeft(2, '0') + dtClaimDetail.Day.ToString().PadLeft(2, '0');
                                                    string strDateCompareWithFormat = dtClaimDetail.Day.ToString().PadLeft(2, '0') + "/" + dtClaimDetail.Month.ToString().PadLeft(2, '0') + "/" + dtClaimDetail.Year.ToString();
                                                    int iAllClaimCount = listClaim.Where(a => a.CLAMNUM.Equals(claimDup) && a.DDTEVISIT.Equals(strDateCompareWithFormat)).ToList().Count;
                                                    int iNotifyCount = 0;
                                                    string[] strSplit = AMENotifyclaimRow["CLAIM_ID"].ToString().Split(',');

                                                    string strDateCompareAME = AMENotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(6, 4) + AMENotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(3, 2) + AMENotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(0, 2);
                                                    if (strDateCompareAME.Equals(strDateCompare))
                                                    {
                                                        for (int i = 0; i < strSplit.Count(); i++)
                                                        {
                                                            if (strSplit[i].Equals(claimDup))
                                                            {
                                                                iNotifyCount++;
                                                            }
                                                        }
                                                    }

                                                    if (iAllClaimCount > iNotifyCount)
                                                    {
                                                        iAdditionalNotifyAME = iAllClaimCount - iNotifyCount;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                            listClaimTmp.Add(claimDetail);
                                    }
                                    else if (claimDetail.VISIT_TYPE.Equals("DEN"))
                                    {
                                        if (null != DentalNotifyclaimRow)
                                        {
                                            DateTime dtDENNotifyClaim = new DateTime(Convert.ToInt32(DentalNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(6, 4)), Convert.ToInt32(DentalNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(3, 2)), Convert.ToInt32(DentalNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(0, 2)));
                                            if (dtClaimDetail >= dtDENNotifyClaim)
                                            {
                                                if (!DentalNotifyclaimRow["CLAIM_ID"].ToString().Contains(claimDetail.CLAMNUM))
                                                    listClaimTmp.Add(claimDetail);

                                                else if (DentalNotifyclaimRow["CLAIM_ID"].ToString().Contains(claimDetail.CLAMNUM))
                                                {
                                                    string claimDup = claimDetail.CLAMNUM;
                                                    string strDateCompare = dtClaimDetail.Year.ToString() + dtClaimDetail.Month.ToString().PadLeft(2, '0') + dtClaimDetail.Day.ToString().PadLeft(2, '0');
                                                    string strDateCompareWithFormat = dtClaimDetail.Day.ToString().PadLeft(2, '0') + "/" + dtClaimDetail.Month.ToString().PadLeft(2, '0') + "/" + dtClaimDetail.Year.ToString();
                                                    int iAllClaimCount = listClaim.Where(a => a.CLAMNUM.Equals(claimDup) && a.DDTEVISIT.Equals(strDateCompareWithFormat)).ToList().Count;
                                                    int iNotifyCount = 0;
                                                    string[] strSplit = DentalNotifyclaimRow["CLAIM_ID"].ToString().Split(',');

                                                    string strDateCompareDEN = DentalNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(6, 4) + DentalNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(3, 2) + DentalNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(0, 2);
                                                    if (strDateCompareDEN.Equals(strDateCompare))
                                                    {
                                                        for (int i = 0; i < strSplit.Count(); i++)
                                                        {
                                                            if (strSplit[i].Equals(claimDup))
                                                            {
                                                                iNotifyCount++;
                                                            }
                                                        }
                                                    }

                                                    if (iAllClaimCount > iNotifyCount)
                                                    {
                                                        iAdditionalNotifyDEN = iAllClaimCount - iNotifyCount;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                            listClaimTmp.Add(claimDetail);

                                    }
                                    else if (claimDetail.VISIT_TYPE.Equals("IPD"))
                                    {
                                        if (null != IPDNotifyclaimRow)
                                        {
                                            DateTime dtIPDNotifyClaim = new DateTime(Convert.ToInt32(IPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(6, 4)), Convert.ToInt32(IPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(3, 2)), Convert.ToInt32(IPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(0, 2)));
                                            if (dtClaimDetail >= dtIPDNotifyClaim)
                                            {
                                                if (!IPDNotifyclaimRow["CLAIM_ID"].ToString().Contains(claimDetail.CLAMNUM))
                                                    listClaimTmp.Add(claimDetail);

                                                else if (IPDNotifyclaimRow["CLAIM_ID"].ToString().Contains(claimDetail.CLAMNUM))
                                                {
                                                    string claimDup = claimDetail.CLAMNUM;
                                                    string strDateCompare = dtClaimDetail.Year.ToString() + dtClaimDetail.Month.ToString().PadLeft(2, '0') + dtClaimDetail.Day.ToString().PadLeft(2, '0');
                                                    string strDateCompareWithFormat = dtClaimDetail.Day.ToString().PadLeft(2, '0') + "/" + dtClaimDetail.Month.ToString().PadLeft(2, '0') + "/" + dtClaimDetail.Year.ToString();
                                                    int iAllClaimCount = listClaim.Where(a => a.CLAMNUM.Equals(claimDup) && a.DDTEVISIT.Equals(strDateCompareWithFormat)).ToList().Count;
                                                    int iNotifyCount = 0;
                                                    string[] strSplit = IPDNotifyclaimRow["CLAIM_ID"].ToString().Split(',');

                                                    string strDateCompareIPD = IPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(6, 4) + IPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(3, 2) + IPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(0, 2);
                                                    if (strDateCompareIPD.Equals(strDateCompare))
                                                    {
                                                        for (int i = 0; i < strSplit.Count(); i++)
                                                        {
                                                            if (strSplit[i].Equals(claimDup))
                                                            {
                                                                iNotifyCount++;
                                                            }
                                                        }
                                                    }

                                                    if (iAllClaimCount > iNotifyCount)
                                                    {
                                                        iAdditionalNotifyIPD = iAllClaimCount - iNotifyCount;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                            listClaimTmp.Add(claimDetail);
                                    }
                                    else if (claimDetail.VISIT_TYPE.Equals("LAB"))
                                    {
                                        if (null != LABNotifyclaimRow)
                                        {
                                            DateTime dtLABNotifyClaim = new DateTime(Convert.ToInt32(LABNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(6, 4)), Convert.ToInt32(LABNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(3, 2)), Convert.ToInt32(LABNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(0, 2)));
                                            if (dtClaimDetail >= dtLABNotifyClaim)
                                            {
                                                if (!LABNotifyclaimRow["CLAIM_ID"].ToString().Contains(claimDetail.CLAMNUM))
                                                    listClaimTmp.Add(claimDetail);

                                                else if (LABNotifyclaimRow["CLAIM_ID"].ToString().Contains(claimDetail.CLAMNUM))
                                                {
                                                    string claimDup = claimDetail.CLAMNUM;
                                                    string strDateCompare = dtClaimDetail.Year.ToString() + dtClaimDetail.Month.ToString().PadLeft(2, '0') + dtClaimDetail.Day.ToString().PadLeft(2, '0');
                                                    string strDateCompareWithFormat = dtClaimDetail.Day.ToString().PadLeft(2, '0') + "/" + dtClaimDetail.Month.ToString().PadLeft(2, '0') + "/" + dtClaimDetail.Year.ToString();
                                                    int iAllClaimCount = listClaim.Where(a => a.CLAMNUM.Equals(claimDup) && a.DDTEVISIT.Equals(strDateCompareWithFormat)).ToList().Count;
                                                    int iNotifyCount = 0;
                                                    string[] strSplit = LABNotifyclaimRow["CLAIM_ID"].ToString().Split(',');

                                                    string strDateCompareLAB = LABNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(6, 4) + LABNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(3, 2) + LABNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(0, 2);
                                                    if (strDateCompareLAB.Equals(strDateCompare))
                                                    {
                                                        for (int i = 0; i < strSplit.Count(); i++)
                                                        {
                                                            if (strSplit[i].Equals(claimDup))
                                                            {
                                                                iNotifyCount++;
                                                            }
                                                        }
                                                    }

                                                    if (iAllClaimCount > iNotifyCount)
                                                    {
                                                        iAdditionalNotifyLAB = iAllClaimCount - iNotifyCount;
                                                    }
                                                }

                                            }
                                        }
                                        else
                                            listClaimTmp.Add(claimDetail);
                                    }
                                    else if (claimDetail.VISIT_TYPE.Equals("OPD"))
                                    {
                                        if (null != OPDNotifyclaimRow)
                                        {
                                            DateTime dtOPDNotifyClaim = new DateTime(Convert.ToInt32(OPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(6, 4)), Convert.ToInt32(OPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(3, 2)), Convert.ToInt32(OPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(0, 2)));
                                            if (dtClaimDetail >= dtOPDNotifyClaim)
                                            {
                                                if (!OPDNotifyclaimRow["CLAIM_ID"].ToString().Contains(claimDetail.CLAMNUM))
                                                    listClaimTmp.Add(claimDetail);

                                                else if (OPDNotifyclaimRow["CLAIM_ID"].ToString().Contains(claimDetail.CLAMNUM))
                                                {
                                                    string claimDup = claimDetail.CLAMNUM;
                                                    string strDateCompare = dtClaimDetail.Year.ToString() + dtClaimDetail.Month.ToString().PadLeft(2, '0') + dtClaimDetail.Day.ToString().PadLeft(2, '0');
                                                    string strDateCompareWithFormat = dtClaimDetail.Day.ToString().PadLeft(2, '0') + "/" + dtClaimDetail.Month.ToString().PadLeft(2, '0') + "/" + dtClaimDetail.Year.ToString();
                                                    int iAllClaimCount = listClaim.Where(a => a.CLAMNUM.Equals(claimDup) && a.START_DATE_HOSPITAL.Equals(strDateCompareWithFormat)).ToList().Count;
                                                    int iNotifyCount = 0;
                                                    string[] strSplit = OPDNotifyclaimRow["CLAIM_ID"].ToString().Split(',');

                                                    string strDateCompareOPD = OPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(6, 4) + OPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(3, 2) + OPDNotifyclaimRow["CLAIM_DATE_R"].ToString().Substring(0, 2);
                                                    if (strDateCompareOPD.Equals(strDateCompare))
                                                    {
                                                        for (int i = 0; i < strSplit.Count(); i++)
                                                        {
                                                            if (strSplit[i].Equals(claimDup))
                                                            {
                                                                iNotifyCount++;
                                                            }
                                                        }
                                                    }

                                                    if (iAllClaimCount > iNotifyCount)
                                                    {
                                                        iAdditionalNotifyOPD = iAllClaimCount - iNotifyCount;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                            listClaimTmp.Add(claimDetail);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    log.Error(ex.ToString());
                                }

                            }


                            // update claim list

                            listClaim = listClaimTmp;

                            iAMENotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("AME")).Count() + iAdditionalNotifyAME;
                            iDentalNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("DEN")).Count() + iAdditionalNotifyDEN;
                            iIPDNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("IPD")).Count() + iAdditionalNotifyIPD;
                            iLabsNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("LAB")).Count() + iAdditionalNotifyLAB;
                            iOPDNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("OPD")).Count() + iAdditionalNotifyOPD;
                        }
                    }
                    else
                    {
                        iAMENotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("AME")).Count();
                        iDentalNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("DEN")).Count();
                        iIPDNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("IPD")).Count();
                        iLabsNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("LAB")).Count();
                        iOPDNotify = listClaim.Where(a => a.VISIT_TYPE.ToUpper().Equals("OPD")).Count();
                    }

                    //return 0;
                }
                else
                {
                    log.Error("no data or error on insert claim");
                }


            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
            finally
            {

            }

            //return 0;
        }
        public void GetMasterData(GetMasterDataRequest req, GetMasterDataResponse res)
        {
            log.Info("in");

            try
            {
                MemberModel member = new MemberModel();
                DataSet dsProvince = member.GetProvince();

                string strAppId = "";
                if (req.osPlatform.ToUpper().Equals("IOS"))
                {
                    strAppId = ConfigurationManager.AppSettings.Get("AppIdIOS");
                }
                else
                {
                    strAppId = ConfigurationManager.AppSettings.Get("AppIdAndroid");
                }

                DataSet dsVersion = member.GetAppVersion(strAppId, req.AppVersion);

                res.appVersionNeedForceUpdate = "N";

                if (null != dsVersion)
                {
                    string strNeedUpdate = dsVersion.Tables[0].Rows[0]["NeedUpdateVersion"].ToString();
                    if (strNeedUpdate.ToUpper().Equals("YES") || strNeedUpdate.ToUpper().Equals("Y"))
                    {
                        res.appVersionNeedForceUpdate = "Y";
                    }

                }
                else
                {
                    // throw error
                    log.Error("No DB for version");
                    throw new BusinessExpection(999, res);
                }



                if (null != dsProvince)
                {
                    List<DataRow> provinceRow = (from ProvinceRows in dsProvince.Tables[0].AsEnumerable()
                                                 select ProvinceRows).ToList();

                    foreach (DataRow row in provinceRow)
                    {
                        ProvinceInfo province = new ProvinceInfo();

                        province.nameEN = row["engname"].ToString(); ;
                        province.nameTH = row["thname"].ToString(); ;


                        res.provincesInfo.Add(province);
                    }
                }
                DataSet dsCity = member.GetCityInfo();

                if (null != dsCity)
                {
                    List<DataRow> cityRows = (from CityRows in dsCity.Tables[0].AsEnumerable()
                                              select CityRows).ToList();

                    foreach (DataRow row in cityRows)
                    {
                        CityInfo city = new CityInfo();

                        city.nameEN = row["engcity"].ToString(); ;
                        city.nameTH = row["thcity"].ToString(); ;


                        res.citiesInfo.Add(city);
                    }
                }



                DataSet dsHospital = member.GetHospital();


                if (null != dsHospital)
                {
                    // assign clinic

                    List<DataRow> clinicRow = (from ClinicRows in dsHospital.Tables[0].AsEnumerable()
                                               where ClinicRows.Field<string>("type") == "Clinic"
                                               select ClinicRows).ToList();

                    List<DataRow> hospitalRow = (from HospitalRows in dsHospital.Tables[0].AsEnumerable()
                                                 where HospitalRows.Field<string>("type") == "Hospital"
                                                 select HospitalRows).ToList();

                    foreach (DataRow row in clinicRow)
                    {
                        ClinicInfo clinic = new ClinicInfo();

                        clinic.nameEN = row["engname"].ToString();
                        clinic.nameTH = row["thname"].ToString();
                        //thaddress+' '+thtumbon+' '+thcity+' '+thlocation+' '+postcode 
                        clinic.addressTH = row["thaddress"].ToString().Trim() + " " + row["thtumbon"].ToString().Trim() + " " + row["thcity"].ToString().Trim() + " " + row["thlocation"].ToString().Trim() + " " + row["postcode"].ToString().Trim();
                        clinic.addressEN = row["engaddress"].ToString().Trim() + " " + row["engtumbon"].ToString().Trim() + " " + row["engcity"].ToString().Trim() + " " + row["englocation"].ToString().Trim() + " " + row["postcode"].ToString().Trim();
                        clinic.telephone = row["tel"].ToString();
                        clinic.fax = row["fax"].ToString();
                        clinic.IPDGroup = row["ipdgroup"].ToString();
                        clinic.OPDGroup = row["opdgroup"].ToString();
                        clinic.Dental = row["dentalgroup"].ToString();
                        clinic.latitude = row["lat"].ToString();
                        clinic.longtitude = row["long"].ToString();
                        clinic.nameforMap = row["engname"].ToString() + " " + row["thname"].ToString();
                        clinic.provinceTH = row["thlocation"].ToString();
                        clinic.provinceEN = row["englocation"].ToString();
                        clinic.cityTH = row["thcity"].ToString();
                        clinic.cityEN = row["engcity"].ToString();
                        clinic.tumbonTH = row["thtumbon"].ToString();
                        clinic.tumbonEN = row["engtumbon"].ToString();
                        res.clinicsInfo.Add(clinic);

                    }
                    // assign hospital

                    foreach (DataRow row in hospitalRow)
                    {
                        HospitalInfo hospital = new HospitalInfo();

                        hospital.nameEN = row["engname"].ToString();
                        hospital.nameTH = row["thname"].ToString();
                        //thaddress+' '+thtumbon+' '+thcity+' '+thlocation+' '+postcode 
                        hospital.addressTH = row["thaddress"].ToString().Trim() + " " + row["thtumbon"].ToString().Trim() + " " + row["thcity"].ToString().Trim() + " " + row["thlocation"].ToString().Trim() + " " + row["postcode"].ToString().Trim();
                        hospital.addressEN = row["engaddress"].ToString().Trim() + " " + row["engtumbon"].ToString().Trim() + " " + row["engcity"].ToString().Trim() + " " + row["englocation"].ToString().Trim() + " " + row["postcode"].ToString().Trim();
                        hospital.telephone = row["tel"].ToString();
                        hospital.fax = row["fax"].ToString();
                        hospital.IPDGroup = row["ipdgroup"].ToString();
                        hospital.OPDGroup = row["opdgroup"].ToString();
                        hospital.Dental = row["dentalgroup"].ToString();
                        hospital.latitude = row["lat"].ToString();
                        hospital.longtitude = row["long"].ToString();
                        hospital.nameforMap = row["engname"].ToString() + " " + row["thname"].ToString();
                        hospital.provinceTH = row["thlocation"].ToString();
                        hospital.provinceEN = row["englocation"].ToString();
                        hospital.cityTH = row["thcity"].ToString();
                        hospital.cityEN = row["engcity"].ToString();
                        hospital.tumbonTH = row["thtumbon"].ToString();
                        hospital.tumbonEN = row["engtumbon"].ToString();
                        res.hospitalInfo.Add(hospital);

                    }
                }

                DataSet dsNews = member.GetNewsInfo();

                if (null != dsNews)
                {
                    List<DataRow> newInfo = (from NewsRows in dsNews.Tables[0].AsEnumerable()
                                             select NewsRows).ToList();

                    foreach (DataRow row in newInfo)
                    {
                        NewInfo news = new NewInfo();

                        news.titleTH = row["title"].ToString();
                        news.titleEN = row["title_en"].ToString();
                        news.link = row["link"].ToString();
                        news.detail = row["detail"].ToString();
                        news.attachTH = row["attach"].ToString();
                        news.attachEN = row["attach_en"].ToString();

                        res.newsInfo.Add(news);
                    }
                }

                DataSet dsHealthTip = member.GetHealthTipsAll();

                if (null != dsHealthTip)
                {
                    List<DataRow> healthTip = (from HealthTipRow in dsHealthTip.Tables[0].AsEnumerable()
                                               select HealthTipRow).ToList();

                    foreach (DataRow row in healthTip)
                    {
                        HealthTip health = new HealthTip();

                        health.fileID = row["FILE_ID"].ToString();
                        health.fileName = row["FILE_NAME"].ToString();
                        health.fileDescription = row["FILE_DESCRIPT"].ToString();

                        res.healthTipsInfo.Add(health);

                    }
                }

                DataSet dsMediaNews = member.GetMediaNews();

                if (null != dsMediaNews)
                {
                    List<DataRow> mediaNews = (from MediaNewsRow in dsMediaNews.Tables[0].AsEnumerable()
                                               select MediaNewsRow).ToList();

                    foreach (DataRow row in mediaNews)
                    {
                        MediaNews mNews = new MediaNews();
                        mNews.title = row["title"].ToString();
                        mNews.detail = row["detail"].ToString();
                        mNews.pictureLink = row["picture_link"].ToString();
                        mNews.coverPictureLink = row["cover_picture_link"].ToString();

                        res.mediaNews.Add(mNews);
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }
        }

        public void UpdateMemberInfo(UpdateMemberInfoRequest req, UpdateMemberInfoResponse res)
        {
            log.Info("in");

            try
            {
                MemberModel model = new MemberModel();

                // get member information.
                DataSet dsMember = model.GetMemberInfo(req.memberNo, req.policyNo);

                if (null != dsMember && dsMember.Tables[0].Rows.Count > 0)
                {

                    if (model.UpdateMemberInfo(req.mobileNo, req.email, req.lineID, req.memberNo, req.policyNo, dsMember.Tables[0].Rows[0]["email"].ToString(), dsMember.Tables[0].Rows[0]["tel"].ToString()))
                    {
                        AuditHelper.UpdateTransEventLog("PersonalInfoUpdate", req.memberNo, req.policyNo, EnumMasEventLogId.PersonalInfoUpdate, req.osPlatform, req.clientIPAddress);
                        res.errorCode = "0";
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }
        }
        public void GetECard(GetECardRequest req, GetECardResponse res)
        {
            log.Info("in");
            try
            {
                MemberModel model = new MemberModel();

                DataSet dsECard = model.GetECard(req.policyNo, req.memberNo);

                if (null != dsECard)
                {
                    res.token = req.token;
                    res.policyNo = req.policyNo;

                    // Tirachit B. : Fix Clear Image Cache : 31 Mar 2020
                    //res.signaturePic = "docs/ecard_signature.png";
                    string current_date = DateTime.Now.ToString();
                    res.signaturePic = "docs/ecard_signature.png?" + current_date;

                    List<DataRow> eCardList = (from ECardRow in dsECard.Tables[0].AsEnumerable()
                                               select ECardRow).ToList();

                    if (null != eCardList && eCardList.Count > 0)
                    {

                        foreach (DataRow row in eCardList)
                        {
                            bool bHasUnlimit = false;
                            string strStartDate = row["datestrt"].ToString().Substring(6, 2) + "/" + row["datestrt"].ToString().Substring(4, 2) + "/" + row["datestrt"].ToString().Substring(0, 4);
                            string strEndDate = row["dateend"].ToString().Substring(6, 2) + "/" + row["dateend"].ToString().Substring(4, 2) + "/" + row["dateend"].ToString().Substring(0, 4);

                            DateTime dtStartDate = new DateTime(Convert.ToInt32(row["datestrt"].ToString().Substring(0, 4)), Convert.ToInt32(row["datestrt"].ToString().Substring(4, 2)), Convert.ToInt32(row["datestrt"].ToString().Substring(6, 2)));
                            DateTime dtEndDate = new DateTime(Convert.ToInt32(row["dateend"].ToString().Substring(0, 4)), Convert.ToInt32(row["dateend"].ToString().Substring(4, 2)), Convert.ToInt32(row["dateend"].ToString().Substring(6, 2)));

                            if (DateTime.Today >= dtStartDate && DateTime.Today <= dtEndDate)
                            {
                                if (null == row["subname"] || row["subname"].ToString().Equals(""))
                                {
                                    res.policyHolder = row["cownname"].ToString();
                                }
                                else
                                {
                                    res.policyHolder = row["subname"].ToString();
                                }
                              
                                string strStartMember = row["mbrno"].ToString().PadLeft(5, '0');
                                string strEndMember = row["dpntno"].ToString().PadLeft(2, '0');

                                // get member nationality first, default is EN, 
                                String strNat = model.GetMemberNationality(req.policyNo, strStartMember + "-" + strEndMember);
                                if (strNat.Trim().ToUpper().Equals("TH"))
                                    strNat = "TH";
                                else
                                    strNat = "EN";
                                
                                // GTLB
                                Decimal dSumGTLBFX, dSumGTLBSA, dSumGADFX, dSumGADSA, dSumGHSRB, dSumGPTDFX,
                                    dSumGPTDSA, dSumER, dSumGHO1, dSumGHO2, dSumGME, dSumGOL, dSumGDTL, dSumHME,
                                    dCopay_ghs, dCopay_hme, dCopay_er, dCopay_gho, dCopay_gol, dCopay_gdtl, dCopay_gme;
                                String strClaimLifeGTL = "", strClaimLifeGPTD = "", strClaimBenefit1GAD = "", strClaimBenefit2IPD = "", strClaimBenefit3ER = "",
                                    strClaimBenefit4OPD_OPD_Dental = "", strClaimBenefit5Accident = "", strClaimBenefit6 = "", strClaimBenefitDental = "",
                                    strClaimBenefitHME = "",
                                    strPlanNo, strTxtGHS, strTxtER, strTxtGHO, strTxtGME,
                                    strTxtGOL,
                                    strGenPage01, strGenPage02, strGenPage03, strGenPage04, strGenPage05,
                                    strGenPage06, strGenPage07, strGenPage08,
                                    strflgghs, strflger, strflggho, strflggdtl, strflggme, strflggol,
                                    strClassinsgho, policyNo;


                                dSumGTLBFX = Convert.ToDecimal(row["sumgtlbfx"].ToString());
                                dSumGTLBSA = Convert.ToDecimal(row["sumgtlbsa"].ToString());
                                dSumGPTDFX = Convert.ToDecimal(row["sumgptdfx"].ToString());
                                dSumGPTDSA = Convert.ToDecimal(row["sumgptdsa"].ToString());
                                dSumGADFX = Convert.ToDecimal(row["sumgadfx"].ToString());
                                dSumGADSA = Convert.ToDecimal(row["sumgadsa"].ToString());
                                dSumGHSRB = Convert.ToDecimal(row["sumghsrb"].ToString());
                                dSumER = Convert.ToDecimal(row["sumer"].ToString());
                                dSumGME = Convert.ToDecimal(row["sumgme"].ToString());
                                dSumGOL = Convert.ToDecimal(row["sumgol"].ToString());
                                dSumGHO1 = Convert.ToDecimal(row["sumgho1"].ToString());
                                dSumGHO2 = Convert.ToDecimal(row["sumgho2"].ToString());
                                dSumGDTL = Convert.ToDecimal(row["sumgdtl"].ToString());
                                dSumHME = row.IsNull("sumhme") ? 0 : Convert.ToDecimal(row["sumhme"].ToString());
                                dCopay_ghs = row.IsNull("copay_ghs") ? 0 : Convert.ToDecimal(row["copay_ghs"].ToString());
                                dCopay_hme = row.IsNull("copay_hme") ? 0 : Convert.ToDecimal(row["copay_hme"].ToString());
                                dCopay_er = row.IsNull("copay_er") ? 0 : Convert.ToDecimal(row["copay_er"].ToString());
                                dCopay_gho = row.IsNull("copay_gho") ? 0 : Convert.ToDecimal(row["copay_gho"].ToString());
                                dCopay_gol = row.IsNull("copay_gol") ? 0 : Convert.ToDecimal(row["copay_gol"].ToString());
                                dCopay_gdtl = row.IsNull("copay_gdtl") ? 0 : Convert.ToDecimal(row["copay_gdtl"].ToString());
                                dCopay_gme = row.IsNull("copay_gme") ? 0 : Convert.ToDecimal(row["copay_gme"].ToString());

                                strTxtGHS = row["txtghs"].ToString();
                                strPlanNo = row["planno"].ToString();
                                strTxtER = row["txter"].ToString();
                                strTxtGHO = row["txtgho"].ToString();
                                strTxtGME = row["txtgme"].ToString();
                                strTxtGOL = row["txtgol"].ToString();
                                strGenPage01 = row["genpage01"].ToString();
                                strGenPage02 = row["genpage02"].ToString();
                                strGenPage03 = row["genpage03"].ToString();
                                strGenPage04 = row["genpage04"].ToString();
                                strGenPage05 = row["genpage05"].ToString();
                                strGenPage06 = row["genpage06"].ToString();
                                strGenPage07 = row["genpage07"].ToString();
                                strGenPage08 = row["genpage08"].ToString();

                                strflgghs = row["flgghs"].ToString();
                                strflger = row["flger"].ToString();
                                strflggho = row["flggho"].ToString();
                                strflggdtl = row["flggdtl"].ToString();
                                strflggme = row["flggme"].ToString();
                                strflggol = row["flggol"].ToString();
                                strClassinsgho = row["classins_gho"].ToString();
                                policyNo = row["chdrnum"].ToString().Trim();

                                const string opdMemberFamily = "กรณีเบิกค่ารักษาแบบคนไข้นอก (OPD) คู่สมรสและบุตร สำรองจ่ายทุกกรณี";
                                string[] cards = {"CARD09", "CARD10", "CARD11"};//Pending ไปก่อน  
                                bool displayEngString = false;
                                if (cards.Contains(strGenPage01) || (strGenPage01.Equals("CARD08")))
                                {
                                    //"CARD08","CARD09", "CARD10", "CARD11" แสดงภาษาอังกฤษเท่านั้น
                                    displayEngString = true;
                                }

                                if (dSumGPTDFX > 0)
                                {
                                    //if ((!strGenPage01.Equals("CARD06")) && (!strGenPage01.Equals("CARD04")) && (!strGenPage01.Equals("CARD07")))
                                    if ((strGenPage01.Equals("CARD01")) || (strGenPage01.Equals("CARD02")) || (strGenPage01.Equals("CARD03")) || (strGenPage01.Equals("CARD09")))
                                    {
                                        if (strNat.Equals("TH") && !displayEngString)
                                        {
                                            strClaimLifeGPTD = "GPTD: " + SetNumberNoDecimalPoint(dSumGPTDFX.ToString()) + " บาท";
                                        }
                                        else
                                        {
                                            strClaimLifeGPTD = "GPTD: " + SetNumberNoDecimalPoint(dSumGPTDFX.ToString()) + " THB";
                                        }
                                    }
                                }
                                else if (dSumGPTDSA > 0)
                                {
                                    //if ((!strGenPage01.Equals("CARD06")) && (!strGenPage01.Equals("CARD04")) && (!strGenPage01.Equals("CARD07")))
                                    if ((strGenPage01.Equals("CARD01")) || (strGenPage01.Equals("CARD02")) || (strGenPage01.Equals("CARD03")) || (strGenPage01.Equals("CARD09"))) 
                                    {
                                        if (strNat.Equals("TH") && !displayEngString)
                                        {
                                            strClaimLifeGPTD = "GPTD: " + SetNumberNoDecimalPoint(dSumGPTDSA.ToString()) + " เท่าของเงินเดือน";
                                        }
                                        else
                                        {
                                            strClaimLifeGPTD = "GPTD: " + SetNumberNoDecimalPoint(dSumGPTDSA.ToString()) + " BMS or FCL";


                                        }
                                    }

                                }
                                // GTLB
                                if (dSumGTLBFX > 0)
                                {
                                    //if ((!strGenPage01.Equals("CARD06")) && (!strGenPage01.Equals("CARD04")) && (!strGenPage01.Equals("CARD07")))
                                    if ((strGenPage01.Equals("CARD01")) || (strGenPage01.Equals("CARD02")) || (strGenPage01.Equals("CARD03")) || (strGenPage01.Equals("CARD09")))
                                    {
                                        if (strNat.Equals("TH") && !displayEngString)
                                        {
                                            strClaimLifeGTL = "GTL: " + SetNumberNoDecimalPoint(dSumGTLBFX.ToString()) + " บาท";
                                        }
                                        else
                                        {
                                            strClaimLifeGTL = "GTL: " + SetNumberNoDecimalPoint(dSumGTLBFX.ToString()) + " THB";
                                        }
                                    }
                                }
                                else if (dSumGTLBSA > 0)
                                {
                                    //if ((!strGenPage01.Equals("CARD06")) && (!strGenPage01.Equals("CARD04")) && (!strGenPage01.Equals("CARD07")))
                                    if ((strGenPage01.Equals("CARD01")) || (strGenPage01.Equals("CARD02")) || (strGenPage01.Equals("CARD03")) || (strGenPage01.Equals("CARD09")))
                                    {
                                        if (strNat.Equals("TH") && !displayEngString)
                                        {
                                            strClaimLifeGTL = "GTL: " + SetNumberNoDecimalPoint(dSumGTLBSA.ToString()) + " เท่าของเงินเดือน";
                                        }
                                        else
                                        {
                                            strClaimLifeGTL = "GTL: " + SetNumberNoDecimalPoint(dSumGTLBSA.ToString()) + " BMS or FCL";

                                        }
                                    }
                                }


                                // GAD
                                if (dSumGADFX > 0)
                                {
                                    //if ((!strGenPage01.Equals("CARD06")) && (!strGenPage01.Equals("CARD04")) && (!strGenPage01.Equals("CARD07")))
                                    if ((strGenPage01.Equals("CARD01")) || (strGenPage01.Equals("CARD02")) || (strGenPage01.Equals("CARD03")) || (strGenPage01.Equals("CARD09")))
                                    {
                                        if (strNat.Equals("TH") && !displayEngString)
                                        {
                                            strClaimBenefit1GAD = "GAD: " + SetNumberNoDecimalPoint(dSumGADFX.ToString()) + " บาท";
                                        }
                                        else
                                        {
                                            strClaimBenefit1GAD = "GAD: " + SetNumberNoDecimalPoint(dSumGADFX.ToString()) + " THB";
                                        }
                                    }

                                }
                                else if (dSumGADSA > 0)
                                {
                                    //if ((!strGenPage01.Equals("CARD06")) && (!strGenPage01.Equals("CARD04")) && (!strGenPage01.Equals("CARD07")))
                                    if ((strGenPage01.Equals("CARD01")) || (strGenPage01.Equals("CARD02")) || (strGenPage01.Equals("CARD03")) || (strGenPage01.Equals("CARD09")))
                                    {
                                        if (strNat.Equals("TH") && !displayEngString)
                                        {
                                            strClaimBenefit1GAD = "GAD: " + SetNumberNoDecimalPoint(dSumGADSA.ToString()) + " เท่าของเงินเดือน";
                                        }
                                        else
                                        {
                                            strClaimBenefit1GAD = "GAD: " + SetNumberNoDecimalPoint(dSumGADSA.ToString()) + " BMS or FCL";
                                        }
                                    }
                                }

                                // IPD

                                if (strTxtGHS.Trim().ToUpper().Equals("UNLIMITED"))
                                {
                                    bHasUnlimit = true;
                                    if (strNat.Equals("TH") && !displayEngString)
                                    {
                                        strClaimBenefit2IPD = "IPD Plan " + strPlanNo + ": (R&B:ค่ารักษาพยาบาลไม่มีข้อยกเว้น)";
                                    }
                                    else
                                    {
                                        strClaimBenefit2IPD = "IPD Plan " + strPlanNo + ": (R&B:Unlimited)";
                                    }
                                }
                                else if (dSumGHSRB > 0)
                                {
                                    if (!strGenPage01.Trim().ToUpper().Equals("CARD08") && !strGenPage01.Trim().ToUpper().Equals("CARD11")) //sunisa add condition on 01/01/2020
                                    {
                                        if (strNat.Equals("TH") && !displayEngString)
                                        {
                                            strClaimBenefit2IPD = "IPD Plan " + strPlanNo + ": (R&B:" + SetNumberNoDecimalPoint(dSumGHSRB.ToString()) + " บาท/วัน) " + strTxtGHS;
                                        }
                                        else
                                        {
                                            strClaimBenefit2IPD = "IPD Plan " + strPlanNo + ": (R&B:" + SetNumberNoDecimalPoint(dSumGHSRB.ToString()) + " THB/Day) " + strTxtGHS;
                                        }
                                    }
                                    else
                                    {
                                        //CARD08,CARD11
                                        strClaimBenefit2IPD = "IPD R&B " + SetNumberNoDecimalPoint(dSumGHSRB.ToString()) + " THB/Day ";
                                    }
                                    
                                }
                                //CARD09 จะแสดง IPD เมื่อ strflgghs เป็น Y/S  [sunisa add on 25/03/2020]
                                //cards ==> {CARD09,CARD10,CARD11}
                                if (cards.Contains(strGenPage01.Trim().ToUpper()))
                                {
                                    if (!strflgghs.ToUpper().Equals("Y") && !strflgghs.ToUpper().Equals("S"))
                                    {
                                        strClaimBenefit2IPD = string.Empty; // strflgghs ไม่ใช่ Y/S จะไม่แสดง IPD
                                    }
                                }

                                // ER
                                if (!strGenPage01.Trim().ToUpper().Equals("CARD07") && !cards.Contains(strGenPage01.Trim().ToUpper()))
                                {
                                    //sunisa add on 08/05/2020
                                    strClaimBenefit3ER = setERTextForEcard(dSumER, strGenPage01.Trim().ToUpper().ToString(), strTxtER, strflger, strNat, displayEngString, ref bHasUnlimit);

                                    if (strGenPage01.Trim().ToUpper().Equals("CARD03") && !string.IsNullOrEmpty(strClaimBenefit3ER))
                                    {
                                        strClaimBenefit3ER = ""; // Reset ค่าเพื่อหาค่าที่แท้จริงของ CARD03
                                        if (string.IsNullOrEmpty(strTxtER))
                                        {
                                            if (strNat.Equals("TH") && !displayEngString)
                                            {
                                                strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " บาท/ครั้ง";
                                            }
                                            else
                                            {
                                                strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " THB/Time";
                                            }
                                        }
                                        else if (strTxtER.Trim().ToUpper().Equals("UNLIMITED"))
                                        {
                                            bHasUnlimit = true;
                                            if (strNat.Equals("TH") && !displayEngString)
                                            {
                                                strClaimBenefit3ER = "ER: ค่ารักษาพยาบาลไม่มีข้อยกเว้น";
                                            }
                                            else
                                            {
                                                strClaimBenefit3ER = "ER: Unlimited";
                                            }
                                        }
                                        else if (!strTxtER.Trim().ToUpper().Equals(""))
                                        {
                                            bool isNumeric = Regex.IsMatch(strTxtER, @"\d");
                                            if (isNumeric)
                                            {
                                                strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " THB/Accident " + strTxtER + " hrs.";
                                            }
                                        }
                                    }
                                    
                                    #region "Old ER Code"
                                    //sunisa comment on 08/05/2020
                                    //if (strTxtER.Trim().ToUpper().Equals("UNLIMITED"))
                                    //{
                                    //    bHasUnlimit = true;
                                    //    if (strNat.Equals("TH") && !displayEngString)
                                    //    {
                                    //        strClaimBenefit3ER = "ER: ค่ารักษาพยาบาลไม่มีข้อยกเว้น";
                                    //    }
                                    //    else
                                    //    {
                                    //        strClaimBenefit3ER = "ER: Unlimited";

                                    //    }
                                    //}
                                    //else if (strTxtER.Trim().ToUpper().Equals("") && dSumER > 0)
                                    //{
                                    //    if (strNat.Equals("TH") && !displayEngString)
                                    //    {
                                    //        strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " บาท/ครั้ง";
                                    //    }
                                    //    else
                                    //    {
                                    //        strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " THB/Time";
                                    //    }
                                    //}
                                    //else if ((!strTxtER.Trim().ToUpper().Equals("")) && dSumER > 0)
                                    //{
                                    //    if (strNat.Equals("TH") && !displayEngString)
                                    //    {
                                    //        strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " บาท/อุบัติเหตุ " + strTxtER + " ชั่วโมง";
                                    //    }
                                    //    else
                                    //    {
                                    //        strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " THB/Accident " + strTxtER + " hrs.";

                                    //    }
                                    //}
                                    #endregion
                                }
                                else if (cards.Contains(strGenPage01.Trim().ToUpper())) //CARD09,10,11
                                {
                                    if (!string.IsNullOrEmpty(strflger) && !strflger.ToUpper().Equals("N"))
                                    {
                                        strClaimBenefit3ER = setERTextForEcard(dSumER, strGenPage01.Trim().ToUpper().ToString(), strTxtER, strflger, strNat, displayEngString, ref bHasUnlimit);
                                    }
                                }
                                else
                                {
                                    // if card07
                                    bHasUnlimit = true;
                                    if (strNat.Equals("TH"))
                                    {
                                        strClaimBenefit3ER = "ER: ค่ารักษาพยาบาลไม่มีข้อยกเว้น";
                                    }
                                    else
                                    {
                                        strClaimBenefit3ER = "ER: Unlimited";

                                    }
                                }

                                // OPD or OPD + Dental [CARD02,CARD03 ไม่มี OPD]
                                if (!strGenPage01.Trim().ToUpper().Equals("CARD07"))
                                {
                                    //Sunisa Add on 05/05/2020 
                                    string OPDBenefit = "";
                                    switch (strGenPage01.Trim().ToUpper().ToString())
                                    {
                                        case "CARD01":
                                            if (strTxtGHO.Trim().ToUpper().Equals("UNLIMITED"))
                                            {
                                                bHasUnlimit = true;
                                                strClaimBenefit4OPD_OPD_Dental = "OPD: Unlimited";
                                            }
                                            else if (strTxtGHO.Trim().Equals("สำรองจ่าย"))
                                            {
                                                if (strNat.Equals("TH"))
                                                {
                                                    strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " บาท/ปี " + strTxtGHO;
                                                }
                                                else
                                                {
                                                    strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " THB/Year " + strTxtGHO;
                                                }
                                            }
                                            else if (strTxtGHO.Trim().Equals(""))
                                            {
                                                if (dSumGHO1 > 0)
                                                {
                                                    if (strNat.Equals("TH"))
                                                    {
                                                        strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " บาท/ครั้ง/วัน ";
                                                    }
                                                    else
                                                    {
                                                        strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " THB/Visit/Day ";
                                                    }
                                                }
                                                else if (dSumGHO2 > 0)
                                                {
                                                    if (strNat.Equals("TH"))
                                                    {
                                                        strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " บาท/ปี ";
                                                    }
                                                    else
                                                    {
                                                        strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " THB/Year ";
                                                    }
                                                }
                                            }
                                            break;
                                        case "CARD04":
                                        case "CARD05":
                                        case "CARD06":
                                            OPDBenefit = setOPDTextForEcard(dSumGHO1, dSumGHO2, strTxtGHO, strNat, displayEngString);
                                            if (!string.IsNullOrEmpty(OPDBenefit))
                                            {
                                                strClaimBenefit4OPD_OPD_Dental = "OPD: " + OPDBenefit;
                                                if (!string.IsNullOrEmpty(strTxtGHO))
                                                {
                                                    strClaimBenefit4OPD_OPD_Dental += strTxtGHO;
                                                }
                                            }                                          
                                            break;
                                        case "CARD08":
                                            OPDBenefit = setOPDTextForEcard(dSumGHO1, dSumGHO2, strTxtGHO, strNat, displayEngString);
                                            if (!string.IsNullOrEmpty(OPDBenefit)) {
                                                strClaimBenefit4OPD_OPD_Dental = "OPD Cash " + OPDBenefit;
                                                if (!string.IsNullOrEmpty(strTxtGHO))
                                                {
                                                    strClaimBenefit4OPD_OPD_Dental += strTxtGHO;
                                                }
                                                if (strClassinsgho.Equals("MF"))
                                                {
                                                    //OPD Member Family
                                                    strClaimBenefit4OPD_OPD_Dental += "/Family";
                                                }
                                                else if ((strClassinsgho.Equals("MO") && !strEndMember.Equals("00")))
                                                {
                                                    strClaimBenefit4OPD_OPD_Dental = "";
                                                }
                                            }
                                            break;
                                        case "CARD09":
                                        case "CARD10":
                                            OPDBenefit = setOPDTextForEcard(dSumGHO1, dSumGHO2, strTxtGHO, strNat, displayEngString);
                                            if (!string.IsNullOrEmpty(OPDBenefit))
                                            {
                                                if (strflggho.ToUpper().Equals("Y") || strflggho.ToUpper().Equals("M"))
                                                {
                                                    strClaimBenefit4OPD_OPD_Dental = "OPD: " + OPDBenefit;
                                                }
                                                else if (!strflggho.ToUpper().Equals("N") && !string.IsNullOrEmpty(strflggho))
                                                {
                                                    strClaimBenefit4OPD_OPD_Dental = "OPD&Dental: " + OPDBenefit;
                                                }
                                            }
                                            break;
                                        case "CARD11":
                                            string[] policies = { "G0002487", "G0002488", "G0002489", "G0002490" }; // policies 4 policy นี้มีแค่ OPD
                                            OPDBenefit = setOPDTextForEcard(dSumGHO1, dSumGHO2, strTxtGHO, strNat, displayEngString);
                                            if (!string.IsNullOrEmpty(OPDBenefit))
                                            {
                                                if (strflggho.ToUpper().Equals("Y") || strflggho.ToUpper().Equals("M"))
                                                {
                                                    strClaimBenefit4OPD_OPD_Dental = "OPD: " + OPDBenefit;
                                                }
                                                else if (!policies.Contains(policyNo) && !strflggho.ToUpper().Equals("N") && !string.IsNullOrEmpty(strflggho))
                                                {
                                                    strClaimBenefit4OPD_OPD_Dental = "OPD&Dental: " + OPDBenefit;
                                                }
                                            }
                                            break;
                                        default:
                                            break;
                                    }

                                    //Sunisa comment on 05/05/2020
                                    #region "OPD Old Code"
                                    //if (strTxtGHO.Trim().ToUpper().Equals("UNLIMITED"))
                                    //{
                                    //    bHasUnlimit = true;
                                    //    if (strNat.Equals("TH") && !displayEngString)
                                    //    {
                                    //        strClaimBenefit4OPD_OPD_Dental = "OPD: ค่ารักษาพยาบาลไม่มีข้อยกเว้น";
                                    //    }
                                    //    else
                                    //    {
                                    //        strClaimBenefit4OPD_OPD_Dental = "OPD: Unlimited";

                                    //    }

                                    //}
                                    //else if (dSumGHO1 > 0) //Per Visit
                                    //{
                                    //    //sunisa modify on 24/04/2020
                                    //    DataSet dsMemberBen = model.GetMemberBenByBenCode(req.policyNo, strStartMember + "-" + strEndMember, "BH01");
                                    //    if (!cards.Contains(strGenPage01.Trim().ToUpper()) && !strGenPage01.Equals("CARD08") && null != dsMemberBen) //Card 1-7
                                    //    {
                                    //        //Card 1-7
                                    //        DataRow MemberBen = (from MemberBenRow in dsMemberBen.Tables[0].AsEnumerable()
                                    //                             select MemberBenRow).First();

                                    //        if (null != MemberBen)
                                    //        {
                                    //            Decimal dPerVis = Convert.ToDecimal(MemberBen["PERVIS"].ToString());
                                    //            Decimal dPerYear = Convert.ToDecimal(MemberBen["PERYEAR"].ToString());

                                    //            if (dPerYear > 0)
                                    //            {
                                    //                if (strNat.Equals("TH"))
                                    //                {
                                    //                    strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " บาท/ปี " + strTxtGHO;
                                    //                }
                                    //                else
                                    //                {
                                    //                    strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " THB/Year " + strTxtGHO;
                                    //                }
                                    //            }
                                    //            else if (dPerVis > 0)
                                    //            {
                                    //                if (strNat.Equals("TH"))
                                    //                {
                                    //                    strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " บาท/ครั้ง/วัน " + strTxtGHO;
                                    //                }
                                    //                else
                                    //                {
                                    //                    strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " THB/Visit/Day " + strTxtGHO;
                                    //                }
                                    //            }
                                    //        }
                                    //    }

                                    //    if (strGenPage01.Equals("CARD08"))
                                    //    {
                                    //        //sunisa add on 01/04/2020
                                    //        strClaimBenefit4OPD_OPD_Dental = "OPD Cash " + SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " THB/Year " + strTxtGHO;
                                    //        if (strClassinsgho.Equals("MF"))
                                    //        {
                                    //            //OPD Member Family strGenPage07
                                    //            strClaimBenefit4OPD_OPD_Dental += "/Family";
                                    //        }
                                    //    }
                                    //    else if (cards.Contains(strGenPage01.Trim().ToUpper()))
                                    //    {
                                    //        //CARD09,CARD10,CARD11
                                    //        if (!strflggho.ToUpper().Equals("Y") && !strflggho.ToUpper().Equals("M"))
                                    //        {
                                    //            strClaimBenefit4OPD_OPD_Dental = string.Empty; // ไม่แสดง OPD ถ้า strflggho ไม่ใช่  Y/M
                                    //        }
                                    //        if (!string.IsNullOrEmpty(strTxtGHO))
                                    //        {
                                    //            strClaimBenefit4OPD_OPD_Dental = strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " THB/Visit/Day (" + strTxtGHO + " Times/Year)";
                                    //        }
                                    //        else
                                    //        {
                                    //            strClaimBenefit4OPD_OPD_Dental = strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " THB/Visit/Day";
                                    //        }
                                    //    }
                                    //}
                                    //else if (dSumGHO2 > 0) //Per Year
                                    //{
                                    //    if (strGenPage01.Equals("CARD01") || strGenPage01.Equals("CARD10"))
                                    //    {
                                    //        if (strNat.Equals("TH") && !displayEngString)
                                    //        {
                                    //            strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " บาท/ปี " + strTxtGHO;
                                    //        }
                                    //        else
                                    //        {
                                    //            strClaimBenefit4OPD_OPD_Dental = "OPD: " + SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " THB/Year " + strTxtGHO;

                                    //        }
                                    //    }
                                    //    else if (strGenPage01.Equals("CARD08")) //sunisa add on 01/04/2020
                                    //    {
                                    //        strClaimBenefit4OPD_OPD_Dental = "OPD Cash " + SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " THB/Year " + strTxtGHO;
                                    //        if (strClassinsgho.Equals("MF"))
                                    //        {
                                    //            //OPD Member Family strGenPage07
                                    //            strClaimBenefit4OPD_OPD_Dental += "/Family";
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        if (strNat.Equals("TH") && !displayEngString)
                                    //        {
                                    //            strClaimBenefit4OPD_OPD_Dental = "OPD&Dental: " + SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " บาท/ปี " + strTxtGHO;
                                    //        }
                                    //        else
                                    //        {
                                    //            strClaimBenefit4OPD_OPD_Dental = "OPD&Dental: " + SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " THB/Year " + strTxtGHO;
                                    //        }
                                    //    }

                                    //    //CARD09 จะแสดง OPD&Dental กรณี strflggho not in ("","N")
                                    //    if (strGenPage01.Trim().ToUpper().Equals("CARD10")) //strGenPage01.Trim().ToUpper().Equals("CARD09") || 
                                    //    {
                                    //        if (strflggho.ToUpper().Equals("") || strflggho.ToUpper().Equals("N"))
                                    //        {
                                    //            strClaimBenefit4OPD_OPD_Dental = string.Empty; // ไม่แสดง OPD&Dental ถ้า strflggho เป็น "" หรือ N
                                    //        }
                                    //    }
                                    //}
                                    #endregion
                                }
                                else
                                {  
                                    //CARD07
                                    // GHO Unlimited
                                    bHasUnlimit = true;
                                    if (strNat.Equals("TH") && !displayEngString)
                                    {
                                        strClaimBenefit4OPD_OPD_Dental = "OPD: ค่ารักษาพยาบาลไม่มีข้อยกเว้น";
                                    }
                                    else
                                    {
                                        strClaimBenefit4OPD_OPD_Dental = "OPD: Unlimited";
                                    }
                                }
                                
                                //DENTAL จะแสดง  Dental กรณี flggdtl <>""  [sunisa add on 25/03/2020]
                                if (dSumGDTL > 0 && !string.IsNullOrEmpty(strflggdtl))
                                {
                                    //cards ==> {CARD09,CARD10,CARD11}
                                    if (cards.Contains(strGenPage01.Trim().ToUpper()))
                                    {
                                        if (strNat.Equals("TH") && !displayEngString)
                                        {
                                            strClaimBenefitDental = "Dental: " + SetNumberNoDecimalPoint(dSumGDTL.ToString()) + " บาท";
                                        }
                                        else
                                        {
                                            strClaimBenefitDental = "Dental: " + SetNumberNoDecimalPoint(dSumGDTL.ToString()) + " THB.";
                                        }
                                    }
                                }                                

                                // Accident
                                if (!strGenPage01.Trim().ToUpper().Equals("CARD07"))
                                {
                                    if (strTxtGME.Trim().ToUpper().Equals("UNLIMITED"))
                                    {
                                        bHasUnlimit = true;
                                        if (strNat.Equals("TH") && !displayEngString)
                                        {
                                            strClaimBenefit5Accident = "AME: ค่ารักษาพยาบาลไม่มีข้อยกเว้น";
                                        }
                                        else
                                        {
                                            strClaimBenefit5Accident = "AME: Unlimited";
                                        }
                                    }
                                    else if (dSumGME > 0)
                                    {
                                        if (strNat.Equals("TH") && !displayEngString)
                                        {
                                            strClaimBenefit5Accident = "AME: " + SetNumberNoDecimalPoint(dSumGME.ToString()) + " บาท/ครั้ง " + strTxtGME;
                                        }
                                        else
                                        {
                                            strClaimBenefit5Accident = "AME: " + SetNumberNoDecimalPoint(dSumGME.ToString()) + " THB/Time " + strTxtGME;
                                        }
                                    }
                                    
                                    //CARD09 จะแสดง AME กรณี strflggme ==> (strflggme = "Y"  OR (strTxtGME <>"" AND strflggme IN ("","N")))  [sunisa add on 25/03/2020]
                                    if (strGenPage01.Trim().ToUpper().Equals("CARD09"))
                                    {
                                        if (!(strflggme.ToUpper().Equals("Y") || (!string.IsNullOrEmpty(strTxtGME) && (strflggme.ToUpper().Equals("N") || strflggme.ToUpper().Equals("")))))
                                        {
                                            strClaimBenefit5Accident = string.Empty; // strflggme <> Y OR (strTxtGME = "" AND strflggme NOT IN ("","N"))
                                        }
                                    }
                                    else if (strGenPage01.Trim().ToUpper().Equals("CARD10"))
                                    {
                                        //CARD10 จะแสดง AME กรณี dSumGME > 0
                                        if (dSumGME <= 0)
                                        {
                                            strClaimBenefit5Accident = string.Empty;
                                        }
                                    }
                                }

                                // X-Ray and LAB
                                if (!strGenPage01.Trim().ToUpper().Equals("CARD07"))
                                {
                                    if (strTxtGOL.Trim().ToUpper().Equals("UNLIMITED"))
                                    {
                                        bHasUnlimit = true;
                                        if (!strGenPage01.Equals("CARD04"))
                                        {
                                            if (strNat.Equals("TH") && !displayEngString)
                                            {
                                                strClaimBenefit6 = "Xray&Lab: ค่ารักษาพยาบาลไม่มีข้อยกเว้น";
                                            }
                                            else
                                            {
                                                strClaimBenefit6 = "Xray&Lab: Unlimited";
                                            }
                                        }
                                    }
                                    else if (dSumGOL > 0)
                                    {
                                        if (!strGenPage01.Equals("CARD04"))
                                        {
                                            if (strNat.Equals("TH") && !displayEngString)
                                            {
                                                strClaimBenefit6 = "Xray&Lab: " + SetNumberNoDecimalPoint(dSumGOL.ToString()) + " บาท/ปี " + strTxtGOL;
                                            }
                                            else
                                            {
                                                strClaimBenefit6 = "Xray&Lab: " + SetNumberNoDecimalPoint(dSumGOL.ToString()) + " THB/Year " + strTxtGOL;
                                            }
                                        }
                                    }

                                    //CARD09 จะแสดง Xray&Lab กรณี strflggol = Y  [sunisa add on 25/03/2020]
                                    if (strGenPage01.Trim().ToUpper().Equals("CARD09") || strGenPage01.Trim().ToUpper().Equals("CARD10"))
                                    {
                                        if (!strflggol.ToUpper().Equals("Y"))
                                        {
                                            strClaimBenefit6 = string.Empty; // strflger จะไม่แสดง ER เป็น "" หรือ N
                                        }
                                    }
                                }

                                //CARD11 IBM
                                if (strGenPage01.Trim().ToUpper().Equals("CARD11"))
                                {
                                    string[] policiesNo = { "G0002487", "G0002488", "G0002489", "G0002490" };
                                    decimal percentage = 100;
                                    if (policiesNo.Contains(policyNo))
                                    {
                                        //IPD
                                        if(dSumGHSRB <= 0)
                                        {
                                            strClaimBenefit2IPD = string.Empty;
                                        }
                                        else if(dCopay_ghs > 0)
                                        {
                                            //Add Co-pay
                                            dCopay_ghs = percentage - dCopay_ghs;
                                            strClaimBenefit2IPD += " (Co-pay" + dCopay_ghs.ToString().Replace(".00", string.Empty) + "%)";
                                        }

                                        //HME
                                        if (dSumHME > 0)
                                        {
                                            if (strNat.Equals("TH") && !displayEngString)
                                            {
                                                strClaimBenefitHME = "Hosp Exp " + SetNumberNoDecimalPoint(dSumHME.ToString()) + " บาท";
                                                if (dCopay_hme > 0)
                                                {
                                                    dCopay_hme = percentage - dCopay_hme;
                                                    strClaimBenefitHME += " (Co-pay" + dCopay_hme.ToString().Replace(".00", string.Empty) + "%)";
                                                }
                                            }
                                            else
                                            {
                                                strClaimBenefitHME = "Hosp Exp " + SetNumberNoDecimalPoint(dSumHME.ToString()) + " THB.";
                                                if (dCopay_hme > 0)
                                                {
                                                    dCopay_hme = percentage - dCopay_hme;
                                                    strClaimBenefitHME += " (Co-pay" + dCopay_hme.ToString().Replace(".00", string.Empty) + "%)";
                                                }
                                            }
                                        }

                                        //ER strClaimBenefit3ER ถูก Assign ค่าตาม Condition ด้านบนมาแล้วจึงเช็คว่ามีค่าหรือไม่ โดยไม่ต้องเช็ค Condition ซ้ำ
                                        if (!string.IsNullOrEmpty(strClaimBenefit3ER) && dCopay_er > 0)
                                        {
                                            //dCopay_er > 0 ==> Add Co-pay
                                            dCopay_er = percentage - dCopay_er;
                                            strClaimBenefit3ER += " (Co-pay" + dCopay_er.ToString().Replace(".00", string.Empty) + "%)";
                                        }

                                        //OPD
                                        if (!string.IsNullOrEmpty(strClaimBenefit4OPD_OPD_Dental) && dCopay_gho > 0)
                                        {
                                            //dCopay_gho > 0 ==> Add Co-pay
                                            dCopay_gho = percentage - dCopay_gho;
                                            strClaimBenefit4OPD_OPD_Dental += " (Co-pay" + dCopay_gho.ToString().Replace(".00", string.Empty) + "%)";
                                        }

                                        //X-ray&Lab 
                                        if (!string.IsNullOrEmpty(strClaimBenefit6) && dCopay_gol > 0)
                                        {
                                            //dCopay_gol > 0 ==> Add Co-pay
                                            dCopay_gol = percentage - dCopay_gol;
                                            strClaimBenefit6 += " (Co-pay" + dCopay_gol.ToString().Replace(".00", string.Empty) + "%)";
                                        }

                                        //Dental 
                                        if (!string.IsNullOrEmpty(strClaimBenefitDental) && dCopay_gdtl > 0)
                                        {
                                            //dCopay_gdtl > 0 ==> Add Co-pay
                                            dCopay_gdtl = percentage - dCopay_gdtl;
                                            strClaimBenefitDental += " (Co-pay" + dCopay_gdtl.ToString().Replace(".00", string.Empty) + "%)";
                                        }

                                        //AME 
                                        if (!string.IsNullOrEmpty(strClaimBenefit5Accident) && dCopay_gme > 0)
                                        {
                                            //dCopay_gme > 0 ==> Add Co-pay
                                            dCopay_gme = percentage - dCopay_gme;
                                            strClaimBenefit5Accident += " (Co-pay" + dCopay_gme.ToString().Replace(".00", string.Empty) + "%)";
                                        }

                                    }
                                    else
                                    { //General Policy
                                       
                                        //IPD
                                        if (dSumGHSRB <= 0)
                                        {
                                            strClaimBenefit2IPD = string.Empty;
                                        }
                                        else if (dCopay_ghs > 0)
                                        {
                                            //Add Co-pay
                                            strClaimBenefit2IPD += " (Co-pay" + dCopay_ghs.ToString().Replace(".00", string.Empty) + "%)";
                                        }

                                        //HME
                                        if (dSumHME > 0)
                                        {
                                            if (strNat.Equals("TH") && !displayEngString)
                                            {
                                                strClaimBenefitHME = "Hosp Exp " + SetNumberNoDecimalPoint(dSumHME.ToString()) + " บาท";
                                                if (dCopay_hme > 0)
                                                {
                                                    dCopay_hme = percentage - dCopay_hme;
                                                    strClaimBenefitHME += " (Co-pay" + dCopay_hme.ToString().Replace(".00", string.Empty) + "%)";
                                                }
                                            }
                                            else
                                            {
                                                strClaimBenefitHME = "Hosp Exp " + SetNumberNoDecimalPoint(dSumHME.ToString()) + " THB.";
                                                if (dCopay_hme > 0)
                                                {
                                                    dCopay_hme = percentage - dCopay_hme;
                                                    strClaimBenefitHME += " (Co-pay" + dCopay_hme.ToString().Replace(".00", string.Empty) + "%)";
                                                }
                                            }
                                        }

                                        //ER strClaimBenefit3ER ถูก Assign ค่าตาม Condition ด้านบนมาแล้วจึงเช็คว่ามีค่าหรือไม่ โดยไม่ต้องเช็ค Condition ซ้ำ
                                        if (!string.IsNullOrEmpty(strClaimBenefit3ER) && dCopay_er > 0)
                                        {
                                            //dCopay_er > 0 ==> Add Co-pay
                                            dCopay_er = percentage - dCopay_er;
                                            strClaimBenefit3ER += " (Co-pay" + dCopay_er.ToString().Replace(".00", string.Empty) + "%)";
                                        }
                                        
                                        //X-ray&Lab 
                                        if (!string.IsNullOrEmpty(strClaimBenefit6) && dCopay_gol > 0)
                                        {
                                            //dCopay_gol > 0 ==> Add Co-pay
                                            dCopay_gol = percentage - dCopay_gol;
                                            strClaimBenefit6 += " (Co-pay" + dCopay_gol.ToString().Replace(".00", string.Empty) + "%)";
                                        }

                                        //Dental 
                                        if (!string.IsNullOrEmpty(strClaimBenefitDental) && dCopay_gdtl > 0)
                                        {
                                            //dCopay_gdtl > 0 ==> Add Co-pay
                                            dCopay_gdtl = percentage - dCopay_gdtl;
                                            strClaimBenefitDental += " (Co-pay" + dCopay_gdtl.ToString().Replace(".00", string.Empty) + "%)";
                                        }

                                        //AME 
                                        if (!string.IsNullOrEmpty(strClaimBenefit5Accident) && dCopay_gme > 0)
                                        {
                                            //dCopay_gme > 0 ==> Add Co-pay
                                            dCopay_gme = percentage - dCopay_gme;
                                            strClaimBenefit5Accident += " (Co-pay" + dCopay_gme.ToString().Replace(".00", string.Empty) + "%)";
                                        }
                                    }
                                }

                                if (strGenPage01.Equals("CARD08"))
                                {
                                    if (!strEndMember.Equals("00"))
                                    {
                                        //Clear Remark มุมซ้ายล่างที่จะแสดงใน E-Card กรณีคู่สมรสและบุตร ก่อนไปทำการ Set ค่าใหม่ตามเงื่อนไข
                                        strGenPage07 = string.Empty;
                                        strGenPage08 = string.Empty;

                                        if (strClassinsgho.Equals("MF"))
                                        {
                                            strGenPage06 = opdMemberFamily;
                                        }
                                        else
                                        {
                                            strGenPage06 = string.Empty;
                                        }
                                    }
                                    
                                }


                                /********************* FILL DATA **************************/
                                if (strEndMember.Equals("00"))
                                {
                                    EmployeeECard emp = new EmployeeECard();

                                    emp.memberTypeTh = "พนักงาน";
                                    emp.memberTypeEn = "Employee";
                                    emp.empMemberNo = strStartMember + "-" + strEndMember;
                                    emp.empFName = row["mbrsalut"].ToString() + " " + row["surnamel"].ToString();
                                    emp.empLName = row["givnamel"].ToString();

                                    // get employee number
                                    String empNum = model.GetEmployeeNum(req.policyNo, emp.empMemberNo);
                                    if (null != empNum && (!empNum.Equals("")))
                                    {
                                        emp.insuranceNo = "(" + empNum + ")";
                                    }
                                    else
                                        emp.insuranceNo = "";

                                    emp.policyStartDT = strStartDate;
                                    emp.policyEndDT = strEndDate;
                                    emp.claimLifeGTL = strClaimLifeGTL;
                                    emp.claimLifeGPTD = strClaimLifeGPTD;
                                    emp.claimBenefit1 = strClaimBenefit1GAD;
                                    emp.claimBenefit2 = strClaimBenefit2IPD;
                                    emp.claimBenefit3 = strClaimBenefit3ER;
                                    emp.claimBenefit4 = strClaimBenefit4OPD_OPD_Dental;
                                    emp.claimBenefit5 = strClaimBenefit5Accident;
                                    emp.claimBenefit6 = strClaimBenefit6;
                                    emp.claimBenefit7 = strClaimBenefitDental;
                                    emp.claimBenefit8 = strClaimBenefitHME;

                                    if (strGenPage01.Trim().Equals("CARD07"))
                                    {
                                        emp.claimBenefit6 += "(Full credit for all exclusions.)";
                                    }
                                    emp.genPage01 = strGenPage01;
                                    emp.genPage02 = strGenPage02;
                                    emp.genPage03 = strGenPage03;
                                    emp.genPage04 = strGenPage04;
                                    emp.genPage05 = strGenPage05;
                                    emp.genPage06 = strGenPage06;
                                    emp.genPage07 = strGenPage07;
                                    emp.genPage08 = strGenPage08;
                                    emp.countryCode = strNat;
                                    res.employeeECard = emp;

                                }
                                else if (strEndMember.Equals("01") || strEndMember.Equals("30") || strEndMember.Equals("31") ||
                                         strEndMember.Equals("32") || strEndMember.Equals("33") || strEndMember.Equals("34") ||
                                         strEndMember.Equals("35") || strEndMember.Equals("36") || strEndMember.Equals("37") ||
                                         strEndMember.Equals("38") || strEndMember.Equals("39")
                                         )
                                {
                                    // spouse
                                    SpouseECard sp = new SpouseECard();

                                    sp.memberTypeTh = "คู่สมรส";
                                    sp.memberTypeEn = "Spouse";
                                    sp.spuMemberNo = strStartMember + "-" + strEndMember;
                                    sp.spuFName = row["mbrsalut"].ToString() + " " + row["surnamel"].ToString();
                                    sp.spuLName = row["givnamel"].ToString();
                                    String empNum = model.GetEmployeeNum(req.policyNo, sp.spuMemberNo);
                                    if (null != empNum && (!empNum.Equals("")))
                                    {
                                        sp.insuranceNo = "(" + empNum + ")";
                                    }
                                    else
                                        sp.insuranceNo = "";
                                    sp.policyStartDT = strStartDate;
                                    sp.policyEndDT = strEndDate;
                                    sp.claimLifeGTL = strClaimLifeGTL;
                                    sp.claimLifeGPTD = strClaimLifeGPTD;
                                    sp.claimBenefit1 = strClaimBenefit1GAD;
                                    sp.claimBenefit2 = strClaimBenefit2IPD;
                                    sp.claimBenefit3 = strClaimBenefit3ER;
                                    sp.claimBenefit4 = strClaimBenefit4OPD_OPD_Dental;
                                    sp.claimBenefit5 = strClaimBenefit5Accident;
                                    sp.claimBenefit6 = strClaimBenefit6;
                                    sp.claimBenefit7 = strClaimBenefitDental;
                                    sp.claimBenefit8 = strClaimBenefitHME;
                                    if (strGenPage01.Trim().Equals("CARD07"))
                                    {
                                        sp.claimBenefit6 += "(Full credit for all exclusions.)";
                                    }
                                    
                                    sp.genPage01 = strGenPage01;
                                    sp.genPage02 = strGenPage02;
                                    sp.genPage03 = strGenPage03;
                                    sp.genPage04 = strGenPage04;
                                    sp.genPage05 = strGenPage05;
                                    sp.genPage06 = strGenPage06;
                                    sp.genPage07 = strGenPage07;
                                    sp.genPage08 = strGenPage08;
                                    sp.countryCode = strNat;
                                    res.spouseECard = sp;
                                }
                                else
                                {
                                    // children
                                    ChildECard child = new ChildECard();

                                    child.memberTypeTh = "บุตร";
                                    child.memberTypeEn = "Children";
                                    child.childMemberNo = strStartMember + "-" + strEndMember;
                                    child.childFName = row["mbrsalut"].ToString() + " " + row["surnamel"].ToString();
                                    child.childLName = row["givnamel"].ToString();
                                    String empNum = model.GetEmployeeNum(req.policyNo, child.childMemberNo);
                                    if (null != empNum && (!empNum.Equals("")))
                                    {
                                        child.insuranceNo = "(" + empNum + ")";
                                    }
                                    else
                                        child.insuranceNo = "";
                                    child.policyStartDT = strStartDate;
                                    child.policyEndDT = strEndDate;
                                    child.claimLifeGTL = strClaimLifeGTL;
                                    child.claimLifeGPTD = strClaimLifeGTL;
                                    child.claimBenefit1 = strClaimBenefit1GAD;
                                    child.claimBenefit2 = strClaimBenefit2IPD;
                                    child.claimBenefit3 = strClaimBenefit3ER;
                                    child.claimBenefit4 = strClaimBenefit4OPD_OPD_Dental;
                                    child.claimBenefit5 = strClaimBenefit5Accident;
                                    child.claimBenefit6 = strClaimBenefit6;
                                    child.claimBenefit7 = strClaimBenefitDental;
                                    child.claimBenefit8 = strClaimBenefitHME;
                                    if (strGenPage01.Trim().Equals("CARD07"))
                                    {
                                        child.claimBenefit6 += "(Full credit for all exclusions.)";
                                    }

                                    child.genPage01 = strGenPage01;
                                    child.genPage02 = strGenPage02;
                                    child.genPage03 = strGenPage03;
                                    child.genPage04 = strGenPage04;
                                    child.genPage05 = strGenPage05;
                                    child.genPage06 = strGenPage06;
                                    child.genPage07 = strGenPage07;
                                    child.genPage08 = strGenPage08;
                                    child.countryCode = strNat;
                                    res.childrenECard.Add(child);
                                }
                            }
                        } // end foreach

                        // update ecard
                        UpdateECardForWeb(res.policyNo, res.policyHolder, res.employeeECard, res.spouseECard, res.childrenECard);


                    }
                    res.errorCode = "0";
                }

                else
                {
                    throw new BusinessExpection(14, res);
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }
        }

        private string setERTextForEcard(decimal dSumER, string cardType ,string strTxtER, string strFlgER, string strNat, bool displayEngString, ref bool bHasUnlimit)
        {
            string strClaimBenefit3ER = "";
            string inputflgER = strFlgER.Trim().ToUpper().ToString();
            string[] flgers = { "Y", "2", "3" };
            if (dSumER > 0)
            {
                if (strTxtER.Trim().ToUpper().Equals("UNLIMITED"))
                {
                    bHasUnlimit = true;
                    if (strNat.Equals("TH") && !displayEngString)
                    {
                        strClaimBenefit3ER = "ER: ค่ารักษาพยาบาลไม่มีข้อยกเว้น";
                    }
                    else
                    {
                        strClaimBenefit3ER = "ER: Unlimited";
                    }
                }
                else if (!cardType.Equals("CARD01") && flgers.Contains(inputflgER))
                {
                    string hr = "";
                    switch (strFlgER.Trim().ToUpper().ToString())
                    {
                        case "Y":
                            hr = "24";
                            break;
                        case "2":
                            hr = "48";
                            break;
                        case "3":
                            hr = "72";
                            break;
                        default:
                            break;
                    }

                    if (strNat.Equals("TH") && !displayEngString)
                    {
                        strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " บาท/อุบัติเหตุ " + hr + " ชั่วโมง";
                    }
                    else
                    {
                        strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " THB/Accident " + hr + " hrs.";
                    }
                }
                else if (strTxtER.Trim().ToUpper().Equals(""))
                {
                    if (strNat.Equals("TH") && !displayEngString)
                    {
                        strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " บาท/ครั้ง";
                    }
                    else
                    {
                        strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " THB/Time";
                    }
                }
                else {
                    if (strNat.Equals("TH") && !displayEngString)
                    {
                        strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " บาท/ครั้ง " + strTxtER;
                    }
                    else
                    {
                        strClaimBenefit3ER = "ER: " + SetNumberNoDecimalPoint(dSumER.ToString()) + " THB/Time " + strTxtER;
                    }
                }              
            }
            
            return strClaimBenefit3ER;
        }

        private string setOPDTextForEcard(decimal dSumGHO1, decimal dSumGHO2, string strTxtGHO, string strNat, bool displayEngString)
        {
            string strClaimBenefit4OPD_OPD_Dental = "";
            if (strTxtGHO.Trim().ToUpper().Equals("UNLIMITED"))
            {
                if (strNat.Equals("TH") && !displayEngString)
                {
                    strClaimBenefit4OPD_OPD_Dental = "ค่ารักษาพยาบาลไม่มีข้อยกเว้น";
                }
                else
                {
                    strClaimBenefit4OPD_OPD_Dental = "Unlimited";
                }
            }
            else if (strTxtGHO.Trim().Equals("สำรองจ่าย"))
            {
                if (strNat.Equals("TH") && !displayEngString)
                {
                    strClaimBenefit4OPD_OPD_Dental = SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " บาท/ปี สำรองจ่าย";
                }
                else
                {
                    strClaimBenefit4OPD_OPD_Dental = SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " THB./Year Reserve";
                }
            }
            else
            {
                if (dSumGHO1 > 0)
                {
                    if (strNat.Equals("TH") && !displayEngString)
                    {
                        strClaimBenefit4OPD_OPD_Dental = SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " บาท/ครั้ง/วัน ";
                    }
                    else
                    {
                        strClaimBenefit4OPD_OPD_Dental = SetNumberNoDecimalPoint(dSumGHO1.ToString()) + " THB./Visit/Day ";
                    }

                    bool isNumeric = Regex.IsMatch(strTxtGHO, @"\d");
                    if (!string.IsNullOrEmpty(strTxtGHO.Trim().ToString()) && isNumeric)
                    {
                        strClaimBenefit4OPD_OPD_Dental += "(" + strTxtGHO + " Times/Year)";
                    }
                    else if (!string.IsNullOrEmpty(strTxtGHO))
                    {
                        strClaimBenefit4OPD_OPD_Dental += strTxtGHO;
                    }
                }
                else if (dSumGHO2 > 0)
                {
                    if (strNat.Equals("TH") && !displayEngString)
                    {
                        strClaimBenefit4OPD_OPD_Dental = SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " บาท/ปี ";
                    }
                    else
                    {
                        strClaimBenefit4OPD_OPD_Dental = SetNumberNoDecimalPoint(dSumGHO2.ToString()) + " THB./Year ";
                    }

                    if (!string.IsNullOrEmpty(strTxtGHO))
                    {
                        strClaimBenefit4OPD_OPD_Dental += strTxtGHO;
                    }
                }

            }

            return strClaimBenefit4OPD_OPD_Dental;
        }

        public void UpdateECardForWeb(string policy, string policyHolder, EmployeeECard emp, SpouseECard spu, List<ChildECard> listChild)
        {
            log.Info("in");

            try
            {
                // employee 
                MemberModel model = new MemberModel();
                string strIssDate = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString();

                if (null != emp.empMemberNo)
                {
                    if (!model.IsExistECardForWeb(policy, emp.empMemberNo))
                    {
                        EcardParams param = new EcardParams
                        {
                            memberNo = emp.empMemberNo,
                            policyStartDT = emp.policyStartDT,
                            policyEndDT = emp.policyEndDT,
                            firstName = emp.empFName,
                            lastName = emp.empLName,
                            claimLifeGTL = emp.claimLifeGTL,
                            claimBenefit1 = emp.claimBenefit1,
                            claimBenefit2 = emp.claimBenefit2,
                            claimBenefit3 = emp.claimBenefit3,
                            claimBenefit4 = emp.claimBenefit4,
                            claimBenefit5 = emp.claimBenefit5,
                            claimBenefit6 = emp.claimBenefit6,
                            claimBenefit7 = emp.claimBenefit7,
                            claimBenefit8 = emp.claimBenefit8,
                            genPage01 = emp.genPage01,
                            genPage02 = emp.genPage02,
                            genPage03 = emp.genPage03,
                            genPage04 = emp.genPage04,
                            genPage05 = emp.genPage05,
                            genPage06 = emp.genPage06,
                            genPage07 = emp.genPage07,
                            genPage08 = emp.genPage08
                        };
                        model.UpdateECard(policy, strIssDate, policyHolder, "", param);

                    }
                }
                if (null != spu.spuMemberNo)
                {
                    if (!model.IsExistECardForWeb(policy, spu.spuMemberNo))
                    {
                        //UpdateECard(policy, spu.spuMemberNo, strIssDate, policyHolder, spu.policyStartDT, spu.policyEndDT, "", spu.spuFName, spu.spuLName, spu.claimLifeGTL, spu.claimBenefit1, spu.claimBenefit2, spu.claimBenefit3, spu.claimBenefit4, spu.claimBenefit5, spu.claimBenefit6, spu.genPage01, spu.genPage02, spu.genPage03, spu.genPage04, spu.genPage05, spu.genPage06, spu.genPage07, spu.genPage08);
                        EcardParams param = new EcardParams
                        {
                            memberNo = spu.spuMemberNo,
                            policyStartDT = spu.policyStartDT,
                            policyEndDT = spu.policyEndDT,
                            firstName = spu.spuFName,
                            lastName = spu.spuLName,
                            claimLifeGTL = spu.claimLifeGTL,
                            claimBenefit1 = spu.claimBenefit1,
                            claimBenefit2 = spu.claimBenefit2,
                            claimBenefit3 = spu.claimBenefit3,
                            claimBenefit4 = spu.claimBenefit4,
                            claimBenefit5 = spu.claimBenefit5,
                            claimBenefit6 = spu.claimBenefit6,
                            claimBenefit7 = spu.claimBenefit7,
                            claimBenefit8 = spu.claimBenefit8,
                            genPage01 = spu.genPage01,
                            genPage02 = spu.genPage02,
                            genPage03 = spu.genPage03,
                            genPage04 = spu.genPage04,
                            genPage05 = spu.genPage05,
                            genPage06 = spu.genPage06,
                            genPage07 = spu.genPage07,
                            genPage08 = spu.genPage08
                        };
                        model.UpdateECard(policy, strIssDate, policyHolder, "", param);
                    }
                }
                foreach (ChildECard child in listChild)
                {
                    if (null != child.childMemberNo)
                    {
                        if (!model.IsExistECardForWeb(policy, child.childMemberNo))
                        {
                            //model.UpdateECard(policy, child.childMemberNo, strIssDate, policyHolder, child.policyStartDT, child.policyEndDT, "", child.childFName, child.childLName, child.claimLifeGTL, child.claimBenefit1, child.claimBenefit2, child.claimBenefit3, child.claimBenefit4, child.claimBenefit5, child.claimBenefit6, child.genPage01, child.genPage02, child.genPage03, child.genPage04, child.genPage05, child.genPage06, child.genPage07, child.genPage08);
                            EcardParams param = new EcardParams
                            {
                                memberNo = child.childMemberNo,
                                policyStartDT = child.policyStartDT,
                                policyEndDT = child.policyEndDT,
                                firstName = child.childFName,
                                lastName = child.childLName,
                                claimLifeGTL = child.claimLifeGTL,
                                claimBenefit1 = child.claimBenefit1,
                                claimBenefit2 = child.claimBenefit2,
                                claimBenefit3 = child.claimBenefit3,
                                claimBenefit4 = child.claimBenefit4,
                                claimBenefit5 = child.claimBenefit5,
                                claimBenefit6 = child.claimBenefit6,
                                claimBenefit7 = child.claimBenefit7,
                                claimBenefit8 = child.claimBenefit8,
                                genPage01 = child.genPage01,
                                genPage02 = child.genPage02,
                                genPage03 = child.genPage03,
                                genPage04 = child.genPage04,
                                genPage05 = child.genPage05,
                                genPage06 = child.genPage06,
                                genPage07 = child.genPage07,
                                genPage08 = child.genPage08
                            };
                            model.UpdateECard(policy, strIssDate, policyHolder, "", param);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
            finally
            {
                log.Info("out");
            }

        }
        public void GetHealthTipDetail(GetHealthTipDetailRequest req, GetHealthTipDetailResponse res)
        {
            log.Info("in");
            try
            {
                MemberModel model = new MemberModel();
                DataSet dsHealthTipDetail = model.GetHealthTipDetail(Convert.ToInt32(req.fileID));

                if (null != dsHealthTipDetail)
                {
                    DataRow healthTip = (from HealthTipRow in dsHealthTipDetail.Tables[0].AsEnumerable()
                                         select HealthTipRow).First();


                    HealthTip health = new HealthTip();

                    res.fileID = healthTip["FILE_ID"].ToString();
                    res.fileName = healthTip["FILE_NAME"].ToString();
                    res.fileType = healthTip["FILE_TYPE"].ToString();
                    res.fileDateShow = healthTip["DATE_SHOW"].ToString();
                    byte[] value = (byte[])(healthTip["FILE_DATA"]);


                    //if (res.fileType.Trim().ToUpper().Equals("JPG"))
                    //{
                    //    res.fileDataForDisplay = "data:image/jpg;base64, " + Convert.ToBase64String(value);
                    //}
                    //else if (res.fileType.Trim().ToUpper().Equals("PDF"))
                    //{
                    //    res.fileDataForDisplay = "data:application/pdf;base64, " + Convert.ToBase64String(value); ;
                    //}
                    res.fileDescription = healthTip["FILE_DESCRIPT"].ToString();




                    //res.fileData = Convert.ToBase64String(value);
                    // create file on path 
                    var filePath = ConfigurationManager.AppSettings.Get("HealthTipPath") + "\\" + res.fileID;

                    if (res.fileType.Trim().ToUpper().Equals("JPG"))
                    {
                        filePath = filePath + ".JPG";
                        log.Debug(filePath);
                        if (!File.Exists(filePath))
                        {
                            var bytes = value;
                            using (var imageFile = new FileStream(filePath, FileMode.Create))
                            {
                                imageFile.Write(bytes, 0, bytes.Length);
                                imageFile.Flush();
                            }
                        }

                        res.filePath = "docs/" + res.fileID + ".JPG";

                    }
                    else if (res.fileType.Trim().ToUpper().Equals("PDF"))
                    {
                        filePath = filePath + ".PDF";
                        log.Debug(filePath);
                        if (!File.Exists(filePath))
                        {
                            var bytes = value;
                            using (var imageFile = new FileStream(filePath, FileMode.Create))
                            {
                                imageFile.Write(bytes, 0, bytes.Length);
                                imageFile.Flush();
                            }
                        }
                        res.filePath = "docs/" + res.fileID + ".PDF";
                    }

                }
                res.errorCode = "0";

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }
        }

        public void UpdateClaimNotifyKey(UpdateClaimNotifyKeyRequest req, UpdateClaimNotifyKeyResponse res)
        {
            log.Info("in");
            try
            {
                MemberModel model = new MemberModel();
                // get record AllClaimDetail by Claim Type, then order by date,
                DataSet dsAllClaim = model.GetAllClaim(req.policyNo, req.memberNo, req.visitType);

                List<DataRow> listAllClaimVisitType = null;
                DateTime dtLast = new DateTime();
                if (null != dsAllClaim)
                {
                    try
                    {
                        listAllClaimVisitType = (from AllClaimRow in dsAllClaim.Tables[0].AsEnumerable()
                                                 select AllClaimRow).ToList();
                        // get last date first.
                        foreach (DataRow dr in listAllClaimVisitType)
                        {
                            DateTime dtTemp = new DateTime(Convert.ToInt32(dr["START_DATE_HOSPITAL"].ToString().Substring(6, 4)), Convert.ToInt32(dr["START_DATE_HOSPITAL"].ToString().Substring(3, 2)), Convert.ToInt32(dr["START_DATE_HOSPITAL"].ToString().Substring(0, 2)));
                            if (dtTemp > dtLast)
                                dtLast = dtTemp;

                        }

                    }
                    catch (Exception)
                    {
                        res.errorCode = "0";
                        log.Info("no all claim");
                    }
                }

                string strClaimId = "", strClaimDate = "";

                if (null != listAllClaimVisitType)
                    strClaimDate = dtLast.Day.ToString().PadLeft(2, '0') + "/" + dtLast.Month.ToString().PadLeft(2, '0') + "/" + dtLast.Year;


                int i = 0;
                if (null != listAllClaimVisitType)
                {
                    foreach (DataRow dr in listAllClaimVisitType)
                    {
                        if (dr["START_DATE_HOSPITAL"].ToString().Equals(strClaimDate))
                        {
                            if (i == 0)
                            {
                                strClaimId = dr["CLAMNUM"].ToString();
                                i++;
                            }
                            else
                            {
                                strClaimId += "," + dr["CLAMNUM"].ToString();
                                i++;
                            }
                        }

                    }
                    // check notify table if any record exist.a.Field<string>("MEMBERNO")

                    DataSet dsNotify = model.GetCalimNotify(req.policyNo, req.memberNo);
                    // get only the type that wanted.
                    DataRow drNotifyByVisitType = null;
                    if (null != dsNotify)
                    {
                        try
                        {
                            drNotifyByVisitType = (from NotifyByVisitRow in dsNotify.Tables[0].AsEnumerable()
                                                   select NotifyByVisitRow).Where(a => a.Field<string>("VISIT_TYPE").Equals(req.visitType)).First();
                        }
                        catch (Exception)
                        {
                            log.Info("no notify by visit type:" + req.visitType);
                        }
                    }
                    // if no then insert
                    if (null == drNotifyByVisitType)
                    {
                        model.InsertClaimNotify(req.policyNo, req.memberNo, req.visitType, strClaimId, strClaimDate);
                    }

                    else
                    {
                        // if yes, then update by key.
                        model.UpdateClaimNotify(req.policyNo, req.memberNo, req.visitType, strClaimId, strClaimDate);
                    }

                    res.errorCode = "0";
                }
                else
                {
                    res.errorCode = "0";
                    res.log = "no data for allclaim";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }

        }

        public void GetIPDClaimDetail(GetIPDClaimDetailRequest req, GetIPDClaimDetailResponse res)
        {
            log.Info("in");
            bool bGHS = false;
            bool bGMJ = false;
            bool bGMT = false;
            try
            {
                MemberModel model = new MemberModel();

                DataSet dsMember = model.GetMemberDetail(req.policy, req.memberNo);
                DataSet dsClaim = model.GetAllClaim(req.policy, req.memberNo, "IPD");

                if (null != dsMember)
                {
                    res.policyNo = dsMember.Tables[0].Rows[0]["CHDRNUM"].ToString();
                    res.memberNo = dsMember.Tables[0].Rows[0]["MEMBERNO"].ToString();
                    res.policyStartDT = dsMember.Tables[0].Rows[0]["CCDATE04"].ToString();
                    res.policyEndDT = dsMember.Tables[0].Rows[0]["CRDATE04"].ToString();
                    res.fName = dsMember.Tables[0].Rows[0]["LGIVNAME"].ToString();
                    res.lName = dsMember.Tables[0].Rows[0]["LSURNAME"].ToString();
                    res.visityType = "IPD";

                    CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
                    if (null != dsClaim)
                    {
                        int i = 0, claimOverTotNo = 0, claimMatTotNo = 0;
                        decimal claimTotAmt = 0, claimOverTotAmt = 0, claimMatTotAmt = 0;

                        foreach (DataRow dr in dsClaim.Tables[0].Rows)
                        {
                            if (dr["PRODUCT_TYPE_CODE"].ToString().Equals("GHS"))
                                bGHS = true;
                            else if (dr["PRODUCT_TYPE_CODE"].ToString().Equals("GMJ"))
                                bGMJ = true;
                            else if (dr["PRODUCT_TYPE_CODE"].ToString().Equals("GMT"))
                                bGMT = true;



                            IPDClaimDetail ipdTempDup = res.claimIPDs.Where(a => a.claimNumber.Equals(dr["CLAMNUM"].ToString()) && a.DependencepNo.Equals(dr["DEPENDENCEP_NO"].ToString())).FirstOrDefault();

                            string strClaimApprvAmt;
                            bool bDup = false;
                            if (null != ipdTempDup)
                            {
                                bDup = true;
                                // has duplicate record.
                                ipdTempDup.claimAmt = (Convert.ToDecimal(ipdTempDup.claimAmt) + Convert.ToDecimal(dr["INCURRED"].ToString())).ToString();
                                ipdTempDup.claimApprvAmt = (Convert.ToDecimal(ipdTempDup.claimApprvAmt) + Convert.ToDecimal(dr["HMOSHARE"].ToString())).ToString();
                                ipdTempDup.claimOverAmt = (Convert.ToDecimal(ipdTempDup.claimOverAmt) + Convert.ToDecimal(dr["MBRSHARE"].ToString())).ToString();
                                ipdTempDup.claimAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(ipdTempDup.claimAmt)));
                                ipdTempDup.claimApprvAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(ipdTempDup.claimApprvAmt)));
                                ipdTempDup.claimOverAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(ipdTempDup.claimOverAmt)));

                                strClaimApprvAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["HMOSHARE"].ToString())));


                            }
                            else
                            {
                                // fill in response.


                                IPDClaimDetail ipd = new IPDClaimDetail();

                                ipd.productCode = dr["PRODUCT_TYPE_CODE"].ToString();
                                ipd.DependencepNo = dr["DEPENDENCEP_NO"].ToString();
                                ipd.startDate = dr["START_DATE_HOSPITAL"].ToString();
                                ipd.endDate = dr["END_DATE_HOSPITAL"].ToString();
                                ipd.claimHospitalTH = dr["HOSPITAL"].ToString();
                                if (null != dr["HOSPITAL_NAME_ENG"])
                                    ipd.claimHospitalEN = dr["HOSPITAL_NAME_ENG"].ToString();
                                else
                                    ipd.claimHospitalEN = "";
                                ipd.claimDiagnoseTH = dr["DIAGNOSE_TH"].ToString();
                                ipd.claimDiagnoseEN = dr["DIAGNOSE_EN"].ToString();
                                ipd.claimType = dr["CLAIM_TYPE"].ToString();
                                ipd.claimTypeDesc = dr["CLAIM_TYPE_DESC"].ToString();
                                ipd.claimAuthDT = dr["DATE_AUTH"].ToString().Substring(6, 2) + "/" + dr["DATE_AUTH"].ToString().Substring(4, 2) + "/" + dr["DATE_AUTH"].ToString().Substring(0, 4);
                                ipd.claimStatTH = dr["GCSTSTHNAME"].ToString();
                                ipd.claimStatEN = dr["GCSTSENNAME"].ToString();
                                ipd.claimAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["INCURRED"].ToString())));
                                ipd.claimApprvAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["HMOSHARE"].ToString())));
                                ipd.claimOverAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["MBRSHARE"].ToString())));
                                ipd.claimNumber = dr["CLAMNUM"].ToString();
                                if (ipd.claimStatEN.Equals("PENDING") || ipd.claimStatEN.Equals("REJECT"))
                                    ipd.claimAuthDT = "";

                                strClaimApprvAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["HMOSHARE"].ToString())));


                                // adjust status for display
                                if (ipd.claimStatEN.Trim().Equals("PENDING") && dr["DATA_FROM"].ToString().Trim().Equals("H"))
                                {
                                    ipd.claimStatEN = "Waiting bill payment";
                                    ipd.claimStatTH = "รอการวางบิลจากโรงพยาบาล";
                                }
                                res.claimIPDs.Add(ipd);
                                //i++;
                            }

                            // end of dup or no dup
                            if (dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVED") || dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVE") || dr["GCSTSENNAME"].ToString().Trim().Equals("อนุมัติ") || dr["GCSTSENNAME"].ToString().Trim().Equals("PENDING"))
                            {
                                //claimTotAmt += Convert.ToDecimal(ipd.claimApprvAmt);
                                if (dr["PRODUCT_TYPE_CODE"].ToString().Equals("GHS"))
                                {
                                    if (!bDup)

                                        i++;

                                    claimTotAmt += Convert.ToDecimal(strClaimApprvAmt);
                                }
                                if (dr["PRODUCT_TYPE_CODE"].ToString().Equals("GMJ"))
                                {
                                    if (!bDup)
                                        claimOverTotNo++;

                                    claimOverTotAmt += Convert.ToDecimal(strClaimApprvAmt);
                                }
                                if (dr["PRODUCT_TYPE_CODE"].ToString().Equals("GMT"))
                                {
                                    if (!bDup)
                                        claimMatTotNo++;

                                    claimMatTotAmt += Convert.ToDecimal(strClaimApprvAmt);
                                }

                            }
                        }

                        // summary claims detail.
                        if (bGHS)
                        {
                            res.claimTotNo = i.ToString();
                            res.claimTotAmt = SetNumberNoDecimalPoint(claimTotAmt.ToString());
                        }
                        else
                        {
                            res.claimTotNo = "0";
                            res.claimTotAmt = "0";
                        }

                        if (bGMJ)
                        {
                            res.claimOverTotNo = claimOverTotNo.ToString();
                            res.claimOverTotAmt = SetNumberNoDecimalPoint(claimOverTotAmt.ToString());
                        }
                        else
                        {
                            res.claimOverTotNo = "0";
                            res.claimOverTotAmt = "0";
                        }
                        if (bGMT)
                        {
                            res.claimMatTotNo = claimMatTotNo.ToString();
                            res.claimMatTotAmt = SetNumberNoDecimalPoint(claimMatTotAmt.ToString());
                        }
                        else
                        {
                            res.claimMatTotNo = "0";
                            res.claimMatTotAmt = "0";
                        }


                        // find the duplicate record



                        // fill in start date.
                        foreach (IPDClaimDetail tmpIPD in res.claimIPDs)
                        {
                            tmpIPD.startDateTime = new DateTime(Convert.ToInt32(tmpIPD.startDate.Substring(6, 4)), Convert.ToInt32(tmpIPD.startDate.Substring(3, 2)), Convert.ToInt32(tmpIPD.startDate.Substring(0, 2)));
                        }
                        // sorting start date
                        res.claimIPDs = res.claimIPDs.OrderByDescending(a => a.startDateTime).ToList();


                        res.errorCode = "0";

                    }
                    else
                    {
                        res.errorCode = "0";
                    }
                }
                else
                {
                    res.errorCode = "0";
                }


            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }

        }
        public void GetOPDClaimDetail(GetOPDClaimDetailRequest req, GetOPDClaimDetailResponse res)
        {
            log.Info("in");
            try
            {
                MemberModel model = new MemberModel();

                DataSet dsMember = model.GetMemberDetail(req.policy, req.memberNo);
                DataSet dsClaim = model.GetAllClaim(req.policy, req.memberNo, "OPD");
                DataSet dsBenefit = model.GetOPDRemainingBenefit(req.policy, req.memberNo);

                decimal dPervis = 0, dPerYear = 0;
                int iMaxday = 0;
                string strBH = "";

                if (null != dsBenefit)
                {
                    DataRow drBenefit = (from BenefitRow in dsBenefit.Tables[0].AsEnumerable()
                                         select BenefitRow).First();


                    strBH = drBenefit["BENCDE"].ToString();
                    dPervis = Convert.ToDecimal(drBenefit["PERVIS"].ToString());
                    dPerYear = Convert.ToDecimal(drBenefit["PERYEAR"].ToString());
                    iMaxday = Convert.ToInt32(drBenefit["MAX_DAY"].ToString());


                }

                if (null != dsMember)
                {
                    res.policyNo = dsMember.Tables[0].Rows[0]["CHDRNUM"].ToString();
                    res.memberNo = dsMember.Tables[0].Rows[0]["MEMBERNO"].ToString();
                    res.policyStartDT = dsMember.Tables[0].Rows[0]["CCDATE04"].ToString();
                    res.policyEndDT = dsMember.Tables[0].Rows[0]["CRDATE04"].ToString();
                    res.fName = dsMember.Tables[0].Rows[0]["LGIVNAME"].ToString();
                    res.lName = dsMember.Tables[0].Rows[0]["LSURNAME"].ToString();
                    res.visityType = "OPD";


                    if (null != dsClaim)
                    {
                        int claimAS400No = 0, claimWebHospNo = 0;
                        decimal claimAS400Amt = 0, claimWebHospAmt = 0;

                        foreach (DataRow dr in dsClaim.Tables[0].Rows)
                        {
                            //if (!dr["GCSTSENNAME"].ToString().Equals("REJECT"))
                            {

                                OPDClaimDetail opdTempDup = res.claimOPDs.Where(a => a.claimNumber.Equals(dr["CLAMNUM"].ToString()) && a.DependencepNo.Equals(dr["DEPENDENCEP_NO"].ToString())).FirstOrDefault();

                                string strClaimApprvAmt;
                                bool bDup = false;
                                //if (null != opdTempDup)
                                if (null != opdTempDup && dr["DATA_FROM"].ToString() == "A")
                                {
                                    bDup = true;
                                    // has duplicate record.
                                    opdTempDup.claimAmt = (Convert.ToDecimal(opdTempDup.claimAmt) + Convert.ToDecimal(dr["INCURRED"].ToString())).ToString();
                                    opdTempDup.claimApprvAmt = (Convert.ToDecimal(opdTempDup.claimApprvAmt) + Convert.ToDecimal(dr["HMOSHARE"].ToString())).ToString();
                                    opdTempDup.claimOverAmt = (Convert.ToDecimal(opdTempDup.claimOverAmt) + Convert.ToDecimal(dr["MBRSHARE"].ToString())).ToString();
                                    opdTempDup.claimAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(opdTempDup.claimAmt)));
                                    opdTempDup.claimApprvAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(opdTempDup.claimApprvAmt)));
                                    opdTempDup.claimOverAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(opdTempDup.claimOverAmt)));

                                    strClaimApprvAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["HMOSHARE"].ToString())));


                                }
                                else
                                {

                                    // fill in response.
                                    OPDClaimDetail opd = new OPDClaimDetail();

                                    opd.productCode = dr["PRODUCT_TYPE_CODE"].ToString();
                                    opd.DependencepNo = dr["DEPENDENCEP_NO"].ToString();
                                    opd.claimNumber = dr["CLAMNUM"].ToString();
                                    opd.startDate = dr["START_DATE_HOSPITAL"].ToString();
                                    opd.endDate = dr["END_DATE_HOSPITAL"].ToString();
                                    opd.claimHospitalTH = dr["HOSPITAL"].ToString();
                                    if (null != dr["HOSPITAL_NAME_ENG"])
                                        opd.claimHospitalEN = dr["HOSPITAL_NAME_ENG"].ToString();
                                    else
                                        opd.claimHospitalEN = "";
                                    opd.claimDiagnoseTH = dr["DIAGNOSE_TH"].ToString();
                                    opd.claimDiagnoseEN = dr["DIAGNOSE_EN"].ToString();
                                    opd.claimType = dr["CLAIM_TYPE"].ToString();
                                    opd.claimTypeDesc = dr["CLAIM_TYPE_DESC"].ToString();
                                    opd.claimAuthDT = dr["DATE_AUTH"].ToString().Substring(6, 2) + "/" + dr["DATE_AUTH"].ToString().Substring(4, 2) + "/" + dr["DATE_AUTH"].ToString().Substring(0, 4);
                                    opd.claimStatTH = dr["GCSTSTHNAME"].ToString();
                                    opd.claimStatEN = dr["GCSTSENNAME"].ToString();
                                    opd.claimAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["INCURRED"].ToString())));
                                    opd.claimApprvAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["HMOSHARE"].ToString())));
                                    opd.claimOverAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["MBRSHARE"].ToString())));


                                    if (opd.claimStatEN.Equals("PENDING") || opd.claimStatEN.Equals("REJECT"))
                                        opd.claimAuthDT = "";

                                    strClaimApprvAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["HMOSHARE"].ToString())));


                                    // adjust status for display
                                    if (opd.claimStatEN.Trim().Equals("PENDING") && dr["DATA_FROM"].ToString().Trim().Equals("H"))
                                    {
                                        opd.claimStatEN = "Waiting bill payment";
                                        opd.claimStatTH = "รอการวางบิลจากโรงพยาบาล";
                                    }

                                    res.claimOPDs.Add(opd);
                                    //i++;
                                }

                                log.Debug("OPD status:" + dr["GCSTSENNAME"].ToString());
                                if (dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVED") || dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVE") || dr["GCSTSENNAME"].ToString().Trim().Equals("อนุมัติ") || dr["GCSTSENNAME"].ToString().Trim().Equals("PENDING"))
                                {
                                    if (dr["DATA_FROM"].ToString().Equals("A"))
                                    {

                                        if (!bDup)

                                            claimAS400No++;

                                        claimAS400Amt += Convert.ToDecimal(strClaimApprvAmt);
                                    }
                                    else if (dr["DATA_FROM"].ToString().Equals("H"))
                                    {
                                        if (!bDup)
                                            claimWebHospNo++;

                                        claimWebHospAmt += Convert.ToDecimal(strClaimApprvAmt);
                                    }
                                }
                            }


                            // summary claims detail.

                            res.claimAS400No = claimAS400No.ToString();
                            res.claimAS400Amt = SetNumberNoDecimalPoint(claimAS400Amt.ToString());
                            res.claimWebHospNo = claimWebHospNo.ToString();
                            res.claimWebHospAmt = SetNumberNoDecimalPoint(claimWebHospAmt.ToString());
                            if (strBH.Equals("BH01") || strBH.Equals("BH04"))
                            {
                                if (iMaxday > 0)
                                    res.claimRemainNo = (iMaxday - (claimAS400No + claimWebHospNo)).ToString();

                                if (dPerYear > 0)
                                    res.claimRemainAmt = SetNumberNoDecimalPoint((dPerYear - (claimAS400Amt + claimWebHospAmt)).ToString());

                                //else if (dPervis > 0)
                                //    res.claimRemainAmt = SetNumberNoDecimalPoint(dPervis.ToString());

                            }
                            // fill in start date.
                            foreach (OPDClaimDetail tmpOPD in res.claimOPDs)
                            {
                                tmpOPD.startDateTime = new DateTime(Convert.ToInt32(tmpOPD.startDate.Substring(6, 4)), Convert.ToInt32(tmpOPD.startDate.Substring(3, 2)), Convert.ToInt32(tmpOPD.startDate.Substring(0, 2)));
                            }
                            // sorting start date
                            res.claimOPDs = res.claimOPDs.OrderByDescending(a => a.startDateTime).ToList();


                        }
                        if (Convert.ToDecimal(res.claimRemainAmt) < 0)
                            res.claimRemainAmt = "0";

                        res.errorCode = "0";
                    }
                    else
                    {
                        res.errorCode = "0";
                    }
                }
                else
                {
                    res.errorCode = "0";
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }

        }
        public void GetAMEClaimDetail(GetAMEClaimDetailRequest req, GetAMEClaimDetailResponse res)
        {
            log.Info("in");
            try
            {
                MemberModel model = new MemberModel();

                DataSet dsMember = model.GetMemberDetail(req.policy, req.memberNo);
                DataSet dsClaim = model.GetAllClaim(req.policy, req.memberNo, "AME");
                DataSet dsBenefit = model.GetAMERemainingBenefit(req.policy, req.memberNo);


                decimal dPervis = 0, dPerYear = 0;
                int iMaxday = 0;
                string strAME = "";

                if (null != dsBenefit)
                {
                    DataRow drBenefit = (from BenefitRow in dsBenefit.Tables[0].AsEnumerable()
                                         select BenefitRow).First();


                    strAME = drBenefit["BENCDE"].ToString();
                    dPervis = Convert.ToDecimal(drBenefit["PERVIS"].ToString());
                    dPerYear = Convert.ToDecimal(drBenefit["PERYEAR"].ToString());
                    iMaxday = Convert.ToInt32(drBenefit["MAX_DAY"].ToString());


                }
                if (null != dsMember)
                {
                    res.policyNo = dsMember.Tables[0].Rows[0]["CHDRNUM"].ToString();
                    res.memberNo = dsMember.Tables[0].Rows[0]["MEMBERNO"].ToString();
                    res.policyStartDT = dsMember.Tables[0].Rows[0]["CCDATE04"].ToString();
                    res.policyEndDT = dsMember.Tables[0].Rows[0]["CRDATE04"].ToString();
                    res.fName = dsMember.Tables[0].Rows[0]["LGIVNAME"].ToString();
                    res.lName = dsMember.Tables[0].Rows[0]["LSURNAME"].ToString();
                    res.visityType = "AME";


                    if (null != dsClaim)
                    {

                        decimal totalAmt = 0, RemainAmt = 0;

                        List<DataRow> ameRows = (from AMEDataRows in dsClaim.Tables[0].AsEnumerable()
                                                 select AMEDataRows).OrderByDescending(a => a.Field<string>("INCURRED_DATE")).ToList();

                        // collect summary
                        string strIncurredDate = "";
                        int iIndex = -1;
                        RemainAmt = dPervis;
                        decimal totalAmtAll = 0;
                        foreach (DataRow dr in ameRows)
                        {
                            //if (!dr["GCSTSENNAME"].ToString().Equals("REJECT"))
                            {
                                if (!strIncurredDate.Equals(dr["INCURRED_DATE"].ToString()))
                                {
                                    iIndex++;
                                    totalAmt = 0;
                                    ClaimSumAMEInfo sumAME = new ClaimSumAMEInfo();

                                    sumAME.incurredDate = dr["INCURRED_DATE"].ToString().Substring(6, 2) + "/" + dr["INCURRED_DATE"].ToString().Substring(4, 2) + "/" + dr["INCURRED_DATE"].ToString().Substring(0, 4);

                                    VisitDTAmt vsdt = new VisitDTAmt();
                                    vsdt.visitDate = dr["DDTEVISIT"].ToString().Substring(6, 2) + "/" + dr["DDTEVISIT"].ToString().Substring(4, 2) + "/" + dr["DDTEVISIT"].ToString().Substring(0, 4);
                                    //String.Format("{0:n}", Convert.ToDecimal(dr["HMOSHARE"].ToString()));
                                    vsdt.incurredAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["INCURRED"].ToString())));

                                    if (dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVED") || dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVE") || dr["GCSTSENNAME"].ToString().Trim().Equals("อนุมัติ") || dr["GCSTSENNAME"].ToString().Trim().Equals("PENDING"))
                                    {
                                        totalAmt += Convert.ToDecimal(dr["INCURRED"].ToString());
                                        totalAmtAll += Convert.ToDecimal(dr["INCURRED"].ToString());
                                    }
                                    sumAME.totalAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", totalAmt));

                                    sumAME.visitDTAmt.Add(vsdt);

                                    strIncurredDate = dr["INCURRED_DATE"].ToString();

                                    res.claimSumAMEs.Add(sumAME);


                                }
                                else
                                {
                                    // in case the date is equal

                                    VisitDTAmt vsdt = new VisitDTAmt();
                                    vsdt.visitDate = dr["DDTEVISIT"].ToString().Substring(6, 2) + "/" + dr["DDTEVISIT"].ToString().Substring(4, 2) + "/" + dr["DDTEVISIT"].ToString().Substring(0, 4);
                                    vsdt.incurredAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["INCURRED"].ToString())));

                                    if (dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVED") || dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVE") || dr["GCSTSENNAME"].ToString().Trim().Equals("อนุมัติ") || dr["GCSTSENNAME"].ToString().Trim().Equals("PENDING"))
                                    {
                                        totalAmt += Convert.ToDecimal(dr["INCURRED"].ToString());
                                        totalAmtAll += Convert.ToDecimal(dr["INCURRED"].ToString());
                                    }

                                    res.claimSumAMEs[iIndex].totalAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", totalAmt));

                                    res.claimSumAMEs[iIndex].visitDTAmt.Add(vsdt);

                                }

                            }


                        }

                        ameRows = (from AMEDataRows in dsClaim.Tables[0].AsEnumerable()
                                   select AMEDataRows).OrderBy(a => a.Field<string>("INCURRED_DATE")).ToList();

                        // cal remain amount
                        strIncurredDate = "";

                        foreach (ClaimSumAMEInfo sum in res.claimSumAMEs)
                        {

                            //String.Format("{0:n}", Convert.ToDecimal(dr["INCURRED"].ToString()));
                            sum.remainAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal((dPervis - Convert.ToDecimal(sum.totalAmt)).ToString())));
                            if (Convert.ToDecimal(sum.remainAmt) < 0)
                                sum.remainAmt = "0";

                            totalAmtAll = totalAmtAll - Convert.ToDecimal(sum.totalAmt);

                        }


                        ameRows = (from AMEDataRows in dsClaim.Tables[0].AsEnumerable()
                                   select AMEDataRows).OrderByDescending(a => a.Field<string>("INCURRED_DATE")).ToList();

                        // collect detail
                        strIncurredDate = "";
                        iIndex = -1;
                        foreach (DataRow dr in ameRows)
                        {

                            // fill in response.
                            AMEClaimDetail ame = new AMEClaimDetail();
                            ame.productCode = dr["PRODUCT_TYPE_CODE"].ToString();
                            ame.incurredDate = dr["INCURRED_DATE"].ToString().Substring(6, 2) + "/" + dr["INCURRED_DATE"].ToString().Substring(4, 2) + "/" + dr["INCURRED_DATE"].ToString().Substring(0, 4);
                            ame.visitDate = dr["DDTEVISIT"].ToString().Substring(6, 2) + "/" + dr["DDTEVISIT"].ToString().Substring(4, 2) + "/" + dr["DDTEVISIT"].ToString().Substring(0, 4);
                            ame.claimHospitalTH = dr["HOSPITAL"].ToString();
                            if (null != dr["HOSPITAL_NAME_ENG"])
                                ame.claimHospitalEN = dr["HOSPITAL_NAME_ENG"].ToString();
                            else
                                ame.claimHospitalEN = "";
                            ame.claimDiagnoseTH = dr["DIAGNOSE_TH"].ToString();
                            ame.claimDiagnoseEN = dr["DIAGNOSE_EN"].ToString();
                            ame.claimType = dr["CLAIM_TYPE"].ToString();
                            ame.claimTypeDesc = dr["CLAIM_TYPE_DESC"].ToString();
                            ame.claimAuthDT = dr["DATE_AUTH"].ToString().Substring(6, 2) + "/" + dr["DATE_AUTH"].ToString().Substring(4, 2) + "/" + dr["DATE_AUTH"].ToString().Substring(0, 4);
                            ame.claimStatTH = dr["GCSTSTHNAME"].ToString();
                            ame.claimStatEN = dr["GCSTSENNAME"].ToString();
                            ame.claimAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["INCURRED"].ToString())));
                            ame.claimApprvAmt = SetNumberNoDecimalPoint(dr["HMOSHARE"].ToString());
                            ame.claimOverAmt = SetNumberNoDecimalPoint(dr["MBRSHARE"].ToString());

                            if (ame.claimStatEN.Equals("PENDING") || ame.claimStatEN.Equals("REJECT"))
                                ame.claimAuthDT = "";

                            // adjust status for display
                            if (ame.claimStatEN.Trim().Equals("PENDING") && dr["DATA_FROM"].ToString().Trim().Equals("H"))
                            {
                                ame.claimStatEN = "Waiting bill payment";
                                ame.claimStatTH = "รอการวางบิลจากโรงพยาบาล";
                            }

                            res.claimAMEs.Add(ame);


                            // fill in start date.
                            foreach (AMEClaimDetail tmpAME in res.claimAMEs)
                            {
                                tmpAME.visitDateTime = new DateTime(Convert.ToInt32(tmpAME.visitDate.Substring(6, 4)), Convert.ToInt32(tmpAME.visitDate.Substring(3, 2)), Convert.ToInt32(tmpAME.visitDate.Substring(0, 2)));
                            }
                            // sorting start date
                            res.claimAMEs = res.claimAMEs.OrderByDescending(a => a.visitDateTime).ToList();
                        }


                        res.errorCode = "0";

                    }
                    else
                    {
                        res.errorCode = "0";
                    }


                }
                else
                {
                    res.errorCode = "0";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }

        }
        public void GetDENClaimDetail(GetDENClaimDetailRequest req, GetDENClaimDetailResponse res)
        {
            log.Info("in");
            try
            {
                MemberModel model = new MemberModel();

                DataSet dsMember = model.GetMemberDetail(req.policy, req.memberNo);
                DataSet dsClaim = model.GetAllClaim(req.policy, req.memberNo, "DEN");
                DataSet dsBenefit = model.GetDENRemainingBenefit(req.policy, req.memberNo);

                decimal dPervis = 0, dPerYear = 0;
                int iMaxday = 0;
                string strBDT = "";

                if (null != dsBenefit)
                {
                    DataRow drBenefit = (from BenefitRow in dsBenefit.Tables[0].AsEnumerable()
                                         select BenefitRow).First();


                    strBDT = drBenefit["BENCDE"].ToString();
                    dPervis = Convert.ToDecimal(drBenefit["PERVIS"].ToString());
                    dPerYear = Convert.ToDecimal(drBenefit["PERYEAR"].ToString());
                    iMaxday = Convert.ToInt32(drBenefit["MAX_DAY"].ToString());


                }
                if (null != dsMember)
                {
                    res.policyNo = dsMember.Tables[0].Rows[0]["CHDRNUM"].ToString();
                    res.memberNo = dsMember.Tables[0].Rows[0]["MEMBERNO"].ToString();
                    res.policyStartDT = dsMember.Tables[0].Rows[0]["CCDATE04"].ToString();
                    res.policyEndDT = dsMember.Tables[0].Rows[0]["CRDATE04"].ToString();
                    res.fName = dsMember.Tables[0].Rows[0]["LGIVNAME"].ToString();
                    res.lName = dsMember.Tables[0].Rows[0]["LSURNAME"].ToString();
                    res.visityType = "DEN";


                    if (null != dsClaim)
                    {
                        int i = 0, claimAS400No = 0, claimWebHospNo = 0;
                        decimal claimAS400Amt = 0, claimWebHospAmt = 0;

                        foreach (DataRow dr in dsClaim.Tables[0].Rows)
                        {
                            //if (!dr["GCSTSENNAME"].ToString().Equals("REJECT"))
                            {
                                // fill in response.
                                DENClaimDetail den = new DENClaimDetail();
                                den.productCode = dr["PRODUCT_TYPE_CODE"].ToString();
                                den.startDate = dr["START_DATE_HOSPITAL"].ToString();
                                den.endDate = dr["END_DATE_HOSPITAL"].ToString();
                                den.claimHospitalTH = dr["HOSPITAL"].ToString();
                                if (null != dr["HOSPITAL_NAME_ENG"])
                                    den.claimHospitalEN = dr["HOSPITAL_NAME_ENG"].ToString();
                                else
                                    den.claimHospitalEN = "";

                                den.claimDiagnoseTH = dr["DIAGNOSE_TH"].ToString();
                                den.claimDiagnoseEN = dr["DIAGNOSE_EN"].ToString();
                                den.claimType = dr["CLAIM_TYPE"].ToString();
                                den.claimTypeDesc = dr["CLAIM_TYPE_DESC"].ToString();
                                den.claimAuthDT = dr["DATE_AUTH"].ToString().Substring(6, 2) + "/" + dr["DATE_AUTH"].ToString().Substring(4, 2) + "/" + dr["DATE_AUTH"].ToString().Substring(0, 4);
                                den.claimStatTH = dr["GCSTSTHNAME"].ToString();
                                den.claimStatEN = dr["GCSTSENNAME"].ToString();
                                den.claimAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["INCURRED"].ToString())));
                                den.claimApprvAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["HMOSHARE"].ToString())));
                                den.claimOverAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["MBRSHARE"].ToString())));



                                if (den.claimStatEN.Equals("PENDING") || den.claimStatEN.Equals("REJECT"))
                                    den.claimAuthDT = "";


                                // adjust status for display
                                if (den.claimStatEN.Trim().Equals("PENDING") && dr["DATA_FROM"].ToString().Trim().Equals("H"))
                                {
                                    den.claimStatEN = "Waiting bill payment";
                                    den.claimStatTH = "รอการวางบิลจากโรงพยาบาล";
                                }
                                res.claimDENs.Add(den);
                                i++;

                                if (dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVED") || dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVE") || dr["GCSTSENNAME"].ToString().Trim().Equals("อนุมัติ") || dr["GCSTSENNAME"].ToString().Trim().Equals("PENDING"))
                                {
                                    if (dr["DATA_FROM"].ToString().Equals("A"))
                                    {
                                        claimAS400No++;
                                        claimAS400Amt += Convert.ToDecimal(den.claimApprvAmt);
                                    }
                                    else if (dr["DATA_FROM"].ToString().Equals("H"))
                                    {
                                        claimWebHospNo++;
                                        claimWebHospAmt += Convert.ToDecimal(den.claimApprvAmt);
                                    }
                                }

                            }
                        }


                        // summary claims detail.

                        res.claimAS400No = claimAS400No.ToString();
                        res.claimAS400Amt = SetNumberNoDecimalPoint(claimAS400Amt.ToString());
                        res.claimWebHospNo = claimWebHospNo.ToString();
                        res.claimWebHospAmt = SetNumberNoDecimalPoint(claimWebHospAmt.ToString());
                        res.claimRemainAmt = "0";
                        if (strBDT.Equals("BDT2") || strBDT.Equals("BDT3"))
                        {

                            if (dPerYear > 0)
                                res.claimRemainAmt = SetNumberNoDecimalPoint((dPerYear - (claimAS400Amt + claimWebHospAmt)).ToString());


                            if (Convert.ToDecimal(res.claimRemainAmt) < 0)
                                res.claimRemainAmt = "0";

                        }


                        // fill in start date.
                        foreach (DENClaimDetail tmpDEN in res.claimDENs)
                        {
                            tmpDEN.startDateTime = new DateTime(Convert.ToInt32(tmpDEN.startDate.Substring(6, 4)), Convert.ToInt32(tmpDEN.startDate.Substring(3, 2)), Convert.ToInt32(tmpDEN.startDate.Substring(0, 2)));
                        }
                        // sorting start date
                        res.claimDENs = res.claimDENs.OrderByDescending(a => a.startDateTime).ToList();
                        res.errorCode = "0";
                    }
                    else
                    {
                        res.errorCode = "0";
                    }
                }
                else
                {
                    res.errorCode = "0";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }

        }
        public void GetLABClaimDetail(GetLABClaimDetailRequest req, GetLABClaimDetailResponse res)
        {
            log.Info("in");
            try
            {
                MemberModel model = new MemberModel();

                DataSet dsMember = model.GetMemberDetail(req.policy, req.memberNo);
                DataSet dsClaim = model.GetAllClaim(req.policy, req.memberNo, "LAB");
                DataSet dsBenefit = model.GetLABRemainingBenefit(req.policy, req.memberNo);

                decimal dPervis = 0, dPerYear = 0;
                int iMaxday = 0;
                string strBOL = "";

                if (null != dsBenefit)
                {
                    DataRow drBenefit = (from BenefitRow in dsBenefit.Tables[0].AsEnumerable()
                                         select BenefitRow).First();


                    strBOL = drBenefit["BENCDE"].ToString();
                    dPervis = Convert.ToDecimal(drBenefit["PERVIS"].ToString());
                    dPerYear = Convert.ToDecimal(drBenefit["PERYEAR"].ToString());
                    iMaxday = Convert.ToInt32(drBenefit["MAX_DAY"].ToString());


                }
                if (null != dsMember)
                {
                    res.policyNo = dsMember.Tables[0].Rows[0]["CHDRNUM"].ToString();
                    res.memberNo = dsMember.Tables[0].Rows[0]["MEMBERNO"].ToString();
                    res.policyStartDT = dsMember.Tables[0].Rows[0]["CCDATE04"].ToString();
                    res.policyEndDT = dsMember.Tables[0].Rows[0]["CRDATE04"].ToString();
                    res.fName = dsMember.Tables[0].Rows[0]["LGIVNAME"].ToString();
                    res.lName = dsMember.Tables[0].Rows[0]["LSURNAME"].ToString();
                    res.visityType = "LAB";


                    if (null != dsClaim)
                    {
                        int i = 0, claimAS400No = 0, claimWebHospNo = 0;
                        decimal claimAS400Amt = 0, claimWebHospAmt = 0;

                        foreach (DataRow dr in dsClaim.Tables[0].Rows)
                        {
                            // fill in response.
                            LABClaimDetail lab = new LABClaimDetail();
                            lab.productCode = dr["PRODUCT_TYPE_CODE"].ToString();
                            lab.startDate = dr["START_DATE_HOSPITAL"].ToString();
                            lab.endDate = dr["END_DATE_HOSPITAL"].ToString();
                            lab.claimHospitalTH = dr["HOSPITAL"].ToString();
                            if (null != dr["HOSPITAL_NAME_ENG"])
                                lab.claimHospitalEN = dr["HOSPITAL_NAME_ENG"].ToString();
                            else
                                lab.claimHospitalEN = "";
                            lab.claimDiagnoseTH = dr["DIAGNOSE_TH"].ToString();
                            lab.claimDiagnoseEN = dr["DIAGNOSE_EN"].ToString();
                            lab.claimType = dr["CLAIM_TYPE"].ToString();
                            lab.claimTypeDesc = dr["CLAIM_TYPE_DESC"].ToString();
                            lab.claimAuthDT = dr["DATE_AUTH"].ToString().Substring(6, 2) + "/" + dr["DATE_AUTH"].ToString().Substring(4, 2) + "/" + dr["DATE_AUTH"].ToString().Substring(0, 4);
                            lab.claimStatTH = dr["GCSTSTHNAME"].ToString();
                            lab.claimStatEN = dr["GCSTSENNAME"].ToString();
                            lab.claimAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["INCURRED"].ToString())));
                            lab.claimApprvAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["HMOSHARE"].ToString())));
                            lab.claimOverAmt = SetNumberNoDecimalPoint(String.Format("{0:n}", Convert.ToDecimal(dr["MBRSHARE"].ToString())));



                            if (lab.claimStatEN.Equals("PENDING") || lab.claimStatEN.Equals("REJECT"))
                                lab.claimAuthDT = "";

                            // adjust status for display
                            if (lab.claimStatEN.Trim().Equals("PENDING") && dr["DATA_FROM"].ToString().Trim().Equals("H"))
                            {
                                lab.claimStatEN = "Waiting bill payment";
                                lab.claimStatTH = "รอการวางบิลจากโรงพยาบาล";
                            }

                            res.claimLABs.Add(lab);
                            i++;
                            if (dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVED") || dr["GCSTSENNAME"].ToString().Trim().Equals("APPROVE") || dr["GCSTSENNAME"].ToString().Trim().Equals("อนุมัติ") || dr["GCSTSENNAME"].ToString().Trim().Equals("PENDING"))
                            {
                                if (dr["DATA_FROM"].ToString().Equals("A"))
                                {
                                    claimAS400No++;
                                    claimAS400Amt += Convert.ToDecimal(lab.claimApprvAmt);
                                }
                                else if (dr["DATA_FROM"].ToString().Equals("H"))
                                {
                                    claimWebHospNo++;
                                    claimWebHospAmt += Convert.ToDecimal(lab.claimApprvAmt);
                                }
                            }

                        }


                        // summary claims detail.

                        res.claimAS400No = claimAS400No.ToString();
                        res.claimAS400Amt = SetNumberNoDecimalPoint(claimAS400Amt.ToString());
                        res.claimWebHospNo = claimWebHospNo.ToString();
                        res.claimWebHospAmt = SetNumberNoDecimalPoint(claimWebHospAmt.ToString());
                        res.claimRemainAmt = "0";
                        if (strBOL.Equals("BOL1") || strBOL.Equals("BOL2") || strBOL.Equals("BOL3"))
                        {

                            if (dPerYear > 0)
                                res.claimRemainAmt = SetNumberNoDecimalPoint((dPerYear - (claimAS400Amt + claimWebHospAmt)).ToString());

                            if (Convert.ToDecimal(res.claimRemainAmt) < 0)
                                res.claimRemainAmt = "0";
                        }
                        // fill in start date.
                        foreach (LABClaimDetail tmpLAB in res.claimLABs)
                        {
                            tmpLAB.startDateTime = new DateTime(Convert.ToInt32(tmpLAB.startDate.Substring(6, 4)), Convert.ToInt32(tmpLAB.startDate.Substring(3, 2)), Convert.ToInt32(tmpLAB.startDate.Substring(0, 2)));
                        }
                        // sorting start date
                        res.claimLABs = res.claimLABs.OrderByDescending(a => a.startDateTime).ToList();
                        res.errorCode = "0";
                    }
                    else
                    {
                        res.errorCode = "0";
                    }
                }
                else
                {
                    res.errorCode = "0";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }

        }
        public void GetMediaNews(LoadMediaNewsRequest req, LoadMediaNewsResponse res)
        {

            log.Info("in");
            try
            {

                MemberModel model = new MemberModel();
                DataSet dsMediaNews = model.GetMediaNews();

                if (null != dsMediaNews)
                {
                    List<DataRow> mediaNews = (from MediaNewsRow in dsMediaNews.Tables[0].AsEnumerable()
                                               select MediaNewsRow).ToList();

                    foreach (DataRow row in mediaNews)
                    {
                        MediaNews mNews = new MediaNews();
                        mNews.title = row["title"].ToString();
                        mNews.detail = row["detail"].ToString();
                        mNews.pictureLink = row["picture_link"].ToString();
                        mNews.coverPictureLink = row["cover_picture_link"].ToString();

                        res.mediaNewsList.Add(mNews);
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }
        }


        public void GetMasterAgreement(GetMasterAgreementRequest req, GetMasterAgreementResponse res)
        {

            log.Info("in");
            try
            {

                MemberModel model = new MemberModel();
                DataSet dsAgreement = model.GetMasterAgreement();
                res.token = req.token;
                if (null != dsAgreement)
                {
                    DataRow agreementRow = (from MediaNewsRow in dsAgreement.Tables[0].AsEnumerable()
                                            select MediaNewsRow).First();

                    res.errorCode = "0";
                    res.descEN = agreementRow["DESC_EN"].ToString();
                    res.descTH = agreementRow["DESC_TH"].ToString();
                    res.titleEN = agreementRow["TITLE_EN"].ToString();
                    res.titleTH = agreementRow["TITLE_TH"].ToString();
                    res.version = agreementRow["VERSION"].ToString();

                    AuditHelper.UpdateTransEventLog("GetMasterAgreement", req.memberNo, req.policyNo, EnumMasEventLogId.AgreementView, req.osPlatform, req.clientIPAddress);
                    return;
                }


                res.errorCode = "14";
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }
        }

        public void TestGetQuery(TestGetRequest req, TestGetResponse res)
        {

            log.Info("in");
            try
            {

                MemberModel model = new MemberModel();
                DataSet dsTemp = null;

                if (req.DB.Equals("MAIN"))
                {
                    dsTemp = model.GetDebugQueryMFCGRP(req.Query);
                }
                else if (req.DB.Equals("WEB"))
                {
                    dsTemp = model.GetDebugQueryWEB(req.Query);
                }
                else if (req.DB.Equals("ECARD"))
                {
                    dsTemp = model.GetDebugQueryECard(req.Query);
                }
                else if (req.DB.Equals("LOG"))
                {

                }

                if (!req.DB.Equals("LOG"))
                {
                    if (null != dsTemp)
                    {


                        res.errorCode = "0";


                        res.ValueResponse = new String[dsTemp.Tables[0].Rows.Count];
                        int jColumn = 0;
                        for (int i = 0; i < dsTemp.Tables[0].Columns.Count; i++)
                        {
                            res.ColumnResponse += dsTemp.Tables[0].Columns[i].ToString() + ",";
                            jColumn++;
                        }

                        int iRow = 0;
                        foreach (DataRow dr in dsTemp.Tables[0].Rows)
                        {
                            for (int j = 0; j < jColumn; j++)
                            {
                                res.ValueResponse[iRow] += dr[j].ToString() + ",";
                            }


                            iRow++;
                        }


                        return;
                    }


                    res.errorCode = "14";
                }
                else if (req.DB.Equals("LOG"))
                {
                    // get log path
                    // get date
                    log.Debug("SSS");
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }
        }


        public void UpdateTransAgreement(UpdateTransAgreementRequest req, UpdateTransAgreementResponse res)
        {

            log.Info("in");
            try
            {

                MemberModel model = new MemberModel();
                bool bUpdate = model.UpdateTransAgreement(req.policyNo, req.memberNo, req.version);

                if (bUpdate)
                {
                    AuditHelper.UpdateTransEventLog("UpdateTransAgreement", req.memberNo, req.policyNo, EnumMasEventLogId.AgreementAccept, req.osPlatform, req.clientIPAddress);
                    res.errorCode = "0";
                    return;
                }
                res.errorCode = "999";
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                res.log = ex.ToString();
                throw;
            }
            finally
            {
                log.Info("out");
            }
        }

    }

}
