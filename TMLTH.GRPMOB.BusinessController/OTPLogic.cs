﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMLTH.GRPMOB.ServicePipeline;

namespace TMLTH.GRPMOB.BusinessController
{
    public class OTPLogic
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string _KeyRegis = "9BE6BD6F-AB3C-4ADF-A153-753636976397";
        string _KeyForgetPass = "CCE4A9D2-F90C-42AB-994D-C1CA64678D49";

        public string GetOTPForRegister(string mobile, string passport, string policyNo, string memberNo, string refId)
        {

            OTPPipeline otp = new OTPPipeline();

            
            return otp.GetOTP(mobile, passport, _KeyRegis, policyNo, memberNo, refId);
        }

        public string ChkOTPForRegister(string mobile, string passport, string policyNo, string memberNo, string OTP, string refId)
        {

            OTPPipeline otp = new OTPPipeline();


            return otp.ChkOTP(mobile, passport, _KeyRegis, policyNo, memberNo, OTP, refId);
        }
        public string ChkOTPForForgetPass(string mobile, string passport, string policyNo, string memberNo, string OTP, string refNo)
        {

            OTPPipeline otp = new OTPPipeline();


            return otp.ChkOTP(mobile, passport, _KeyForgetPass, policyNo, memberNo, OTP, refNo);
        }
        public string GetOTPForForgetPass(string mobile, string passport, string policyNo, string memberNo, string refId)
        {
            log.Info("in");
            try
            {

                OTPPipeline otp = new OTPPipeline();


                return otp.GetOTP(mobile, passport, _KeyForgetPass, policyNo, memberNo, refId);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
            finally {
                log.Info("out");
            }
            return null;
        }

        public string GetRefCode()
        {
            OTPPipeline otp = new OTPPipeline();

            return otp.GenerateRefCode();
        }
    }
}
