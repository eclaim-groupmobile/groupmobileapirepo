﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THMLTH.GRPMOB.DTO.Common;
using TMLTH.GRPMOB.Model.EDM;

namespace TMLTH.GRPMOB.Model
{
    public class MemberModel : BaseModelSQL
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public DataSet GetGUserInfoInFamily(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "select * from guserpass where chdrnum = @ChdrNum and SUBSTRING(usernum,1,5) = @MemberNoSub;";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNoSub", SqlDbType.Char, 10).Value = memberNo.Substring(0, 5);
                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();

                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }
        public DataSet GetMemberBenefit(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                /*select * from SG_MEMEBER_DETAIL as smd, SG_CLAIM_MN as scm, SG_BENEFIT_HEALTH as sbh 
                WHERE smd.CHDRNUM = 'G0001115' AND SUBSTRING(smd.MEMBERNO,1,5) = '00394'
  AND smd.CHDRNUM = scm.CHDRNUM and smd.MEMBERNO = scm.MEMBERNO
  AND smd.MEMBERNO = sbh.MEMBERNO
  AND sbh.BENCDE in ('BH04', 'BA06', 'BH11', 'BH01', 'BDT2', 'BDT3', 'BA05', 'BH23', 'BOL1', 'BOL2', 'BOL3')
  order by smd.MEMBERNO;*/
                _Cmd.CommandText = "select * from SG_MEMEBER_DETAIL as smd, SG_CLAIM_MN as scm, SG_BENEFIT_HEALTH as sbh, PRDNAME as prdmaster, BNFNAME as bnfmaster WHERE smd.CHDRNUM=@ChdrNum AND SUBSTRING(smd.MEMBERNO,1,5) = @MemberNoSub  AND smd.CHDRNUM = scm.CHDRNUM and smd.MEMBERNO = scm.MEMBERNO AND smd.MEMBERNO = sbh.MEMBERNO AND sbh.BENCDE in ('BH04', 'BA06', 'BH11', 'BH01', 'BDT2', 'BDT3', 'BA05', 'BH23', 'BOL1', 'BOL2', 'BOL3')   AND scm.PRODTYP = prdmaster.PRDCODE AND sbh.BENCDE = bnfmaster.BENCDE order by smd.MEMBERNO; ";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNoSub", SqlDbType.Char, 10).Value = memberNo.Substring(0, 5);
                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetMemberDetailAllFamily(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "select * from SG_MEMEBER_DETAIL WHERE CHDRNUM=@ChdrNum AND SUBSTRING(MEMBERNO,1,5) = @MemberNoSub AND DTETRM = '99999999'";
                _Cmd.CommandText = "select * from SG_MEMEBER_DETAIL WHERE CHDRNUM=@ChdrNum AND SUBSTRING(MEMBERNO,1,5) = @MemberNoSub";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNoSub", SqlDbType.Char, 10).Value = memberNo.Substring(0, 5);
                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public DataSet GetMemberDetail(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "select * from SG_MEMEBER_DETAIL WHERE CHDRNUM=@ChdrNum AND MEMBERNO = @MemberNo AND DTETRM = '99999999'";
                _Cmd.CommandText = "select * from SG_MEMEBER_DETAIL WHERE CHDRNUM=@ChdrNum AND MEMBERNO = @MemberNo";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;
                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public string GetMemberNationality(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                //select country.CTRYCODE from SG_MEMEBER_DETAIL as smd, CLNTPF as country WHERE smd.CHDRNUM='G0001115' AND SUBSTRING(smd.MEMBERNO,1,5) = '00394' AND smd.DTETRM = '99999999' AND smd.CLNTNUM = country.CLNTNUM
                _Cmd.CommandText = "select country.CTRYCODE from SG_MEMEBER_DETAIL as smd, CLNTPF as country WHERE smd.CHDRNUM=@ChdrNum AND smd.MEMBERNO = @MemberNo AND smd.DTETRM = '99999999' AND smd.CLNTNUM = country.CLNTNUM;";
                _Cmd.CommandText = "select country.CTRYCODE from SG_MEMEBER_DETAIL as smd, CLNTPF as country WHERE smd.CHDRNUM=@ChdrNum AND smd.MEMBERNO = @MemberNo AND smd.CLNTNUM = country.CLNTNUM;";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;
                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Close();
                _Conn.Open();

                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (null == ds || ds.Tables[0].Rows[0]["CTRYCODE"].ToString().Equals(""))
                        return "N/A";
                    else
                        return ds.Tables[0].Rows[0]["CTRYCODE"].ToString().Trim();

                }
                else
                    return "N/A";

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetMemberClaim(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                /*select * from SG_MEMEBER_DETAIL as smd, SG_CLAIM_MN as scm, SG_BENEFIT_HEALTH as sbh 
                  WHERE smd.CHDRNUM = 'G0001115' AND SUBSTRING(smd.MEMBERNO,1,5) = '00394'
                  AND smd.CHDRNUM = scm.CHDRNUM and smd.MEMBERNO = scm.MEMBERNO
                  AND smd.MEMBERNO = sbh.MEMBERNO
                  AND sbh.BENCDE in ('BH04', 'BA06', 'BH11', 'BH01', 'BDT2', 'BDT3', 'BA05', 'BH23', 'BOL1', 'BOL2', 'BOL3')
                  order by smd.MEMBERNO;*/
                _Cmd.CommandText = "select smd.*,scm.*,prdmaster.PRDENNAME prd_en_name, prdmaster.PRDTHNAME prd_th_name  from SG_MEMEBER_DETAIL as smd, SG_CLAIM_MN as scm, PRDNAME as prdmaster WHERE smd.CHDRNUM=@ChdrNum AND SUBSTRING(smd.MEMBERNO,1,5) = @MemberNoSub  AND smd.CHDRNUM = scm.CHDRNUM and smd.MEMBERNO = scm.MEMBERNO AND scm.PRODTYP = prdmaster.PRDCODE AND smd.DTETRM = '99999999';";
                _Cmd.CommandText = "select smd.*,scm.*,prdmaster.PRDENNAME prd_en_name, prdmaster.PRDTHNAME prd_th_name  from SG_MEMEBER_DETAIL as smd, SG_CLAIM_MN as scm, PRDNAME as prdmaster WHERE smd.CHDRNUM=@ChdrNum AND SUBSTRING(smd.MEMBERNO,1,5) = @MemberNoSub  AND smd.CHDRNUM = scm.CHDRNUM and smd.MEMBERNO = scm.MEMBERNO AND scm.PRODTYP = prdmaster.PRDCODE;";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNoSub", SqlDbType.Char, 10).Value = memberNo.Substring(0, 5);
                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();

                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetMemberBen(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                /*select * from SG_MEMEBER_DETAIL as smd, SG_CLAIM_MN as scm, SG_BENEFIT_HEALTH as sbh 

                  WHERE smd.CHDRNUM = 'G0001115' AND SUBSTRING(smd.MEMBERNO,1,5) = '00394'
                 AND smd.CHDRNUM = scm.CHDRNUM and smd.MEMBERNO = scm.MEMBERNO
                 AND smd.MEMBERNO = sbh.MEMBERNO
                 AND sbh.BENCDE in ('BH04', 'BA06', 'BH11', 'BH01', 'BDT2', 'BDT3', 'BA05', 'BH23', 'BOL1', 'BOL2', 'BOL3')
                 order by smd.MEMBERNO;*/
                /* //nid comment
                              _Cmd.CommandText = "select * from SG_MEMEBER_DETAIL as smd, SG_BENEFIT_HEALTH as sbh,  BNFNAME as bnfmaster WHERE smd.CHDRNUM=@ChdrNum AND SUBSTRING(smd.MEMBERNO,1,5) = @MemberNoSub  AND smd.MEMBERNO = sbh.MEMBERNO AND sbh.GROUP_ID=@ChdrNum AND sbh.BENCDE = bnfmaster.BENCDE AND smd.DTETRM = '99999999' order by smd.MEMBERNO; ";
                              _Cmd.CommandText = "select * from SG_MEMEBER_DETAIL as smd, SG_BENEFIT_HEALTH as sbh,  BNFNAME as bnfmaster WHERE smd.CHDRNUM=@ChdrNum AND SUBSTRING(smd.MEMBERNO,1,5) = @MemberNoSub  AND smd.MEMBERNO = sbh.MEMBERNO AND sbh.GROUP_ID=@ChdrNum AND sbh.BENCDE = bnfmaster.BENCDE order by smd.MEMBERNO; ";
                              _Cmd.CommandType = System.Data.CommandType.Text;

                              _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                              _Cmd.Parameters.Add("@MemberNoSub", SqlDbType.Char, 10).Value = memberNo.Substring(0, 5);
                              _Da.SelectCommand = _Cmd;
                              log.Debug(_Cmd.CommandText);
                              _Conn.Open();

                              log.Info("========DB Invoke========");
                              _Da.Fill(ds);
                              log.Info("========DB Invoke========");
                              _Conn.Close();
                */

                //nid change query call StoredProcedure
                _Cmd.CommandText = "SP_GMB_GET_MEMBER_BENEFIT";

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNoSub", SqlDbType.Char, 10).Value = memberNo.Substring(0, 5);
                _Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();
                //end nid
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetMemberBenByBenCode(string policyNo, string memberNo, string benCode)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                /*select * from SG_MEMEBER_DETAIL as smd, SG_CLAIM_MN as scm, SG_BENEFIT_HEALTH as sbh 
                WHERE smd.CHDRNUM = 'G0001115' AND SUBSTRING(smd.MEMBERNO,1,5) = '00394'
  AND smd.CHDRNUM = scm.CHDRNUM and smd.MEMBERNO = scm.MEMBERNO
  AND smd.MEMBERNO = sbh.MEMBERNO
  AND sbh.BENCDE in ('BH04', 'BA06', 'BH11', 'BH01', 'BDT2', 'BDT3', 'BA05', 'BH23', 'BOL1', 'BOL2', 'BOL3')
  order by smd.MEMBERNO;*/
                _Cmd.CommandText = "select * from SG_MEMEBER_DETAIL as smd, SG_BENEFIT_HEALTH as sbh,  BNFNAME as bnfmaster WHERE smd.CHDRNUM=@ChdrNum AND smd.MEMBERNO=@MemberNo  AND sbh.GROUP_ID=@ChdrNum AND bnfmaster.BENCDE=@BenCode AND smd.MEMBERNO = sbh.MEMBERNO AND sbh.BENCDE = bnfmaster.BENCDE AND smd.DTETRM = '99999999' order by smd.MEMBERNO; ";
                _Cmd.CommandText = "select * from SG_MEMEBER_DETAIL as smd, SG_BENEFIT_HEALTH as sbh,  BNFNAME as bnfmaster WHERE smd.CHDRNUM=@ChdrNum AND smd.MEMBERNO=@MemberNo  AND sbh.GROUP_ID=@ChdrNum AND bnfmaster.BENCDE=@BenCode AND smd.MEMBERNO = sbh.MEMBERNO AND sbh.BENCDE = bnfmaster.BENCDE order by smd.MEMBERNO; ";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;
                _Cmd.Parameters.Add("@BenCode", SqlDbType.Char, 10).Value = benCode;
                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();

                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public DataSet GetOPDRemainingBenefit(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                /*select SG_BENEFIT_HEALTH.PERVIS, SG_BENEFIT_HEALTH.MAX_DAY, SG_BENEFIT_HEALTH.PERYEAR From SG_BENEFIT_HEALTH 
Where SG_BENEFIT_HEALTH. GROUP_ID =  'G0001115'
     AND  SG_BENEFIT_HEALTH. MEMBERNO  = '00394-00'
     AND (SG_BENEFIT_HEALTH.BENCDE in ('BH01', 'BH04'))*/
                _Cmd.CommandText = "select * From SG_BENEFIT_HEALTH WHERE SG_BENEFIT_HEALTH.GROUP_ID=@ChdrNum AND SG_BENEFIT_HEALTH. MEMBERNO=@MemberNo AND SG_BENEFIT_HEALTH.BENCDE in ('BH01', 'BH04')";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;

                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();

                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetLABRemainingBenefit(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                /*select SG_BENEFIT_HEALTH.PERVIS, SG_BENEFIT_HEALTH.MAX_DAY, SG_BENEFIT_HEALTH.PERYEAR From SG_BENEFIT_HEALTH 
Where SG_BENEFIT_HEALTH. GROUP_ID =  'G0001115'
     AND  SG_BENEFIT_HEALTH. MEMBERNO  = '00394-00'
     AND (SG_BENEFIT_HEALTH.BENCDE in ('BH01', 'BH04'))*/
                _Cmd.CommandText = "select * From SG_BENEFIT_HEALTH WHERE SG_BENEFIT_HEALTH.GROUP_ID=@ChdrNum AND SG_BENEFIT_HEALTH. MEMBERNO=@MemberNo AND SG_BENEFIT_HEALTH.BENCDE in ('BOL1', 'BOL2', 'BOL3')";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;

                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();

                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetAMERemainingBenefit(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                /*select SG_BENEFIT_HEALTH.PERVIS, SG_BENEFIT_HEALTH.MAX_DAY, SG_BENEFIT_HEALTH.PERYEAR From SG_BENEFIT_HEALTH 
Where SG_BENEFIT_HEALTH. GROUP_ID =  'G0001115'
     AND  SG_BENEFIT_HEALTH. MEMBERNO  = '00394-00'
     AND (SG_BENEFIT_HEALTH.BENCDE in ('BH01', 'BH04'))*/
                _Cmd.CommandText = "select * From SG_BENEFIT_HEALTH WHERE SG_BENEFIT_HEALTH.GROUP_ID=@ChdrNum AND SG_BENEFIT_HEALTH. MEMBERNO=@MemberNo AND SG_BENEFIT_HEALTH.BENCDE='BA05'";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;

                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();

                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetDENRemainingBenefit(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                /*select SG_BENEFIT_HEALTH.PERVIS, SG_BENEFIT_HEALTH.MAX_DAY, SG_BENEFIT_HEALTH.PERYEAR From SG_BENEFIT_HEALTH 
Where SG_BENEFIT_HEALTH. GROUP_ID =  'G0001115'
     AND  SG_BENEFIT_HEALTH. MEMBERNO  = '00394-00'
     AND (SG_BENEFIT_HEALTH.BENCDE in ('BH01', 'BH04'))*/
                _Cmd.CommandText = "select * From SG_BENEFIT_HEALTH WHERE SG_BENEFIT_HEALTH.GROUP_ID=@ChdrNum AND SG_BENEFIT_HEALTH. MEMBERNO=@MemberNo AND SG_BENEFIT_HEALTH.BENCDE in ('BDT2', 'BDT3')";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;

                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();

                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetProvince()
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _CmdWeb = _ConnWeb.CreateCommand();

                _CmdWeb.CommandText = "SELECT * FROM tProvince_grpmobile";
                _CmdWeb.CommandType = System.Data.CommandType.Text;


                _DaWeb.SelectCommand = _CmdWeb;
                log.Debug(_CmdWeb.CommandText);
                _ConnWeb.Open();
                log.Info("========DB Invoke========");
                _DaWeb.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnWeb.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }
        public DataSet GetHospital()
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _CmdWeb = _ConnWeb.CreateCommand();

                _CmdWeb.CommandText = "SELECT * FROM tHospital";
                _CmdWeb.CommandType = System.Data.CommandType.Text;


                _DaWeb.SelectCommand = _CmdWeb;
                log.Debug(_CmdWeb.CommandText);
                _ConnWeb.Open();

                log.Info("========DB Invoke========");
                _DaWeb.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnWeb.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetCityInfo()
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _CmdWeb = _ConnWeb.CreateCommand();

                _CmdWeb.CommandText = "select distinct(engcity), thcity from tHospital where englocation = 'Bangkok' order by engcity;";
                _CmdWeb.CommandType = System.Data.CommandType.Text;


                _DaWeb.SelectCommand = _CmdWeb;
                log.Debug(_CmdWeb.CommandText);
                _ConnWeb.Open();

                log.Info("========DB Invoke========");
                _DaWeb.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnWeb.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetNewsInfo()
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _CmdWeb = _ConnWeb.CreateCommand();

                _CmdWeb.CommandText = "SELECT * FROM tgroup_news";
                _CmdWeb.CommandType = System.Data.CommandType.Text;


                _DaWeb.SelectCommand = _CmdWeb;
                log.Debug(_CmdWeb.CommandText);
                _ConnWeb.Open();
                log.Info("========DB Invoke========");
                _DaWeb.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnWeb.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetMediaNews()
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _CmdWeb = _ConnWeb.CreateCommand();

                _CmdWeb.CommandText = "SELECT * FROM tMediaNews";
                _CmdWeb.CommandType = System.Data.CommandType.Text;


                _DaWeb.SelectCommand = _CmdWeb;
                log.Debug(_CmdWeb.CommandText);
                _ConnWeb.Open();
                log.Info("========DB Invoke========");
                _DaWeb.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnWeb.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetHealthTipsAll()
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "EMP_GET_FILE_ALL";
                _Cmd.CommandType = System.Data.CommandType.StoredProcedure;


                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public DataSet GetAppVersion(string appId, string appVersion)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _CmdAppVersion = _ConnAppVersion.CreateCommand();

                _CmdAppVersion.CommandText = "SP_GET_APP_VERSION";
                _CmdAppVersion.Parameters.Add("@APP_ID", SqlDbType.VarChar, 100).Value = appId;
                _CmdAppVersion.Parameters.Add("@APP_VERSION", SqlDbType.VarChar, 10).Value = appVersion;

                _CmdAppVersion.CommandType = System.Data.CommandType.StoredProcedure;


                _DaAppVersion.SelectCommand = _CmdAppVersion;
                log.Debug(_CmdAppVersion.CommandText);
                _ConnAppVersion.Open();
                log.Info("========DB Invoke========");
                _DaAppVersion.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnAppVersion.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public DataSet GetHealthTipDetail(int fileID)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "EMP_GET_FILE_BY_ID";
                _Cmd.Parameters.Add("@INPUT_ID", SqlDbType.Int, 4).Value = fileID;
                //_Cmd.Parameters.Add(new SqlParameter("@INPUT_ID", SqlDbType.Int , fileID));
                _Cmd.CommandType = System.Data.CommandType.StoredProcedure;


                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetECard(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _CmdECard = _ConnECard.CreateCommand();
                string ecardPrefix = ConfigurationManager.AppSettings.Get("ECardPrefix");
                //_CmdECard.CommandText = "SELECT * FROM " + ecardPrefix + "tcpmbrcard where chdrnum=@PolicyNo and (mbrno=@MemberPureNo or mbrno=@MemberOneZero or mbrno=@MemberTwoZero)   order by datestrt";
                _CmdECard.CommandText = " SELECT * FROM " + ecardPrefix + "tcpmbrcard WHERE UPPER(genpage01) IN ('CARD01','CARD02','CARD03','CARD04','CARD05','CARD06','CARD07','CARD08','CARD09','CARD10','CARD11')  ";
                _CmdECard.CommandText += " AND chdrnum=@PolicyNo and (mbrno=@MemberPureNo or mbrno=@MemberOneZero or mbrno=@MemberTwoZero) ";
                _CmdECard.CommandText += " ORDER BY datestrt";

                
                //SELECT * FROM tcpmbrcard where chdrnum='G0001115' and (mbrno='394' or mbrno = '0394' or mbrno = '00394')
                _CmdECard.CommandType = System.Data.CommandType.Text;
                _CmdECard.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                string strMemberWHZero = memberNo.TrimStart(new Char[] { '0' });
                string[] strTemp = strMemberWHZero.Split('-');
                string strPureMember = strTemp[0];
                _CmdECard.Parameters.Add("@MemberPureNo", SqlDbType.Char, 10).Value = strPureMember;
                _CmdECard.Parameters.Add("@MemberOneZero", SqlDbType.Char, 10).Value = strPureMember.PadLeft(4, '0');
                _CmdECard.Parameters.Add("@MemberTwoZero", SqlDbType.Char, 10).Value = strPureMember.PadLeft(5, '0');

                _DaECard.SelectCommand = _CmdECard;
                log.Debug(_CmdECard.CommandText);
                _ConnECard.Open();
                log.Info("========DB Invoke========");
                _DaECard.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnECard.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }

        public string GetEmployeeNum(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "SELECT EMPNO FROM SG_MEMEBER_DETAIL where CHDRNUM=@PolicyNo and MEMBERNO=@MemberNo ";

                //SELECT * FROM tcpmbrcard where chdrnum='G0001115' and (mbrno='394' or mbrno = '0394' or mbrno = '00394')
                _Cmd.CommandType = System.Data.CommandType.Text;
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.VarChar, 8).Value = policyNo;

                _Cmd.Parameters.Add("@MemberNo", SqlDbType.VarChar, 8).Value = memberNo;

                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0]["EMPNO"].ToString().Trim();

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }

        public bool IsExistECard(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                // nid comment for test 
                _CmdECard = _ConnECard.CreateCommand();
                string ecardPrefix = ConfigurationManager.AppSettings.Get("ECardPrefix"); 
                //_CmdECard.CommandText = "SELECT * FROM " + ecardPrefix + "tcpmbrcard where chdrnum=@PolicyNo and (mbrno=@MemberPureNo or mbrno=@MemberOneZero or mbrno=@MemberTwoZero)   order by datestrt";
                _CmdECard.CommandText = " SELECT * FROM " + ecardPrefix + "tcpmbrcard WHERE UPPER(genpage01) IN ('CARD01','CARD02','CARD03','CARD04','CARD05','CARD06','CARD07','CARD08','CARD09','CARD10','CARD11')  ";
                _CmdECard.CommandText += " AND chdrnum=@PolicyNo AND (mbrno=@MemberPureNo OR mbrno=@MemberOneZero OR mbrno=@MemberTwoZero) ";
                _CmdECard.CommandText += " ORDER BY datestrt";

                //SELECT * FROM tcpmbrcard where chdrnum='G0001115' and (mbrno='394' or mbrno = '0394' or mbrno = '00394')
                _CmdECard.CommandType = System.Data.CommandType.Text;
                _CmdECard.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                string strMemberWHZero = memberNo.TrimStart(new Char[] { '0' });
                string[] strTemp = strMemberWHZero.Split('-');
                string strPureMember = strTemp[0];
                _CmdECard.Parameters.Add("@MemberPureNo", SqlDbType.Char, 10).Value = strPureMember;
                _CmdECard.Parameters.Add("@MemberOneZero", SqlDbType.Char, 10).Value = strPureMember.PadLeft(4, '0');
                _CmdECard.Parameters.Add("@MemberTwoZero", SqlDbType.Char, 10).Value = strPureMember.PadLeft(5, '0');

                _DaECard.SelectCommand = _CmdECard;
                log.Debug(_CmdECard.CommandText);
                _ConnECard.Open();
                log.Info("========DB Invoke========");
                _DaECard.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnECard.Close();

                // nid add for test 
                //_CmdMemberCard = _ConnMemberCard.CreateCommand();
                //string ecardPrefix = ConfigurationManager.AppSettings.Get("ECardPrefix");
                //_CmdMemberCard.CommandText = "SELECT * FROM " + ecardPrefix + "tcpmbrcard where chdrnum=@PolicyNo and (mbrno=@MemberPureNo or mbrno=@MemberOneZero or mbrno=@MemberTwoZero)   order by datestrt desc";
                //_CmdMemberCard.CommandType = System.Data.CommandType.Text;
                //_CmdMemberCard.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                //string strMemberWHZero = memberNo.TrimStart(new Char[] { '0' });
                //string[] strTemp = strMemberWHZero.Split('-');
                //string strPureMember = strTemp[0];
                //_CmdMemberCard.Parameters.Add("@MemberPureNo", SqlDbType.Char, 10).Value = strPureMember;
                //_CmdMemberCard.Parameters.Add("@MemberOneZero", SqlDbType.Char, 10).Value = strPureMember.PadLeft(4, '0');
                //_CmdMemberCard.Parameters.Add("@MemberTwoZero", SqlDbType.Char, 10).Value = strPureMember.PadLeft(5, '0');

                //_DaMemberCard.SelectCommand = _CmdMemberCard;
                //log.Debug(_CmdMemberCard.CommandText);
                //_ConnMemberCard.Open();
                //log.Info("========DB Invoke========");
                //_DaMemberCard.Fill(ds);
                //log.Info("========DB Invoke========");
                //_ConnMemberCard.Close();
                //end nid

                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string strStartDate = row["datestrt"].ToString().Substring(6, 2) + "/" + row["datestrt"].ToString().Substring(4, 2) + "/" + row["datestrt"].ToString().Substring(0, 4);
                        string strEndDate = row["dateend"].ToString().Substring(6, 2) + "/" + row["dateend"].ToString().Substring(4, 2) + "/" + row["dateend"].ToString().Substring(0, 4);

                        DateTime dtStartDate = new DateTime(Convert.ToInt32(row["datestrt"].ToString().Substring(0, 4)), Convert.ToInt32(row["datestrt"].ToString().Substring(4, 2)), Convert.ToInt32(row["datestrt"].ToString().Substring(6, 2)));
                        DateTime dtEndDate = new DateTime(Convert.ToInt32(row["dateend"].ToString().Substring(0, 4)), Convert.ToInt32(row["dateend"].ToString().Substring(4, 2)), Convert.ToInt32(row["dateend"].ToString().Substring(6, 2)));

                        if (DateTime.Today >= dtStartDate && DateTime.Today <= dtEndDate)
                        {

                            return true;
                        }

                    }

                    return false;
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }

        public bool IsExistECardForWeb(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                string strDate = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "SELECT * FROM ECardForWeb where ChdrNum=@PolicyNo and MbrNo=@MemberNo and IssDate=@IssDate";

                //SELECT * FROM tcpmbrcard where chdrnum='G0001115' and (mbrno='394' or mbrno = '0394' or mbrno = '00394')
                _Cmd.CommandType = System.Data.CommandType.Text;
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;
                _Cmd.Parameters.Add("@IssDate", SqlDbType.Char, 10).Value = strDate;

                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Close();
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return true;

                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }
        //public bool UpdateECard(string ChdrNum, string MbrNo, string IssDate, string SubName, string DateStart, string DateEnd, string MbrSalut, string FirstName, string LastName, string ClaimLifeGTL, string ClaimBenefit1, string ClaimBenefit2, string ClaimBenefit3, string ClaimBenefit4, string ClaimBenefit5, string ClaimBenefit6, string GenPage01, string GenPage02, string GenPage03, string GenPage04, string GenPage05, string GenPage06, string GenPage07, string GenPage08)
        public bool UpdateECard(string ChdrNum, string IssDate, string SubName, string MbrSalut, EcardParams param)
        {
            log.Info("in"); //EmployeeECard emp
            string MbrNo = param.memberNo;
            string DateStart = param.policyStartDT;
            string DateEnd = param.policyEndDT;
            string FirstName = param.firstName;
            string LastName = param.lastName;
            string ClaimLifeGTL = param.claimLifeGTL;
            string ClaimBenefit1 = param.claimBenefit1;
            string ClaimBenefit2 = param.claimBenefit2;
            string ClaimBenefit3 = param.claimBenefit3;
            string ClaimBenefit4 = param.claimBenefit4;
            string ClaimBenefit5 = param.claimBenefit5;
            string ClaimBenefit6 = param.claimBenefit6;
            string ClaimBenefit7 = param.claimBenefit7;
            string ClaimBenefit8 = param.claimBenefit8;
            string GenPage01 = param.genPage01;
            string GenPage02 = param.genPage02;
            string GenPage03 = param.genPage03;
            string GenPage04 = param.genPage04;
            string GenPage05 = param.genPage05;
            string GenPage06 = param.genPage06;
            string GenPage07 = param.genPage07;
            string GenPage08 = param.genPage08;

            try
            {
                string strDate = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString();
                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd = new SqlCommand("INSERT into ECardForWeb (ChdrNum, MbrNo, IssDate, SubName,DateStart,DateEnd,MbrSalut,FirstName,LastName,ClaimLifeGTL,ClaimBenefit1,ClaimBenefit2,ClaimBenefit3,ClaimBenefit4,ClaimBenefit5,ClaimBenefit6,ClaimBenefit7,ClaimBenefit8,GenPage01,GenPage02,GenPage03,GenPage04,GenPage05,GenPage06,GenPage07,GenPage08, Createdate) " +
                       "VALUES (@ChdrNum, @MbrNo, @IssDate, @SubName, @DateStart, @DateEnd, @MbrSalut, @FirstName, @LastName, @ClaimLifeGTL, @ClaimBenefit1, @ClaimBenefit2, @ClaimBenefit3, @ClaimBenefit4, @ClaimBenefit5, @ClaimBenefit6, @ClaimBenefit7, @ClaimBenefit8, @GenPage01, @GenPage02, @GenPage03, @GenPage04, @GenPage05, @GenPage06, @GenPage07, @GenPage08, @Createdate)", _Conn);

                //CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";

                _Cmd.CommandType = CommandType.Text;
                // Add the parameters for the InsertCommand.
                // Add the parameters for the InsertCommand.
                _Cmd.Parameters.Add("@ChdrNum", SqlDbType.NVarChar, 8).Value = ChdrNum;
                _Cmd.Parameters.Add("@MbrNo", SqlDbType.NVarChar, 8).Value = MbrNo;
                _Cmd.Parameters.Add("@IssDate", SqlDbType.NVarChar, 10).Value = IssDate;
                _Cmd.Parameters.Add("@SubName", SqlDbType.NVarChar, 200).Value = SubName;
                _Cmd.Parameters.Add("@DateStart", SqlDbType.NVarChar, 10).Value = DateStart;
                _Cmd.Parameters.Add("@DateEnd", SqlDbType.NVarChar, 10).Value = DateEnd;
                _Cmd.Parameters.Add("@MbrSalut", SqlDbType.NVarChar, 30).Value = MbrSalut;
                _Cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value = FirstName;
                _Cmd.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value = LastName;
                _Cmd.Parameters.Add("@ClaimLifeGTL", SqlDbType.NVarChar, 100).Value = ClaimLifeGTL;
                _Cmd.Parameters.Add("@ClaimBenefit1", SqlDbType.NVarChar, 100).Value = ClaimBenefit1;
                _Cmd.Parameters.Add("@ClaimBenefit2", SqlDbType.NVarChar, 100).Value = ClaimBenefit2;
                _Cmd.Parameters.Add("@ClaimBenefit3", SqlDbType.NVarChar, 100).Value = ClaimBenefit3;
                _Cmd.Parameters.Add("@ClaimBenefit4", SqlDbType.NVarChar, 100).Value = ClaimBenefit4;
                _Cmd.Parameters.Add("@ClaimBenefit5", SqlDbType.NVarChar, 100).Value = ClaimBenefit5;
                _Cmd.Parameters.Add("@ClaimBenefit6", SqlDbType.NVarChar, 100).Value = ClaimBenefit6;
                _Cmd.Parameters.Add("@ClaimBenefit7", SqlDbType.NVarChar, 100).Value = ClaimBenefit7;
                _Cmd.Parameters.Add("@ClaimBenefit8", SqlDbType.NVarChar, 100).Value = ClaimBenefit8;
                _Cmd.Parameters.Add("@GenPage01", SqlDbType.NVarChar, 100).Value = GenPage01;
                _Cmd.Parameters.Add("@GenPage02", SqlDbType.NVarChar, 100).Value = GenPage02;
                _Cmd.Parameters.Add("@GenPage03", SqlDbType.NVarChar, 100).Value = GenPage03;
                _Cmd.Parameters.Add("@GenPage04", SqlDbType.NVarChar, 100).Value = GenPage04;
                _Cmd.Parameters.Add("@GenPage05", SqlDbType.NVarChar, 100).Value = GenPage05;
                _Cmd.Parameters.Add("@GenPage06", SqlDbType.NVarChar, 100).Value = GenPage06;
                _Cmd.Parameters.Add("@GenPage07", SqlDbType.NVarChar, 100).Value = GenPage07;
                _Cmd.Parameters.Add("@GenPage08", SqlDbType.NVarChar, 100).Value = GenPage08;
                _Cmd.Parameters.Add("@Createdate", SqlDbType.DateTime).Value = DateTime.Now;

                _Da.InsertCommand = _Cmd;


                log.Debug(_Cmd.CommandText);
                _Conn.Close();
                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.InsertCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Info(ex.ToString());

                throw ex;

            }
            finally
            {
                log.Info("out");
            }

        }

        public bool UpdateMemberInfo(string mobileNo, string email, string lineID, string memberNo, string policyNo, string previousEmail, string previousTel)
        {
            log.Info("in");
            try
            {
                string strDate = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString();
                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd = new SqlCommand("UPDATE guserpass SET mobile_no=@MobileNo, tel=@MobileNo , email=@Email, line_id=@LineID, prev_email=@PreviousEmail, prev_tel=@PreviousTel, email_upddate=@UpdDate, tel_upddate=@UpdDate WHERE chdrnum=@PolicyNo AND usernum=@MemberNo", _Conn);

                //CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";

                _Cmd.CommandType = CommandType.Text;
                // Add the parameters for the InsertCommand.
                // Add the parameters for the InsertCommand.
                _Cmd.Parameters.Add("@MobileNo", SqlDbType.Char, 20).Value = mobileNo;
                _Cmd.Parameters.Add("@Email", SqlDbType.NVarChar, 100).Value = email;
                _Cmd.Parameters.Add("@LineID", SqlDbType.Char, 20).Value = lineID;
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;
                _Cmd.Parameters.Add("@PreviousEmail", SqlDbType.VarChar, 100).Value = previousEmail;
                _Cmd.Parameters.Add("@PreviousTel", SqlDbType.VarChar, 50).Value = previousTel;
                _Cmd.Parameters.Add("@UpdDate", SqlDbType.DateTime, 10).Value = DateTime.Now;

                _Da.UpdateCommand = _Cmd;


                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.UpdateCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }

        public DataSet GetMemberInfo(string memberNo, string policyNo)
        {
            log.Info("in");
            try
            {

                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd = new SqlCommand("SELECT * FROM guserpass where chdrnum=@PolicyNo and usernum=@MemberNo", _Conn);

                //CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";

                _Cmd.CommandType = CommandType.Text;
                // Add the parameters for the InsertCommand.
                // Add the parameters for the InsertCommand.
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;


                _Da.SelectCommand = _Cmd;


                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                DataSet ds = new DataSet();
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                return ds;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }
        public DataSet GetClaimAS400(string memberNo, string policyNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "SP_GET_CLAIM_AS400";

                _Cmd.Parameters.Add("@INPUT_USER_ID", SqlDbType.VarChar, 8).Value = memberNo;
                _Cmd.Parameters.Add("@INPUT_GROUP_NO", SqlDbType.VarChar, 8).Value = policyNo;

                _Cmd.CommandType = System.Data.CommandType.StoredProcedure;


                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }


        public DataSet GetClaimHospitalCheck(string memberNo, string policyNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "SP_GET_DATA_HOSPITAL_CHECK";

                _Cmd.Parameters.Add("@CONTRACT_NUMBER", SqlDbType.VarChar, 8).Value = policyNo;
                _Cmd.Parameters.Add("@MEMBER_NO", SqlDbType.VarChar, 5).Value = memberNo.Substring(0, 5);
                _Cmd.Parameters.Add("@DEPENDENCEP_NO", SqlDbType.VarChar, 2).Value = memberNo.Substring(6, 2);

                _Cmd.CommandType = System.Data.CommandType.StoredProcedure;


                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public String GetCompanyNameEN(string policyNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "select * from tbdpolicyinfo_gmac where chdrnum=@PolicyNo";

                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.VarChar, 10).Value = policyNo;


                _Cmd.CommandType = System.Data.CommandType.Text;


                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0]["compname_en"].ToString();

                }
                else
                    return "";

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public bool DeleteClaimByMemberNo(string memberNo, string policyNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "DELETE FROM AllClaimDetails WHERE CONTRACT_NUMBER=@Policy AND MEMBER_NO=@MemberNo";


                _Cmd.Parameters.Add("@MemberNo", SqlDbType.VarChar, 8).Value = memberNo;
                _Cmd.Parameters.Add("@Policy", SqlDbType.VarChar, 8).Value = policyNo;

                _Cmd.CommandType = System.Data.CommandType.Text;


                _Da.DeleteCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.DeleteCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (result == 0)
                    return false;
                else
                    return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public bool InsertAllClaims(List<AllClaimsDetailsEDM> listAllClaims)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();

                foreach (AllClaimsDetailsEDM claim in listAllClaims)
                {
                    _Cmd = _Conn.CreateCommand();

                    _Cmd.CommandText = "INSERT INTO AllClaimDetails (CONTRACT_NUMBER, MEMBER_NO, DEPENDENCEP_NO, CLAMNUM, VISIT_TYPE, PRODUCT_TYPE_CODE, START_DATE_HOSPITAL, END_DATE_HOSPITAL, DIAGNOSE_TH, DIAGNOSE_EN, FORFEIT, INCURRED, HMOSHARE, MBRSHARE, HOSPITAL, GCSTS, GCSTSENNAME, GCSTSTHNAME, PRODUCT_TYPE, CLAIM_TYPE, CLAIM_TYPE_DESC, DATE_AUTH, DDTEVISIT, INCURRED_DATE, HOSPITAL_NAME_ENG, DATA_FROM) VALUES " +
                                       "(@CONTRACT_NUMBER, @MEMBER_NO, @DEPENDENCEP_NO, @CLAMNUM, @VISIT_TYPE, @PRODUCT_TYPE_CODE, @START_DATE_HOSPITAL, @END_DATE_HOSPITAL, @DIAGNOSE_TH, @DIAGNOSE_EN, @FORFEIT, @INCURRED, @HMOSHARE, @MBRSHARE, @HOSPITAL, @GCSTS, @GCSTSENNAME, @GCSTSTHNAME, @PRODUCT_TYPE, @CLAIM_TYPE, @CLAIM_TYPE_DESC, @DATE_AUTH, @DDTEVISIT, @INCURRED_DATE, @HOSPITAL_NAME_ENG, @DATA_FROM)";

                    _Cmd.Parameters.Add("@CONTRACT_NUMBER", SqlDbType.VarChar, 8).Value = claim.CONTRACT_NUMBER;
                    _Cmd.Parameters.Add("@MEMBER_NO", SqlDbType.VarChar, 8).Value = claim.MEMBER_NO;
                    _Cmd.Parameters.Add("@DEPENDENCEP_NO", SqlDbType.VarChar, 2).Value = claim.DEPENDENCEP_NO;
                    _Cmd.Parameters.Add("@CLAMNUM", SqlDbType.VarChar, 8).Value = claim.CLAMNUM;
                    _Cmd.Parameters.Add("@VISIT_TYPE", SqlDbType.VarChar, 3).Value = claim.VISIT_TYPE;
                    _Cmd.Parameters.Add("@PRODUCT_TYPE_CODE", SqlDbType.VarChar, 3).Value = claim.PRODUCT_TYPE_CODE;
                    _Cmd.Parameters.Add("@START_DATE_HOSPITAL", SqlDbType.VarChar, 10).Value = claim.START_DATE_HOSPITAL;
                    _Cmd.Parameters.Add("@END_DATE_HOSPITAL", SqlDbType.VarChar, 10).Value = claim.END_DATE_HOSPITAL;
                    _Cmd.Parameters.Add("@DIAGNOSE_TH", SqlDbType.VarChar, 255).Value = claim.DIAGNOSE_TH;
                    _Cmd.Parameters.Add("@DIAGNOSE_EN", SqlDbType.VarChar, 255).Value = claim.DIAGNOSE_EN;
                    _Cmd.Parameters.Add("@FORFEIT", SqlDbType.VarChar, 255).Value = claim.FORFEIT;
                    _Cmd.Parameters.Add("@INCURRED", SqlDbType.Decimal).Value = claim.INCURRED;
                    _Cmd.Parameters.Add("@HMOSHARE", SqlDbType.Decimal).Value = claim.HMOSHARE;
                    _Cmd.Parameters.Add("@MBRSHARE", SqlDbType.Decimal).Value = claim.MBRSHARE;
                    _Cmd.Parameters.Add("@HOSPITAL", SqlDbType.VarChar, 255).Value = claim.HOSPITAL;
                    _Cmd.Parameters.Add("@GCSTS", SqlDbType.VarChar, 2).Value = claim.GCSTS;
                    _Cmd.Parameters.Add("@GCSTSENNAME", SqlDbType.VarChar, 50).Value = claim.GCSTSENNAME;
                    _Cmd.Parameters.Add("@GCSTSTHNAME", SqlDbType.VarChar, 50).Value = claim.GCSTSTHNAME;
                    _Cmd.Parameters.Add("@PRODUCT_TYPE", SqlDbType.VarChar, 255).Value = claim.PRODUCT_TYPE;
                    _Cmd.Parameters.Add("@CLAIM_TYPE", SqlDbType.VarChar, 3).Value = claim.CLAIM_TYPE;
                    _Cmd.Parameters.Add("@CLAIM_TYPE_DESC", SqlDbType.VarChar, 50).Value = claim.CLAIM_TYPE_DESC;
                    _Cmd.Parameters.Add("@DATE_AUTH", SqlDbType.VarChar, 10).Value = claim.DATE_AUTH;
                    _Cmd.Parameters.Add("@DDTEVISIT", SqlDbType.VarChar, 10).Value = claim.DDTEVISIT;
                    _Cmd.Parameters.Add("@INCURRED_DATE", SqlDbType.VarChar, 10).Value = claim.INCURRED_DATE;
                    _Cmd.Parameters.Add("@HOSPITAL_NAME_ENG", SqlDbType.VarChar, 255).Value = claim.HOSPITAL_NAME_ENG;
                    _Cmd.Parameters.Add("@DATA_FROM", SqlDbType.VarChar, 1).Value = claim.DATA_FROM;

                    _Cmd.CommandType = System.Data.CommandType.Text;


                    _Da.InsertCommand = _Cmd;
                    log.Debug(_Cmd.CommandText);
                    _Conn.Open();
                    log.Info("========DB Invoke========");
                    int result = _Da.InsertCommand.ExecuteNonQuery();
                    log.Info("========DB Invoke========");
                    _Conn.Close();

                }

                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public DataSet GetCalimNotify(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "SELECT * FROM ClaimNotify where CONTRACT_NUMBER=@PolicyNo and MEMBER_NO=@MEMBER_NO";
                _Cmd.CommandType = System.Data.CommandType.Text;
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MEMBER_NO", SqlDbType.Char, 10).Value = memberNo;

                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }

        public DataSet GetAllClaim(string policyNo, string memberNo, string visitType)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "SELECT * FROM AllClaimDetails where CONTRACT_NUMBER=@PolicyNo and MEMBER_NO=@MemberNo and VISIT_TYPE=@VisitType";
                _Cmd.CommandType = System.Data.CommandType.Text;
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;
                _Cmd.Parameters.Add("@VisitType", SqlDbType.Char, 3).Value = visitType;

                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }

        }

        public bool InsertClaimNotify(string policyNo, string memberNo, string visitType, string claimId, string claimDate)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "INSERT INTO ClaimNotify (CONTRACT_NUMBER, MEMBER_NO, VISIT_TYPE, CLAIM_ID, CLAIM_DATE_R) VALUES " +
                                   "(@PolicyNo, @MemberNo, @VisitType, @ClaimId, @ClaimDate)";

                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.VarChar, 8).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.VarChar, 8).Value = memberNo;
                _Cmd.Parameters.Add("@VisitType", SqlDbType.VarChar, 3).Value = visitType;
                _Cmd.Parameters.Add("@ClaimId", SqlDbType.VarChar, 255).Value = claimId;
                _Cmd.Parameters.Add("@ClaimDate", SqlDbType.VarChar, 10).Value = claimDate;
                _Cmd.CommandType = System.Data.CommandType.Text;


                _Da.InsertCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.InsertCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public bool UpdateClaimNotify(string policyNo, string memberNo, string visitType, string claimId, string claimDate)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = "UPDATE ClaimNotify SET CLAIM_ID=@ClaimId, CLAIM_DATE_R=@ClaimDate WHERE CONTRACT_NUMBER=@PolicyNo AND MEMBER_NO=@MemberNo AND VISIT_TYPE=@VisitType";

                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.VarChar, 8).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.VarChar, 8).Value = memberNo;
                _Cmd.Parameters.Add("@VisitType", SqlDbType.VarChar, 3).Value = visitType;
                _Cmd.Parameters.Add("@ClaimId", SqlDbType.VarChar, 255).Value = claimId;
                _Cmd.Parameters.Add("@ClaimDate", SqlDbType.VarChar, 10).Value = claimDate;
                _Cmd.CommandType = System.Data.CommandType.Text;


                _Da.UpdateCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.UpdateCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public DataSet GetMasterAgreement()
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                /*select * from SG_MEMEBER_DETAIL as smd, SG_CLAIM_MN as scm, SG_BENEFIT_HEALTH as sbh 
                WHERE smd.CHDRNUM = 'G0001115' AND SUBSTRING(smd.MEMBERNO,1,5) = '00394'
  AND smd.CHDRNUM = scm.CHDRNUM and smd.MEMBERNO = scm.MEMBERNO
  AND smd.MEMBERNO = sbh.MEMBERNO
  AND sbh.BENCDE in ('BH04', 'BA06', 'BH11', 'BH01', 'BDT2', 'BDT3', 'BA05', 'BH23', 'BOL1', 'BOL2', 'BOL3')
  order by smd.MEMBERNO;*/
                _Cmd.CommandText = "select * from MAS_AGREEMENT order by VERSION desc";
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public DataSet GetDebugQueryMFCGRP(string sql)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();

                _Cmd.CommandText = sql;
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public DataSet GetDebugQueryWEB(string sql)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _CmdWeb = _ConnWeb.CreateCommand();

                _CmdWeb.CommandText = sql;
                _CmdWeb.CommandType = System.Data.CommandType.Text;

                _DaWeb.SelectCommand = _CmdWeb;
                log.Debug(_CmdWeb.CommandText);
                _ConnWeb.Close();
                _ConnWeb.Open();
                log.Info("========DB Invoke========");
                _DaWeb.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnWeb.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }

        public DataSet GetDebugQueryECard(string sql)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _CmdECard = _ConnECard.CreateCommand();

                _CmdECard.CommandText = sql;
                _CmdECard.CommandType = System.Data.CommandType.Text;

                _DaECard.SelectCommand = _CmdECard;
                log.Debug(_CmdECard.CommandText);
                _ConnECard.Close();
                _ConnECard.Open();
                log.Info("========DB Invoke========");
                _DaECard.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnECard.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
        public bool UpdateTransAgreement(string policy, string memberNo, string version)
        {
            log.Info("in");
            try
            {
                string strDate = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" + DateTime.Now.Minute.ToString().PadLeft(2, '0') + ":" + DateTime.Now.Second.ToString().PadLeft(2, '0');
                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd = new SqlCommand("INSERT into TRANS_AGREEMENT (POLICY_NO, MEMBER_NO, MAS_AGREEMENT_VERSION, CREATED_DATE) " +
                       "VALUES (@PolicyNo, @MemberNo, @Version, @CreatedDate)", _Conn);

                //CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";

                _Cmd.CommandType = CommandType.Text;
                // Add the parameters for the InsertCommand.
                // Add the parameters for the InsertCommand.
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.NVarChar, 50).Value = policy;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.NVarChar, 50).Value = memberNo;
                _Cmd.Parameters.Add("@Version", SqlDbType.BigInt).Value = Convert.ToInt64(version);
                _Cmd.Parameters.Add("@CreatedDate", SqlDbType.NVarChar, 50).Value = strDate;


                _Da.InsertCommand = _Cmd;


                log.Debug(_Cmd.CommandText);
                _Conn.Close();
                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.InsertCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Info(ex.ToString());

                throw ex;

            }
            finally
            {
                log.Info("out");
            }

        }


        public bool UpdateAuditLog(string serviceName, string memberNo, string policyNo, EnumMasEventLogId eventId, string osPlatform, string ClientIP)
        {
            log.Info("in");
            try
            {
                string strDate = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Hour.ToString().PadLeft(2, '0') + ":" + DateTime.Now.Minute.ToString().PadLeft(2, '0') + ":" + DateTime.Now.Second.ToString().PadLeft(2, '0');
                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd = new SqlCommand("INSERT into TRANS_EVENT_LOG (APP_ID, SERVICE_NAME, MEMBER_NO, POLICY_NO, EVENT_ID, CREATED_DATE, OS_PF, CLIENT_IP) " +
                                      "VALUES ('GRP_MOBILE', @ServiceName, @MemberNo, @PolicyNo, @EventId, @CreatedDate, @OsPlatform, @ClientIP)", _Conn);

                //CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";


                _Cmd.CommandType = CommandType.Text;
                // Add the parameters for the InsertCommand.
                // Add the parameters for the InsertCommand.
                _Cmd.Parameters.Add("@ServiceName", SqlDbType.NVarChar, 50).Value = serviceName;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.NVarChar, 50).Value = memberNo;
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.NVarChar, 50).Value = policyNo;
                _Cmd.Parameters.Add("@EventId", SqlDbType.Int, 50).Value = Convert.ToInt32(eventId);
                _Cmd.Parameters.Add("@CreatedDate", SqlDbType.NVarChar, 50).Value = strDate;
                _Cmd.Parameters.Add("@OsPlatform", SqlDbType.NVarChar, 50).Value = osPlatform;
                _Cmd.Parameters.Add("@ClientIP", SqlDbType.VarChar, 50).Value = ClientIP;


                _Da.InsertCommand = _Cmd;


                log.Debug(_Cmd.CommandText);
                _Conn.Close();
                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.InsertCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Info(ex.ToString());

                throw ex;

            }
            finally
            {
                log.Info("out");
            }

        }
    }
}
