﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;

namespace TMLTH.GRPMOB.Model
{
    public enum EnumMasEventLogId
    {
        LoginPass = 1,
        RegisterMissingUser = 2,
        ForgetPasswordChanged = 3,
        AgreementAccept = 4,
        PersonalInfoUpdate = 5,
        LoginWrongPass = 6,
        AgreementView = 7,
        RegisterExistUser = 8,
        RegisterPass = 9,
        ChangePassword = 10,
        ForgetPasswordError = 11,
        Logout = 12,
    }
    public class BaseModelSQL
    {
        protected ConnectionStringSettings _MySQLConSettings = ConfigurationManager.ConnectionStrings["MyDBConnectionString"];
        protected SqlConnection _Conn;
        protected SqlDataReader _MySQLDataReader;
        protected SqlCommand _Cmd;
        protected SqlDataAdapter _Da;


        protected ConnectionStringSettings _MySQLConSettingsWeb = ConfigurationManager.ConnectionStrings["MyDBConnectionStringWeb"];
        protected SqlConnection _ConnWeb;
        protected SqlDataReader _MySQLDataReaderWeb;
        protected SqlCommand _CmdWeb;
        protected SqlDataAdapter _DaWeb;


        protected ConnectionStringSettings _MySQLConSettingsECard = ConfigurationManager.ConnectionStrings["MyDBConnectionStringECard"];
        protected SqlConnection _ConnECard;
        protected SqlDataReader _MySQLDataReaderECard;
        protected SqlCommand _CmdECard;
        protected SqlDataAdapter _DaECard;



        protected ConnectionStringSettings _MySQLConSettingsAppVersion = ConfigurationManager.ConnectionStrings["MyDBConnectionStringAppVersion"];
        protected SqlConnection _ConnAppVersion;
        protected SqlDataReader _MySQLDataReaderAppVersion;
        protected SqlCommand _CmdAppVersion;
        protected SqlDataAdapter _DaAppVersion;


        //for test nid
        //protected ConnectionStringSettings _MyDBConnectionStringMemberCard = ConfigurationManager.ConnectionStrings["MyDBConnectionStringMemberCard"];
        //protected SqlConnection _ConnMemberCard;
        //protected SqlDataReader _MySQLDataReaderMemberCard;
        //protected SqlCommand _CmdMemberCard;
        //protected SqlDataAdapter _DaMemberCard;


        public BaseModelSQL()
        {
            _Conn = new SqlConnection(_MySQLConSettings.ConnectionString);
            _Da = new SqlDataAdapter();


            _ConnWeb = new SqlConnection(_MySQLConSettingsWeb.ConnectionString);
            _DaWeb = new SqlDataAdapter();


            _ConnECard = new SqlConnection(_MySQLConSettingsECard.ConnectionString);
            _DaECard = new SqlDataAdapter();



            _ConnAppVersion = new SqlConnection(_MySQLConSettingsAppVersion.ConnectionString);
            _DaAppVersion = new SqlDataAdapter();

            //For test nid
            //_ConnMemberCard = new SqlConnection(_MyDBConnectionStringMemberCard.ConnectionString);
            //_DaMemberCard = new SqlDataAdapter();

        }


    }
}
