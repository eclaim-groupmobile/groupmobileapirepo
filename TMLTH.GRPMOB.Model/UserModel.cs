﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace TMLTH.GRPMOB.Model
{
    public class UserModel:BaseModelSQL
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public DataSet GetUserInfo(string memberNo, string policyNo)
        {
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                //select * from SG_MEMEBER_DETAIL WHERE CHDRNUM=@CHDRNUM and MEMBERNO =@MEMBERNO	
                _Cmd.CommandText = "select * from guserpass as guser, SG_MEMEBER_DETAIL as smd where guser.chdrnum = smd.CHDRNUM AND guser.usernum = smd.MEMBERNO AND smd.CHDRNUM='" + policyNo + "' AND smd.MEMBERNO='" + memberNo + "'";
                log.Debug(_Cmd.CommandText);
                //_MySQLCommand.CommandType = System.Data.CommandType.Text;

                _Da.SelectCommand = _Cmd;

                _Conn.Open();

                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");

                _Conn.Close();

                return ds;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }

        }
        public bool IsExistOnSG_MEMBER_DETAIL(string memberNo, string policyNo)
        {
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                //select * from SG_MEMEBER_DETAIL WHERE CHDRNUM=@CHDRNUM and MEMBERNO =@MEMBERNO	
                _Cmd.CommandText = "SELECT count(*) from SG_MEMEBER_DETAIL WHERE CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";
                //_MySQLCommand.CommandType = System.Data.CommandType.Text;
                log.Debug(_Cmd.CommandText);
                _Da.SelectCommand = _Cmd;

                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == 1)
                    return true;
                else
                    return false;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
          
        }

        public bool IsDOBValid(string reqDOB,string memberNo, string policyNo)
        {
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                //select * from SG_MEMEBER_DETAIL WHERE CHDRNUM=@CHDRNUM and MEMBERNO =@MEMBERNO	
                _Cmd.CommandText = "SELECT CLTDOB from SG_MEMEBER_DETAIL WHERE CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";
                log.Debug(_Cmd.CommandText);
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Da.SelectCommand = _Cmd;

                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string strTemp = ds.Tables[0].Rows[0]["CLTDOB"].ToString();

                    string strDate = strTemp.Substring(6,4) + strTemp.Substring(3,2) + strTemp.Substring(0,2);

                    if (reqDOB.Equals(strDate))
                        return true;
                    else
                        return false;
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }

        }
        public bool IsCitizenValid(string reqCitizen, string memberNo, string policyNo)
        {

                try
                {
                    DataSet ds = new DataSet();
                    _Cmd = _Conn.CreateCommand();
                    //select * from SG_MEMEBER_DETAIL WHERE CHDRNUM=@CHDRNUM and MEMBERNO =@MEMBERNO	
                    _Cmd.CommandText = "SELECT SECUITYNO from SG_MEMEBER_DETAIL WHERE CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";
                    log.Debug(_Cmd.CommandText);
                    _Cmd.CommandType = System.Data.CommandType.Text;

                    _Da.SelectCommand = _Cmd;

                    _Conn.Open();
                    log.Info("========DB Invoke========");
                    _Da.Fill(ds);
                    log.Info("========DB Invoke========");
                    _Conn.Close();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strTemp = ds.Tables[0].Rows[0]["SECUITYNO"].ToString();                                             

                        if (reqCitizen.Equals(strTemp.Trim()))
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;

                }
                catch (Exception ex)
                {
                log.Error(ex.ToString());
                throw ex;
                }

            }
        public bool IsMemberInfoValid(string reqDOB, string reqCitizen, string memberNo, string policyNo)
        {

            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                //select * from SG_MEMEBER_DETAIL WHERE CHDRNUM=@CHDRNUM and MEMBERNO =@MEMBERNO	
                _Cmd.CommandText = "SELECT * from SG_MEMEBER_DETAIL WHERE CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";
                log.Debug(_Cmd.CommandText);
                _Cmd.CommandType = System.Data.CommandType.Text;

                _Da.SelectCommand = _Cmd;

                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    // check citizen

                    string strTemp = ds.Tables[0].Rows[0]["SECUITYNO"].ToString();

                    if (!reqCitizen.Equals(strTemp.Trim()))
                        return false;

                    // check dob
                    strTemp = ds.Tables[0].Rows[0]["CLTDOB"].ToString();

                    string strDate = strTemp.Substring(6, 4) + strTemp.Substring(3, 2) + strTemp.Substring(0, 2);

                    if (!reqDOB.Equals(strDate))
                        return false;
                    //everything is valid;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }

        }
        public bool IsUserRegis(string username, string policyNo)
        {

            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd.CommandText = "SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum='" + policyNo +"' AND usernum='" + username + "'";
                log.Debug(_Cmd.CommandText);
                //_MySQLCommand.CommandType = System.Data.CommandType.Text;

                _Da.SelectCommand = _Cmd;

                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == 0)
                    return false;
                else
                    return true;
                        
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }

        }


        public bool IsUserRegisCompleted(string username, string policyNo)
        {

            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd.CommandText = "SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum='" + policyNo + "' AND usernum='" + username + "' AND completed='Y'";
                log.Debug(_Cmd.CommandText);
                //_MySQLCommand.CommandType = System.Data.CommandType.Text;

                _Da.SelectCommand = _Cmd;

                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == 0)
                    return false;
                else
                    return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }

        }
        public bool IsAnyRecordIncompleted(string username, string policyNo)
        {

            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd.CommandText = "SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum='" + policyNo + "' AND usernum='" + username + "' AND completed!='Y'";
                log.Debug(_Cmd.CommandText);
                //_MySQLCommand.CommandType = System.Data.CommandType.Text;

                _Da.SelectCommand = _Cmd;

                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == 0)
                    return false;
                else
                    return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }

        }


        public bool IsPasswordCorrect(string username, string policyNo, string password)
        {

            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd.CommandText = "SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum='" + policyNo + "' AND usernum='" + username + "' AND password1='" + password + "'";
                log.Debug(_Cmd.CommandText);
                //_MySQLCommand.CommandType = System.Data.CommandType.Text;

                _Da.SelectCommand = _Cmd;

                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == 0)
                    return false;
                else
                    return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }

        }
        // if (user.DoRegister(req.citizenID, req.DOB, req.email, req.lineID, req.memberNo, req.OTP, req.policyNo))
        public bool DoRegister(string citizenId, string DOB, string email, string lineId, string memberNo, string OTP, string policyNo, string mobileNo)
        {

            try
            {
                if (IsAnyRecordIncompleted(memberNo, policyNo))
                {
                    _Cmd = new SqlCommand("UPDATE guserpass SET passport = @Passport, mobile_no=@MobileNo, tel=@MobileNo , email=@Email, line_id=@LineId, completed=@Completed " +
                                          "WHERE chdrnum=@ChdrNum AND usernum=@UserNum", _Conn);
                    // Add the parameters for the UpdateCommand.
                    _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                    _Cmd.Parameters.Add("@UserNum", SqlDbType.Char, 10).Value = memberNo;
                    _Cmd.Parameters.Add("@Passport", SqlDbType.VarChar, 50).Value = citizenId;
                    _Cmd.Parameters.Add("@MobileNo", SqlDbType.VarChar, 20).Value = mobileNo;
                    _Cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100).Value = email;
                    _Cmd.Parameters.Add("@LineId", SqlDbType.VarChar, 20).Value = lineId;
                    _Cmd.Parameters.Add("@Completed", SqlDbType.VarChar, 1).Value = "N";
                }
                else
                {
                    //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                    _Cmd = new SqlCommand("INSERT INTO guserpass (chdrnum, usernum, passport, mobile_no, email, line_id, completed, tel) " +
                                             "VALUES (@ChdrNum, @UserNum, @Passport, @MobileNo, @Email, @LineId, @Completed, @MobileNo)", _Conn);

                    // Add the parameters for the InsertCommand.
                    _Cmd.Parameters.Add("@ChdrNum", SqlDbType.Char, 10).Value = policyNo;
                    _Cmd.Parameters.Add("@UserNum", SqlDbType.Char, 10).Value = memberNo;
                    _Cmd.Parameters.Add("@Passport", SqlDbType.VarChar, 50).Value = citizenId;
                    _Cmd.Parameters.Add("@MobileNo", SqlDbType.VarChar, 20).Value = mobileNo;
                    _Cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100).Value = email;
                    _Cmd.Parameters.Add("@LineId", SqlDbType.VarChar, 20).Value = lineId;
                    _Cmd.Parameters.Add("@Completed", SqlDbType.VarChar, 1).Value = "N";
                }
                _Cmd.CommandType = CommandType.Text;

                log.Debug(_Cmd.CommandText);
                _Da.InsertCommand = _Cmd;



                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.InsertCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch(Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
        }

        public bool DoUpdateLogin(string token, string installationId, string lastLogin, string memberNo, string policyNo)
        {
            try
            {


                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd = new SqlCommand("UPDATE guserpass SET token=@Token, installation_id=@InstallationId, last_login=@LastLogin WHERE chdrnum=@PolicyNo AND usernum=@MemberNo", _Conn);

                //CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";

                _Cmd.CommandType = CommandType.Text;
                // Add the parameters for the InsertCommand.
                // Add the parameters for the InsertCommand.
                _Cmd.Parameters.Add("@Token", SqlDbType.Char, 255).Value = token;
                _Cmd.Parameters.Add("@InstallationId", SqlDbType.Char, 50).Value = installationId;
                _Cmd.Parameters.Add("@LastLogin", SqlDbType.Char, 50).Value = lastLogin;
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;

                _Da.UpdateCommand = _Cmd;

                log.Debug(_Cmd.CommandText);
                _Conn.Close();
                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.UpdateCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
        }
        public bool DoClearToken(string token, string installationId, string memberNo, string policyNo)
        {
            try
            {


                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd = new SqlCommand("UPDATE guserpass SET token=@Token, installation_id=@InstallationId WHERE chdrnum=@PolicyNo AND usernum=@MemberNo", _Conn);

                //CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";

                _Cmd.CommandType = CommandType.Text;
                // Add the parameters for the InsertCommand.
                // Add the parameters for the InsertCommand.
                _Cmd.Parameters.Add("@Token", SqlDbType.Char, 255).Value = token;
                _Cmd.Parameters.Add("@InstallationId", SqlDbType.Char, 50).Value = installationId;
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;

                _Da.UpdateCommand = _Cmd;

                log.Debug(_Cmd.CommandText);
                _Conn.Close();
                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.UpdateCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
        }
        public bool DoUpdatePassword(string memberNo, string policyNo, string password)
        {

            try
            {


                //SELECT COUNT(*) as Num FROM guserpass WHERE chdrnum = @CHDRNUM AND usernum = @MEMBERNO
                _Cmd = new SqlCommand("UPDATE guserpass SET password1=@PassWord, completed='Y' WHERE chdrnum=@PolicyNo AND usernum=@MemberNo", _Conn);

                //CHDRNUM='" + policyNo + "' AND MEMBERNO='" + memberNo + "'";

                _Cmd.CommandType = CommandType.Text;
                // Add the parameters for the InsertCommand.
                // Add the parameters for the InsertCommand.
                _Cmd.Parameters.Add("@PassWord", SqlDbType.Char, 100).Value = password;
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.Char, 10).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.Char, 10).Value = memberNo;


                _Da.UpdateCommand = _Cmd;

                log.Debug(_Cmd.CommandText);

                _Conn.Open();
                log.Info("========DB Invoke========");
                int result = _Da.UpdateCommand.ExecuteNonQuery();
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
        }


        public bool UpdateTempOTP(string action, string citizenId, string memberNo, string policyNo, string mobileNo, string OTP, string refId)
        {
            try
            {
                DataSet ds = new DataSet();
                _CmdWeb = _ConnWeb.CreateCommand();

                _CmdWeb.CommandText = "select * from tTempOTP where action=@Action and member_no=@MemberNo and policy_no=@PolicyNo";
                _CmdWeb.CommandType = System.Data.CommandType.Text;
                _CmdWeb.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action;
                _CmdWeb.Parameters.Add("@MemberNo", SqlDbType.VarChar, 50).Value = memberNo;
                _CmdWeb.Parameters.Add("@PolicyNo", SqlDbType.VarChar, 50).Value = policyNo;


                _DaWeb.SelectCommand = _CmdWeb;
                log.Debug(_CmdWeb.CommandText);
                _ConnWeb.Open();
                log.Info("========DB Invoke========");
                _DaWeb.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnWeb.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    // update the current reord
                    _CmdWeb = null;
                    _CmdWeb = _ConnWeb.CreateCommand();

                    _CmdWeb.CommandText = "update tTempOTP set citizen_id=@CitizenId, mobile_no=@MobileNo, otp_value=@OTPValue, status='VALID', ref_id=@RefId where action=@Action and member_no=@MemberNo and policy_no=@PolicyNo";
                    _CmdWeb.CommandType = System.Data.CommandType.Text;
                    _CmdWeb.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action;
                    _CmdWeb.Parameters.Add("@MemberNo", SqlDbType.VarChar, 50).Value = memberNo;
                    _CmdWeb.Parameters.Add("@PolicyNo", SqlDbType.VarChar, 50).Value = policyNo;

                    // value to be update
                    _CmdWeb.Parameters.Add("@CitizenId", SqlDbType.VarChar, 50).Value = citizenId;
                    _CmdWeb.Parameters.Add("@MobileNo", SqlDbType.VarChar, 50).Value = mobileNo;
                    _CmdWeb.Parameters.Add("@OTPValue", SqlDbType.VarChar, 50).Value = OTP;
                    _CmdWeb.Parameters.Add("@RefId", SqlDbType.VarChar, 50).Value = refId;


                    _DaWeb.UpdateCommand = _CmdWeb;
                    log.Debug(_CmdWeb.CommandText);
                    _ConnWeb.Open();
                    log.Info("========DB Invoke========");
                    _DaWeb.UpdateCommand.ExecuteNonQuery();
                    log.Info("========DB Invoke========");
                    _ConnWeb.Close();
                    return true;
                }
                else
                {
                    // insert new record

                    _CmdWeb = null;
                    _CmdWeb = _ConnWeb.CreateCommand();

                    _CmdWeb.CommandText = "INSERT INTO tTempOTP (action, citizen_id, member_no, policy_no, mobile_no, status, otp_value, ref_id) values(@Action, @CitizenId, @MemberNo, @PolicyNo, @MobileNo, 'VALID', @OTPValue, @RefId)";
                    _CmdWeb.CommandType = System.Data.CommandType.Text;
                    _CmdWeb.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action;
                    _CmdWeb.Parameters.Add("@MemberNo", SqlDbType.VarChar, 50).Value = memberNo;
                    _CmdWeb.Parameters.Add("@PolicyNo", SqlDbType.VarChar, 50).Value = policyNo;
                    _CmdWeb.Parameters.Add("@CitizenId", SqlDbType.VarChar, 50).Value = citizenId;
                    _CmdWeb.Parameters.Add("@MobileNo", SqlDbType.VarChar, 50).Value = mobileNo;
                    _CmdWeb.Parameters.Add("@OTPValue", SqlDbType.VarChar, 50).Value = OTP;
                    _CmdWeb.Parameters.Add("@RefId", SqlDbType.VarChar, 50).Value = refId;


                    _DaWeb.InsertCommand = _CmdWeb;
                    log.Debug(_CmdWeb.CommandText);
                    _ConnWeb.Open();
                    log.Info("========DB Invoke========");
                    _DaWeb.InsertCommand.ExecuteNonQuery();
                    log.Info("========DB Invoke========");
                    _ConnWeb.Close();
                    return true;
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }


        }

        public string GetTempOTPStatus(string action, string citizenId, string memberNo, string policyNo, string mobileNo, string OTP, string refId)
        {
            try
            {
                DataSet ds = new DataSet();
                _CmdWeb = _ConnWeb.CreateCommand();

                _CmdWeb.CommandText = "select * from tTempOTP where action=@Action and member_no=@MemberNo and policy_no=@PolicyNo and ref_id=@RefId and otp_value=@OTPValue";
                _CmdWeb.CommandType = System.Data.CommandType.Text;
                _CmdWeb.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action;
                _CmdWeb.Parameters.Add("@MemberNo", SqlDbType.VarChar, 50).Value = memberNo;
                _CmdWeb.Parameters.Add("@PolicyNo", SqlDbType.VarChar, 50).Value = policyNo;
                _CmdWeb.Parameters.Add("@RefId", SqlDbType.VarChar, 50).Value = refId;
                _CmdWeb.Parameters.Add("@OTPValue", SqlDbType.VarChar, 50).Value = OTP;


                _DaWeb.SelectCommand = _CmdWeb;
                log.Debug(_CmdWeb.CommandText);
                _ConnWeb.Open();
                log.Info("========DB Invoke========");
                _DaWeb.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnWeb.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0]["status"].ToString();
                }
                else
                {
                    return "INVALID";
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }


        }
        public DataSet TestGetTempOTP(string action, string memberNo)
        {
            try
            {
                DataSet ds = new DataSet();
                _CmdWeb = _ConnWeb.CreateCommand();

                _CmdWeb.CommandText = "select * from tTempOTP where action=@Action and member_no=@MemberNo";
                _CmdWeb.CommandType = System.Data.CommandType.Text;
                _CmdWeb.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action;
                _CmdWeb.Parameters.Add("@MemberNo", SqlDbType.VarChar, 50).Value = memberNo;



                _DaWeb.SelectCommand = _CmdWeb;
                log.Debug(_CmdWeb.CommandText);
                _ConnWeb.Open();
                log.Info("========DB Invoke========");
                _DaWeb.Fill(ds);
                log.Info("========DB Invoke========");
                _ConnWeb.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;
                }
                else
                {
                    return null; ;
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }


        }

        public DataSet GetAcceptAgreement(string policyNo, string memberNo)
        {
            log.Info("in");
            try
            {
                DataSet ds = new DataSet();
                _Cmd = _Conn.CreateCommand();
                /*select * from SG_MEMEBER_DETAIL as smd, SG_CLAIM_MN as scm, SG_BENEFIT_HEALTH as sbh 
                WHERE smd.CHDRNUM = 'G0001115' AND SUBSTRING(smd.MEMBERNO,1,5) = '00394'
  AND smd.CHDRNUM = scm.CHDRNUM and smd.MEMBERNO = scm.MEMBERNO
  AND smd.MEMBERNO = sbh.MEMBERNO
  AND sbh.BENCDE in ('BH04', 'BA06', 'BH11', 'BH01', 'BDT2', 'BDT3', 'BA05', 'BH23', 'BOL1', 'BOL2', 'BOL3')
  order by smd.MEMBERNO;*/
                _Cmd.CommandText = "select * from TRANS_AGREEMENT where MEMBER_NO = @MemberNo AND POLICY_NO = @PolicyNo order by MAS_AGREEMENT_VERSION desc";
                _Cmd.CommandType = System.Data.CommandType.Text;
                _Cmd.Parameters.Add("@PolicyNo", SqlDbType.NVarChar, 50).Value = policyNo;
                _Cmd.Parameters.Add("@MemberNo", SqlDbType.NVarChar, 50).Value = memberNo;


                _Da.SelectCommand = _Cmd;
                log.Debug(_Cmd.CommandText);
                _Conn.Open();
                log.Info("========DB Invoke========");
                _Da.Fill(ds);
                log.Info("========DB Invoke========");
                _Conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;

                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }
        }
    }

 


    

}
