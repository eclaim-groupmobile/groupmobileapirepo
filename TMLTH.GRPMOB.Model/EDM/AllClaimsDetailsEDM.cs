﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMLTH.GRPMOB.Model.EDM
{
    public class AllClaimsDetailsEDM
    {
        public String CONTRACT_NUMBER { get; set; }
        public String MEMBER_NO { get; set; }
        public String DEPENDENCEP_NO { get; set; }
        public String CLAMNUM { get; set; }
        public String VISIT_TYPE { get; set; }
        public String PRODUCT_TYPE_CODE { get; set; }
        public String START_DATE_HOSPITAL { get; set; }
        public String END_DATE_HOSPITAL { get; set; }
        public String DIAGNOSE_TH { get; set; }
        public String DIAGNOSE_EN { get; set; }
        public String FORFEIT { get; set; }
        public Decimal INCURRED { get; set; }
        public Decimal HMOSHARE { get; set; }
        public Decimal MBRSHARE { get; set; }
        public String HOSPITAL { get; set; }
        public String GCSTS { get; set; }
        public String GCSTSENNAME { get; set; }
        public String GCSTSTHNAME { get; set; }
        public String PRODUCT_TYPE { get; set; }
        public String CLAIM_TYPE { get; set; }
        public String CLAIM_TYPE_DESC { get; set; }
        public String DATE_AUTH { get; set; }
        public String DDTEVISIT { get; set; }
        public String INCURRED_DATE { get; set; }
        public String HOSPITAL_NAME_ENG { get; set; }
        public String DATA_FROM { get; set; }
    }
}
