﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


namespace TMLTH.GRPMOB.ServicePipeline
{
    public class OTPPipeline
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string GetOTP(string mobile, string passport, string key, string policyNo, string memberNo, string refId)
        {
            log.Info("in");
            try
            {
                // set security protocol
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //Trust all certificates
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender, certificate, chain, sslPolicyErrors) => true);

                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                string securityProtocol = ConfigurationManager.AppSettings.Get("OTPSecurityProtocol");

                if (null == securityProtocol)
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                }
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                else  if (securityProtocol.Equals("1"))
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                }
                else if (securityProtocol.Equals("1,2"))
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11;
                }
                else if (securityProtocol.Equals("1,2,3"))
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                }
                else if (securityProtocol.Equals("1,2,3,4"))
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                }
                else if (securityProtocol.Equals("2"))
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                }
                else if (securityProtocol.Equals("3"))
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }
                else if (securityProtocol.Equals("4"))
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                }

                log.Debug(System.Net.ServicePointManager.SecurityProtocol.ToString());
                //var otpClient = new OTPSOAPService.wsOTP_AuthenSoapClient();
                var otpClient = new OTPServiceProdWSDL.wsOTPSoapClient();
                log.Info("call request OTP");
                log.Info("OTP URL: " + otpClient.Endpoint.ListenUri.AbsoluteUri.ToString());
                string strResponse = otpClient.Request_OTP(mobile, passport, key, policyNo + memberNo, refId);
                log.Info("end request OTP");

                log.Info("OTP is " + strResponse);
                return strResponse;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }


        }


        public string ChkOTP(string mobile, string passport, string key, string policyNo, string memberNo, string OTP, string refId)
        {
            log.Info("in");
            try
            {
                // set security protocol
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //Trust all certificates
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender, certificate, chain, sslPolicyErrors) => true);

                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                log.Debug(System.Net.ServicePointManager.SecurityProtocol.ToString());


                //var otpClient = new OTPSOAPService.wsOTP_AuthenSoapClient();
                var otpClient = new OTPServiceProdWSDL.wsOTPSoapClient();
                    log.Info("call enquire OTP");
                string strResponse = otpClient.Enqurie(mobile, key, OTP, policyNo + memberNo, refId);
                log.Info("end enquire OTP");

                log.Info("status is " + strResponse);
                return strResponse;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                log.Info("out");
            }


        }

        public string GenerateRefCode()
        {
            Random random = new Random();
            string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            int length = 6;

            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                result.Append(characters[random.Next(characters.Length)]);
            }
            return result.ToString();
        }
        public void OverrideSSL()
        {
            //Trust all certificates
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                ((sender, certificate, chain, sslPolicyErrors) => true);

            // trust sender
            System.Net.ServicePointManager.ServerCertificateValidationCallback
                            = ((sender, cert, chain, errors) => cert.Subject.Contains("YourServerName"));

        

        }
    }
}
