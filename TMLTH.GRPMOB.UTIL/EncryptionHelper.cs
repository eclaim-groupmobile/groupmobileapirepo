﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Security.Cryptography;


namespace TMLTH.GRPMOB.UTIL
{

    public static class AesEncyptsDecrypts
    {
        public static string aes128Encrypt(string inputvalue)
        {
            string ret = "";

            try
            {
                string sKey = "6odsJ5zkJnr+L86YRXsP2w==";
                string sIV = "I1ugVxAUi5f7JoPxTC+FlA==";
                ret = EncryptionHelper.EncryptStringToBytes_Aes(inputvalue, sKey, sIV);
            }
            catch (Exception ex)
            {

            }

            return ret;
        }


        public static string aes128Decrypt(string inputvalue)
        {
            string ret = "";

            try
            {
                string sKey = "6odsJ5zkJnr+L86YRXsP2w==";
                string sIV = "I1ugVxAUi5f7JoPxTC+FlA==";
                ret = EncryptionHelper.DecryptStringFromBytes_Aes(inputvalue, sKey, sIV);
            }
            catch (Exception ex)
            {

            }

            return ret;
        }
        public static class EncryptionHelper
    {
        public class EncryptResult
        {
            public string EncryptedMsg { get; set; }
            public string IV { get; set; }
        }

 
            public static string GenerateRandomSaltString(RNGCryptoServiceProvider rng, int size)
            {
                var bytes = new byte[size - 1 + 1];
                rng.GetBytes(bytes);
                return Convert.ToBase64String(bytes);
            }

            public static byte[] GenerateRandomSaltByte(RNGCryptoServiceProvider rng, int size)
            {
                var bytes = new byte[size - 1 + 1];
                rng.GetBytes(bytes);
                string x = Convert.ToBase64String(bytes);
                var b = Convert.FromBase64String(x);
                return b;
            }

            public static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
            {
                if (plainText == null || plainText.Length <= 0)
                    throw new ArgumentNullException("plainText");
                if (Key == null || Key.Length <= 0)
                    throw new ArgumentNullException("Key");
                if (IV == null || IV.Length <= 0)
                    throw new ArgumentNullException("IV");
                byte[] encrypted = new byte[16] ;

                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.Key = Key;
                    aesAlg.IV = IV;
                    aesAlg.Mode = CipherMode.CBC;
                    aesAlg.Padding = PaddingMode.PKCS7;
                    ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                    using (MemoryStream msEncrypt = new MemoryStream())
                    {
                        using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(plainText);
                            }

                            encrypted = msEncrypt.ToArray();
                        }
                    }
                }

                return encrypted;
            }

            public static string EncryptStringToBytes_Aes(string plainText, string Key, string IV)
            {
                if (plainText == null || plainText.Length <= 0)
                    throw new ArgumentNullException("plainText");
                if (Key == null || Key.Length <= 0)
                    throw new ArgumentNullException("Key");
                if (IV == null || IV.Length <= 0)
                    throw new ArgumentNullException("IV");
                byte[] encrypted = new byte[16] ;
                byte[] bKey = Convert.FromBase64String(Key);
                byte[] bIV = Convert.FromBase64String(IV);
                encrypted = EncryptStringToBytes_Aes(plainText, bKey, bIV);
                return Convert.ToBase64String(encrypted);
            }

            public static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
            {
                if (cipherText == null || cipherText.Length <= 0)
                    throw new ArgumentNullException("cipherText");
                if (Key == null || Key.Length <= 0)
                    throw new ArgumentNullException("Key");
                if (IV == null || IV.Length <= 0)
                    throw new ArgumentNullException("IV");
                string plaintext = null;

                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.Key = Key;
                    aesAlg.IV = IV;
                    aesAlg.Mode = CipherMode.CBC;
                    aesAlg.Padding = PaddingMode.PKCS7;
                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                    using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                    {
                        using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                            {
                                plaintext = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }

                return plaintext;
            }

            public static string DecryptStringFromBytes_Aes(string bcipherText, string Key, string IV)
            {
                if (bcipherText == null || bcipherText.Length <= 0)
                    throw new ArgumentNullException("cipherText");
                if (Key == null || Key.Length <= 0)
                    throw new ArgumentNullException("Key");
                if (IV == null || IV.Length <= 0)
                    throw new ArgumentNullException("IV");
                string plaintext = null;
                byte[] cipherText = Convert.FromBase64String(bcipherText);
                byte[] bKey = Convert.FromBase64String(Key);
                byte[] bIV = Convert.FromBase64String(IV);
                plaintext = DecryptStringFromBytes_Aes(cipherText, bKey, bIV);
                return plaintext;
            }

    }
}
