﻿using System.Runtime.Serialization;

namespace THMLTH.GRPMOB.DTO.Common
{
    public class EcardParams
    {
        [DataMember]
        public string memberNo { get; set; }
        [DataMember]
        public string policyStartDT { get; set; }
        [DataMember]
        public string policyEndDT { get; set; }
        [DataMember]
        public string firstName { get; set; }
        [DataMember]
        public string lastName { get; set; }
        [DataMember]
        public string claimLifeGTL { get; set; }
        [DataMember]
        public string claimBenefit1 { get; set; }
        [DataMember]
        public string claimBenefit2 { get; set; }
        [DataMember]
        public string claimBenefit3 { get; set; }
        [DataMember]
        public string claimBenefit4 { get; set; }
        [DataMember]
        public string claimBenefit5 { get; set; }
        [DataMember]
        public string claimBenefit6 { get; set; }
        [DataMember]
        public string claimBenefit7 { get; set; }
        [DataMember]
        public string claimBenefit8 { get; set; }
        [DataMember]
        public string genPage01 { get; set; }
        [DataMember]
        public string genPage02 { get; set; }
        [DataMember]
        public string genPage03 { get; set; }
        [DataMember]
        public string genPage04 { get; set; }
        [DataMember]
        public string genPage05 { get; set; }
        [DataMember]
        public string genPage06 { get; set; }
        [DataMember]
        public string genPage07 { get; set; }
        [DataMember]
        public string genPage08 { get; set; }
    }
}
