﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract]
    public class GetECardResponse : BaseResponse
    {
        [DataMember]
        public String token { get; set; }
        [DataMember]
        public String policyNo { get; set; }
        [DataMember]
        public String policyHolder { get; set; }
        [DataMember]
        public String signaturePic { get; set; }

        [DataMember]
        public EmployeeECard employeeECard { get; set; }
        [DataMember]
        public SpouseECard spouseECard { get; set; }
        [DataMember]
        public List<ChildECard> childrenECard { get; set; }

        public GetECardResponse()
        {
            employeeECard = new EmployeeECard();
            spouseECard = new SpouseECard();
            childrenECard = new List<ChildECard>();
        }

    }

    public class EmployeeECard
    {
        [DataMember]
        public String memberTypeTh { get; set; }
        [DataMember]
        public String memberTypeEn { get; set; }
        [DataMember]
        public String empMemberNo { get; set; }
        [DataMember]
        public String empFName { get; set; }
        [DataMember]
        public String empLName { get; set; }
        [DataMember]
        public String countryCode { get; set; }
        [DataMember]
        public String insuranceNo { get; set; }
        [DataMember]
        public String policyStartDT { get; set; }
        [DataMember]
        public String policyEndDT { get; set; }
        [DataMember]
        public String claimLifeGTL { get; set; }
        [DataMember]
        public String claimLifeGPTD { get; set; }
        [DataMember]
        public String claimBenefit1 { get; set; }
        [DataMember]
        public String claimBenefit2 { get; set; }
        [DataMember]
        public String claimBenefit3 { get; set; }        
        [DataMember]
        public String claimBenefit4 { get; set; }
        [DataMember]
        public String claimBenefit5 { get; set; }
        [DataMember]
        public String claimBenefit6 { get; set; }
        [DataMember]
        public String claimBenefit7 { get; set; }
        [DataMember]
        public String claimBenefit8 { get; set; }
        [DataMember]
        public String genPage01 { get; set; }
        [DataMember]
        public String genPage02 { get; set; }
        [DataMember]
        public String genPage03 { get; set; }
        [DataMember]
        public String genPage04 { get; set; }
        [DataMember]
        public String genPage05 { get; set; }
        [DataMember]
        public String genPage06 { get; set; }
        [DataMember]
        public String genPage07 { get; set; }
        [DataMember]
        public String genPage08 { get; set; }
    }

    public class SpouseECard
    {
        [DataMember]
        public String memberTypeTh { get; set; }
        [DataMember]
        public String memberTypeEn { get; set; }
        [DataMember]
        public String spuMemberNo { get; set; }
        [DataMember]
        public String spuFName { get; set; }
        [DataMember]
        public String spuLName { get; set; }
        [DataMember]
        public String countryCode { get; set; }
        [DataMember]
        public String insuranceNo { get; set; }
        [DataMember]
        public String policyStartDT { get; set; }
        [DataMember]
        public String policyEndDT { get; set; }
        [DataMember]
        public String claimLifeGTL { get; set; }
        [DataMember]
        public String claimLifeGPTD { get; set; }
        [DataMember]
        public String claimBenefit1 { get; set; }
        [DataMember]
        public String claimBenefit2 { get; set; }
        [DataMember]
        public String claimBenefit3 { get; set; }
        [DataMember]
        public String claimBenefit4 { get; set; }
        [DataMember]
        public String claimBenefit5 { get; set; }
        [DataMember]
        public String claimBenefit6 { get; set; }
        [DataMember]
        public String claimBenefit7 { get; set; }
        [DataMember]
        public String claimBenefit8 { get; set; }
        [DataMember]
        public String genPage01 { get; set; }
        [DataMember]
        public String genPage02 { get; set; }
        [DataMember]
        public String genPage03 { get; set; }
        [DataMember]
        public String genPage04 { get; set; }
        [DataMember]
        public String genPage05 { get; set; }
        [DataMember]
        public String genPage06 { get; set; }
        [DataMember]
        public String genPage07 { get; set; }
        [DataMember]
        public String genPage08 { get; set; }
    }

    public class ChildECard
    {
        [DataMember]
        public String memberTypeTh { get; set; }
        [DataMember]
        public String memberTypeEn { get; set; }
        [DataMember]
        public String childMemberNo { get; set; }
        [DataMember]
        public String childFName { get; set; }
        [DataMember]
        public String childLName { get; set; }
        [DataMember]
        public String countryCode { get; set; }
        [DataMember]
        public String insuranceNo { get; set; }
        [DataMember]
        public String policyStartDT { get; set; }
        [DataMember]
        public String policyEndDT { get; set; }
        [DataMember]
        public String claimLifeGTL { get; set; }
        [DataMember]
        public String claimLifeGPTD { get; set; }
        [DataMember]
        public String claimBenefit1 { get; set; }
        [DataMember]
        public String claimBenefit2 { get; set; }
        [DataMember]
        public String claimBenefit3 { get; set; }
        [DataMember]
        public String claimBenefit4 { get; set; }
        [DataMember]
        public String claimBenefit5 { get; set; }
        [DataMember]
        public String claimBenefit6 { get; set; }
        [DataMember]
        public String claimBenefit7 { get; set; }
        [DataMember]
        public String claimBenefit8 { get; set; }
        [DataMember]
        public String genPage01 { get; set; }
        [DataMember]
        public String genPage02 { get; set; }
        [DataMember]
        public String genPage03 { get; set; }
        [DataMember]
        public String genPage04 { get; set; }
        [DataMember]
        public String genPage05 { get; set; }
        [DataMember]
        public String genPage06 { get; set; }
        [DataMember]
        public String genPage07 { get; set; }
        [DataMember]
        public String genPage08 { get; set; }
    }
}
