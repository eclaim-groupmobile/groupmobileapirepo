﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract]
    public class TestGetResponse:BaseResponse
    {
        [DataMember]
        public String ColumnResponse { get; set; }
        [DataMember]
        public String [] ValueResponse  { get; set; }


    }


}
