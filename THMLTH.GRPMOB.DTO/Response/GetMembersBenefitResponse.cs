﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract] // DataContract เป็นตัวระบุว่าให้เปิดเผย Type นี้ผ่านทาง web service
    public class GetMembersBenefitResponse : BaseResponse
    {
        [DataMember(Order = 1)]
        public String policyNo { get; set; }
        [DataMember(Order = 2)]
        public String startDate { get; set; }
        [DataMember(Order = 3)]
        public String endDate { get; set; }
        [DataMember(Order = 4)]
        public String companyNM { get; set; }
        [DataMember(Order = 5)]
        public String token { get; set; }
        [DataMember(Order = 6)]
        public String language { get; set; }
        [DataMember(Order = 7)]
        public String claimTotNotify { get; set; }
        [DataMember(Order = 7)]
        public String hasECard { get; set; }


        [DataMember(Order = 8)]
        public EmployeeInfo employeeInfo { get; set; }

        [DataMember(Order = 9)]
        public SpouseInfo spouseInfo { get; set; }

        [DataMember(Order = 10)]
        public List<ChildInfo> childrensInfo { get; set; }

        public GetMembersBenefitResponse()
        {
            employeeInfo = new EmployeeInfo();
            spouseInfo = new SpouseInfo();
            childrensInfo = new List<ChildInfo>();
        }

    }


    public class EmployeeInfo
    {
        [DataMember(Order = 1)]
        public String memberTypeTh { get; set; }
        [DataMember(Order = 2)]
        public String memberTypeEn { get; set; }
        [DataMember(Order = 3)]
        public String empMemberNo { get; set; }
        [DataMember(Order = 4)]
        public String empFName { get; set; }
        [DataMember(Order = 5)]
        public String empLName { get; set; }
        [DataMember(Order = 6)]
        public String empDOB { get; set; }
        [DataMember(Order = 7)]
        public String empSex { get; set; }
        [DataMember(Order = 8)]
        public String empAgeTH { get; set; }
        [DataMember(Order = 8)]
        public String empAgeEN { get; set; }
        [DataMember(Order = 9)]
        public String empMobile { get; set; }
        [DataMember(Order = 10)]
        public String empEmail { get; set; }
        [DataMember(Order = 11)]
        public String empLineID { get; set; }
        [DataMember(Order = 12)]
        public String empCitizenID { get; set; }
        [DataMember(Order = 13)]
        public String empNationality { get; set; }
        [DataMember(Order = 14)]
        public String empCompanyName { get; set; }
        [DataMember(Order = 14)]
        public String empCompanyNameEN { get; set; }
        [DataMember(Order = 15)]
        public String empStartDate { get; set; }
        [DataMember(Order = 16)]
        public String empEndDate { get; set; }
        [DataMember(Order = 16)]
        public String empClaimIPDNotify { get; set; }
        [DataMember(Order = 16)]
        public String empClaimOPDNotify { get; set; }
        [DataMember(Order = 16)]
        public String empClaimLabsNotify { get; set; }
        [DataMember(Order = 16)]
        public String empClaimDentalNotify { get; set; }
        [DataMember(Order = 16)]
        public String empClaimAMENotify { get; set; }
        [DataMember]
        public String empHasIPD { get; set; }
        [DataMember]
        public String empHasOPD { get; set; }
        [DataMember]
        public String empHasAME { get; set; }
        [DataMember]
        public String empHasLAB { get; set; }
        [DataMember]
        public String empHasDEN { get; set; }

        [DataMember(Order = 17)]
        public EmpBenefitInfo empBenefitInfo { get; set; }


        public EmployeeInfo()
        {
            empBenefitInfo = new EmpBenefitInfo();
        }
        

    }

    public class EmpBenefitInfo
    {
        [DataMember]
        public String empGTLDetlTH { get; set; }
        [DataMember]
        public String empGTLDetlEN { get; set; }
        [DataMember]
        public String empGTL { get; set; }
        [DataMember]
        public String empGADDetlTH { get; set; }
        [DataMember]
        public String empGADDetlEN { get; set; }
        [DataMember]
        public String empGAD { get; set; }
        [DataMember]
        public String empGDADetlTH { get; set; }
        [DataMember]
        public String empGDADetlEN { get; set; }
        [DataMember]
        public String empGDA { get; set; }
        [DataMember]
        public String empGRCDetlTH { get; set; }
        [DataMember]
        public String empGRCDetlEN { get; set; }
        [DataMember]
        public String empGRC { get; set; }
        [DataMember]
        public String empGPTDDetlTH { get; set; }
        [DataMember]
        public String empGPTDDetlEN { get; set; }
        [DataMember]
        public String empGPTD { get; set; }

        public String empGCIDetlTH { get; set; }
        [DataMember]
        public String empGCIDetlEN { get; set; }
        [DataMember]
        public String empGCI { get; set; }
        [DataMember]
        public List<EmpIPDInfo> empIPDList { get; set; }
        [DataMember]
        public List<EmpOPDInfo> empOPDList { get; set; }
        [DataMember]
        public List<EmpDentalInfo> empDentalList { get; set; }
        [DataMember]
        public List<EmpAccidentInfo> empAccidentList { get; set; }
        [DataMember]
        public List<EmpCriticalInfo> empCriticalList { get; set; }
        [DataMember]
        public List<EmpMaternityInfo> empMaternityList { get; set; }

        public EmpBenefitInfo()
        {
            empIPDList = new List<EmpIPDInfo>();
            empOPDList = new List<EmpOPDInfo>();
            empDentalList = new List<EmpDentalInfo>();
            empAccidentList = new List<EmpAccidentInfo>();
            empCriticalList = new List<EmpCriticalInfo>();
            empMaternityList = new List<EmpMaternityInfo>();
        }


    }

    public class EmpIPDInfo
    {
        [DataMember]
        public String empBHCode { get; set; }
        [DataMember]
        public String empIPDDetlTH { get; set; }
        [DataMember]
        public String empIPDDetlEN { get; set; }
        [DataMember]
        public String empIPDCoPay { get; set; }
        [DataMember]
        public String empIPDDeduc { get; set; }
    }
    public class EmpOPDInfo
    {
        [DataMember]
        public String empBHCode { get; set; }
        [DataMember]
        public String empOPDDetlTH { get; set; }
        [DataMember]
        public String empOPDDetlEN { get; set; }
        [DataMember]
        public String empOPDCoPay { get; set; }
        [DataMember]
        public String empOPDDeduc { get; set; }
    }
    public class EmpDentalInfo
    {
        [DataMember]
        public String empBHCode { get; set; }
        [DataMember]
        public String empDentalDetlTH { get; set; }
        [DataMember]
        public String empDentalDetlEN { get; set; }
        [DataMember]
        public String empDentalCoPay { get; set; }
        [DataMember]
        public String empDentalDeduc { get; set; }
    }
    public class EmpAccidentInfo
    {
        [DataMember]
        public String empBHCode { get; set; }
        [DataMember]
        public String empAccidentDetlTH { get; set; }
        [DataMember]
        public String empAccidentDetlEN { get; set; }
        [DataMember]
        public String empAccidentCoPay { get; set; }
        [DataMember]
        public String empAccidentDeduc { get; set; }
    }
    public class EmpCriticalInfo
    {
        [DataMember]
        public String empBHCode { get; set; }
        [DataMember]
        public String empCriticalDetlTH { get; set; }
        [DataMember]
        public String empCriticalDetlEN { get; set; }
        [DataMember]
        public String empCriticalCoPay { get; set; }
        [DataMember]
        public String empCriticalDeduc { get; set; }
    }
    public class EmpMaternityInfo
    {
        [DataMember]
        public String empBHCode { get; set; }
        [DataMember]
        public String empMaternityDetlTH { get; set; }
        [DataMember]
        public String empMaternityDetlEN { get; set; }
        [DataMember]
        public String empMaternityCoPay { get; set; }
        [DataMember]
        public String empMaternityDeduc { get; set; }
    }
    public class SpouseInfo
    {
        [DataMember]
        public String memberTypeTh { get; set; }
        [DataMember]
        public String memberTypeEn { get; set; }
        [DataMember]
        public String spuMemberNo { get; set; }
        [DataMember]
        public String spuFName { get; set; }
        [DataMember]
        public String spuLName { get; set; }
        [DataMember]
        public String spuDOB { get; set; }
        [DataMember]
        public String spuSex { get; set; }
        [DataMember]
        public String spuAgeTH { get; set; }
        [DataMember]
        public String spuAgeEN { get; set; }
        [DataMember]
        public String spuMobile { get; set; }
        [DataMember]
        public String spuEmail { get; set; }
        [DataMember]
        public String spuLineID { get; set; }
        [DataMember]
        public String spuCitizenID { get; set; }
        [DataMember]
        public String spuNationality { get; set; }
        [DataMember]
        public String spuCompanyName { get; set; }
        [DataMember]
        public String spuCompanyNameEN { get; set; }
        [DataMember]
        public String spuStartDate { get; set; }
        [DataMember]
        public String spuEndDate { get; set; }
        [DataMember]
        public String spuClaimIPDNotify { get; set; }
        [DataMember]
        public String spuClaimOPDNotify { get; set; }
        [DataMember]
        public String spuClaimLabsNotify { get; set; }
        [DataMember]
        public String spuClaimDentalNotify { get; set; }
        [DataMember]
        public String spuClaimAMENotify { get; set; }

        [DataMember]
        public String spuHasIPD { get; set; }
        [DataMember]
        public String spuHasOPD { get; set; }
        [DataMember]
        public String spuHasAME { get; set; }
        [DataMember]
        public String spuHasLAB { get; set; }
        [DataMember]
        public String spuHasDEN { get; set; }

        [DataMember]
        public SpuBenefitInfo spuBenefitInfo { get; set; }

        public SpouseInfo()

        {
            spuBenefitInfo = new SpuBenefitInfo();

        }
    }

    public class SpuBenefitInfo
    {
        [DataMember]
        public String spuGTLDetlTH { get; set; }
        [DataMember]
        public String spuGTLDetlEN { get; set; }
        [DataMember]
        public String spuGTL { get; set; }
        [DataMember]
        public String spuGADDetlTH { get; set; }
        [DataMember]
        public String spuGADDetlEN { get; set; }
        [DataMember]
        public String spuGAD { get; set; }
        [DataMember]
        public String spuGDADetlTH { get; set; }
        [DataMember]
        public String spuGDADetlEN { get; set; }
        [DataMember]
        public String spuGDA { get; set; }
        [DataMember]
        public String spuGRCDetlTH { get; set; }
        [DataMember]
        public String spuGRCDetlEN { get; set; }
        [DataMember]
        public String spuGRC { get; set; }
        [DataMember]
        public String spuGPTDDetlTH { get; set; }
        [DataMember]
        public String spuGPTDDetlEN { get; set; }
        [DataMember]
        public String spuGPTD { get; set; }

        [DataMember]
        public String spuGCIDetlTH { get; set; }
        [DataMember]
        public String spuGCIDetlEN { get; set; }
        [DataMember]
        public String spuGCI { get; set; }


        [DataMember]
        public List<SpuIPDInfo> spuIPDList { get; set; }
        [DataMember]
        public List<SpuOPDInfo> spuOPDList { get; set; }
        [DataMember]
        public List<SpuDentalInfo> spuDentalList { get; set; }
        [DataMember]
        public List<SpuAccidentInfo> spuAccidentList { get; set; }
        [DataMember]
        public List<SpuCriticalInfo> spuCriticalList { get; set; }
        [DataMember]
        public List<SpuMaternityInfo> spuMaternityList { get; set; }



        public SpuBenefitInfo()
        {
            spuIPDList = new List<SpuIPDInfo>();
            spuOPDList = new List<SpuOPDInfo>();
            spuDentalList = new List<SpuDentalInfo>();
            spuAccidentList = new List<SpuAccidentInfo>();
            spuCriticalList = new List<SpuCriticalInfo>();
            spuMaternityList = new List<SpuMaternityInfo>();
        }
    }

    public class SpuIPDInfo
    {
        [DataMember]
        public String spuBHCode { get; set; }
        [DataMember]
        public String spuIPDDetlTH { get; set; }
        [DataMember]
        public String spuIPDDetlEN { get; set; }
        [DataMember]
        public String spuIPDCoPay { get; set; }
        [DataMember]
        public String spuIPDDeduc { get; set; }
    }
    public class SpuOPDInfo
    {
        [DataMember]
        public String spuBHCode { get; set; }
        [DataMember]
        public String spuOPDDetlTH { get; set; }
        [DataMember]
        public String spuOPDDetlEN { get; set; }
        [DataMember]
        public String spuOPDCoPay { get; set; }
        [DataMember]
        public String spuOPDDeduc { get; set; }
    }
    public class SpuDentalInfo
    {
        [DataMember]
        public String spuBHCode { get; set; }
        [DataMember]
        public String spuDentalDetlTH { get; set; }
        [DataMember]
        public String spuDentalDetlEN { get; set; }
        [DataMember]
        public String spuDentalCoPay { get; set; }
        [DataMember]
        public String spuDentalDeduc { get; set; }
    }
    public class SpuAccidentInfo
    {
        [DataMember]
        public String spuBHCode { get; set; }
        [DataMember]
        public String spuAccidentDetlTH { get; set; }
        [DataMember]
        public String spuAccidentDetlEN { get; set; }
        [DataMember]
        public String spuAccidentCoPay { get; set; }
        [DataMember]
        public String spuAccidentDeduc { get; set; }
    }
    public class SpuCriticalInfo
    {
        [DataMember]
        public String spuBHCode { get; set; }
        [DataMember]
        public String spuCriticalDetlTH { get; set; }
        [DataMember]
        public String spuCriticalDetlEN { get; set; }
        [DataMember]
        public String spuCriticalCoPay { get; set; }
        [DataMember]
        public String spuCriticalDeduc { get; set; }
    }
    public class SpuMaternityInfo
    {
        [DataMember]
        public String spuBHCode { get; set; }
        [DataMember]
        public String spuMaternityDetlTH { get; set; }
        [DataMember]
        public String spuMaternityDetlEN { get; set; }
        [DataMember]
        public String spuMaternityCoPay { get; set; }
        [DataMember]
        public String spuMaternityDeduc { get; set; }
    }
    public class ChildInfo
    {
        [DataMember]
        public String memberTypeTh { get; set; }
        [DataMember]
        public String memberTypeEn { get; set; }
        [DataMember]
        public String childMemberNo { get; set; }
        [DataMember]
        public String childFName { get; set; }
        [DataMember]
        public String childLName { get; set; }
        [DataMember]
        public String childDOB { get; set; }
        [DataMember]
        public String childSex { get; set; }
        [DataMember]
        public String childAgeTH { get; set; }
        [DataMember]
        public String childAgeEN { get; set; }
        [DataMember]
        public String childMobile { get; set; }
        [DataMember]
        public String childEmail { get; set; }
        [DataMember]
        public String childLineID { get; set; }
        [DataMember]
        public String childCitizenID { get; set; }
        [DataMember]
        public String childNationality { get; set; }
        [DataMember]
        public String childCompanyName { get; set; }
        [DataMember]
        public String childCompanyNameEN { get; set; }
        [DataMember]
        public String childStartDate { get; set; }
        [DataMember]
        public String childEndDate { get; set; }
        [DataMember]
        public String childClaimIPDNotify { get; set; }
        [DataMember]
        public String childClaimOPDNotify { get; set; }
        [DataMember]
        public String childClaimLabsNotify { get; set; }
        [DataMember]
        public String childClaimDentalNotify { get; set; }
        [DataMember]
        public String childClaimAMENotify { get; set; }

        [DataMember]
        public String childHasIPD { get; set; }
        [DataMember]
        public String childHasOPD { get; set; }
        [DataMember]
        public String childHasAME { get; set; }
        [DataMember]
        public String childHasLAB { get; set; }
        [DataMember]
        public String childHasDEN { get; set; }

        [DataMember]
        public ChildBenefitInfo childBenefitInfo { get; set; }

        public ChildInfo()
        {
            childBenefitInfo = new ChildBenefitInfo();
        }

    }

    public class ChildBenefitInfo
    {
        [DataMember]
        public String childGTLDetlTH { get; set; }
        [DataMember]
        public String childGTLDetlEN { get; set; }
        [DataMember]
        public String childGTL { get; set; }
        [DataMember]
        public String childGADDetlTH { get; set; }
        [DataMember]
        public String childGADDetlEN { get; set; }
        [DataMember]
        public String childGAD { get; set; }
        [DataMember]
        public String childGDADetlTH { get; set; }
        [DataMember]
        public String childGDADetlEN { get; set; }
        [DataMember]
        public String childGDA { get; set; }
        [DataMember]
        public String childGRCDetlTH { get; set; }
        [DataMember]
        public String childGRCDetlEN { get; set; }
        [DataMember]
        public String childGRC { get; set; }
        [DataMember]
        public String childGPTDDetlTH { get; set; }
        [DataMember]
        public String childGPTDDetlEN { get; set; }
        [DataMember]
        public String childGPTD { get; set; }
        [DataMember]
        public String childGCIDetlTH { get; set; }
        [DataMember]
        public String childGCIDetlEN { get; set; }
        [DataMember]
        public String childGCI { get; set; }


        [DataMember]
        public List<ChildIPDInfo> childIPDList { get; set; }
        [DataMember]
        public List<ChildOPDInfo> childOPDList { get; set; }
        [DataMember]
        public List<ChildDentalInfo> childDentalList { get; set; }
        [DataMember]
        public List<ChildAccidentInfo> childAccidentList { get; set; }
        [DataMember]
        public List<ChildCriticalInfo> childCriticalList { get; set; }
        [DataMember]
        public List<ChildMaternityInfo> childMaternityList { get; set; }


        public ChildBenefitInfo()
        {
            childIPDList = new List<ChildIPDInfo>();
            childOPDList = new List<ChildOPDInfo>();
            childDentalList = new List<ChildDentalInfo>();
            childAccidentList = new List<ChildAccidentInfo>();
            childCriticalList = new List<ChildCriticalInfo>();
            childMaternityList = new List<ChildMaternityInfo>();
        }
    }
    public class ChildIPDInfo
    {
        [DataMember]
        public String childBHCode { get; set; }
        [DataMember]
        public String childIPDDetlTH { get; set; }
        [DataMember]
        public String childIPDDetlEN { get; set; }
        [DataMember]
        public String childIPDCoPay { get; set; }
        [DataMember]
        public String childIPDDeduc { get; set; }
    }
    public class ChildOPDInfo
    {
        [DataMember]
        public String childBHCode { get; set; }
        [DataMember]
        public String childOPDDetlTH { get; set; }
        [DataMember]
        public String childOPDDetlEN { get; set; }
        [DataMember]
        public String childOPDCoPay { get; set; }
        [DataMember]
        public String childOPDDeduc { get; set; }
    }
    public class ChildDentalInfo
    {
        [DataMember]
        public String childBHCode { get; set; }
        [DataMember]
        public String childDentalDetlTH { get; set; }
        [DataMember]
        public String childDentalDetlEN { get; set; }
        [DataMember]
        public String childDentalCoPay { get; set; }
        [DataMember]
        public String childDentalDeduc { get; set; }
    }
    public class ChildAccidentInfo
    {
        [DataMember]
        public String childBHCode { get; set; }
        [DataMember]
        public String childAccidentDetlTH { get; set; }
        [DataMember]
        public String childAccidentDetlEN { get; set; }
        [DataMember]
        public String childAccidentCoPay { get; set; }
        [DataMember]
        public String childAccidentDeduc { get; set; }
    }
    public class ChildCriticalInfo
    {
        [DataMember]
        public String childBHCode { get; set; }
        [DataMember]
        public String childCriticalDetlTH { get; set; }
        [DataMember]
        public String childCriticalDetlEN { get; set; }
        [DataMember]
        public String childCriticalCoPay { get; set; }
        [DataMember]
        public String childCriticalDeduc { get; set; }
    }
    public class ChildMaternityInfo
    {
        [DataMember]
        public String childBHCode { get; set; }
        [DataMember]
        public String childMaternityDetlTH { get; set; }
        [DataMember]
        public String childMaternityDetlEN { get; set; }
        [DataMember]
        public String childMaternityCoPay { get; set; }
        [DataMember]
        public String childMaternityDeduc { get; set; }
    }
}
