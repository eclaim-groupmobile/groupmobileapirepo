﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TMLTH.GRPMOB.DTO.Response;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract]
    public class ChkOTPResponse:BaseResponse
    {            
        [DataMember]
        public String token { get; set; }
        [DataMember]
        public String refID { get; set; }
        [DataMember]
        public String status { get; set; }
    }
}
