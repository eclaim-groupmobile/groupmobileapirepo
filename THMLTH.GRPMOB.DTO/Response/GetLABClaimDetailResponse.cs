﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TMLTH.GRPMOB.DTO.Response;

namespace TMLTH.GRPMOB.DTO.Response
{
    public class GetLABClaimDetailResponse:BaseResponse
    {
        [DataMember]
        public String policyNo { get; set; }
        [DataMember]
        public String memberNo { get; set; }
        [DataMember]
        public String policyStartDT { get; set; }
        [DataMember]
        public String policyEndDT { get; set; }
        [DataMember]
        public String fName { get; set; }
        [DataMember]
        public String lName { get; set; }
        [DataMember]
        public String visityType { get; set; }
        [DataMember]
        public String claimAS400No { get; set; }
        [DataMember]
        public String claimAS400Amt { get; set; }
        [DataMember]
        public String claimWebHospNo { get; set; }
        [DataMember]
        public String claimWebHospAmt { get; set; }
        [DataMember]
        public String claimRemainNo { get; set; }
        [DataMember]
        public String claimRemainAmt { get; set; }
        [DataMember]
        public List<LABClaimDetail> claimLABs { get; set; }

        public GetLABClaimDetailResponse()
        {
            claimLABs = new List<LABClaimDetail>();
        }
    }
    public class LABClaimDetail
    {
        [DataMember]
        public String productCode { get; set; }
        [DataMember]
        public String startDate { get; set; }
        [DataMember]
        public String endDate { get; set; }
        [DataMember]
        public String claimHospitalTH { get; set; }
        [DataMember]
        public String claimHospitalEN { get; set; }
        [DataMember]
        public String claimDiagnoseTH { get; set; }
        [DataMember]
        public String claimDiagnoseEN { get; set; }
        [DataMember]
        public String claimType { get; set; }
        [DataMember]
        public String claimTypeDesc { get; set; }
        [DataMember]
        public String claimAuthDT { get; set; }


        [DataMember]
        public String claimStatTH { get; set; }
        [DataMember]
        public String claimStatEN { get; set; }
        [DataMember]
        public String claimAmt { get; set; }
        [DataMember]
        public String claimApprvAmt { get; set; }
        [DataMember]
        public String claimOverAmt { get; set; }
        public DateTime startDateTime { get; set; }
    }
}
