﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract]
    public class UpdateMemberInfoResponse:BaseResponse
    {
        [DataMember]
        public String token { get; set; }
    }
}
