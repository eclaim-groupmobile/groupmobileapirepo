﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TMLTH.GRPMOB.DTO.Response;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract]
    public class GetAMEClaimDetailResponse:BaseResponse
    {
        [DataMember]
        public String policyNo { get; set; }
        [DataMember]
        public String memberNo { get; set; }
        [DataMember]
        public String policyStartDT { get; set; }
        [DataMember]
        public String policyEndDT { get; set; }
        [DataMember]
        public String fName { get; set; }
        [DataMember]
        public String lName { get; set; }
        [DataMember]
        public String visityType { get; set; }
        [DataMember]
        public String claimAS400No { get; set; }
        [DataMember]
        public String perVisitAmt { get; set; }
        [DataMember]
        public String claimAS400Amt { get; set; }
        [DataMember]
        public String claimWebHospNo { get; set; }
        [DataMember]
        public String claimWebHospAmt { get; set; }
        [DataMember]
        public String claimRemainNo { get; set; }
        [DataMember]
        public String claimRemainAmt { get; set; }


        [DataMember(Order = 1)]
        public List<ClaimSumAMEInfo> claimSumAMEs { get; set; }
        [DataMember(Order = 2)]
        public List<AMEClaimDetail> claimAMEs { get; set; }

        public GetAMEClaimDetailResponse()
        {
            claimSumAMEs = new List<ClaimSumAMEInfo>();
            claimAMEs = new List<AMEClaimDetail>();
        }
    }
    public class ClaimSumAMEInfo
    {
        [DataMember]
        public String incurredDate { get; set; }
        [DataMember]
        public String totalAmt { get; set; }
        [DataMember]
        public String remainAmt { get; set; }

        public String claimStatus { get; set; }


        [DataMember]
        public List<VisitDTAmt> visitDTAmt { get; set; }

        public ClaimSumAMEInfo()
        {
            visitDTAmt = new List<VisitDTAmt>();
        }


    }
    public class VisitDTAmt
    {
        [DataMember]
        public String visitDate { get; set; }
        [DataMember]
        public String incurredAmt { get; set; }

    }
     public class AMEClaimDetail
    {
        [DataMember]
        public String productCode { get; set; }

        [DataMember]
        public String incurredDate { get; set; }
        [DataMember]
        public String visitDate { get; set; }
        [DataMember]
        public String claimHospitalTH { get; set; }
        [DataMember]
        public String claimHospitalEN { get; set; }
        [DataMember]
        public String claimDiagnoseTH { get; set; }
        [DataMember]
        public String claimDiagnoseEN { get; set; }
        [DataMember]
        public String claimType { get; set; }
        [DataMember]
        public String claimTypeDesc { get; set; }
        [DataMember]
        public String claimAuthDT { get; set; }

        [DataMember]
        public String claimStatTH { get; set; }
        [DataMember]
        public String claimStatEN { get; set; }
        [DataMember]
        public String claimAmt { get; set; }
        [DataMember]
        public String claimApprvAmt { get; set; }
        [DataMember]
        public String claimOverAmt { get; set; }
        public DateTime visitDateTime { get; set; }
    }
}
