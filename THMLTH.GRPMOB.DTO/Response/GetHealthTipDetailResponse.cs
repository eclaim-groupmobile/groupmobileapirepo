﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract]
    public class GetHealthTipDetailResponse : BaseResponse
    {
        [DataMember]
        public String fileID { get; set; }
        [DataMember]
        public String fileName { get; set; }
        [DataMember]
        public String fileType { get; set; }
        [DataMember]
        public String filePath { get; set; }
        [DataMember]
        public String fileDateShow { get; set; }
        [DataMember]
        public String fileDescription { get; set; }
        [DataMember]
        public String fileData { get; set; }
        [DataMember]
        public String fileDataForDisplay { get; set; }
    }
}
