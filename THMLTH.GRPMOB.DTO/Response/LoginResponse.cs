﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract] // DataContract เป็นตัวระบุว่าให้เปิดเผย Type นี้ผ่านทาง web service
    public class LoginResponse : BaseResponse
    {
        [DataMember(Order =1)]
        public String citizenID { get; set; }


        [DataMember(Order = 2)]
        public String dob { get; set; }
        [DataMember(Order = 3)]
        public String email { get; set; }
        [DataMember(Order = 4)]
        public String firstName { get; set; }
        [DataMember(Order = 5)]
        public String forceAcceptAgreement { get; set; }
        [DataMember(Order = 6)]
        public String lastName { get; set; }
        [DataMember(Order = 7)]
        public String lineID { get; set; }

        [DataMember(Order = 8)]
        public String mobileNo { get; set; }

        [DataMember(Order = 9)]
        public String needReRegister { get; set; }
        [DataMember(Order = 10)]
        public String token { get; set; }





    }
}
