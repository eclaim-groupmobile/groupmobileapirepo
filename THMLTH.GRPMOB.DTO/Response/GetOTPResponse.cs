﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract] // DataContract เป็นตัวระบุว่าให้เปิดเผย Type นี้ผ่านทาง web service
    public class GetOTPResponse:BaseResponse
    {
        [DataMember]
        public String token { get; set; }
        [DataMember]
        public String refID { get; set; }
        //[DataMember]
        //public String OTP { get; set; }
    }
}