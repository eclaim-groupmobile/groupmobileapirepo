﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract]
    public class GetMasterAgreementResponse : BaseResponse
    {
        [DataMember(Order =1)]
        public String descEN { get; set; }
        [DataMember(Order = 2)]
        public String descTH { get; set; }
        [DataMember(Order = 3)]
        public String titleEN { get; set; }
        [DataMember(Order = 4)]
        public String titleTH { get; set; }
        [DataMember(Order = 5)]
        public String token { get; set; }
        [DataMember(Order = 6)]
        public String version { get; set; }
    }
}
