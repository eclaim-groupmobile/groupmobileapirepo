﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract]
    public class LoadMediaNewsResponse:BaseResponse
    {
        [DataMember]
        public String test { get; set; }
        [DataMember]
        public List<MediaNews> mediaNewsList { get; set; }
        public LoadMediaNewsResponse()
        {
            mediaNewsList = new List<MediaNews>();
        }

    }

}
