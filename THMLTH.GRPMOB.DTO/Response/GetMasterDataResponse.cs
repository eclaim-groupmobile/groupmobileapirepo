﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TMLTH.GRPMOB.DTO.Response;

namespace TMLTH.GRPMOB.DTO.Response
{
    [DataContract]
    public class GetMasterDataResponse : BaseResponse
    {

        [DataMember(Order = 1)]
        public String token { get; set; }

        [DataMember(Order = 2)]
        public List<ProvinceInfo> provincesInfo { get; set; }


        [DataMember(Order = 3)]
        public List<CityInfo> citiesInfo { get; set; }

        [DataMember(Order = 4)]
        public List<ClinicInfo> clinicsInfo { get; set; }


        [DataMember(Order = 5)]
        public List<HospitalInfo> hospitalInfo { get; set; }

        [DataMember(Order = 6)]
        public List<NewInfo> newsInfo { get; set; }

        [DataMember(Order = 7)]
        public List<HealthTip> healthTipsInfo { get; set; }

        [DataMember(Order = 8)]
        public List<MediaNews> mediaNews { get; set; }

        [DataMember(Order = 9)]
        public String appVersionNeedForceUpdate { get; set; }

        public GetMasterDataResponse()
        {
            //employeeInfo = new EmployeeInfo();
            //spouseInfo = new SpouseInfo();
            provincesInfo = new List<ProvinceInfo>();
            citiesInfo = new List<CityInfo>();
            clinicsInfo = new List<ClinicInfo>();
            hospitalInfo = new List<HospitalInfo>();
            newsInfo = new List<NewInfo>();
            healthTipsInfo = new List<HealthTip>();
            mediaNews = new List<MediaNews>();
        }
    }

    public class ProvinceInfo
    {
        [DataMember]
        public String nameEN { get; set; }
        [DataMember]
        public String nameTH { get; set; }
    }

    public class CityInfo
    {
        [DataMember]
        public String nameEN { get; set; }
        [DataMember]
        public String nameTH { get; set; }
    }

    public class ClinicInfo
    {
        [DataMember]
        public String nameEN { get; set; }
        [DataMember]
        public String nameTH { get; set; }
        [DataMember]
        public String addressTH { get; set; }
        [DataMember]
        public String addressEN { get; set; }
        [DataMember]
        public String telephone { get; set; }
        [DataMember]
        public String fax { get; set; }
        [DataMember]
        public String IPDGroup { get; set; }
        [DataMember]
        public String OPDGroup { get; set; }
        [DataMember]
        public String Dental { get; set; }
        [DataMember]
        public String latitude { get; set; }
        [DataMember]
        public String longtitude { get; set; }
        [DataMember]
        public String nameforMap { get; set; }
        [DataMember]
        public String provinceTH { get; set; }
        [DataMember]
        public String provinceEN { get; set; }
        [DataMember]
        public String cityTH { get; set; }
        [DataMember]
        public String cityEN { get; set; }
        [DataMember]
        public String tumbonTH { get; set; }
        [DataMember]
        public String tumbonEN { get; set; }
    }

    public class HospitalInfo
    {
        [DataMember]
        public String nameEN { get; set; }
        [DataMember]
        public String nameTH { get; set; }
        [DataMember]
        public String addressTH { get; set; }
        [DataMember]
        public String addressEN { get; set; }
        [DataMember]
        public String telephone { get; set; }
        [DataMember]
        public String fax { get; set; }
        [DataMember]
        public String IPDGroup { get; set; }
        [DataMember]
        public String OPDGroup { get; set; }
        [DataMember]
        public String Dental { get; set; }
        [DataMember]
        public String latitude { get; set; }
        [DataMember]
        public String longtitude { get; set; }
        [DataMember]
        public String nameforMap { get; set; }
        [DataMember]
        public String provinceTH { get; set; }
        [DataMember]
        public String provinceEN { get; set; }
        [DataMember]
        public String cityTH { get; set; }
        [DataMember]
        public String cityEN { get; set; }
        [DataMember]
        public String tumbonTH { get; set; }
        [DataMember]
        public String tumbonEN { get; set; }

    }

    public class NewInfo
    {
        [DataMember]
        public String titleTH { get; set; }
        [DataMember]
        public String titleEN { get; set; }
        [DataMember]
        public String detail { get; set; }
        [DataMember]
        public String attachTH { get; set; }
        [DataMember]
        public String attachEN { get; set; }
        [DataMember]
        public String link { get; set; }

    }

    public class HealthTip
    {
        [DataMember]
        public String fileID { get; set; }
        [DataMember]
        public String fileName { get; set; }
        [DataMember]
        public String fileDescription { get; set; }
    }

    public class MediaNews
    {
        [DataMember]
        public String title { get; set; }
        [DataMember]
        public String displayOrder { get; set; }
        [DataMember]
        public String coverPictureLink { get; set; }
        [DataMember]
        public String detail { get; set; }
        [DataMember]
        public String pictureLink { get; set; }


    }
}