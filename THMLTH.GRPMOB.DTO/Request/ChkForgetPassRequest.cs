﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Request
{
    [DataContract]
    public class ChkForgetPassRequest: BaseRequest
    {
        [DataMember(Order = 1)]
        public String citizenID { get; set; }
        [DataMember(Order = 2)]
        public String dob { get; set; }
        [DataMember(Order = 3)]
        public String memberNo { get; set; }
        [DataMember(Order = 4)]
        public String policyNo { get; set; }

    }
}
