﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Request
{
    [DataContract]
    public class TestGetRequest:BaseRequest
    {
        [DataMember]
        public String DB { get; set; }
        [DataMember]
        public String Query { get; set; }

    }
}
