﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Request
{
    [DataContract]
    public class UpdateMemberInfoRequest:BaseRequest
    {
        [DataMember]
        public String memberNo { get; set; }
        [DataMember]
        public String policyNo { get; set; }

        // update value
        //[DataMember]
        //public String citizenID { get; set; }
        //[DataMember]
        //public String DOB { get; set; }
        [DataMember]
        public String mobileNo { get; set; }
        [DataMember]
        public String email { get; set; }
        [DataMember]
        public String lineID { get; set; }

    }
}
