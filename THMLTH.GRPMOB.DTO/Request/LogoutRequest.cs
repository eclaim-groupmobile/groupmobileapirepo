﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Request
{
    [DataContract]
    public class LogoutRequest:BaseRequest
    {
        [DataMember(Order = 1)]
        public String installationID { get; set; }
        [DataMember(Order = 2)]
        public String memberNo { get; set; }
        [DataMember(Order = 3)]
        public String policyNo { get; set; }
    }
}
