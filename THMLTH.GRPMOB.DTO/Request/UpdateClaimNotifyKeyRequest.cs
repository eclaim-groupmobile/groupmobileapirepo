﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Request
{
    [DataContract]
    public class UpdateClaimNotifyKeyRequest : BaseRequest
    {
        [DataMember]
        public String policyNo { get; set; }
        [DataMember]
        public String memberNo { get; set; }
        [DataMember]
        public String visitType { get; set; }
        [DataMember]
        public String loginedMemberNo { get; set; }
    }
}
