﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Request
{
    [DataContract]
    public class GetECardRequest:BaseRequest
    {            
        [DataMember]
        public String policyNo { get; set; }
        [DataMember]
        public String memberNo { get; set; }

    }
}
