﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Request
{
    [DataContract] // DataContract เป็นตัวระบุว่าให้เปิดเผย Type นี้ผ่านทาง web service
    public class UpdPwdRequest:BaseRequest
    {
        [DataMember(Order =1)]
        public String action { get; set; }
        
        [DataMember]
        public String policyNo { get; set; }
        [DataMember]
        public String memberNo { get; set; }
        [DataMember]
        public String oldPassWord { get; set; }
        [DataMember]
        public String passWord { get; set; }
        [DataMember]
        public String installationID { get; set; }
    }
}