﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Request
{
    [DataContract] // DataContract เป็นตัวระบุว่าให้เปิดเผย Type นี้ผ่านทาง web service
    public class BaseRequest
    {
        [DataMember(Order = 1)]
        public String osPlatform { get; set; }



        [DataMember(Order =2)]
        public String token { get; set; }

        
        public String serviceName { get; set; }
        public String clientIPAddress { get; set; }
    }
}