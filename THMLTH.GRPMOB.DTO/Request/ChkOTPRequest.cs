﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TMLTH.GRPMOB.DTO.Request;

namespace TMLTH.GRPMOB.DTO.Request
{
    [DataContract] // DataContract เป็นตัวระบุว่าให้เปิดเผย Type นี้ผ่านทาง web service
    public class ChkOTPRequest : BaseRequest
    {
        [DataMember]
        public String OTP { get; set; }
        [DataMember]
        public String policyNo { get; set; }
        [DataMember]
        public String memberNo { get; set; }
        [DataMember]
        public String citizenID { get; set; }
        [DataMember]
        public String action { get; set; }
        [DataMember]
        public String refID { get; set; }
        [DataMember]
        public String mobileNo { get; set; }
    }
}
