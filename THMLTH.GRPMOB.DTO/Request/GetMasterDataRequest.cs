﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TMLTH.GRPMOB.DTO.Request
{
    [DataContract]
    public class GetMasterDataRequest:BaseRequest
    {
        [DataMember]
        public String AppVersion { get; set; }
    }
}
